<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();
Route::get('/test','HomeController@index');
/**
 * 后台业务部分
 */
Route::group(['middleware' => ['web', 'auth'], 'prefix' => '', 'namespace' => 'Admin'], function () {
    /* 首页 */
    Route::get('/', 'IndexController@index');

    /* 管理员管理 */
    Route::get('/user', 'UserController@index');
    Route::get('/user/data', 'UserController@data');
    Route::post('/user/add', 'UserController@add');
    Route::post('/user/edit', 'UserController@edit');

    /**
     * 标签管理
     */
    route::get('/label','LabelController@index');
    route::get('/label/data', 'LabelController@data');
    route::post('/label/add', 'LabelController@add');
    route::post('/label/edit', 'LabelController@edit');

    /**
     * 文章管理
     */
    route::get('/article','ArticleController@index');
    route::get('/article/data', 'ArticleController@data');
    route::post('/article/save','ArticleController@save');
    route::post('/article/edit','ArticleController@edit');
    route::post('/article/top','ArticleController@top');


    /**
     * 视频管理
     */
    Route::get('/video', 'VideoController@index');
    Route::get('/video/data', 'VideoController@data');
    Route::post('/video/add', 'VideoController@add');
    Route::post('/video/edit', 'VideoController@edit');
    route::post('/video/top','VideoController@top');

    /**
     * 医院管理
     */
    route::get('/hospital','HospitalController@index');
    route::get('/hospital/data','HospitalController@data');
    route::post('/hospital/add','HospitalController@add');
    route::post('/hospital/edit','HospitalController@edit');


    /**
     * 医生管理
     */
    Route::get('/doctor', 'DoctorController@index');
    Route::get('/doctor/data', 'DoctorController@data');
    Route::post('/doctor/add', 'DoctorController@add');
    Route::post('/doctor/edit', 'DoctorController@edit');


    /**
     * 公益活动
     */
    route::get('/volunteer','VolunteerController@index');
    route::get('/volunteer/data', 'VolunteerController@data');
    route::post('/volunteer/save','VolunteerController@save');
    route::post('/volunteer/edit','VolunteerController@edit');


    /**
     * 厂家管理
     */
    route::get('/vender','VenderController@index');
    route::get('/vender/data','VenderController@data');
    route::post('/vender/add','VenderController@add');
    route::post('/vender/edit','VenderController@edit');

    /**
     * 上传图片
     */
    route::post('/image/upload','UploadController@index');
    route::post('/image/uploads','UploadController@uploads');


    /**
     * 咨询
     */
    Route::post('/counseling/assist','\App\Http\Controllers\Wechat\ChatController@assist');


    //抽奖设置
    Route::get('/prize', 'PrizeController@index');
    Route::get('/prize/data', 'PrizeController@data');
    Route::post('/prize/add', 'PrizeController@add');
    Route::post('/prize/edit', 'PrizeController@edit');


        /**
         * 随访业务
         */
    Route::group(['prefix' => 'follow_up', 'namespace' => 'FollowUp'], function() {
            /* 信息常量 */
            Route::get('/information', 'InformationController@index');
            Route::get('/information/data', 'InformationController@data');
            Route::post('/information/add', 'InformationController@add');
            Route::post('/information/edit', 'InformationController@edit');
            Route::get('/information/search', 'InformationController@search');
            /* 问卷 */
            Route::get('/survey', 'SurveyController@index');
            Route::get('/survey/data', 'SurveyController@data');
            Route::get('/survey/template/list', 'SurveyController@templateList');
            Route::post('/survey/template', 'SurveyController@template');
            Route::post('/survey/preview', 'SurveyController@preview');
            Route::post('/class/preview', 'SurveyController@classPreview');
            Route::post('/survey/single', 'SurveyController@single');
            Route::post('/survey/save', 'SurveyController@save');
            Route::get('/survey/autoComplete', 'SurveyController@autoComplete');
            Route::post('/survey/edit/get', 'SurveyController@editGet');


            Route::post('/survey/preview/two', 'SurveyController@previewTwo');
            Route::post('/class/preview/two', 'SurveyController@classpreviewTwo');
            Route::get('/survey/preview/page/{id}', 'SurveyController@previewPage');
            Route::get('/getSurveyInfo/{id}', 'SurveyController@getSurveyInfo');
        });

    /**
     * 活动报名
     */
    route::get('/enroll','EnrollController@index');
    route::get('/enroll/data', 'EnrollController@data');
    route::get('/enroll/order/data', 'EnrollController@orderData');
    route::post('/enroll/step1Save','EnrollController@step1Save');
    route::post('/enroll/save','EnrollController@save');
    route::post('/enroll/edit','EnrollController@edit');
    route::get('/enroll/export','EnrollController@export');
    route::get('/enroll/export/order','EnrollController@exportOrder');
    route::post('/enroll/order/info','EnrollController@orderInfo');
    route::post('/enroll/order/edit','EnrollController@orderEdit');
    route::post('/enroll/order/table','EnrollController@orderTable');
    route::post('/enroll/order/ticket', 'EnrollController@orderTicket');

});

Route::get('/push/preview/class/{id}', 'PushController@classPreview');
Route::get('/push/preview/class/two/{id}', 'PushController@twoclassPreview');
Route::post('/preview/class/save', 'PushController@classSave');
Route::get('/push/score', 'PushController@score');

/**
 * 微信公众号
 */
Route::group(['middleware' => [], 'prefix' => 'weixin', 'namespace' => 'Pay'], function() {
    Route::post('/notify', 'NotifyController@index');
});
Route::group(['middleware' => [], 'prefix' => 'weixin', 'namespace' => 'Weixin'], function () {
    Route::any('/','IndexController@index');
    Route::get('/menu', 'InterfaceController@menu'); //菜单
    /**
     * 会员码接口
     */
    Route::any('/check/discount','CodeController@code');
    /**
     * 积分商城接口
     */
    Route::any('/integral/get','IntegralController@get');
    Route::any('/integral/add','IntegralController@add');
});
Route::group(['middleware' => ['web', 'weauth'], 'prefix' => 'wechat', 'namespace' => 'Wechat'], function () {
    /**
     * 患教
     */
    Route::get('/education','EducationController@index');
    Route::get('/edu/all','EducationController@all');
    Route::get('/edu/video','EducationController@video');
    Route::get('/edu/article','EducationController@article');
    Route::get('/edu/video/list','EducationController@videoList');
    Route::get('/edu/video/list/more','EducationController@videoMore');
    Route::get('/edu/video/search','EducationController@videoSearch');
    Route::post('/edu/set/time','EducationController@setTime');
    Route::post('/edu/save/integral','EducationController@saveIntegral');
    /**
     * 点赞、收藏。。。
     */
    Route::post('/like','EducationController@like');
    /**
     * 公益活动
     */
    Route::get('/public/activity','ActivityController@index');
    Route::get('/public/activity/detail','ActivityController@detail');
    Route::get('/public/activity/hospital','ActivityController@hospital');
    Route::post('/public/activity/getDistance','ActivityController@getDistance');
    Route::get('/public/activity/getSearch','ActivityController@getSearch');
    Route::post('/public/activity/search/hospital','ActivityController@getHospital');
    /**
     *活动报名
     */
     Route::get('/signup', 'SignController@index');
     Route::get('/signup/detail/{id}', 'SignController@detail');
     Route::get('/signup/form/{id}', 'SignController@form');
     Route::post('/signup/save', 'SignController@add');
     Route::get('/signup/list/{id}', 'SignController@list');
     Route::get('/signup/pay/ticket', 'SignController@ticket');
     Route::post('/signup/ticket/save', 'SignController@ticketSave');
     /*活动签到*/
    Route::get('/enroll/sign', 'EnrollSignController@index');
    Route::get('/enroll/sign/result', 'EnrollSignController@result');
    /**
     * 个人中心
     */
    Route::get('/person','PersonController@index');
    //添加个人资料
    Route::get('/person/information','PersonController@information');
    Route::post('/information/save','PersonController@save');
    /**
     * 跳转商城加载页面
     */
    Route::get('/load','PersonController@load');
    /**
     * 捐赠支付
     */
    Route::get('/donation','PersonController@donation');

    /**
     * 每日签到
     */
    Route::get('/sign','PersonController@sign');
    Route::post('/sign/add','PersonController@signAdd');
    /**
     * 积分记录
     */
    Route::get('/record','PersonController@record');
    /**
     * 官网简介
     */
    Route::get('/website','WebsiteController@index');

    /**
     * 咨询
     */
    Route::get('/counseling', 'ChatController@index');

    /**
     * 问卷列表
     */
    Route::get('/survey/list','SurveyController@list');
    /**
     * 问卷/自测
     */
    Route::get('/survey','SurveyController@index');
    Route::get('/survey/result','SurveyController@result');
    /**
     * 问卷保存
     *
     */
    Route::post('/survey/save','SurveyController@save');


    /**
     * 抽奖
     */
    Route::get('/prize','PrizeController@index');
    Route::post('/prize/getResult', 'PrizeController@getResult');

});

Route::group(['middleware' => ['web'],'prefix' => 'wechat', 'namespace' => 'Wechat'], function () {
    //活动报名（预报名）
//    Route::get('/signup', 'SignController@index');
//    Route::get('/signup/detail/{id}', 'SignController@detail');
//    Route::get('/signup/form/{id}', 'SignController@form');
//    Route::post('/signup/save', 'SignController@add');
//    Route::get('/signup/list/{id}', 'SignController@list');
//    Route::get('/signup/pay/ticket', 'SignController@ticket');
//    Route::post('/signup/ticket/save', 'SignController@ticketSave');
});

