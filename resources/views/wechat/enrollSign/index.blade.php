@extends('wxbase')
@section('head')
    <title>会议签到</title>
    <style>
        body {
        }
        body:before{
            background-image: url("/images/wechat/sign-bg.png");
            background-repeat: no-repeat;
            background-size: 100vw 100vh;
            content: "";
            width: 100vw;
            height: 100vh;
            position: absolute;
            top: 0;
            left: 0;
            z-index: -1;/*-1 可以当背景*/
            opacity: 0.6;
        }
        .content {
            margin-top: 35vw;
        }
        .panel {
            background: none;
        }
    </style>
@endsection
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-body">
                        <form role="form" class="">
                            <div class="form-group row">
                                <label for="" class="col-sm-2 control-label">手机号码</label>
                                <div class="col-sm-8">
                                    <input type="number" placeholder="请输入预留手机号码" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2 control-label">姓名</label>
                                <div class="col-sm-8">
                                    <input type="text" placeholder="姓名" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2 control-label">单位</label>
                                <div class="col-sm-8">
                                    <input type="text" placeholder="单位" class="form-control">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-body text-center">
                        <button class="btn btn-info success">确认信息</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jscontent')
    <script src="http://res.wx.qq.com/open/js/jweixin-1.4.0.js"></script>
    <script>
        wx.config(<?php echo $js; ?>);
        wx.ready(function() {
            wx.hideMenuItems({
                menuList: ["menuItem:copyUrl","menuItem:editTag","menuItem:delete","menuItem:originPage","menuItem:readMode", "menuItem:openWithQQBrowser", "menuItem:openWithSafari","menuItem:share:email","menuItem:share:brand","menuItem:share:qq","menuItem:share:QZone","menuItem:share:timeline","menuItem:share:appMessage","menuItem:favorite"] // 要隐藏的菜单项，只能隐藏“传播类”和“保护类”按钮
            });
        });
    </script>
    <script>
        var initFn = function () {
            init.eventFn();
        }
        var init = {
            eventFn: function() {
                $('.success').on('click', function() {
                    window.location.href = '/wechat/enroll/sign/result';
                })
            }
        }
        initFn();
    </script>
@endsection