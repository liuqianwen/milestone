@extends('wxbase')
@section('head')
    <title>签到信息</title>
    <style>
        body {
            /*background: #f1f1f1;*/
            padding: 20px;
            /*background: url("/images/wechat/sign-bg.png");*/
        }
        body:before{
            background-image: url("/images/wechat/result-bg.png");
            background-repeat: no-repeat;
            background-size: 100vw 100vh;
            content: "";
            width: 100vw;
            height: 100vh;
            position: absolute;
            top: 0;
            left: 0;
            z-index: -1;/*-1 可以当背景*/
            opacity: 0.6;
        }
        .title {
            font-size: 1.5em;
            margin-bottom: 15px;
        }
        .info {
            line-height: 1.5em;
            display: flex;
            justify-content: space-between;
            color: #333333;
            margin-bottom: 80px;
        }
        .info-content {
            font-size: 1.8em;
            width: 50%;
            text-align: center;
            margin: 0 auto;
        }
        .button {
            margin-top: 80px;
            display: flex;
            justify-content: center;
            font-size: 1.2em;
        }
        .button div {
            padding: 10px;
            width: 90px;
            height: 90px;
            border-radius: 50%;
            cursor: pointer;
            display: flex;
            flex-wrap: wrap;
            align-items: center;
            text-align: center;
        }
    </style>
@endsection
@section('content')
    <div class="content">
        <div class="title">您的序列号：30</div>
        <div class="info">
            <span>姓名：张三</span>
            <span>单位：报名单位</span>
        </div>
        <div class="info-content">
            请将此画面展示给工作人员领取代表证
        </div>
        <div class="button">
            <div class="btn-info">
                工作人员确认键
            </div>
        </div>
    </div>
@endsection

@section('jscontent')
    <script src="http://res.wx.qq.com/open/js/jweixin-1.4.0.js"></script>
    <script>
        wx.config(<?php echo $js; ?>);
        wx.ready(function() {
            wx.hideMenuItems({
                menuList: ["menuItem:copyUrl","menuItem:editTag","menuItem:delete","menuItem:originPage","menuItem:readMode", "menuItem:openWithQQBrowser", "menuItem:openWithSafari","menuItem:share:email","menuItem:share:brand","menuItem:share:qq","menuItem:share:QZone","menuItem:share:timeline","menuItem:share:appMessage","menuItem:favorite"] // 要隐藏的菜单项，只能隐藏“传播类”和“保护类”按钮
            });
        });
    </script>
    <script>
        var initFn = function () {
        }
        var init = {

        }
        initFn();
    </script>
@endsection