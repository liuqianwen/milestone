@extends('wxbase')
@section('head')
    <title>公益活动详情</title>
    <link rel="stylesheet" href="/css/wechat/act-detail.css">
@endsection
@section('content')
    <div class="content">
        <div class="top">
            <img src="{{$data['image']}}" alt="">
            <span class="title">{{$data['name']}}</span>
            <span class="introduce">说明：{{$data['description']}}</span>
        </div>
        <div class="article over-flow article-height">
            {!! $data['content'] !!}
            <div class="read-all">
                <img src="/images/wechat/more-all.png" alt="">
                <span>阅读全文</span>
            </div>
        </div>
    </div>
    <div class="institution">
        @if(count($data['hospital'])> 0)
            <div class="title">
                <span>—— 参与项目 ——</span>
                <span class="more" style="font-size: 14px; color: #8d8d8d;">查看其它</span>
            </div>
        @endif
        <div class="inst-list">
            @foreach ($data['hospital'] as $v)
                <div class="list hospital" data-id="{{$v['_id']}}">
                    <div class="info">
                        <img src="{{$v['logo']}}" alt="">
                        <div class="right">
                            <span>{{$v['name']}}</span>
                            <span class="introduce">{{$v['introduce']}}</span>
                            <div>
                                <span class="introduce address">地址：{{$v['address']}}</span>
                                <span style="color: #8d8d8d; margin-left: 10px;width: 4em; text-align: right"></span>
                            </div>
                        </div>
                    </div>
                    <div class="icon">
                        @if(in_array(1,$v['badge']))
                            <img src="/images/wechat/1-z.png" alt="">
                        @else
                            <img src="/images/wechat/1-f.png" alt="">
                        @endif
                        @if(in_array(2,$v['badge']))
                            <img src="/images/wechat/2-z.png" alt="">
                        @else
                            <img src="/images/wechat/2-f.png" alt="">
                        @endif
                        @if(in_array(3,$v['badge']))
                            <img src="/images/wechat/3-z.png" alt="">
                        @else
                            <img src="/images/wechat/3-f.png" alt="">
                        @endif
                        @if(in_array(4,$v['badge']))
                            <img src="/images/wechat/4-z.png" alt="">
                        @else
                            <img src="/images/wechat/4-z.png" alt="">
                        @endif

                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="factory">
        @if(count($factory) > 0)
        <span>—— 公益厂商 ——</span>
        @endif
        @foreach ($factory as $v)
        <div class="list hospital" data-id="{{$v['_id']}}">
            <div class="info">
                <img src="{{$v['logo']}}" alt="">
                <div class="right">
                    <span>{{$v['name']}}</span>
                    <span class="introduce">{{$v['introduce']}}</span>
                </div>
            </div>
        </div>
            @endforeach
    </div>
    {{--<div id="allmap">--}}

    {{--</div>--}}
    <input type="hidden" id="act-id" value="{{$data['_id']}}">
@endsection

@section('jscontent')
    {{--<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=RdBeGMrogIDsfZK4bAYjDSI0fbqtMLC7"></script>--}}
    <script type="text/javascript" src="http://res2.wx.qq.com/open/js/jweixin-1.4.0.js?i={{time()}}"></script>
    <script>
        wx.config(<?php echo $js; ?>);
    </script>
    <script>
        wx.ready(function() {
            init.getData();
        })
        var detail = function() {
            init.clickFn();
            init.getData();
        };
        var init = {
            mapFn : function() {
               //微信获取定位
                wx.getLocation({
                    type:'gcj02',
                    success: function(res) {
                        var lat = parseFloat(res.latitude) + parseFloat(0.0060);
                        var long = parseFloat(res.longitude) + parseFloat(0.0065);
                        // init.setCookie(lat,long,'city');
                        var id = $('#act-id').val();
                        init.ajaxFn(id,lat,long);
                    },
                    cancel: function (res) {
                        alert('用户拒绝授权获取地理位置');
                    },
                    fail: function (res) {
                        alert(JSON.stringify(res));
                }
                })
            },
            ajaxFn: function(id,lat,long) {
                var option = {
                    url: '/wechat/public/activity/getDistance',
                    method: 'post',
                    data: {id: id,lat:lat,long: long},
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                };
                $.ajax(option).done(function(res){
                    if (res.status == 0) {
                        var hospital = '';
                        $.each (res.data,function(index,value) {
                            if (value.type == 1) {
                                hospital += '<div class="list hospital" data-id="'+value._id+'">\n' +
                                    '            <div class="info">\n' +
                                    '                <img src="'+value.logo+'" alt="">\n' +
                                    '                <div class="right">\n' +
                                    '                    <span>'+value.name+'</span>\n' +
                                    '                    <span class="introduce">'+value.introduce+'</span>\n' +
                                    '                    <div>\n' +
                                    '                        <span class="introduce address">地址：'+value.address+'</span>\n' +
                                    '                        <span style="color: #8d8d8d; margin-left: 10px;">'+value.distance+'</span>\n' +
                                    '                    </div>\n' +
                                    '                </div>\n' +
                                    '            </div>\n' +
                                    '            <div class="icon">\n' ;
                                if(IsInArray(value.badge,1)) {
                                    hospital += '                <img src="/images/wechat/1-z.png" alt="">\n';
                                }else {
                                    hospital += '                    <img src="/images/wechat/1-f.png" alt="">\n' ;
                                }
                                if(IsInArray(value.badge,2)) {
                                    hospital += '                <img src="/images/wechat/2-z.png" alt="">\n';
                                }else {
                                    hospital += '                    <img src="/images/wechat/2-f.png" alt="">\n' ;
                                }
                                if(IsInArray(value.badge,3)) {
                                    hospital += '                <img src="/images/wechat/3-z.png" alt="">\n';
                                }else {
                                    hospital += '                    <img src="/images/wechat/3-f.png" alt="">\n' ;
                                }
                                if(IsInArray(value.badge,4)) {
                                    hospital += '                <img src="/images/wechat/4-z.png" alt="">\n';
                                }else {
                                    hospital += '                    <img src="/images/wechat/4-f.png" alt="">\n' ;
                                }
                                hospital +='\n' +
                                    '            </div>\n' +
                                    '        </div>';
                            }
                        })
                        $('.inst-list').empty().append(hospital);
                    }
                    function IsInArray(arr,val){

                        var testStr=','+arr.join(",")+",";

                        return testStr.indexOf(","+val+",")!=-1;

                    }
                })
            },
            clickFn: function() {
                $('.read-all').on('click',function() {
                    $('.article').removeClass('over-flow');
                    $(this).addClass('hidden');
                    $('.article').removeClass('article-height');
                })
                $('body').on('click','.hospital',function() {
                    var id = $(this).data('id');
                    window.location.href = '/wechat/public/activity/hospital?id='+id;
                })
                $('body').on('click','.more',function () {
                    window.location.href = '/wechat/public/activity/getSearch';
                })
            },
            // setCookie: function(lat,long,city) {
            //     var exp = new Date();
            //     exp.setTime(exp.getTime() + 1000*60*10);
            //     document.cookie = 'lat='+lat+';expires='+ exp.toGMTString();
            //     document.cookie = 'long='+long+';expires='+ exp.toGMTString();
            //     document.cookie = 'city='+city+';expires='+ exp.toGMTString();
            // },
            // getCookie: function(name) {
            //     var exp = new Date();
            //     exp.setTime(exp.getTime() - 1);
            //     var cval = document.cookie.match(new RegExp('(^|)'+name+'=([^;]*)(;|$)'));
            //     return cval;
            // },
            getData: function() {
                // var id   = $('#act-id').val();
                // var lat  = init.getCookie('lat');
                // var long = init.getCookie('long');
                // if(lat != null) {
                //     init.ajaxFn(id,lat[2],long[2]);
                // } else {
                    init.mapFn();
                // }
            }
        }
        detail();

    </script>
@endsection