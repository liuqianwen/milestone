@extends('wxbase')
@section('head')
    <title>公益活动</title>
    <link rel="stylesheet" href="/css/wechat/act-index.css">
@endsection
@section('content')
    <div class="top">
        <div class="search">
            <img src="/images/wechat/search.png" alt=""/>
            <input type="search" placeholder="请输入关键词">
        </div>
        <span class="cancel display-none">取消</span>
    </div>
    <div class="banner">
        <div class="wrap">
            <ul class="pic">
                @foreach ($top as $v)
                <li class="detail" data-id="{{$v['_id']}}">
                    <img src="{{$v['image']}}" alt="" width="640" height="480">
                    <div class="text">
                        <span>{{$v['name']}}</span>
                        {{--<span class="introduce">说明大家都是健身卡的积分开始打飞机卡士大夫斯柯达附近萨克</span>--}}
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="week-activity">
        @if (count($week) > 0)
        <div class="title">
            <img src="/images/wechat/week-icon.png" alt="">
            <span>本周公益</span>
        </div>
        @endif
        <div class="week-content">
            @foreach ($week as $v)
            <div class="week-list detail" data-id="{{$v['_id']}}">
                <img src="{{$v['image']}}" alt="">
                <span>{{$v['name']}}</span>
            </div>
            @endforeach
        </div>
    </div>
    <div class="more-activity">

        <span>更多公益活动</span>
        @foreach ($top as $v)
        <div class="item detail" data-id="{{$v['_id']}}">
            <img src="{{$v['image']}}" alt="">
            <div class="right">
                <span>{{$v['name']}}</span>
                <span class="introduce">{{$v['description']}}</span>
            </div>
        </div>
            @endforeach
    </div>
@endsection

@section('jscontent')
    <script>
        var initFn = function () {
            init.lunboFn();
            init.searchFn();
            init.detailFn();
        }
        var init = {
            lunboFn: function () {
                //轮播图
                $('.wrap .pic li:first').addClass('on');
                var wrap=$('.wrap'),
                    pic=$('.wrap .pic li'),
                    list=$('.wrap .cercle span'),
                    index=0,
                    timer=null,
                    moveEndX,startX,startY,moveEndY,X,Y;
                changePic(index);
                // 定义并调用自动播放函数
                timer = setInterval(autoPlay, 4000);

                // 触摸整个容器时停止自动播放
                wrap.on('touchstart', function () {
                    clearInterval(timer);
                })

                // 触摸结束时继续播放至下一张
                wrap.on('touchend', function () {
                    timer = setInterval(autoPlay, 4000);
                })

                function autoPlay () {
                    if (++index >= pic.length) index = 0;
                    changePic(index);
                }

                // 定义图片切换函数
                function changePic (curIndex) {
                    pic.each(function(index,value){
                        $(this).css('display','none');
                        list.each(function(len,v) {
                            if(index == len) {
                                $(this).removeClass('on');
                            }
                            if(len == curIndex) {
                                $(this).addClass('on');
                            }
                        })
                        if(index == curIndex) {
                            $(this).show();
                            $(this).find('.text').show();
                        }
                    })
                }
                $(".wrap li").on("touchstart", function(e) {
                    // 判断默认行为是否可以被禁用
                    if (e.cancelable) {
                        // 判断默认行为是否已经被禁用
                        if (!e.defaultPrevented) {
                            e.preventDefault();
                        }
                    }
                    startX = e.originalEvent.changedTouches[0].pageX,
                        startY = e.originalEvent.changedTouches[0].pageY;
                });
                $(".wrap li").on("touchend", function(e) {
                    // 判断默认行为是否可以被禁用
                    if (e.cancelable) {
                        // 判断默认行为是否已经被禁用
                        if (!e.defaultPrevented) {
                            e.preventDefault();
                        }
                    }
                    moveEndX = e.originalEvent.changedTouches[0].pageX,
                        moveEndY = e.originalEvent.changedTouches[0].pageY,
                        X = moveEndX - startX,
                        Y = moveEndY - startY;
                    //左滑
                    if ( X > 10 ) {
                        if(index == 0) {
                            index = pic.length -1;
                        }else{
                            index = index -1;
                        }
                        changePic(index);
                    }
                    //右滑
                    else if ( X < -10 ) {
                        if(index == pic.length -1) {
                            index = 0;
                        }else{
                            index = index + 1;
                        }
                        changePic(index);
                    } else {
                        var id =$(this).data('id');
                        window.location.href = '/wechat/public/activity/detail?id='+id;
                    }
                });
            },
            searchFn: function () {
                $('.search input').on('focus',function () {
                    $('.search').addClass('search-a');
                    $('.cancel').removeClass('display-none');
                })
                //搜索内容 获取键盘搜索按钮事件
                $(".search input").on('keypress', function(e) {
                    var keycode = e.keyCode;
                    //获取搜索框的值
                    var searchText = $(this).val();
                    if (keycode == '13') {
                        e.preventDefault();
                        //请求搜索接口
                        if (searchText == '') {
                            alert('请输入检索内容！');
                        } else {
                            var type = $('#type').val();
                            var data = {};
                            var search = $('.search input').val();
                            window.location.href = '/wechat/public/activity';
                        }
                    }
                });
                $('.cancel').on('click',function () {
                    window.location.href = '/wechat/public/activity';
                })
            },
            detailFn: function () {
                $('body').on('click','.detail',function(){
                    var id = $(this).data('id');
                    window.location.href = '/wechat/public/activity/detail?id=' + id;
                })
            }
        }
        initFn();
    </script>
@endsection