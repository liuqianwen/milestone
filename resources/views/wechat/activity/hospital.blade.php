@extends('wxbase')
@section('head')
    <title>@if($data['type'] == 1)医院详情@else 厂商详情 @endif</title>
    <style>
        /*厂商*/
        a {
            color: #000;
        }
        a:hover {
            text-decoration: none;
            color: #000;
        }
        .goods img {
            width: 80px !important;
            height: 60px !important;
            border-radius:0 !important;
        }
        /*图*/
        .content {
            width:100vw;
            position: relative;
        }
        .content img {
            width: 100%;
            height: 55vw;
        }
        .content .name {
            position: absolute;
            bottom:0;
            width: 100%;
            display: inline-block;
            line-height: 28px;
            color: #fff;
            padding: 10px;
            font-size: 16px;
        }
        /*简介说明*/
        .introduce {
            display: flex;
            flex-direction: column;
            padding: 10px;
            line-height: 1.5em;
        }
        .introduce .text {
            overflow: hidden;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            -webkit-line-clamp: 3;
        }
        .introduce span {
            padding: 5px 0;
        }
        .dh {
            width: 20px;
            height: 20px;
            margin-left: 10px;
        }
        /*徽章*/
        .huizhang {

        }
        .title {
            display: inline-block;
            background: #f1f1f1;
            line-height: 40px;
            padding: 0 10px;
            width: 100%;
        }
        ul,li {
            padding: 0;
            margin: 0;
        }
        ul {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            margin: 10px;
            justify-content: space-between;
        }
        li {
            list-style: none;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            padding: 10px 0;
        }
        li img {
            width: 42px;
            margin-bottom: 10px;
        }
        /*医生推荐*/
        .doctor {

        }
        .doctor .list {
            display: flex;
            padding: 10px;
            border-bottom: 1px solid #e1e1e1;
        }
        .doctor .list img {
            width: 60px;
            height: 60px;
            display: inline-block;
            border-radius: 50%;
        }
        .doctor .list .info {
            display: flex;
            flex-direction: column;
        }
        .doctor .list .info span {
            padding: 5px 10px;
        }
        /*地图*/
        #allmap {
            overflow: scroll !important;
            width: 100% !important;
            height: 200px;
        }
    </style>
@endsection
@section('content')
    <div class="content">
        <img src="{{$data['logo']}}" alt="">
        <span class="name">{{$data['name']}}</span>
    </div>
    @if($data['type'] == 1)
    <div class="introduce">
        <span class="text">医院简介：{{$data['introduce']}}</span>
        <span>地址：{{$data['address']}}
            <a href="https://uri.amap.com/marker?position={{$data['pointlng']}},{{$data['pointlat']}}" class="dh-app">
                <img class="dh" style="margin:2px" src="/images/wechat/dh.png" />
            </a>
        </span>
    </div>
    <div class="huizhang">
        @if(count($data['badge']) > 0)
       <span class="title">医院徽章</span>
        @endif
        <div>
            <ul>
                @foreach ($data['badge'] as $v)
                    @if($v == 1)
                        <li><img src="/images/wechat/1-z.png" alt=""><span>先进医院</span></li>
                    @elseif($v == 2)
                        <li><img src="/images/wechat/2-z.png" alt=""><span>先进医院</span></li>
                    @elseif($v == 3)
                        <li><img src="/images/wechat/3-z.png" alt=""><span>先进医院</span></li>
                    @elseif($v == 4)
                        <li><img src="/images/wechat/4-z.png" alt=""><span>卫生认证</span></li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
    <div class="doctor">
        <span class="title">医生推荐</span>
        @foreach ($doctor as $v)
        <div class="list">
            <img src="{{$v['headimg']}}" alt="">
            <div class="info">
                <span class="name">{{$v['name']}} ({{$v['position']}})</span>
                <span class="introduce">擅长：{{$v['good']}}</span>
            </div>
        </div>
        @endforeach
    </div>
        @else
        <div class="introduce">
            <span class="text">简介：{{$data['introduce']}}</span>
            <span>地址：{{$data['address']}}
                <a href="https://uri.amap.com/marker?position={{$data['pointlng']}},{{$data['pointlat']}}" class="dh-app">
                <img class="dh" style="margin:2px" src="/images/wechat/dh.png" />
            </a>
        </span>
        </span>
        </div>
        @if($data['genre'] == 2)
        <div class="doctor goods">
            <span class="title">商品推荐</span>
            @foreach ($data['goods'] as $v)
                <a href="{{$v['url']}}">
                <div class="list">
                        <img src="{{$v['image']}}" alt="">
                        <div class="info">
                            <span class="name">{{$v['title']}} </span>
                            <span class="introduce">{{$v['description']}}</span>
                        </div>
                </div>
                </a>
            @endforeach
        </div>
            @elseif($data['genre'] == 1)
            {!! $data['content'] !!}
            @endif
    @endif
    <input type="hidden" id="address" value="{{$data['address']}}">
    <input type="hidden" id="lat" value="{{$data['pointlat']}}">
    <input type="hidden" id="long" value="{{$data['pointlng']}}">
@endsection

@section('jscontent')

@endsection