@extends('wxbase')
@section('head')
    <title>更多</title>
    <link rel="stylesheet" href="/css/wechat/zepto.css">
    <style>
        .content-all {
            margin-top: 60px;
        }
        .current-city {
            position: fixed;
            top: 0;
            width: 100vw;
            line-height: 40px;
            padding: 10px;
            color: #000;
            background: #f5f5f5;
        }
        .current-city .city-1 {
            margin-right: 10px;
        }
        .current-city img {
            width: 20px;
            height: 20px;
        }
        .hidden {
            display: none !important;
        }
        .city {
            padding: 0 20px;
        }
        /*医院*/
        .institution {
            /*padding: 10px;*/
            margin-top: 10px;
        }
        .institution > span,.factory > span {
            width: 100%;
            text-align: center;
            line-height: 40px;
            display: inline-block;
            font-size: 16px;
            background: #f1f1f1;
        }
        .institution .list,.factory .list {
            display: flex;
            flex-direction: column;
            margin-bottom: 10px;
            border-bottom: 1px solid #e1e1e1;
            /*padding-bottom: 10px;*/
            padding: 10px;
        }
        .institution .list .info,.factory .list .info {
            display: flex;
        }
        .institution .list .info .right,.factory .list .info .right{
            display: flex;
            flex-direction: column;
            width: calc(100vw - 22vw - 20px);
            padding: 0 10px;
            justify-content: center;
        }
        .institution .list .info .right .introduce ,.factory .list .info .right .introduce{
            color: #8d8d8d;
            overflow: hidden;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            -webkit-line-clamp: 1;
        }
        .factory .list .info .right .introduce {
            -webkit-line-clamp: 2;
        }
        .institution .list .info .right .address{

        }
        .institution .list .info .right div {
            display: flex;
            justify-content: space-between;
        }
        .institution .list .info img,.factory .list .info img {
            width: 20vw;
            height: 20vw;
        }
        .institution .list .icon {
            display: flex;
            margin-left: 20px;
        }
        .institution .list .icon img {
            height: 20px;
            width: 20px;
            margin-right: 10px;
            margin-top: 10px;
        }
        /*无数据*/
        .no-data {
            width: 100%;
            text-align: center;
            color: #8d8d8d;
            margin-top: 40vw;
        }
        .no-data img {
            width: 80px;
        }
        .no-data span {
            display: block;
            line-height: 30px;
        }
    </style>
@endsection
@section('content')
    <div class="current-city">
        当前位置：
        <span class="city-1">全部</span>
        <span class="change-city">
            <img src="/images/wechat/change-icon.png" alt="">
        </span>
    </div>
    <div class="content-all">
        <div class="institution">
            <div class="inst-list">
                @foreach ($data as $v)
                    <div class="list hospital" data-id="{{$v['_id']}}">
                        <div class="info">
                            <img src="{{$v['logo']}}" alt="">
                            <div class="right">
                                <span>{{$v['name']}}</span>
                                <span class="introduce">{{$v['introduce']}}</span>
                                <div>
                                    <span class="introduce address">地址：{{$v['address']}}</span>
                                    <span style="color: #8d8d8d; margin-left: 10px;width: 4em; text-align: right"></span>
                                </div>
                            </div>
                        </div>
                        <div class="icon">
                            @if(in_array(1,$v['badge']))
                                <img src="/images/wechat/1-z.png" alt="">
                            @else
                                <img src="/images/wechat/1-f.png" alt="">
                            @endif
                            @if(in_array(2,$v['badge']))
                                <img src="/images/wechat/2-z.png" alt="">
                            @else
                                <img src="/images/wechat/2-f.png" alt="">
                            @endif
                            @if(in_array(3,$v['badge']))
                                <img src="/images/wechat/3-z.png" alt="">
                            @else
                                <img src="/images/wechat/3-f.png" alt="">
                            @endif
                            @if(in_array(4,$v['badge']))
                                <img src="/images/wechat/4-z.png" alt="">
                            @else
                                <img src="/images/wechat/4-z.png" alt="">
                            @endif

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div id="content" class="hidden"></div>
    </div>
    <!--地图api-->
    <div id="allmap"></div>
    <div id="l-map"></div>
@endsection

@section('jscontent')
    <script src="/js/wechat/zepto.js"></script>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=RdBeGMrogIDsfZK4bAYjDSI0fbqtMLC7"></script>
    <script>
        var search = {
            zepto: function () {
                $(function () {
                    //加载城市事件
                    $('.container').show();
                    //选择城市 start
                    $('body').on('click', '.city-list p', function () {
                        var city = $(this).html();
                        $('#content').addClass('hidden');
                        $('.current-city .city-1').text(city)
                        search.getData(city);
                    });
                    //点击索引查询城市
                    $('body').on('click', '.letter a', function () {
                        var s = $(this).html();
                        $(window).scrollTop($('#' + s + '1').offset().top - 55);
                        $("#showLetter span").html(s);
                        $("#showLetter").show().delay(500).hide(0);
                    });
                    //中间的标记显示
                    $('body').on('onMouse', '.showLetter span', function () {
                        $("#showLetter").show().delay(500).hide(0);
                    });

                })
            },
            getPosition: function(city) {
                //浏览器定位
                var map = new BMap.Map("allmap");
                var point = new BMap.Point();
                map.centerAndZoom(point,12);
                var geolocation = new BMap.Geolocation();
                geolocation.getCurrentPosition(function(r){
                    if(this.getStatus() == BMAP_STATUS_SUCCESS){
                        var mk = new BMap.Marker(r.point);
                        map.addOverlay(mk);
                        map.panTo(r.point);
                        search.setCookie(r.point.lat,r.point.lng,r.address.city);
                        if(!city) {
                            city = r.address.city;
                        }
                        $('.current-city .city-1').text(city);
                        search.getData(city,r.point);
                    }
                    else {
                        alert('failed'+this.getStatus());
                    }
                });
            },
            setCookie: function(lat,long,city) {
                var exp = new Date();
                exp.setTime(exp.getTime() + 1000*60*10);
                document.cookie = 'lat='+lat+';expires='+ exp.toGMTString();
                document.cookie = 'long='+long+';expires='+ exp.toGMTString();
                document.cookie = 'city='+city+';expires='+ exp.toGMTString();
            },
            getCookie: function(name) {
                var exp = new Date();
                exp.setTime(exp.getTime() - 1);
                var cval = document.cookie.match(new RegExp('(^|)'+name+'=([^;]*)(;|$)'));
                return cval;
            },
            changeCity: function() {
                $('body').on('click','.change-city',function () {
                    $('#content').removeClass('hidden');
                    $('.institution').addClass('hidden');
                })
            },
            ajaxFn: function(city,point) {
                var option = {
                    url: '/wechat/public/activity/search/hospital',
                    method: 'post',
                    data: {city:city,point:point},
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                };
                $.ajax(option).done(function(res) {
                    if (res.status == 0) {
                        $('.institution').removeClass('hidden');
                        var data = res.data;
                        var hospital = '';
                        if (data.length == 0) {
                            hospital += '<div class="no-data">\n' +
                                '                    <img src="/images/wechat/hospital.png" alt="">\n' +
                                '                    <span>没有相关数据...</span>\n' +
                                '                </div>';
                        }
                        $.each(data,function(index,value) {
                            hospital += '<div class="list hospital" data-id="'+value._id+'">\n' +
                                '            <div class="info">\n' +
                                '                <img src="'+value.logo+'" alt="">\n' +
                                '                <div class="right">\n' +
                                '                    <span>'+value.name+'</span>\n' +
                                '                    <span class="introduce">'+value.introduce+'</span>\n' +
                                '                    <div>\n' +
                                '                        <span class="introduce address">地址：'+value.address+'</span>\n' +
                                '                        <span style="color: #8d8d8d; margin-left: 10px;">'+value.distance+'</span>\n' +
                                '                    </div>\n' +
                                '                </div>\n' +
                                '            </div>\n' +
                                '            <div class="icon">\n' ;
                            if (value.badge){
                                if(IsInArray(value.badge,1)) {
                                    hospital += '                <img src="/images/wechat/1-z.png" alt="">\n';
                                }else {
                                    hospital += '                    <img src="/images/wechat/1-f.png" alt="">\n' ;
                                }
                                if(IsInArray(value.badge,2)) {
                                    hospital += '                <img src="/images/wechat/2-z.png" alt="">\n';
                                }else {
                                    hospital += '                    <img src="/images/wechat/2-f.png" alt="">\n' ;
                                }
                                if(IsInArray(value.badge,3)) {
                                    hospital += '                <img src="/images/wechat/3-z.png" alt="">\n';
                                }else {
                                    hospital += '                    <img src="/images/wechat/3-f.png" alt="">\n' ;
                                }
                                if(IsInArray(value.badge,4)) {
                                    hospital += '                <img src="/images/wechat/4-z.png" alt="">\n';
                                }else {
                                    hospital += '                    <img src="/images/wechat/4-f.png" alt="">\n' ;
                                }
                            }
                            hospital +='\n' +
                                '            </div>\n' +
                                '        </div>';
                        })
                        $('.inst-list').empty().append(hospital);
                    }
                    function IsInArray(arr,val){

                        var testStr=','+arr.join(",")+",";

                        return testStr.indexOf(","+val+",")!=-1;

                    }
                })
            },
            getData: function(city) {
                var id   = $('#act-id').val();
                var lat  = search.getCookie('lat');
                var long = search.getCookie('long');
                var city = city;
                if (lat) {
                    var point = {};
                    point.lat = lat[2];
                    point.lng = long[2];
                    if(city) {
                        search.ajaxFn(city,point);
                    } else {
                        city = search.getCookie('city');
                        $('.current-city .city-1').text(city[2])
                        search.ajaxFn(city[2],point);

                    }
                }else {
                    search.getPosition();
                }

            },
            getDetail: function() {
                $('body').on('click','.hospital',function() {
                    var id = $(this).data('id');
                    window.location.href = '/wechat/public/activity/hospital?id=' + id;
                })
            }
        }
        function init() {
            search.changeCity();
            search.zepto();
            search.getDetail();
            search.getData();
        }
        init();
    </script>
@endsection