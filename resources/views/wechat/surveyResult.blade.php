@extends('wxbase')
@section('head')
    <title>结果反馈</title>
    <style>
        .s-content {
            padding: 10px;
        }
        .c-info {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            margin-top: 20px;
        }
        .c-info img {
            width: 40vw;
            height: 40vw;
        }
        .process {
            display: flex;
            margin-top: 20px;
        }
        .process .stage {
            display: flex;
            flex-direction: column;
            align-items: center;
        }
        .list div {
            display: inline-block;
            width: 5px;
            height: 40px;
            background: #8d8d8d;
            margin-right: 10px;
        }
        .light .list div{
            background: #00AAFF;
        }

        .remark {
            padding: 10px;
            border-radius: 3.5px;
            background: #f5f5f5;
            line-height: 1.8em;
            margin-top: 20px;
        }
        .bottom {
            display: flex;
            justify-content: space-around;
            line-height: 40px;
            margin-top: 20px;
        }
        .bottom .left {
            color: #ffcc00;
        }
        .bottom .right {
            color: #8d8d8d;
        }
    </style>
@endsection
@section('content')
    <div class="s-content">
        <div class="c-info">
            @if($result == 1)
            <img src="/images/wechat/difengxian.png" alt="">
            @elseif($result == 2)
                <img src="/images/wechat/zhongfengxian.png" alt="">
            @elseif($result == 3)
                <img src="/images/wechat/gaofengxian.png" alt="">
            @endif
            <div class="process">
                <div class="stage low @if($result == 1 || $result == 2 || $result == 3) light @endif">
                    <div class="list">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <span>低风险</span>
                </div>
                <div class="stage middle @if($result == 2 || $result == 3) light @endif">
                    <div class="list">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <span>中风险</span>
                </div>
                <div class="stage deep @if($result == 3) light @endif">
                    <div class="list">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <span>高风险</span>
                </div>
            </div>
        </div>
        <div class="remark">
           注：测评结果仅供参考，不可用于其他任何用途，必要时请及时寻求专业意见。
        </div>
        <div class="bottom">
            <div class="left">解决方案</div>
            <div class="right">再测一次</div>
        </div>
    </div>
    <input type="hidden" id="id" value="{{$id}}">
@endsection
@section('jscontent')
    <script>
        $('.bottom .right').on('click',function (e) {
            var id = $('#id').val();
            window.history.go(-1);
        })
        $('.bottom .left').on('click',function(e) {
            alert('待定');
        })
    </script>
@endsection