<html lang="en" style="font-size: 33.7px;"><head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="_token" content="{{ csrf_token() }}"/>
    <title>抽奖</title>
    <link rel="stylesheet" href="http://www.17sucai.com/preview/98055/2018-12-04/%E4%B9%9D%E5%AE%AB%E6%A0%BC%E6%8A%BD%E5%A5%96/css/common.css">
    {{--<link rel="stylesheet" href="css/index.css">--}}
    <style>
        body {
            display: block;
            position: relative;
            font-size: 0.16rem;
            font-family: "Microsoft Yahei", "Arial", sans-serif;
            background-color:#ff735a;
        }
        #wrap {
            overflow: hidden;
            position: relative;
            width: 100%;
            height: 17.4rem;
            background-image: url("../image/bg.jpg");
            background-size:100% 100%;
            -webkit-background-size:100% 100%;
        }
        .time {
            position: absolute;
            left: 0.66666667rem;
            top: 1rem;
            width: 1.73333333rem;
            line-height: 0.37333333rem;
            font-size: 0.29333333rem;
            color: #fff;
            -o-transform: rotate(-30deg);
            -ms-transform: rotate(-30deg);
            -moz-transform: rotate(-30deg);
            -webkit-transform: rotate(-30deg);
            transform: rotate(-30deg);
        }

        @keyframes twinkle {
            100% {
                opacity: 0;
            }
        }
        .stars:nth-child(even) {
            -o-animation: bigSmall 1s 1s linear infinite alternate;
            -ms-animation: bigSmall 1s 1s linear infinite alternate;
            -moz-animation: bigSmall 1s 1s linear infinite alternate;
            -webkit-animation: bigSmall 1s 1s linear infinite alternate;
            animation: bigSmall 1s 1s linear infinite alternate;
        }
        @keyframes bigSmall {
            100% {
                -o-transform: scale(0.5);
                -ms-transform: scale(0.5);
                -moz-transform: scale(0.5);
                -webkit-transform: scale(0.5);
                transform: scale(0.5);
            }
        }
        /*主体*/
        .main {
            position: absolute;
            left: 0;
            /*bottom: 0;*/
            width: 100%;
            height: 12rem;
        }
        .rule,
        .my {
            position: absolute;
            top: 0.4rem;
            width: 1.6rem;
            height: 0.6rem;
            background: no-repeat center / contain;
        }
        .rule {
            left: 2.26666667rem;
            background-image: url("http://www.17sucai.com/preview/98055/2018-12-04/%E4%B9%9D%E5%AE%AB%E6%A0%BC%E6%8A%BD%E5%A5%96/image/rule.png");
        }
        .my {
            right: 2.26666667rem;
            background-image: url("http://www.17sucai.com/preview/98055/2018-12-04/%E4%B9%9D%E5%AE%AB%E6%A0%BC%E6%8A%BD%E5%A5%96/image/my.png");
        }
        /*游戏盒子*/
        .box {
            position: absolute;
            left: 0.4rem;
            top: 1.5rem;
            width: 9.2rem;
            height: 8.02666667rem;
            background-image: url("http://www.17sucai.com/preview/98055/2018-12-04/%E4%B9%9D%E5%AE%AB%E6%A0%BC%E6%8A%BD%E5%A5%96/image/choujiang.png");
            background-size:100% 100%;
            -webkit-background-size:100% 100%;
        }

        @keyframes jump {
            100% {
                top: -0.66666667rem;
            }
        }
        .main-h2{
            background:url(../image/bg2.png) no-repeat;
            width:9rem;
            height:1rem;
            line-height:1rem;
            margin:0px auto;
            text-align: center;
            font-size: 0.4rem;
            color: #fff;
            background-size:100% 100%;
            -webkit-background-size:100% 100%;
        }
        #change {
            color: #fde817;
        }
        /*小彩灯*/
        .light {
            position: absolute;
            left: 0;
            bottom: 0;
            width: 100%;
            /*  height: 6.93333333rem;*/
            height: 100%;
        }
        .light li {
            position: relative;
            width: 0.52rem;
            height: 100%;
        }
        .light li p {
            margin-top: 0.7rem;
            width: 0.52rem;
            height: 0.52rem;
            background-image: url("http://www.17sucai.com/preview/98055/2018-12-04/%E4%B9%9D%E5%AE%AB%E6%A0%BC%E6%8A%BD%E5%A5%96/image/dark.png");
        }
        .light li p.blin {
            background-image: url("http://www.17sucai.com/preview/98055/2018-12-04/%E4%B9%9D%E5%AE%AB%E6%A0%BC%E6%8A%BD%E5%A5%96/image/light.png");
        }


        /*小彩灯*/
        .light2 {
            position: absolute;
            left: 0;
            bottom: 0;
            width: 100%;
            height: 100%;
        }
        .light2 li{
            position:absolute;
            width:100%;

            left:0rem;
        }
        .light2 li.ft{ top:0rem;}
        .light2 li.fb{ bottom:0rem;}
        .light2 li p {
            width:0.52rem;
            height:0.52rem;
            float:left;
            margin-left:0.88rem;
            background-image: url("http://www.17sucai.com/preview/98055/2018-12-04/%E4%B9%9D%E5%AE%AB%E6%A0%BC%E6%8A%BD%E5%A5%96/image/dark.png");
        }
        .light2 li p.blin {
            background-image: url("http://www.17sucai.com/preview/98055/2018-12-04/%E4%B9%9D%E5%AE%AB%E6%A0%BC%E6%8A%BD%E5%A5%96/image/light.png");
        }



        /*游戏区域*/
        .play {
            position: absolute;
            left: 50%;
            top: 50%;
            z-index: 1;
            margin: -3.4rem 0 0 -4rem;
            width: 8rem;
            height: 5.86666667rem;
        }
        .play li {
            position: relative;
            float: left;
            margin: 0.3rem 0 0 0.09333333rem;
            width:2.54666667rem;
            height:1.86666667rem;
            background-image: url("http://www.17sucai.com/preview/98055/2018-12-04/%E4%B9%9D%E5%AE%AB%E6%A0%BC%E6%8A%BD%E5%A5%96/image/normal.png");
        }
        .play li:nth-child(4) {
            left: 5.28rem;
        }
        .play li:nth-child(6) {
            top: 2.2rem;
        }
        .play li:nth-child(7) {
            left: 2.64rem;
        }
        .play li:nth-child(8) {
            left: -2.64rem;
        }
        .play li:nth-child(9) {
            left: -5.28rem;
            top: -2.2rem;
        }
        .play li div {
            position: relative;
            z-index: 1;
            width: 100%;
            height: 100%;
            text-align: center;
        }
        /*!*奖品图片大小*!*/
        .play li div img {
            width: 2rem;
            height: 1.16rem;
            margin-top: .3rem;
        }
        /*奖品图片大小*/
        /*.play li div img {*/
            /*width: 2.54666667rem;*/
            /*height: 1.86666667rem;*/
        /*}*/
        .play li div p {
            position: absolute;
            left: 50%;
            bottom: 0rem;
            margin-left: -1.28rem;
            width: 100%;
            font-size: 0.29333333rem;
            color: #c14600;
        }
        .play li.select {
            background-image: url("http://www.17sucai.com/preview/98055/2018-12-04/%E4%B9%9D%E5%AE%AB%E6%A0%BC%E6%8A%BD%E5%A5%96/image/select.png");
        }
        /*开始按钮*/
        #btn {
            background-image: url("http://www.17sucai.com/preview/98055/2018-12-04/%E4%B9%9D%E5%AE%AB%E6%A0%BC%E6%8A%BD%E5%A5%96/image/start.png");
        }
        /*奖品展示*/
        .awards {
            bottom: 0.37333333rem;
            margin-left: -4.6rem;
            width: 9.2rem;
            height: 1.86666667rem;
            background-color: #fd851f;
            box-shadow: 0 0.26666667rem 0.26666667rem rgba(248, 85, 40, 0.5) inset;
            border-radius: 0.13333333rem;
        }


        .banner{ width:100%;}
        .banner img{ display:block;width:100%;}
        .activity,.activity2{padding:0rem 0rem 0.3rem 0rem; }
        .activity2{  padding:0rem 0rem 0.6rem 0rem; }
        .activity-amin{width:85%; margin:0rem auto;}
        .activity-amin h2{ margin: 0rem auto 0.4rem auto;
            width: 4.4rem;
            height: 0.8rem;
            background-color: #ffe771;
            text-align: center;
            line-height: 0.8rem;
            font-size: 0.4rem;
            color: #bc5700;
            border-radius: 0.34666667rem;
            letter-spacing:0.2rem
        }
        .activity-amin p{
            width: 100%;
            line-height: 0.6rem;
            font-size: 0.2rem;
            color: #fff;
            letter-spacing:0.1rem;
            margin-bottom:0.6rem;
        }

        .wt1{ width:94%;  display:block; border:none; background-color:#FFF; padding:5% 3%; border-radius:0.34666667rem; font-size: 0.4rem; margin-bottom:6%}
        .wt2{width:50%; float:left}
        .wt3{ width:40%; float:right; background-color:#efc84d; color:#FFF}
        .wt4{width:100%;background-color:#efc84d; color:#FFF; font-size:0.6rem;}
        .prompt1{clear:both; width:100%; text-align:center; font-size: 0.4rem; margin-bottom:6%; color:#ffe66f; display:none}
        .tb0{ width:100%; margin-bottom:6%; }
        .tb0 tr th{ width:20%; line-height:0.9rem; text-align:center; color:#fff886; border-bottom:1px solid #ffffff;}
        .tb0 tr th:nth-child(1){width:42%; }
        .tb0 tr th:nth-child(2){width:38%; }
        .tb0 tr td{ width:20%; line-height:0.9rem; text-align:center; color:#FFF;border-bottom:1px solid #ffffff;}



        .main2{ width:90%; position:absolute; bottom:0.7rem; left:50%; margin-left:-45%;  }
        a.a-main{ display:inline-block; color:#FFF;width:47%; padding:0.3rem 0rem; text-align:center; font-size: 0.5rem; border-radius:0.2rem; letter-spacing:0.1rem;background-color:#d03434; }
        a.a1{ margin-right:4%}
        .img1{ display:block; width:100%; margin:0.2rem auto 0.2rem auto;}

        a.a-main2{ display:block; color:#FFF; margin:0px auto; width:8.7rem; padding:0.3rem 0rem; text-align:center; font-size: 0.5rem; border-radius:0.2rem; letter-spacing:0.1rem}
        a.a3{ background-color:#efc84d; margin:0.2rem auto 0.4rem auto;}
    </style>
    <!-- 移动端适配 -->
    <script>
        var html = document.querySelector('html');
        changeRem();
        window.addEventListener('resize', changeRem);

        function changeRem() {
            var width = html.getBoundingClientRect().width;
            html.style.fontSize = width / 10 + 'px';
        }
    </script>
</head>

<body>
<div id="wrap">

    <!--主体-->
    <div class="main">
    {{--<h2 class="main-h2">您今日还有 <span id="change"> 3 </span> 次抽奖机会</h2>--}}

    <!--游戏区域-->
        <div class="box">
            <ul class="light clearfix">
                <li class="fl">
                    <p class="blin"></p>
                    <p class=""></p>
                    <p class="blin"></p>
                    <p class=""></p>
                    <p class="blin"></p>
                    <p class=""></p>
                </li>
                <li class="fr">
                    <p class=""></p>
                    <p class="blin"></p>
                    <p class=""></p>
                    <p class="blin"></p>
                    <p class=""></p>
                    <p class="blin"></p>
                </li>
            </ul>
            <ul class="light2 clearfix">
                <li class="ft">
                    <p class="blin"></p>
                    <p class=""></p>
                    <p class="blin"></p>
                    <p class=""></p>
                    <p class="blin"></p>
                    <p class=""></p>
                </li>
                <li class="fb">
                    <p class=""></p>
                    <p class="blin"></p>
                    <p class=""></p>
                    <p class="blin"></p>
                    <p class=""></p>
                    <p class="blin"></p>
                </li>
            </ul>
            <!--九宫格-->
            <ul class="play clearfix">
            @foreach($data as $k=>$v)
                @if($k == 4)
                    <!--开始按钮-->
                        <li id="btn"></li>
                    @endif
                    <li class="prize @if($k==0) select @endif">
                        <div>
                            {{--<img src="http://www.17sucai.com/preview/98055/2018-12-04/%E4%B9%9D%E5%AE%AB%E6%A0%BC%E6%8A%BD%E5%A5%96/image/box1.png">--}}
                            <img src="{{$v['img']}}" alt="">
                            <p>{{$v['prize']}}</p>
                        </div>
                    </li>
                @endforeach
            </ul>

        </div>
        {{--<div class="main2">--}}
        {{--<a href="#" class="a-main a1">活动规则</a>--}}
        {{--<a href="personal-center.html" class="a-main">我的奖品</a>--}}
        {{--</div>--}}
    </div>

    <!--游戏规则弹窗-->
    <div id="mask-rule">
        <div class="box-rule">
            <h2>活动规则</h2>
            <span id="close-rule"></span>
            <div class="con">
                <div class="text">
                    <p>1.活动时间：2018年10月18日——2018年12月31日。<br>
                        2.本次活动为福建某某公司专属特权活动，仅针对目标用户参与。<br>
                        3.活动期间，福建某某公司专属客户每天可参与抽奖一次。<br>
                        4.本次活动奖品为20元话费、50元话费、100元话费、300M省内加餐包、500M省内加餐包、1G省内加餐包。<br>
                        5.每个用户当月获得话费和流量奖品将于次月到账。</p>
                </div>
            </div>
        </div>
    </div>

    <!--中奖提示-->
    <div id="mask">
        <div class="box-rule">
            <div class="box-text">
                <div id="text1">

                </div>
                <a href="#" class="btn">确定</a>
            </div>
        </div>
    </div>

    <!--提示-->
    <div id="mask2">
        <div class="box-rule">
            <p>您剩余抽奖次数为<span>0</span>，不能抽奖~</p>
            <a href="#" class="btn">确定</a>
        </div>
    </div>


</div>

<script src="http://www.17sucai.com/preview/98055/2018-12-04/九宫格抽奖/js/jquery-1.11.3.min.js"></script>
{{--<script src="http://www.17sucai.com/preview/98055/2018-12-04/九宫格抽奖/js/common.js"></script>--}}
{{--<script src="http://www.17sucai.com/preview/98055/2018-12-04/九宫格抽奖/js/index.js"></script>--}}
<script>
    var $maskRule = $("#mask-rule"),//规则遮罩层
        $mask = $("#mask"),//红包遮罩层
        $winning = $(".winning"),//红包
        $card = $("#card"),
        $close = $("#close");
    //link = false;//判断是否在链接跳转中

    //规则
    $(".a1").click(function () {
        $maskRule.show();
    });
    $("#close-rule").click(function () {
        $maskRule.hide();
    });

    /*中奖信息提示*/
    function win(mark1,type,name,img) {
        //遮罩层显示
        $mask.show();
        var mark2=mark1+1;
        switch(type)
        {
            case 0:
                $("#text1").html('<div class="div-h4"><img src="http://www.17sucai.com/preview/98055/2018-12-04/%E4%B9%9D%E5%AE%AB%E6%A0%BC%E6%8A%BD%E5%A5%96/image/box0-1.png"/></div>'+
                    '<b>'+ name +'</b>');
                break;
            default:
                $("#text1").html("<h3>恭喜您获得</h3>"+ "<p>"+ name +"</p>");
                    // "<b>流量一张</b>");
                break;
        }
        //关闭弹出层
        $("#close,.win,.btn").click(function () {
            //$close.click(function () {
            $mask.hide();
            $("#mask2").hide();
            $winning.removeClass("reback");
            $card.removeClass("pull");
        });
        /*$(".win,.btn").click(function () {
            link = true;
        });*/
    }
    $(function () {
        var $blin = $(".light p"),//所有彩灯
            $blin2 = $(".light2 p"),//所有彩灯
            $prize = $(".play li").not("#btn"),//含谢谢参与的所有奖品
            $change = $("#change"),//显示剩余机会
            $btn = $("#btn"),//开始抽奖按钮
            length = $prize.length,//奖品总数
            data = {count: 3},//次数
            bool = true,//判断是否可点击,true为可点击
            mark = 0,//标记当前位置，区间为0-7
            timer;//定时器

        init();
        //默认动画效果
        function init() {
            timer = setInterval(function () {
                //不能调用animate函数，不然标记会有问题
                $blin.toggleClass("blin");//彩灯动画
                $blin2.toggleClass("blin");//彩灯动画
                //九宫格动画
                length++;
                length %= 8;
                $prize.eq(length - 1).removeClass("select");
                $prize.eq(length).addClass("select");

                //位置标记的改变
                mark++;
                mark %= 8;
            }, 1000);
        }

        //点击抽奖
        $btn.click(function () {
            if (bool) {//若未按下
                bool = false;
                if (data.count > 0) {//若还有次数
                    // data.count--;
                    // $change.html(data.count);
                    var option = {
                        type: 'POST',
                        url: '/wechat/prize/getResult',
                        dataType: 'json',
                        data: {data: data},
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    };
                    $.ajax(option).done(function (msg) {
                        if(msg.status == 0){
                            clickFn(msg.result,msg.type,msg.name,msg.img);
                        }

                    });
                } else {
                    $("#mask2").show();
                }
            }
        });

        //点击旋转
        function clickFn(result,type,name,img) {
            clearInterval(timer);//点击抽奖时清除定时器
            var random = 32 + 8 - mark + result;
            mark  =  result;
            // //默认先转4圈
            // var random = 32 + mark;//圈数 * 奖品总数
            //调用旋转动画
            for (var i = 1; i <= random; i++) {
                setTimeout(animate(), 2 * i * i);//第二个值越大，慢速旋转时间越长
            }
            //停止旋转动画
            setTimeout(function () {
                console.log("中了" + mark);
                setTimeout(function () {
                    bool = true;
                    win(mark,type,name,img);
                }, 1000);
            }, 2 * random * random);
        }

        //动画效果
        function animate() {
            return function () {
                $blin.toggleClass("blin");//彩灯动画
                $blin2.toggleClass("blin");//彩灯动画
                //九宫格动画
                length++;
                length %= 8;
                $prize.eq(length - 1).removeClass("select");
                $prize.eq(length).addClass("select");
            }
        }

        //中奖信息提示
        $("#close,.win,.btn").click(function () {
            clearInterval(timer);//关闭弹出时清除定时器
            init();
        });


    });
</script>
</body></html>