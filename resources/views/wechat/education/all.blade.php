@extends('wxbase')
@section('head')
    <link rel="stylesheet" href="/css/wechat/edu-common.css">
    <style>
        .content {
            /*padding: 2vw;*/
        }
        .content .title {
            font-size: 1.1em;
            color: #8d8d8d;
            margin: 15px;
            display: inline-block;
        }
        ul {
            display: flex;
            flex-wrap: wrap;
        }
        ul li {
            width: 45vw;
            text-align: center;
            background-color: #c3d2d1;
            margin-left: 3vw;
            margin-bottom: 10px;
            padding: 10px;
            border-radius: 3.5px;
        }
    </style>
@endsection
@section('content')
    <div class="top">
        <div class="search">
            <img src="/images/wechat/search.png" alt=""/>
            <input type="search" placeholder="请输入关键词">
        </div>
        <span class="cancel display-none">取消</span>
    </div>
    <div class="content">
        <span class="title">口腔知识库</span>
        <ul>
            <li class="item" data-id="all">全部</li>
            @foreach ($data as $v)
            <li class="item" data-id="{{$v['id']}}">{{$v['name']}}</li>
            @endforeach
        </ul>
    </div>
    <div class="before-search display-none">
        <span class="rm">热门搜索</span>
        <div class="rm-list">
            @foreach ($label as $v)
            <span class="rm-tag" data-id="{{$v['id']}}">{{$v['name']}}</span>
            @endforeach
        </div>
    </div>
    <input type="hidden" id="type" value="{{$type}}">
@endsection

@section('jscontent')
    <script>
        //分类切换隐藏显示
        function all() {
            init.searchFn();
            init.tagFn();
        }
        var init = {
            searchFn: function () {
                //点击输入框显示
                $('.search input').on('focus',function() {
                    $('.top .cancel').removeClass('display-none');
                    $('.search').addClass('search-a');
                    $('.before-search').removeClass('display-none');
                    $('.content').addClass('display-none');
                })
                //点击取消显示
                $('.cancel').on('click',function () {
                    $('.top .cancel').addClass('display-none');
                    $('.search').removeClass('search-a');
                    $('.before-search').addClass('display-none');
                    $('.content').removeClass('display-none');
                    $('.search input').val('');
                })
                //搜索内容 获取键盘搜索按钮事件
                $(".search input").on('keypress', function(e) {
                    var keycode = e.keyCode;
                    //获取搜索框的值
                    var searchText = $(this).val();
                    if (keycode == '13') {
                        e.preventDefault();
                        //请求搜索接口
                        if (searchText == '') {
                            alert('请输入检索内容！');
                        } else {
                            var type = $('#type').val();
                            var search = $('.search input').val();
                            switch (type){
                                case '1':
                                    window.location = '/wechat/education?type= '+ type + '&search=' + search;
                                    break;
                                case '2':
                                    window.location.href = '/wechat/edu/video/search?type= ' + type + '&search='+search;
                                    break;
                            }
                        }
                    }
                });
                //热门标签搜索
                $('.before-search .rm-tag').on('click', function (e) {
                    var type = $('#type').val();
                    var id = $(this).data('id');
                    switch (type){
                        case '1':
                            window.location.href = '/wechat/education?type= ' + type + '&id='+id;
                            break;
                        case '2':
                            window.location.href = '/wechat/edu/video/search?type= ' + type + '&id='+id;
                            break;
                    }
                })
            },
            tagFn: function () {
                $('.content .item').on('click',function (e) {
                    var type = $('#type').val();
                    var id = $(this).data('id');
                    switch (type){
                        case '1':
                            window.location.href = '/wechat/education?type= ' + type + '&id='+id;
                            break;
                        case '2':
                            window.location.href = '/wechat/edu/video/search?type= ' + type + '&id='+id;
                            break;
                    }
                })
            }

        }
        all();
    </script>
@endsection