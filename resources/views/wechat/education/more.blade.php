@extends('wxbase')
@section('head')
    <title>老人专区</title>
    <link rel="stylesheet" href="/css/wechat/edu-common.css">
    <link rel="stylesheet" href="/css/wechat/edu-index.css">
    <style>
        .type-3 .item {
            display: flex;
            flex-direction: column;
            border-bottom: 1px solid #e1e1e1;
        }
        .type-3 .item-content {
            display: flex;
            flex-direction: row;
        }
        .type-3 .item img {
            width: 33vw;
            height: 20vw;
            display: inline-block;
        }
        .type-3 .right-1 {
            width: 56vw;
            padding-left: 10px;
        }
        .type-3 .item .introduce {
            -webkit-line-clamp: 1;
        }
        ul li:first-child
    </style>
@section('content')
    <div class="top">
        <div class="search">
            <img src="/images/wechat/search.png" alt=""/>
            <input type="search" placeholder="请输入关键词">
        </div>
        <span class="cancel display-none">取消</span>
    </div>
    <div class="content">
        <div class="type-content">
            <div class="type-3 type-item" >
                <div class="item" data-id="5b9b6f3c76337905e608ca1b">
                    <div class="item-content">
                        <img src="https://ayzypublic.s3.cn-north-1.amazonaws.com.cn/8020/attachment/20180914/wNgf5BmQNNxqv89t8aTLVnfqdcNKAxmK.jpeg" alt="">
                        <div class="right-1">
                            <span class="title">8020口腔保健宣传动画测试标题测试标题测试表态加分减分减肥法</span>
                            <span class="introduce">说明牛萨萨的发生科技法拉盛就反馈按理说放假啊来上课发拉屎咖啡就暗示法拉盛房价</span>
                        </div>
                    </div>
                    <div class="tag">
                        <ul>
                            <li>口腔</li>
                            <li>牙疼</li>
                            <li>牙齿</li>
                        </ul>
                    </div>
                </div>
                <div class="item" data-id="5b9b6f3c76337905e608ca1b">
                    <div class="item-content">
                        <img src="https://ayzypublic.s3.cn-north-1.amazonaws.com.cn/8020/attachment/20180914/X0JVBZdWWrCKgWdblKl58h9avQuUIyZc.jpeg" alt="">
                        <div class="right-1">
                            <span class="title">8020口腔保健宣传动画测试标题测试标题测试表态加分减分减肥法</span>
                            <span class="introduce">说明牛萨萨的发生科技法拉盛就反馈按理说放假啊来上课发拉屎咖啡就暗示法拉盛房价</span>
                        </div>
                    </div>
                    <div class="tag" style="line-height: 38px">
                        <ul>
                            <li>洁牙</li>
                            <li>刷牙</li>
                        </ul>
                    </div>
                </div>
                <div class="item" data-id="5b9b6f3c76337905e608ca1b">
                    <div class="item-content">
                        <img src="https://ayzypublic.s3.cn-north-1.amazonaws.com.cn/8020/attachment/20180914/wNgf5BmQNNxqv89t8aTLVnfqdcNKAxmK.jpeg" alt="">
                        <div class="right-1">
                            <span class="title">8020口腔保健宣传动画测试标题测试标题测试表态加分减分减肥法</span>
                            <span class="introduce">说明牛萨萨的发生科技法拉盛就反馈按理说放假啊来上课发拉屎咖啡就暗示法拉盛房价</span>
                        </div>
                    </div>
                    <div class="tag">
                        <ul>
                            <li>口腔</li>
                            <li>牙疼</li>
                            <li>牙齿</li>
                        </ul>
                    </div>
                </div>
                <div class="item" data-id="5b9b6f3c76337905e608ca1b">
                    <div class="item-content">
                        <img src="https://ayzypublic.s3.cn-north-1.amazonaws.com.cn/8020/attachment/20180914/X0JVBZdWWrCKgWdblKl58h9avQuUIyZc.jpeg" alt="">
                        <div class="right-1">
                            <span class="title">8020口腔保健宣传动画测试标题测试标题测试表态加分减分减肥法</span>
                            <span class="introduce">说明牛萨萨的发生科技法拉盛就反馈按理说放假啊来上课发拉屎咖啡就暗示法拉盛房价</span>
                        </div>
                    </div>
                    <div class="tag" style="line-height: 38px">
                        <ul>
                            <li>洁牙</li>
                            <li>刷牙</li>
                        </ul>
                    </div>
                </div>
                <div class="item" data-id="5b9b6f3c76337905e608ca1b">
                    <div class="item-content">
                        <img src="https://ayzypublic.s3.cn-north-1.amazonaws.com.cn/8020/attachment/20180914/wNgf5BmQNNxqv89t8aTLVnfqdcNKAxmK.jpeg" alt="">
                        <div class="right-1">
                            <span class="title">8020口腔保健宣传动画测试标题测试标题测试表态加分减分减肥法</span>
                            <span class="introduce">说明牛萨萨的发生科技法拉盛就反馈按理说放假啊来上课发拉屎咖啡就暗示法拉盛房价</span>
                        </div>
                    </div>
                    <div class="tag">
                        <ul>
                            <li>口腔</li>
                            <li>牙疼</li>
                            <li>牙齿</li>
                        </ul>
                    </div>
                </div>
                <div class="item" data-id="5b9b6f3c76337905e608ca1b">
                    <div class="item-content">
                        <img src="https://ayzypublic.s3.cn-north-1.amazonaws.com.cn/8020/attachment/20180914/X0JVBZdWWrCKgWdblKl58h9avQuUIyZc.jpeg" alt="">
                        <div class="right-1">
                            <span class="title">8020口腔保健宣传动画测试标题测试标题测试表态加分减分减肥法</span>
                            <span class="introduce">说明牛萨萨的发生科技法拉盛就反馈按理说放假啊来上课发拉屎咖啡就暗示法拉盛房价</span>
                        </div>
                    </div>
                    <div class="tag" style="line-height: 38px">
                        <ul>
                            <li>洁牙</li>
                            <li>刷牙</li>
                        </ul>
                    </div>
                </div>
                <div class="item" data-id="5b9b6f3c76337905e608ca1b">
                    <div class="item-content">
                        <img src="https://ayzypublic.s3.cn-north-1.amazonaws.com.cn/8020/attachment/20180914/wNgf5BmQNNxqv89t8aTLVnfqdcNKAxmK.jpeg" alt="">
                        <div class="right-1">
                            <span class="title">8020口腔保健宣传动画测试标题测试标题测试表态加分减分减肥法</span>
                            <span class="introduce">说明牛萨萨的发生科技法拉盛就反馈按理说放假啊来上课发拉屎咖啡就暗示法拉盛房价</span>
                        </div>
                    </div>
                    <div class="tag">
                        <ul>
                            <li>口腔</li>
                            <li>牙疼</li>
                            <li>牙齿</li>
                        </ul>
                    </div>
                </div>
                <div class="item" data-id="5b9b6f3c76337905e608ca1b">
                    <div class="item-content">
                        <img src="https://ayzypublic.s3.cn-north-1.amazonaws.com.cn/8020/attachment/20180914/X0JVBZdWWrCKgWdblKl58h9avQuUIyZc.jpeg" alt="">
                        <div class="right-1">
                            <span class="title">8020口腔保健宣传动画测试标题测试标题测试表态加分减分减肥法</span>
                            <span class="introduce">说明牛萨萨的发生科技法拉盛就反馈按理说放假啊来上课发拉屎咖啡就暗示法拉盛房价</span>
                        </div>
                    </div>
                    <div class="tag" style="line-height: 38px">
                        <ul>
                            <li>洁牙</li>
                            <li>刷牙</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="before-search before-search2 display-none">
        <span class="rm">热门搜索</span>
        <div class="rm-list">
            {{--@foreach ($label1 as $v)--}}
            {{--<span class="rm-tag" data-id="{{$v['id']}}">{{$v['name']}}</span>--}}
            {{--@endforeach--}}
        </div>
    </div>
    <input type="hidden" id="type" value="2">
@endsection

@section('jscontent')
    <script src="/js/wechat/edu-index.js"></script>
@endsection