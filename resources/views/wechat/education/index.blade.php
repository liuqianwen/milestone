@extends('wxbase')
@section('head')
    <title>知识和讲堂</title>
    <link rel="stylesheet" href="/css/wechat/edu-common.css?id=1">
    <link rel="stylesheet" href="/css/wechat/edu-index.css">
    <style>
        .f-button {
            width: 100%;
            display: flex;
            justify-content: flex-end;
        }
        .button {
            padding: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            width: 63px;
            text-align: center;
            background: #ffe319;
        }
        .button img{
            width: 24px;
        }
        .display-none {
            display: none;
        }
    </style>
@endsection
@section('content')
    <div class="banner">
        <div class="wrap">
            <ul class="pic">
                @foreach ($top as $v)
                    <li class="item" data-id="{{$v['_id']}}">
                        <img src="{{$v['image']}}" alt="" width="640" height="480" >
                        <div class="text">
                            <span>{{$v['name']}}</span>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="top">
        <div class="search">
            <img src="/images/wechat/search.png" alt=""/>
            <input type="search" placeholder="请输入关键词">
        </div>
        <span class="cancel display-none">取消</span>
    </div>
    <div class="content">
        {{--@if($is_end == 0)--}}
        {{--<div class="time">--}}
            {{--<img src="/images/wechat/clock-icon.png" alt="">--}}
            {{--<span>10:00</span>--}}
        {{--</div>--}}
        {{--<div class="f-button display-none">--}}
            {{--<div class="button">--}}
                {{--@if($is_first == 1)--}}
                {{--<span class="h4">5</span>--}}
                {{--@else--}}
                    {{--<span class="h4">10</span>--}}
                    {{--@endif--}}
                {{--<span>领取</span>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--@else--}}
            {{--<div class="f-button">--}}
                {{--<div class="button">--}}
                    {{--@if($is_first == 1)--}}
                        {{--<span class="h4">5</span>--}}
                    {{--@else--}}
                        {{--<span class="h4">10</span>--}}
                    {{--@endif--}}
                    {{--<span>领取</span>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--@endif--}}
        <div class="type">
            <span class="type-name active" data-type="1">学术文献</span>
            <span class="type-name" data-type="2">教学视频</span>
            <span class="type-name" data-type="3">历史文章</span>
        </div>
        <div class="top-title">
            <span>{{$text}}</span>
            <span class="more">更多 <img src="/images/wechat/more.png" alt=""></span>
        </div>
        <div class="type-content">
            <div class="type-1 type-item">
                @if(count($article) == 0)
                    <span class="no-data">
                        <img src="/images/wechat/no-data.png" alt="">
                        <span>没有数据哟(∩_∩)</span>
                    </span>
                    @endif
                @foreach ($article as $v)
                <div class="item" data-id="{{$v['_id']}}">
                    <div class="art">
                        <div class="left">
                            <span class="title">{{$v['name']}}</span>
                            <span class="introduce">{{$v['description']}}</span>
                        </div>
                        <div class="right">
                            <img src="{{$v['image']}}" alt="">
                        </div>
                    </div>
                    <div class="tag">
                        <ul>
                            @foreach ($v['type_id'] as $v1)
                            <li>{{$v1}}</li>
                            @endforeach
                        </ul>
                        <span class="read">{{$v['read']}}次浏览</span>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="before-search before-search1 display-none">
        <span class="rm">热门搜索</span>
        <div class="rm-list">
            @foreach ($label1 as $v)
            <span class="rm-tag" data-id="{{$v['id']}}">{{$v['name']}}</span>
            @endforeach
        </div>
    </div>
    <input type="hidden" id="type" value="1">
@endsection

@section('jscontent')
    <script src="/js/wechat/edu-index.js?time={{time()}}"></script>
    <script>
        {{--var time    = "{{$time}}";--}}
        {{--var endtime = "{{$endtime}}";--}}
        {{--var len = endtime * 1000 - time * 1000;--}}
        {{--var timer;--}}
        // var index = function () {
        //     init.clickFn();
            // init.countFn();
        // }
        // var init = {
            // countFn: function () {
            //      init.countDown()
            //     timer = setInterval(init.countDown, 1000);
            // },
            // countDown: function () {
            //     if (len >= 0) {
            //         hours = parseInt((len % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            //         minutes = parseInt((len % (1000 * 60 * 60)) / (1000 * 60));
            //         seconds = Math.floor((len % (1000 * 60)) / 1000);
            //         len = len - 1000;
            //         if (minutes < 10 ) {
            //             minutes = '0' + minutes;
            //         }
            //         if (seconds < 10) {
            //             seconds = '0' + seconds;
            //         }
            //         msg = minutes + ":" + seconds;
            //         $('.time span').text(msg);
            //     } else{
            //         clearInterval(timer);
            //         var option = {
            //             url: '/wechat/edu/set/time',
            //             method:'post',
            //             data: {is_end: 1},
            //             headers:{
            //                 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            //             }
            //         }
            //         $.ajax(option).done(function (res){
            //             if (res.status == 0) {
            //                 $('.f-button').removeClass('display-none');
            //                 $('.time').addClass('display-none');
            //             }
            //         })
            //     }
            // },
            // clickFn: function () {
            //     $('.f-button').on('click',function () {
            //         var num = $(this).find('span.h4').text();
            //         var option = {
            //             url: '/wechat/edu/save/integral',
            //             method:'post',
            //             headers:{
            //                 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            //             }
            //         }
            //         $.ajax(option).done(function (res) {
            //             if (res.status == 0) {
            //                 layer.msg('已领取'+num+'积分');
            //                 window.location = window.location;
            //             }
            //         })
            //     })
            //
            // }
        // }
        // index();
    </script>
@endsection