@extends('wxbase')
<style>
    ul {
        padding: 0;
        margin: 0;
    }
    ul li {
        list-style: none;
        padding: 0;
        margin: 0;
    }
        /*搜索样式*/
    .top {
        padding: 10px 0;
        background: #f1f1f1;
    }
    .search {
        position: relative;
        width: 90vw;
        line-height: 40px;
        margin: 0 auto;
        border: 1px solid #e2e2e2;
        -webkit-border-radius: 5.5px;
        -moz-border-radius: 5.5px;
        border-radius: 5.5px;
        background: #fff;
    }
    .search input {
        border:none;
        margin-left:10px;
        width:calc(100% - 56px);
        height: 40px;
    }
    .search img {
        width:30px;
        height:30px;
        margin-left: 10px;
    }
</style>
@section('content')
    <div class="top">
        <div class="search">
            <img src="/images/wechat/search.png" alt=""/>
            <input type="search" placeholder="请输入关键词">
        </div>
    </div>
    <input type="hidden" id="type" value="1">
@endsection

@section('jscontent')
    <script>
        //分类切换隐藏显示
        function all() {
        }
        var init = {

        }
        all();
    </script>
@endsection