@extends('wxbase')
@section('head')
    <title>视频详情</title>
    <link rel="stylesheet" href="/css/wechat/edu-common.css">
    <style>
        body {
            cursor: pointer;
        }
        .content {
            display: flex;
            flex-direction: column;
            padding: 10px;
            border-bottom: 1px solid #e1e1e1;
        }
        .content span {
            padding: 5px 10px;
        }
        .title {
            font-size: 1.1em;
        }
        .introduce {
            color: #8d8d8d;
        }
        /*推荐*/
        .tuijian {
            display: flex;
            flex-direction: column;
        }
        .tuijian > span {
            padding: 10px;
        }
        .tuijian .item {
            display: flex;
            padding: 10px;
            border-bottom: 1px solid #e1e1e1;
        }
        .item img {
            width: 40vw;
            height: 24vw;
        }
        .item .right {
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            width: 55vw;
        }
        .item .right span {
            padding: 0 5px;
        }
        .item .introduce {
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
            display: inline-block;
        }
        .read {
            color: #8d8d8d;
            font-size: 0.8em;
        }
        /*标签*/
        /*.tag {*/
            /*display: flex;*/
            /*line-height: 34px;*/
            /*justify-content: space-between;*/
            /*margin-bottom: 15px;*/
        /*}*/
        ul {
            margin-left: 10px;
        }
        ul li {
            display: inline;
            padding: 5px;
            margin-right: 5px;
            border-radius: 20%;
            background: #f7f7f7;
            font-size: 0.8em;
            color: #8d8d8d;
        }
        /*咨询*/
        .icon {
            padding: 0 10px;
            line-height: 30px;
            text-align: center;
            width: 60px;
        }
        .icon img {
            width: 30px;
        }
        .icon span {
            padding: 0;
        }
        .like {
            display: flex;
            flex-direction: row;
            justify-content: flex-end;
        }
        /*倒计时*/
        /*.time {*/
            /*margin: 0 !important;*/
        /*}*/
        /*.time span {*/
            /*padding: 5px !important;*/
        /*}*/
    </style>
@endsection
@section('content')
    <div class="video-player">
        <video width="100%" src="{{$data['video_url']}}" controls="controls" poster="{{$data['image']}}">您的浏览器不支持video标签</video>
    </div>
    <div class="content">
        <span class="title"> {{$data['name']}}</span>
        <span class="introduce">{{$data['description']}}</span>
        <div class="tag">
            <ul>
                @foreach ($data['type_id'] as $v)
                <li>{{$v}}</li>
                @endforeach
            </ul>
        </div>
        <div class="like">
            <div class="zan icon">
                @if($like == 1)
                <img src="/images/wechat/dz-active.png" alt="">
                <span>已赞</span>
                    @else
                    <img src="/images/wechat/dz-no.png" alt="">
                    <span>赞</span>
                    @endif
            </div>
            <div class="sc icon">
                @if($collect == 1)
                <img src="/images/wechat/sc-active.png" alt="">
                <span>收藏</span>
                    @else
                    <img src="/images/wechat/sc-no.png" alt="">
                    <span>收藏</span>
                    @endif
            </div>
            <div class="zx consult icon">
                <img src="/images/wechat/zixun.png" alt="">
                <span>咨询</span>
            </div>
        </div>
    </div>
{{--    <div class="tuijian">--}}
{{--        <span>推荐观看</span>--}}
{{--        <div class="item" data-id="1">--}}
{{--            <img src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1536817131892&di=69525af4aaeedefe45d3bdfc43585459&imgtype=0&src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fimages%2F20170920%2F4cd8388689ad4c9cab6ec79594cba0e1.gif" alt="">--}}
{{--            <div class="right">--}}
{{--                <span class="title">口腔结构就是开发静安寺快递费加拉斯</span>--}}
{{--                <span class="introduce">口腔结构卡萨缴费时间发阿拉丝机法拉盛框架发了斯柯达发就阿谁离开房间爱上了打飞机阿乐山大佛加啊开始打飞机卡时代峰峻</span>--}}
{{--                <span class="read">200次观看</span>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="item" data-id="1">--}}
{{--            <img src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1536819912951&di=a93f3c4a4f6710975dc49be0368bc885&imgtype=0&src=http%3A%2F%2Fhiphotos.baidu.com%2Ffeed%2Fpic%2Fitem%2F960a304e251f95cabfdde79fc3177f3e670952ea.jpg" alt="">--}}
{{--            <div class="right">--}}
{{--                <span class="title">口腔结构</span>--}}
{{--                <span class="introduce">口腔结构卡萨缴费时间发阿拉丝机法拉盛框架发了斯柯达发就阿谁离开房间爱上了打飞机阿乐山大佛加啊开始打飞机卡时代峰峻</span>--}}
{{--                <span class="read">179次观看</span>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="item" data-id="1">--}}
{{--            <img src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1536820086277&di=c3c00591278aa7b2862cbb3647734388&imgtype=0&src=http%3A%2F%2Fp0.meituan.net%2Fdpgroup%2Fd75ba972f025dde6f6f91566210947fb365156.gif" alt="">--}}
{{--            <div class="right">--}}
{{--                <span class="title">洁牙注意事项</span>--}}
{{--                <span class="introduce">讲到洗牙，相信很多人都以为洗牙是为了美白牙齿，其实洗牙的目的是将残留在牙齿上的物质洗掉，保证口腔健康，减少牙结石的形成，那么洗牙后注意事项有哪些呢</span>--}}
{{--                <span class="read">175次观看</span>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="item" data-id="1">--}}
{{--            <img src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1536817131892&di=69525af4aaeedefe45d3bdfc43585459&imgtype=0&src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fimages%2F20170920%2F4cd8388689ad4c9cab6ec79594cba0e1.gif" alt="">--}}
{{--            <div class="right">--}}
{{--                <span class="title">口腔结构就是开发静安寺快递费加拉斯</span>--}}
{{--                <span class="introduce">口腔结构卡萨缴费时间发阿拉丝机法拉盛框架发了斯柯达发就阿谁离开房间爱上了打飞机阿乐山大佛加啊开始打飞机卡时代峰峻</span>--}}
{{--                <span class="read">200次观看</span>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="item" data-id="1">--}}
{{--            <img src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1536819912951&di=a93f3c4a4f6710975dc49be0368bc885&imgtype=0&src=http%3A%2F%2Fhiphotos.baidu.com%2Ffeed%2Fpic%2Fitem%2F960a304e251f95cabfdde79fc3177f3e670952ea.jpg" alt="">--}}
{{--            <div class="right">--}}
{{--                <span class="title">口腔结构</span>--}}
{{--                <span class="introduce">口腔结构卡萨缴费时间发阿拉丝机法拉盛框架发了斯柯达发就阿谁离开房间爱上了打飞机阿乐山大佛加啊开始打飞机卡时代峰峻</span>--}}
{{--                <span class="read">179次观看</span>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="item" data-id="1">--}}
{{--            <img src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1536820086277&di=c3c00591278aa7b2862cbb3647734388&imgtype=0&src=http%3A%2F%2Fp0.meituan.net%2Fdpgroup%2Fd75ba972f025dde6f6f91566210947fb365156.gif" alt="">--}}
{{--            <div class="right">--}}
{{--                <span class="title">洁牙注意事项</span>--}}
{{--                <span class="introduce">讲到洗牙，相信很多人都以为洗牙是为了美白牙齿，其实洗牙的目的是将残留在牙齿上的物质洗掉，保证口腔健康，减少牙结石的形成，那么洗牙后注意事项有哪些呢</span>--}}
{{--                <span class="read">175次观看</span>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <input type="hidden" id="id" value="{{$user['openId']}}">
    <input type="hidden" id="name" value="{{$user['name']}}">
    <input type="hidden" id="avatar" value="{{$user['avatar']}}">
    <input type="hidden" id="source_id" value="{{$data['_id']}}">
    <input type="hidden" id="like" value="{{$like}}">
    <input type="hidden" id="collect" value="{{$collect}}">
@endsection

@section('jscontent')
    <script>
        var video = function () {
            init.likeFn();
            init.detailFn();
            init.consultFn();
        }
        var init = {
            detailFn: function () {
                $('.item').on('click',function () {
                    var id = $(this).data('id');
                    window.location.href = '/wechat/edu/video?id=' + id;
                })
            },
            likeFn: function () {
                $('.zan').on('click',function () {
                    var status = $('#like').val();
                    var data       = {};
                    var that = $(this);
                    data.openId    = $('#id').val();
                    data.source_id = $('#source_id').val();
                    data.type      = 2;
                    data.status    = status;
                    data.status_type = 1;
                    var option = {
                        url: '/wechat/like',
                        method: 'post',
                        data: {data:data},
                        headers:{
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    };
                    switch (status){
                        case '0':
                            layer.open({
                                content: "已赞",
                                time: 1,
                                shade: false,
                                type: 1,
                                style: 'border:none; background-color:#8d8d8d; color:#fff;padding:8px;border-radius: 3.5px;opacity:0.8',
                            });
                            $(this).find('img').attr('src','/images/wechat/dz-active.png').addClass('active');
                            $(this).find('span').text('已赞');
                            break;
                        case '1':
                            layer.open({
                                content: "已取消赞",
                                time: 1,
                                shade: false,
                                type: 1,
                                style: 'border:none; background-color:#8d8d8d; color:#fff;padding:8px;border-radius: 3.5px;opacity:0.8',
                            });
                            $(this).find('img').attr('src','/images/wechat/dz-no.png').removeClass('active');
                            $(this).find('span').text('赞');
                            break;
                    }
                    $.ajax(option).done(function (res) {
                        if (res.status == 0) {
                            $('#like').val(res.data);
                        }
                    })
                })
                $('.sc').on('click',function () {
                    var status = $('#collect').val();
                    var data       = {};
                    var that       = $(this);
                    data.openId    = $('#id').val();
                    data.source_id = $('#source_id').val();
                    data.type      = 2;
                    data.status    = status;
                    data.status_type = 2;
                    var option = {
                        url: '/wechat/like',
                        method: 'post',
                        data: {data:data},
                        headers:{
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    };
                    switch (status) {
                        case '0':
                            layer.open({
                                content: "已收藏",
                                time: 2,
                                shade: false,
                                type: 1,
                                style: 'border:none; background-color:#8d8d8d; color:#fff;padding:8px;border-radius: 3.5px;opacity:0.8',
                            });
                            $(this).find('img').attr('src','/images/wechat/sc-active.png').addClass('active');
                            break;
                        case '1':
                            layer.open({
                                content: "已取消收藏",
                                time: 2,
                                shade: false,
                                type: 1,
                                style: 'border:none; background-color:#8d8d8d; color:#fff;padding:8px;border-radius: 3.5px;opacity:0.8',
                            });
                            $(this).find('img').attr('src','/images/wechat/sc-no.png').removeClass('active');
                            break;
                    }
                    $.ajax(option).done(function (res) {
                        if (res.status == 0) {
                            $('#collect').val(res.data);
                        }
                    })

                })
            },
            consultFn: function() {
                $('.consult').on('click',function () {
                    var id     = $('#id').val();
                    var name   = $('#name').val();
                    var avatar = $('#avatar').val();
//                    window.location.href = 'http://test.c.aiyizhuyi.com/wechat/chat?openId='+id+'&nickname='+name+'&headimgurl='+avatar;
                    window.location.href = '/wechat/counseling';
                })
            }
        };
        video();
    </script>
@endsection