@extends('wxbase')
@section('head')
    <title>视频科普</title>
    <link rel="stylesheet" href="/css/wechat/edu-common.css">
    <link rel="stylesheet" href="/css/wechat/edu-index.css">
    <style>
        /*.content {*/
            /*padding: 0;*/
        /*}*/
        /*.type {*/
            /*padding: 10px 10px 0 10px;*/
        /*}*/
        .item-list {
            /*background: #f5f5f5;*/
        }
        .type-item {
            padding: 0;
        }
        .type-2 .top-title {
            padding: 0 10px;
            /*background: #f9f9f9;*/
        }
        .item-content{
            padding: 0 10px;
        }
        .line {
            /*height: 8px;*/
            /*width: 100%;*/
            /*background: #f1f1f1;*/
        }
    </style>
@endsection
@section('content')
    <div class="top">
        <div class="search">
            <img src="/images/wechat/search.png" alt=""/>
            <input type="search" placeholder="请输入关键词">
        </div>
        <span class="cancel display-none">取消</span>
    </div>
    <div class="content">
        <div class="type">
            <span class="type-name" data-type="1">学术文献</span>
            <span class="type-name active" data-type="2">教学视频</span>
            <span class="type-name" data-type="3">历史文章</span>
        </div>
        <div class="type-content">
            <div class="type-2 type-item" >
                <div class="item-list">
                    {{--热门--}}
                    <div class="top-title">
                        <span>{{$text}}</span>
                        <span class="more" data-type="all">更多 <img src="/images/wechat/more.png" alt=""></span>
                    </div>
                    @if(count($video) == 0)
                    <span class="no-data">
                    <img src="/images/wechat/no-data.png" alt="">
                    <span>没有数据哟(∩_∩)</span>
                    </span>
                    @endif
                    <div class="item-content">
                        @foreach ($video as $v)
                        <div class="item" data-id="{{$v['_id']}}">
                            <img src="{{$v['image']}}" alt="">
                            <span class="title">{{$v['name']}}</span>
                        </div>
                        @endforeach
                        {{--<div class="item" data-id="{{$video[1]['_id']}}">--}}
                            {{--<img src="{{$video[1]['image']}}" alt="">--}}
                            {{--<span class="title">{{$video[1]['name']}}</span>--}}
                        {{--</div>--}}
                        {{--<div class="item" data-id="{{$video[2]['_id']}}">--}}
                            {{--<img src="{{$video[2]['image']}}" alt="">--}}
                            {{--<span class="title">{{$video[2]['name']}}</span>--}}
                        {{--</div>--}}
                        {{--<div class="item" data-id="{{$video[3]['_id']}}">--}}
                            {{--<img src="{{$video[3]['image']}}" alt="">--}}
                            {{--<span class="title">{{$video[3]['name']}}</span>--}}
                        {{--</div>--}}
                    </div>
                </div>
{{--                <div class="line"></div>--}}
{{--                <div class="item-list">--}}
{{--                    <div class="top-title">--}}
{{--                        <span>猜你想看</span>--}}
{{--                        <span class="more" data-type="1">更多 <img src="/images/wechat/more.png" alt=""></span>--}}
{{--                    </div>--}}
{{--                    <div class="item-content">--}}
{{--                        <div class="item" data-id="{{$all[3]['_id']}}">--}}
{{--                            <img src="{{$all[3]['image']}}" alt="">--}}
{{--                            <span class="title">{{$all[3]['name']}}</span>--}}
{{--                        </div>--}}
{{--                        <div class="item" data-id="{{$all[0]['_id']}}">--}}
{{--                            <img src="{{$all[0]['image']}}" alt="">--}}
{{--                            <span class="title">{{$all[0]['name']}}</span>--}}
{{--                        </div>--}}
{{--                        <div class="item" data-id="{{$all[1]['_id']}}">--}}
{{--                            <img src="{{$all[2]['image']}}" alt="">--}}
{{--                            <span class="title">{{$all[1]['name']}}</span>--}}
{{--                        </div>--}}
{{--                        <div class="item" data-id="{{$all[2]['_id']}}">--}}
{{--                            <img src="{{$all[2]['image']}}" alt="">--}}
{{--                            <span class="title">{{$all[2]['name']}}</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="line"></div>--}}
{{--                <div class="item-list">--}}
{{--                    --}}{{--儿童区--}}
{{--                    <div class="top-title">--}}
{{--                        <span>儿童专区</span>--}}
{{--                        <span class="more" data-type="2">更多 <img src="/images/wechat/more.png" alt=""></span>--}}
{{--                    </div>--}}
{{--                    <div class="item-content">--}}
{{--                        <div class="item" data-id="{{$all[0]['_id']}}">--}}
{{--                            <img src="{{$all[0]['image']}}" alt="">--}}
{{--                            <span class="title">{{$all[0]['name']}}</span>--}}
{{--                        </div>--}}
{{--                        <div class="item" data-id="{{$all[1]['_id']}}">--}}
{{--                            <img src="{{$all[1]['image']}}" alt="">--}}
{{--                            <span class="title">{{$all[1]['name']}}</span>--}}
{{--                        </div>--}}
{{--                        <div class="item" data-id="{{$all[2]['_id']}}">--}}
{{--                            <img src="{{$all[2]['image']}}" alt="">--}}
{{--                            <span class="title">{{$all[3]['name']}}</span>--}}
{{--                        </div>--}}
{{--                        <div class="item" data-id="{{$all[3]['_id']}}">--}}
{{--                            <img src="{{$all[3]['image']}}" alt="">--}}
{{--                            <span class="title">{{$all[3]['name']}}</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="line"></div>--}}
{{--                <div class="item-list">--}}
{{--                    --}}{{--老人区--}}
{{--                    <div class="top-title">--}}
{{--                        <span>老人专区</span>--}}
{{--                        <span class="more" data-type="3">更多 <img src="/images/wechat/more.png" alt=""></span>--}}
{{--                    </div>--}}
{{--                    <div class="item-content">--}}
{{--                        <div class="item" data-id="{{$all[0]['_id']}}">--}}
{{--                            <img src="{{$all[0]['image']}}" alt="">--}}
{{--                            <span class="title">{{$all[0]['name']}}</span>--}}
{{--                        </div>--}}
{{--                        <div class="item" data-id="{{$all[1]['_id']}}">--}}
{{--                            <img src="{{$all[1]['image']}}" alt="">--}}
{{--                            <span class="title">{{$all[1]['name']}}</span>--}}
{{--                        </div>--}}
{{--                        <div class="item" data-id="{{$all[2]['_id']}}">--}}
{{--                            <img src="{{$all[2]['image']}}" alt="">--}}
{{--                            <span class="title">{{$all[2]['name']}}</span>--}}
{{--                        </div>--}}
{{--                        <div class="item" data-id="{{$all[3]['_id']}}">--}}
{{--                            <img src="{{$all[3]['image']}}" alt="">--}}
{{--                            <span class="title">{{$all[3]['name']}}</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>
    <div class="before-search before-search2 display-none">
        <span class="rm">热门搜索</span>
        <div class="rm-list">
            @foreach ($label1 as $v)
            <span class="rm-tag" data-id="{{$v['id']}}">{{$v['name']}}</span>
            @endforeach
        </div>
    </div>
    <input type="hidden" id="type" value="2">
@endsection

@section('jscontent')
    {{--<script src="/js/wechat/edu-index.js"></script>--}}
    <script>
        var video = function() {
            init.searchFn();
            init.detailFn();
            init.tabFn();
            init.moreFn();
        };
        var init = {
            searchFn: function () {
                //点击输入框显示
                $('.search input').on('focus',function() {
                    $('.top .cancel').removeClass('display-none');
                    $('.search').addClass('search-a');
                    var type = $('#type').val();
                    switch(type) {
                        case '1':
                            $('.before-search1').removeClass('display-none');
                            break;
                        case '2':
                            $('.before-search2').removeClass('display-none');
                            break;
                    }
                    $('.content').addClass('display-none');
                })
                //点击取消显示
                $('.cancel').on('click',function () {
                    var type = $('#type').val();
                    switch(type) {
                        case '1':
                            window.location.href = '/wechat/education';
                            break;
                        case '2':
                            window.location.href = '/wechat/edu/video/list'
                            break;
                        case '3':
                            window.location.href = 'https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzIzMzAwMTU1MQ==&scene=124#wechat_redirect';
                            break;
                    }
                })
                //搜索内容 获取键盘搜索按钮事件
                $(".search input").on('keypress', function(e) {
                    var keycode = e.keyCode;
                    //获取搜索框的值
                    var searchText = $(this).val();
                    if (keycode == '13') {
                        e.preventDefault();
                        //请求搜索接口
                        if (searchText == '') {
                            alert('请输入检索内容！');
                        } else {
                            var type = $('#type').val();
                            var data = {};
                            var search = $('.search input').val();
                            window.location.href = '/wechat/edu/video/search?type='+type+'&search='+search;
                        }
                    }
                });
                //热门标签搜索
                $('.before-search .rm-tag').on('click', function (e) {
                    var id = $(this).data('id');
                    var type = $('#type').val();
                    var data = {};
                    data.id = id;
                    data.type = type;
                    window.location.href = '/wechat/edu/video/search?type='+type+'&id='+id;
                })
            },
            moreFn: function () {
                $('.top-title .more').on('click',function () {
                    var type = $('#type').val();
                        window.location.href = '/wechat/edu/all?type= ' + type;
                })
            },
            tabFn: function () {
                $('.type .type-name').on('click', function () {
                    var type = $(this).data('type');
                    $('#type').val(type);
                    $(this).addClass('active');
                    $(this).siblings().removeClass('active');
                    switch (type) {
                        case 1:
                            window.location.href = '/wechat/education';
                            break;
                        case 2:
                            window.location.href = '/wechat/edu/video/list';
                            break;
                        case 3:
                            window.location.href = 'https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzIzMzAwMTU1MQ==&scene=124#wechat_redirect';
                            break;
                    }
                })
            },
            detailFn: function() {
                //视频详情
                $('body').on('click','.type-2 .item,.type-3 .item',function () {
                    var id = $(this).data('id');
                    window.location.href = '/wechat/edu/video?id= ' + id;
                })
            }
        }
        video();
    </script>
@endsection