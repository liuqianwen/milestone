@extends('wxbase')
@section('head')
    <title>文章详情</title>
    <style>
        body {
            cursor: pointer;
        }
        .content {
            padding: 10px 15px;
            margin-bottom: 50px;
        }
        .name{
            padding:10px 10px 0 10px;
            font-size: 24px;
            font-weight: 700;
            display: inline-block;
            margin-top:10px;
        }
        .content img{
            display: inline-block;
            max-width:100%;
	    height:auto;
        }
        .description {
            border-left: 8px solid #d0e5f2;
            padding:5px 10px;
            line-height: 1.4;
            font-size: 100%;
            background: #f1f1f1;
            color: #8d8d8d;
            margin:10px;
        }
        .bottom {
            position: fixed;
            bottom: 50px;
            right: 10px;
            background: #fff;
            border: 1px solid #eee;
            border-radius: 3.5px;
            display: flex;
            flex-direction: column;
            text-align: left;
        }
        .bottom img {
            width: 30px;
        }
        .bottom > div {
            padding: 10px ;
        }
        /*倒计时*/
        .time {
            text-align: right;
            margin-bottom: 15px;
            position: fixed;
            top: 10px;
            right: 10px;
            background: #fff;
        }
        .time img {
            width: 24px;
        }
        .time span {
            line-height: 24px;
            display: inline-block;
        }
    </style>
@endsection
@section('content')
    <span class="name">{{$data['name']}}</span>
    <div class="description">
        {{$data['description']}}
    </div>
    <div class="content">
        {!!$data['content']!!}
    </div>
    {{--<div class="bottom">--}}
        {{--@if($like)--}}
        {{--<div class="zan">--}}
            {{--<img src="/images/wechat/dz-active.png"  alt="" class="active">--}}
            {{--<span>已赞</span>--}}
        {{--</div>--}}
            {{--@else--}}
            {{--<div class="zan">--}}
                {{--<img src="/images/wechat/dz-no.png" alt="">--}}
                {{--<span>赞</span>--}}
            {{--</div>--}}
        {{--@endif--}}
        {{--@if($collect)--}}
        {{--<div class="sc">--}}
            {{--<img src="/images/wechat/sc-active.png" alt="" class="active">--}}
            {{--<span>收藏</span>--}}
        {{--</div>--}}
            {{--@else--}}
            {{--<div class="sc">--}}
                {{--<img src="/images/wechat/sc-no.png" alt="">--}}
                {{--<span>收藏</span>--}}
            {{--</div>--}}
            {{--@endif--}}
        {{--<div class="consult">--}}
            {{--<img src="/images/wechat/zixun.png" alt="">--}}
            {{--<span>咨询</span>--}}
        {{--</div>--}}
    {{--</div>--}}
    <input type="hidden" id="id" value="{{$user['openId']}}">
    <input type="hidden" id="name" value="{{$user['name']}}">
    <input type="hidden" id="avatar" value="{{$user['avatar']}}">
    <input type="hidden" id="source_id" value="{{$data['_id']}}">
    <input type="hidden" id="like" value="{{$like}}">
    <input type="hidden" id="collect" value="{{$collect}}">
@endsection

@section('jscontent')
    <script src="http://res2.wx.qq.com/open/js/jweixin-1.4.0.js"></script>
    <script>
        wx.config(<?php echo $js; ?>);
        wx.error(function(res) {
            console.log(res);
        })
        wx.ready(function () {   //需在用户可能点击分享按钮前就先调用
            var id = $('#source_id').val();
            var openId = $('#id').val();
            wx.onMenuShareAppMessage({
                title: 'aa', // 分享标题
                desc: '测试分享', // 分享描述
                link: 'http://test.8020.aiyizhuyi.com/wechat/edu/article?id='+id + '&member='+openId, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
                imgUrl: 'http://test.8020.aiyizhuyi.com/images/wechat/dh.png', // 分享图标
                type: '', // 分享类型,music、video或link，不填默认为link
                dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
                success: function (res) {
                    console.log(res.errMsg)
                    alert(res.errMsg)
                    // 用户点击了分享后执行的回调函数
                }
            });
//            wx.updateAppMessageShareData({
//                title: 'aa', // 分享标题
//                desc: '分享内容', // 分享描述
//                link: 'http://test.8020.aiyizhuyi.com/wechat/education', // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
//                imgUrl: 'http://test.8020.aiyizhuyi.com/images/wechat/dh.png', // 分享图标
//            }, function(res) {
//                //这里是回调函数
////                console.log(res)
////                alert(123)
//            });
        });
        var article = function() {
            init.setCssFn();
            init.cosultFn();
            init.likeFn();
            init.shareFn();
        }
        var init =  {
            setCssFn: function() {
                $('blockquote:eq(0)').css({
                    'border-left': '8px solid #d0e5f2',
                    'padding': '5px 10px',
                    'margin':'10px 0',
                    'line-height': '1.4',
                    'font-size': '100%',
                    'background-color':'#f1f1f1',
                    'color':'#8d8d8d'
                })
            },
            cosultFn: function () {
                $('.consult').on('click',function () {
//                    var id     = $('#id').val();
//                    var name   = $('#name').val();
//                    var avatar = $('#avatar').val();
//                    window.location.href = 'http://test.c.aiyizhuyi.com/wechat/chat?openId='+id+'&nickname='+name+'&headimgurl='+avatar;
                    window.location.href = '/wechat/counseling';
                })
            },
            likeFn: function () {
                $('.zan').on('click',function () {
                    var status = $('#like').val();
                    console.log(status);
                    var data       = {};
                    var that = $(this);
                    data.openId    = $('#id').val();
                    data.source_id = $('#source_id').val();
                    data.type      = 1;
                    data.status    = status;
                    data.status_type = 1;
                    var option = {
                        url: '/wechat/like',
                        method: 'post',
                        data: {data:data},
                        headers:{
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    };
                    switch (status){
                        case '0':
                            layer.open({
                                content: "已赞",
                                time: 1,
                                shade: false,
                                type: 1,
                                style: 'border:none; background-color:#8d8d8d; color:#fff;padding:8px;border-radius: 3.5px;opacity:0.8',
                            });
                            $(this).find('img').attr('src','/images/wechat/dz-active.png').addClass('active');
                            $(this).find('span').text('已赞');
                            break;
                        case '1':
                            layer.open({
                                content: "已取消赞",
                                time: 1,
                                shade: false,
                                type: 1,
                                style: 'border:none; background-color:#8d8d8d; color:#fff;padding:8px;border-radius: 3.5px;opacity:0.8',
                            });
                            $(this).find('img').attr('src','/images/wechat/dz-no.png').removeClass('active');
                            $(this).find('span').text('赞');
                            break;
                    }
                    $.ajax(option).done(function (res) {
                        if (res.status == 0) {
                            $('#like').val(res.data);
                        }
                    })
                })
                $('.sc').on('click',function () {
                    var status = $('#collect').val();
                    var data       = {};
                    var that       = $(this);
                    data.openId    = $('#id').val();
                    data.source_id = $('#source_id').val();
                    data.type      = 1;
                    data.status    = status;
                    data.status_type = 2;
                    var option = {
                        url: '/wechat/like',
                        method: 'post',
                        data: {data:data},
                        headers:{
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    };
                    switch (status) {
                        case '0':
                            layer.open({
                                content: "已收藏",
                                time: 1,
                                shade: false,
                                type: 1,
                                style: 'border:none; background-color:#8d8d8d; color:#fff;padding:8px;border-radius: 3.5px;opacity:0.8',
                            });
                            $(this).find('img').attr('src','/images/wechat/sc-active.png').addClass('active');
                            break;
                        case '1':
                            layer.open({
                                content: "已取消收藏",
                                time: 1,
                                shade: false,
                                type: 1,
                                style: 'border:none; background-color:#8d8d8d; color:#fff;padding:8px;border-radius: 3.5px;opacity:0.8',
                            });
                            $(this).find('img').attr('src','/images/wechat/sc-no.png').removeClass('active');
                            break;
                    }
                    $.ajax(option).done(function (res) {
                        if (res.status == 0) {
                            $('#collect').val(res.data);
                        }
                    })

                })
            },
            shareFn: function () {
                $('body').on('click','.share',function (e) {

                })
            }
        }
        article();
    </script>
@endsection
