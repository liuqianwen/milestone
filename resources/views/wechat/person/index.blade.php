@extends('wxbase')
@section('head')
    <title>个人中心</title>
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/layui-v2.2.3/layui/css/layui.css">
    <style>
        .head{
            height: 200px;
            overflow-y: hidden;
            background-image: url('https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537349366802&di=80804bc15920ed3947cffd0bd509774a&imgtype=0&src=http%3A%2F%2Fimages.zhulang.com%2Fface%2Fusers%2F35%2F746%2F35746860_1499312828.jpg');
            text-align: center;
            background-repeat: no-repeat;
            background-size: 100%;
        }
        .headimg{
            line-height: 150px;
            text-align: center;

        }
        .headimg img{
            width: 80px;
            height: 80px;
            border-radius: 50%;
            box-shadow: 0px 0px 2px 3px rgb(245,188,203);
        }
        .nickname{
            margin-top: -15px;
            color: #ffffff;
            font-size: 20px;
        }
        .prefect{
            height: 70px;
            text-align: center;
        }
        .grey{
            clear: both;
            height: 10px;
            background: #eeeeee;

        }
        .button{
            width: 120px;
            height: 40px;
            border:1px solid #6FCCF4;
            color: #6FCCF4;
            border-radius: 5px;
            line-height: 40px;
            margin-top: 30px;
            margin-left: calc(50% - 60px);
        }
        .services{
            padding: 20px;
        }
        .services .title{
            font-size: 16px;
            color: black;
        }
        .services .box{
            width: calc(50% - 5px);
            height: 80px;
            float: left;
            margin-bottom: 20px;
            border-radius: 5px;
        }
        .services .box:first-child{
            background: rgb(190,219,249);
            margin-right: 10px;
        }
        .services .box:nth-child(2){
            background: rgb(192,233,219);

        }
        .box .name{
            padding: 28px 5px 0 10px;
            width: calc(100% - 70px);
            float: left;
        }
        .icons{
            width: 50px;
            height: 50px;
            float: right;
            margin-top: 15px;
            margin-right: 20px;
            border-radius: 50%;
            text-align: center;
            line-height: 50px;
        }
        .icons img{
            width: 24px;
        }
        .box .name p:first-child{
            font-size: 16px;
        }
        /*.box .name p:last-child{*/
            /*color: #8b95ab;*/
        /*}*/

        .item {
            display: flex;
            margin-bottom: 10px;
        }
        .item img {
            width: 30vw;
            height: 20vw;
            display: inline-block;
            -webkit-border-radius: 3.5px;
            -moz-border-radius: 3.5px;
            border-radius: 3.5px;
        }
        .knowledge{
            padding: 20px;
        }
        .knowledge span:first-child{
            font-size: 16px;
            float: left;
        }
        .knowledge span:nth-child(2){
            color: #999;
            float: right;
        }
        .article{
            width: 100%;
            border-top: 1px solid #eeeeee;
            padding: 10px;
            height:100px;
        }
        .article .left{
            width: 80px;
            height: 80px;
            float: left;
        }
        .left img{
            width: 100%;
            height: 100%;
            border-radius: 50%;
        }
        .article .right{
            width: calc(100% - 90px);
            float: right;
            /*margin-top: 20px;*/
        }
        .article .right .title{
            font-size: 16px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .article .right .introduce{
            word-break: break-all;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            -webkit-line-clamp: 2;
            overflow: hidden;
        }
        .vip{
            height: 140px;
        }
        /*.vip p:first-child{*/
            /*font-size: 16px;*/
            /*padding-left: 20px;*/
            /*margin-top: 10px;*/
            /*padding-bottom: 5px;*/
            /*border-bottom: 1px solid #ddd;*/

        /*}*/
        .vip .score{
            width: 120px;
            text-align: center;
            float: left;
            border-right: 1px solid #ddd;
        }
        .vip .sign_in{
            width: calc(100% - 130px);
            float: right;
            margin-right: 10px;
        }
        .showScore p{
            margin-top: 10px;
        }
        .showScore p span{
            color:orange;
        }
        .sign_in{
            text-align: center;
        }
        .qiandao{
            width: 100%;
            display:flex;
            flex-direction:row;
        }
        .qiandao div{
            flex: 1;
        }
        .qiandao div img{
            width: 40px;
        }
        .xin{
            position: relative;
            margin-top: 15px;

        }
        .zhi{
            position: absolute;
            left: calc((100vw - 130px)/5/2 - 7px);
            top: 10px
        }

        .vip .text:first-child{
            font-size: 16px;
            float: left;
            margin-left: 20px;
            width: calc(50% - 20px);
            border-bottom: 1px solid #eeeeee;
            display: block;
            height: 30px;
            margin-top: 10px;
        }
        .vip .text:nth-child(2){
            color: #999;
            float: right;
            margin-right: 20px;
            width: calc(50% - 20px);
            border-bottom: 1px solid #eeeeee;
            display: block;
            height: 30px;
            margin-top: 10px;
            text-align: right;

        }
        .code{
            /*margin: auto;*/
            text-align: center;
        }
        .code .code_span{
            width: 120px;
            background: #ffffff;
            position: absolute;
            right:10px;
        }
        .to_right {
            width: 0;
            height: 0;
            border-left: 10px solid #ffffff;
            border-top: 10px solid transparent;
            border-bottom: 10px solid transparent;
            position: absolute;
            right: 0;
        }

    </style>
@endsection
@section('content')
    <div class="content">
        <div class="head">
            <div class="headimg">
                <img src="{{$member['headimgurl']}}" alt="">
            </div>
            <div class="nickname">{{$member['nickname']}}</div>
            <div class="code">
                <div class="code_span">会员码：{{$member['card']}}</div>
                <div class="to_right"></div>
            </div>
            <div class=""></div>
        </div>
        <div class="prefect">
            @if($member['name'])
                <div class="button">编辑资料</div>
                @else
                <div class="button">完善资料</div>
                @endif

        </div>
        <div class="grey"></div>
        <div class="vip">
            <span class="text">我的积分</span>
            <span class="text record">更多积分记录</span>
            <div style="clear: both;"></div>
            <div class="score">
                <div class="showScore">
                    <img src="/images/wechat/huangguan.png" alt="">
                    <p>当前积分:<span>{{$num}}</span></p>
                </div>
            </div>
            <div class="sign_in">
                <div class="qiandao">
                    <div class="xin">
                        <img src="/images/wechat/xinxing1.png" class="images" alt="">
                        <span class="zhi">点</span>
                    </div>
                    <div class="xin">
                        <img src="/images/wechat/xinxing2.png" alt="">
                        <span class="zhi">我</span>
                    </div>
                    <div class="xin">
                        <img src="/images/wechat/xinxing3.png" alt="">
                        <span class="zhi">有</span>
                    </div>
                    <div class="xin">
                        <img src="/images/wechat/xinxing4.png" alt="">
                        <span class="zhi">惊</span>
                    </div>
                    <div class="xin">
                        <img src="/images/wechat/xinxing5.png" alt="">
                        <span class="zhi">喜</span>
                    </div>
                </div>
                <p style="text-align: center;margin-top: 10px;color:#999">点击此处签到</p>
            </div>
        </div>
        <div class="grey"></div>
        <div class="services">
            <p class="title">推荐服务</p>
            <div class="serve">
                <div class="box survey">
                    <div class="name">
                        <p>口腔自测</p>
                        {{--<p>预防口腔必看</p>--}}
                    </div>
                    <div class="icons" style="background: rgb(87,147,221)">
                        <img src="/images/wechat/survey.png" alt="">
                    </div>
                </div>
                <div class="box donation">
                    <div class="name">
                        <p>我的公益</p>
                        {{--<p>查看我的公益记录</p>--}}
                    </div>
                    <div class="icons" style="background:rgb(70,200,152) ">
                        <img src="/images/wechat/volunteer.png" alt="">
                    </div>
                </div>
                <div class="box shop" style="background: rgb(251,201,205);margin-right: 10px;">
                    <div class="name">
                        <p>健康商城</p>
                        {{--<p>口腔健康用品</p>--}}
                    </div>
                    <div class="icons" style="background:rgb(243,106,116) ">
                        <img src="/images/wechat/shop.png" alt="">
                    </div>
                </div>
                <div class="box" style="background: rgb(250,223,207)">
                    <div class="name">
                        <p>我的收藏</p>
                        {{--<p>个人收藏用品</p>--}}
                    </div>
                    <div class="icons" style="background:rgb(248,158,93) ">
                        <img src="/images/wechat/collect.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="grey"></div>
        <div class="knowledge">
            <span>阅读记录</span>
            <span>查看更多记录</span>
            <div style="clear: both;"></div>
            @foreach($data as $v)
            <a href="/wechat/edu/article?id={{$v->_id}}">
                <div class="item">
                    <div class="article">
                        <div class="left">
                            <img src="{{$v->image}}" alt="">
                        </div>
                        <div class="right">
                            <p class="title">{{$v->name}}</p>
                            <p class="introduce">{{$v->description}}</p>
                        </div>
                    </div>
                </div>
            </a>
            @endforeach
        </div>

    </div>
@endsection
@section('jscontent')
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/layui-v2.2.3/layui/layui.all.js"></script>
    <script>
        $('.shop').click(function(){
            window.location.href = '/wechat/load';
        })
        $('.prefect .button').click(function(){
            window.location.href = '/wechat/person/information';
        })
        $('.sign_in').click(function(){
            window.location.href = '/wechat/sign'
        })
        $('.record').click(function(){
            window.location.href = '/wechat/record';
        })
        $('.donation').click(function(){
            window.location.href = '/wechat/donation';
        })
        $('.survey').click(function(){
            window.location.href = '/wechat/survey/list';
        })

    </script>
@endsection