@extends('wxbase')
@section('head')
    <title>个人信息</title>

    <style>
        *{margin:0;padding:0;}
        .grey{
            background: rgb(239,239,239);
            color:rgb(179,179,179);
            height: 40px;
            line-height: 40px;
            padding-left: 20px;
            margin-bottom: 0;
        }
        .info{
            width: 100%;
        }
        ul{
            list-style:none;

        }
        ul li{
            height: 50px;
            border-bottom: 1px solid #EEEEEE;
            padding-left: 25px;
            line-height: 50px;
        }
        li span{
            width: 5em;
            display: inline-block;
        }
        li input,li select{
            padding-left: 5px;
        }
        li input,li select{
            height: 40px;
            width: calc(100% - 5em - 30px);
            border: none;
            outline:none;
            color:rgb(251,137,25) ;
            background:#fff;
            -webkit-appearance: none;

        }
        input::input-placeholder{
            color:rgb(251,137,25) ;
        }
        input::-webkit-input-placeholder{
            color:rgb(251,137,25) ;
        }
        input::-moz-placeholder{
            color:rgb(251,137,25) ;
        }
        input:-ms-input-placeholder{
            color:rgb(251,137,25) ;
        }
        .maladylabel{
            margin-bottom:10px;
            border:1px solid #e9eef1;
            float:left;
            margin-right: 30px;
            text-align:center;
            line-height: 25px;
            color:grey;
            border-radius: 10px;
            padding:3px 5px 3px 5px;
        }
        .tag{
            padding: 30px 0 30px 30px;
        }
        footer{
            width: 100%;
            text-align: center;
            padding: 40px;
        }
        footer div{
            height: 40px;
            background: rgb(251,137,25);
            color: #ffffff;
            line-height: 40px;
            border-radius: 5px;
        }
    </style>
@endsection
@section('content')
    <div class="warrap">
        <div class="info">
            <p class="grey">基本资料</p>
            <ul>
                <li>
                    <span>姓名</span>
                    <input type="text" class="name" placeholder="真实姓名" value="{{$member->name}}">
                </li>
                <li>
                    <span>性别</span>
                    <select name="sex"  class="sex" id="sex">
                    <option value='' disabled selected style='display:none;'>请选择性别</option>
                    <option value="1" @if($member->sex == 1) selected @endif>男</option>
                    <option value="2" @if($member->sex == 2) selected @endif>女</option>
                    </select>
                    </span>
                </li>
                <li>
                    <span>出生日期</span>
                    <input type="text" class="birthday" placeholder="出生日期" value="{{$member->birthday}}">
                </li>
            </ul>
        </div>
        <div class="malady">
            <p class="grey">口腔情况</p>
            <div class="tag">
                <div class="maladylabel" style="">口腔溃疡</div>
                <div class="maladylabel" style="">牙周病</div>
                <div class="maladylabel" style="">智齿</div>
                <div class="maladylabel" style="">牙龈出血</div>
                <div class="maladylabel" style="">牙周病</div>
                <div class="maladylabel" style="">牙龈肿痛</div>
                <div class="maladylabel" style="">大舌头</div>
                <div class="maladylabel" style="">牙髓炎</div>
            </div>
        </div>
        <div style="clear: both;height: 20px"></div>
        <div class="interested">
            <p class="grey">感兴趣</p>
            <div class="tag">
                <div class="maladylabel" style="">洗牙</div>
                <div class="maladylabel" style="">乳牙</div>
                <div class="maladylabel" style="">美白</div>
                <div class="maladylabel" style="">镶牙</div>
                <div class="maladylabel" style="">牙套矫正</div>
                <div class="maladylabel" style="">种植牙</div>
                <div class="maladylabel" style="">补牙</div>
                <div class="maladylabel" style="">正畸</div>
            </div>
        </div>
        <div style="clear: both;height: 20px"></div>
        <footer><div class="save">保存 </div></footer>
    </div>
@endsection
@section('jscontent')
    <script>
        $(function(){
            $('.birthday').mobiscroll().date({
                lang: 'zh',
                theme: 'mobiscroll',
                headerText: false,
                display: 'bottom',
                dateFormat:'yy-mm-dd'
            });
                    $('.sex').mobiscroll().select({
            theme: 'mobiscroll',
            display: 'bottom',
            lang: 'zh'
        });
        $('#sex_dummy').remove();
        $('#sex').removeClass('mbsc-comp').removeClass('mbsc-sel-hdn');

            $('.maladylabel').click(function() {
                var label = $(this);
                if (label.hasClass('active')) {
                    label.removeClass('active');
                    label.css('background', 'white');
                    label.css('color', 'grey');
                    label.css('border-color', '#e9eef1');
                } else {
                    label.addClass('active');
                    label.css('background', 'rgb(251,137,25)');
                    label.css('border-color', 'rgb(251,137,25)');
                    label.css('color', 'white');
                }
            });

            //保存
            $('.save').click(function(){
                var obj = {};
                obj.name = $('.name').val();
                obj.sex = $('.sex').val();
                obj.birthday = $('.birthday').val();
                if(obj.name == ''){
                    layer.msg('请填写信息');
                    return false;
                }
                $.ajax({
                    type: 'POST',
                    url: '/wechat/information/save',
                    data: {data: obj},
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    success: function (m) {
                        switch (m.status) {
                            case 0:
                                    window.location.href = '/wechat/person';
                                break;
                            case 2:
                            default:
                                swal('失败', m.message, 'warning');
                        }
                    }
                });
            })
        })
    </script>
@endsection