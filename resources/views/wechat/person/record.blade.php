@extends('wxbase')
@section('head')
    <title>积分记录</title>
    <style>
        *{margin:0;padding:0}
        .header{
            background: url(/images/wechat/scoreBg.png);
            width: 100%;
            height: 100px;
            text-align: center;
            position: relative;
            box-shadow: 2px 3px 3px #ddd;
        }
        .ellipse{
            /*margin:auto;*/
            width: 180px;
            height: 110px;
            background: rgb(14,150,237);
            border-radius: 50% / 50%;
            /*margin-top: -50px;*/
            opacity: 0.6;
            position: absolute;
            top:-30px;
            left: calc(50vw - 90px);
            box-shadow: 0px 20px 20px rgb(212,250,251);
            color: #ffffff;
        }
        .ellipse p:first-child{
            margin-top: 35px;
            font-size: 30px;
            margin-bottom: 0 !important;
        }
        .chunk{
            height: 100px;
            border-bottom: 1px solid #eeeeee;
            padding: 20px 20px 0 20px;
        }
        .num{
            width: 50px;
            text-align: right;
            float: right;
            font-size: 20px;
            margin-top: 15px;
        }
        .text{
            width: calc(100% - 50px);
            float: left;
        }
        .description{
            font-size: 20px;
            font-weight: 600;
        }
        .date{
            color: #777;
        }
        .add{
            color:rgb(21,195,30) ;
        }
    </style>
@endsection
@section('content')
        <div class="content">
            <div class="header">
                <div class="ellipse">
                    <p>{{$num}}</p>
                    <p>我的积分</p>
                </div>
            </div>
            <div class="record">
                @foreach($data as $v)
                <div class="chunk">
                    <div class="text">
                        <p class="description">{{$v->description}}</p>
                        <p class="date">{{date('Y-m-d H:i:s',$v->timestamp)}}</p>
                    </div>
                    <div class="num @if($v->math == 0) add @endif">
                     {{$v->math == 0 ? '+' : '-'}}{{$v->num}}
                    </div>
                </div>
                    @endforeach
            </div>
        </div>
@endsection
@section('jscontent')
    <script>
    </script>
@endsection