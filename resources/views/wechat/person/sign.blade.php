@extends('wxbase')
@section('head')
    <title>每日签到</title>
    <style>
        *{margin:0;padding:0}
        li{list-style:none}
        img{vertical-align:top}

        /* 以上可合并 */
        .clear{clear:both}
        .clear:after{clear:both;display:table;content:''}
        .qiandao-sprits{background-image:url(http://www.17sucai.com/preview/37827/2016-02-18/%E7%AD%BE%E5%88%B0/images/qiandao_sprits.png);background-repeat:no-repeat;background-position: -2px 0px;}
        .qiandao-tran{-webkit-transition:all .3s ease-out;-moz-transition:all .3s ease-out;-o-transition:all .3s ease-out;transition:all .3s ease-out;-ms-transition:all .3s ease-out}
        .qiandao-radius{-webkit-border-radius:5px;-moz-border-radius:5px;-o-border-radius:5px;-ms-border-radius:5px}
        .qiandao-warp{height:1306px;background:url(http://www.17sucai.com/preview/37827/2016-02-18/%E7%AD%BE%E5%88%B0/images/qiandao_warp_bg.jpg) no-repeat top center}
        .qiandap-box{margin:0 auto;background: #FFBD66;}
        .qiandao-con{margin: 0 auto;
            background-image: url(/images/wechat/qiandao_con.jpg);
            background-size: 100%;
            background-repeat: no-repeat;}
        .qiandao-left{}

        .current-date{float:left;padding-top:5px;padding-left:55px;color:#b25d06;font-size:18px}
        .qiandao-history{    float: right;
            width: 92px;
            height: 30px;
            border-radius: 4px;
            background-color: #b25d06;
            color: #fff;
            text-align: center;
            font-size: 12px;
            line-height: 30px;
            cursor: pointer;
            margin: 3px 10px 0 0;}
        .qiandao-history:hover{background-color:#9c4f01}
        .qiandao-top{padding-top:20px;}
        .qiandao-bottom{
            background: #fff;
            width: 90%;
            margin: 0 auto;
            border-radius: 10px;
            height: auto;
            overflow: hidden;}
        .just-qiandao{margin:0 auto 20px;width:212px;height:67px;cursor:pointer}
        .just-qiandao.actived,.just-qiandao:active{background-position:0 -68px}
        .qiandao-notic{color:#b25d06;text-align:center;font-size:18px}
        .qiandao-rule-list{ padding:10px;color:#8d8ebb;font-size:1pc;line-height:26px}
        .qiandao-rule-list h4{font-weight:bolder;font-size:1pc}
        .qiandao-main{    width: 90%;
            margin: 0 auto;    height: auto;
            overflow: hidden;}
        .qiandao-list{margin-top:56px;margin-right:-10px}
        .qiandao-list li{position:relative;float:left;margin:0 1px 1px 0;width:39.5px;height:39.5px; background:#fffff1;font-size: 30px;
            text-align: center;
            /* line-height: 48.42px; */
            color: #980303;
            font-weight: bold;}
        .qiandao-list li.date1{background-position:-204px 0}
        .qiandao-list li.date2{background-position:-245px 0}
        .qiandao-list li.date3{background-position:0 -40px}
        .qiandao-list li.date4{background-position:-40px -40px}
        .qiandao-list li.date5{background-position:-82.5px -40px}
        .qiandao-list li.date6{background-position:-258px -86px}
        .qiandao-list li.date7{background-position:-344px -86px}
        .qiandao-list li.date8{background-position:-430px -86px}
        .qiandao-list li.date9{background-position:-516px -86px}
        .qiandao-list li.date10{background-position:0 -172px}
        .qiandao-list li.date11{background-position:-86px -172px}
        .qiandao-list li.date12{background-position:-172px -172px}
        .qiandao-list li.date13{background-position:-258px -172px}
        .qiandao-list li.date14{background-position:-344px -172px}
        .qiandao-list li.date15{background-position:-430px -172px}
        .qiandao-list li.date16{background-position:-516px -172px}
        .qiandao-list li.date17{background-position:0 -258px}
        .qiandao-list li.date18{background-position:-86px -258px}
        .qiandao-list li.date19{background-position:-172px -258px}
        .qiandao-list li.date20{background-position:-258px -258px}
        .qiandao-list li.date21{background-position:-344px -258px}
        .qiandao-list li.date22{background-position:-430px -258px}
        .qiandao-list li.date23{background-position:-516px -258px}
        .qiandao-list li.date24{background-position:0 -344px}
        .qiandao-list li.date25{background-position:-86px -344px}
        .qiandao-list li.date26{background-position:-172px -344px}
        .qiandao-list li.date27{background-position:-258px -344px}
        .qiandao-list li.date28{background-position:-344px -344px}
        .qiandao-list li.date29{background-position:-430px -344px}
        .qiandao-list li.date30{background-position:-516px -344px}
        .qiandao-list li.date31{background-position:0 -430px}
        .qiandao-list li .qiandao-icon{position:absolute;top:0;left:0;z-index:2;display:none;width:39.5px;height:39.5px;background:url(http://www.17sucai.com/preview/37827/2016-02-18/%E7%AD%BE%E5%88%B0/images/qiandao_icon.png) no-repeat center center; background-size:100%}
        .qiandao-list li.qiandao .qiandao-icon{display:block}
        .qiandao-layer{position:fixed;top:0;bottom:0;left:0;z-index:888;display:none;width:100%}
        .qiandao-layer-bg{width:100%;height:100%;background-color:#000;opacity:.55;filter:alpha(opacity=55)}
        .qiandao-layer-con{    position: fixed;
            top: 35%;
            margin-top: -100px;
            right: 0;
            left: 0;
            /* margin-top: 150px; */
            z-index: 999;
            padding-top: 30px;
            border: 3px #33b23f solid;
            border-radius: 5px;
            background-color: #fff;}
        .qiandao-history-layer .qiandao-layer-con{}
        .close-qiandao-layer{position:absolute;top:13px;right:13px;width:1pc;height:1pc;background-position:-228px -51px}
        .qiandao-history-inf{margin-top:25px;color:#666;text-align:center;font-size:14px}
        .qiandao-history-inf li{float:left;width:25%}
        .qiandao-history-inf li h4{color:#33b23f;font-size:20px;line-height:30px}
        .qiandao-history-table{overflow:scroll;min-height:100px;max-height:200px;margin:20px;-webkit-border-radius:5px 5px 0 0;-moz-border-radius:5px 5px 0 0;border-radius:5px 5px 0 0;-o-border-radius:5px 5px 0 0;-ms-border-radius:5px 5px 0 0}
        .qiandao-history-table table{width:100%;color:#666;text-align:center;font-size:1pc;border-spacing:0}
        .qiandao-history-table table th{width:33.3%;background-color:#f2f2f2;text-align:center;line-height:40px}
        .qiandao-history-table td{width:33.3%;border-bottom:1px #e5e5e5 dashed;line-height:35px;font-size: 14px}
        .qiandao-active .qiandao-layer-con{width:80%;height:200px;margin-left: 10%;}
        .yiqiandao{margin:36px 0 0 40px;color:#666;font-size:14px;line-height:38px}
        .yiqiandao .yiqiandao-icon{float:left;margin:0 25px;width:178px;height:38px;background-position:-217px 0}
        .qiandao-jiangli{position:relative;margin:45px auto;width:335px;height:170px;background-position:0 -146px}
        .qiandao-jiangli span{position:absolute;top:58px;left:50px;display:block;width:178px;height:106px;color:#ff7300;text-align:center;font-weight:bolder;font-size:30px;line-height:106px}
        .qiandao-jiangli span em{padding-left:5px;font-style:normal;font-size:1pc}
        .qiandao-share{display:block;margin:60px auto 0;width:318px;height:3pc;border-radius:5px;background-color:#4ab854;color:#fff;text-align:center;text-decoration:none;font-size:18px;line-height:3pc}
        .qiandao-share:hover{background-color:#3e9d46}
        .yiqiandao-icon-p{
            display: inline-block;
            text-align: center;
            width: 100%;
            margin-left: -40px;
        }
    </style>
@endsection
@section('content')

    <div class="mobile">
        <div >
            <div class="qiandap-box" style="padding-bottom:30px;">
                <div class="qiandao-con clear">
                    <div class="qiandao-left">
                        <div class="qiandao-left-top clear">
                            <div class="current-date">2016年1月6日</div>
                            <div class="qiandao-history qiandao-tran qiandao-radius" id="js-qiandao-history">签到统计</div>
                        </div>
                        <div class="qiandao-main" id="js-qiandao-main">
                            <ul class="qiandao-list" id="js-qiandao-list">
                            </ul>
                        </div>
                    </div>
                    <div class="qiandao-right">
                        <div class="qiandao-top">
                            <div class="just-qiandao qiandao-sprits @if($status == 1) actived @endif" id="js-just-qiandao"> </div>
                        </div>
                        <div class="qiandao-bottom">
                            <div class="qiandao-rule-list">
                                <h4>签到规则</h4>
                                <p>首次签到获得10积分</p>
                                <p>连续签到每天增加3分奖励</p>
                                <p>连续第7日、15日、30日签到，可分别获得额外奖励50、100、150积分</p>
                            </div>
                            <div class="qiandao-rule-list">
                                <h4>其他说明</h4>
                                <p>连续签到天数以自然月为周期，每月初重新计算</p>
                                <p>如中途间断，签到天数重新计算</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- 我的签到 layer start -->
        <div class="qiandao-layer qiandao-history-layer" style="display: none;">
            <div class="qiandao-layer-con qiandao-radius">
                <a href="javascript:;" class="close-qiandao-layer qiandao-sprits"></a>
                <ul class="qiandao-history-inf clear">
                    <li>
                        <p>连续签到</p>
                        <h4 class="lianxu">{{$data['lianxu']}}</h4>
                    </li>
                    <li>
                        <p>本月签到</p>
                        <h4 class="benyue">{{$data['benyue']}}</h4>
                    </li>
                    <li>
                        <p>总共签到数</p>
                        <h4 class="zong">{{$data['zong']}}</h4>
                    </li>
                    <li>
                        <p>签到累计奖励</p>
                        <h4 class="leiji">{{$data['num']}}</h4>
                    </li>
                </ul>
                <div class="qiandao-history-table">
                    <table>
                        <thead>
                        <tr>
                            <th>签到日期</th>
                            <th>奖励</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                    </table><table>
                        <tbody>
                        @foreach($record as $v)
                        <tr>
                            <td>{{date('Y-m-d H:i:s',$v->timestamp)}}</td>
                            <td>{{$v->num}}</td>
                            <td>{{$v->description}}</td>
                        </tr>
                        @endforeach
                        </tbody></table>

                </div>
            </div>
            <div class="qiandao-layer-bg"></div>
        </div>
        <!-- 我的签到 layer end -->
        <div class="qiandao-layer qiandao-active" style="display: none;">
            <div class="qiandao-layer-con qiandao-radius">
                <a href="javascript:;" class="close-qiandao-layer qiandao-sprits"></a>
                <div class="yiqiandao clear">
                    <div class="yiqiandao-icon qiandao-sprits"></div>
                    <p class="yiqiandao-icon-p">您已连续签到<span>2</span>天</p>
                </div>
                {{--<div class="qiandao-jiangli qiandao-sprits">--}}
                    {{--<span class="qiandao-jiangli-num">0.55<em>元</em></span>--}}
                {{--</div>--}}
                {{--<a href="#" class="qiandao-share qiandao-tran">分享获取双倍收益</a>--}}
            </div>
            <div class="qiandao-layer-bg"></div>
        </div>

    </div>

@endsection
@section('jscontent')
    <script>
        $(window).load(function() {
            $("#status").fadeOut();
            $("#preloader").delay(350).fadeOut("slow");
        })
    </script>
    <script type="text/javascript">
        $(function() {
            var widthUl = $('#js-qiandao-main').width();
            // li的宽度和高度
            var widthLi = (widthUl - 7)/7;

            if(widthLi < 45){
                $('#js-qiandao-list').css('marginTop','40px');
            }else if(widthLi > 45 && widthLi < 50){
                $('#js-qiandao-list').css('marginTop','50px');
            }
            var signFun = function() {

                // 已经签到的天
                var dateArray = JSON.parse("{{$day}}");

                var $dateBox = $("#js-qiandao-list"),
                        $currentDate = $(".current-date"),
                        $qiandaoBnt = $("#js-just-qiandao"),
                        _html = '',
                        _handle = true,
                        myDate = new Date();
                if("{{$status}}" == 1){
                    _handle = false;
                }
                $currentDate.text(myDate.getFullYear() + '年' + parseInt(myDate.getMonth() + 1) + '月' + myDate.getDate() + '日');

                var monthFirst = new Date(myDate.getFullYear(), parseInt(myDate.getMonth()), 1).getDay();

                var d = new Date(myDate.getFullYear(), parseInt(myDate.getMonth() + 1), 0);
                var totalDay = d.getDate(); //获取当前月的天数

                for (var i = 0; i < 42; i++) {
                    _html += ' <li style="width:'+widthLi+'px;height:'+widthLi+'px;line-height:'+widthLi+'px"><div class="qiandao-icon"></div></li>'
                }
                $dateBox.html(_html) //生成日历网格

                var $dateLi = $dateBox.find("li");

                for (var i = 0; i < totalDay; i++) {
                    $dateLi.eq(i + monthFirst).addClass("date" + parseInt(i + 1)).attr('data-num',parseInt(i + 1));
                    for (var j = 0; j < dateArray.length; j++) {
                        if (i == dateArray[j]) {
                            $dateLi.eq(i + 1).addClass("qiandao");
                        }
                    }
                } //生成当月的日历且含已签到

                for(var i = 1; i < 32; i++){
                    var liNum = $dateLi.eq(i).attr('data-num');
                    if(liNum != 'undefined' && liNum != '' && liNum != null){
                        $('.date'+liNum).append(liNum);
                    }
                } //	嵌入天

                // 没有天的li背景变色
                for(var i = 0; i < $dateLi.length; i++){
                    if(i < monthFirst || i>(totalDay+monthFirst-1)){
                        $dateLi.eq(i).css('background','#eee');
                    }
                }

                $(".date" + myDate.getDate()).addClass('able-qiandao');

                $dateBox.on("click", "li", function() {
                    if ($(this).hasClass('able-qiandao') && _handle) {
                        $(this).addClass('qiandao');
                        qiandaoFun();
                    }
                }) //签到

                $qiandaoBnt.on("click", function() {
                    if (_handle) {
                        qiandaoFun();
                    }
                }); //签到

                function qiandaoFun() {
                    $.ajax({
                        type: 'POST',
                        url: '/wechat/sign/add',
                        data: {},
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        success: function (m) {
                            switch (m.status) {
                                case 0:
                                        var str = '<tr>' +
                                                        '<td>'+ m.data['everyday']['date']+'</td>'+
                                                        '<td>'+ m.data['everyday']['num']+'</td>'+
                                                        '<td>'+ m.data['everyday']['description']+'</td>'+
                                                '</tr>';
                                        if(m.data['extra']){
                                            str +=  '<tr>' +
                                                    '<td>'+ m.data['extra']['date']+'</td>'+
                                                    '<td>'+ m.data['extra']['num']+'</td>'+
                                                    '<td>'+ m.data['extra']['description']+'</td>'+
                                                    '</tr>';
                                        }
                                        $('.qiandao-history-table tbody').append(str);
                                        $('.lianxu').text(parseInt($('.lianxu').text()) + m.tmp['lianxu']);
                                        $('.benyue').text(parseInt($('.benyue').text()) + m.tmp['benyue']);
                                        $('.zong').text(parseInt($('.zong').text()) + m.tmp['zong']);
                                        $('.leiji').text(parseInt($('.leiji').text()) + m.tmp['leiji']);
                                        $('.yiqiandao-icon-p span').text($('.lianxu').text());
                                    break;
                                case 1:
                                    window.location.href = '/wechat/sign';
                                    break;
                            }
                        }
                    });
                    $qiandaoBnt.addClass('actived');
                    openLayer("qiandao-active", qianDao);
                    _handle = false;

                }

                function qianDao() {
                    $(".date" + myDate.getDate()).addClass('qiandao');
                }
            }();

            function openLayer(a, Fun) {
                $('.' + a).fadeIn(Fun)
            } //打开弹窗

            var closeLayer = function() {
                $("body").on("click", ".close-qiandao-layer", function() {
                    $(this).parents(".qiandao-layer").fadeOut()
                })
            }() //关闭弹窗

            $("#js-qiandao-history").on("click", function() {
                openLayer("qiandao-history-layer", myFun);

                function myFun() {
                    console.log(1)
                } //打开弹窗返回函数
            })
            $('.qiandao-icon').css('width',widthLi+'px').css('height',widthLi+'px');

        })



    </script>

@endsection