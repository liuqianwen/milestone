@extends('wxbase')
@section('head')
    <title>我要捐款</title>
    <style>
        body{
            background: #f5f5f5 !important;
        }
        input{
            outline: none !important;
        }
        .order {
            height: 60px;
            line-height: 60px;
            text-align: center;
            width: 100%;
            padding: 20px;
        }
        .order .line {
            display: inline-block;
            width: calc((100% - 16em)/2);
            border-top: 1px solid #ccc ;
        }
        .order .txt {
            color: #686868;
            vertical-align: middle;
            width: 12em;
            display: inline-block;
            font-size: 16px;
            padding-top: 5px;
        }
        .checkMoney{
            display: flex;
            flex-flow: row wrap;
            padding: 20px;
            padding-bottom: 10px;
            margin-bottom: 10px;
        }
        .checkMoney .money{
            flex:1;
            margin-left: 10px;
            margin-right: 10px;
            height: 60px;
            border: 2px solid #eeeeee;
            line-height: 60px;
            text-align: center;
            border-radius: 5px;
            background: #ffffff;
        }
        .checkMoney .money span{
            color: #FFBA02;
        }
        .money img{
            display: none;
        }

        .active{
            border: 2px solid #FFBA02 !important;
            position: relative;
        }
        .active img{
            display: block;
            position: absolute;
            bottom: 0;
            right: 0;
            width: 20px;
        }
        .inputMoney{
            margin-left: 10px;
            margin-right: 10px;
            margin-top: 20px;
            height: 50px;
        }
        .anonymity,.integral{
            padding: 20px 20px 20px 30px;
            font-size: 15px;
            border-bottom: 1px solid #eeeeee;
        }
        .anonymity span,.integral span{
            margin-top: 8px;
            display: inline-block;
        }
        .integral strong{
                font-size: 13px;
            margin-left: 5px;
        }
        .integral img{
           margin-left: 5px;
            width: 16px;
        }
        .switch {
            width: 50px;
            height: 28px;
            position: relative;
            border: 1px solid #dfdfdf;
            background-color: #fdfdfd;
            box-shadow: #dfdfdf 0 0 0 0 inset;
            border-radius: 20px;
            background-clip: content-box;
            display: inline-block;
            -webkit-appearance: none;
            user-select: none;
            outline: none;
            margin-top: -10px;
        }
        .switch:before {
            content: '';
            width: 26px;
            height: 26px;
            position: absolute;
            top: 0;
            left: 0;
            border-radius: 20px;
            background-color: #fff;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
        }
        .switch:checked {
            border-color: #FFBA02;
            box-shadow: #FFBA02 0 0 0 16px inset;
            background-color: #FFBA02;
        }
        .switch:checked:before {
            left: 25px;
        }
        .switch.switch-anim {
            transition: border cubic-bezier(0, 0, 0, 1) 0.4s, box-shadow cubic-bezier(0, 0, 0, 1) 0.4s;
        }
        .switch.switch-anim:before {
            transition: left 0.3s;
        }
        .switch.switch-anim:checked {
            box-shadow: #FFBA02 0 0 0 16px inset;
            background-color: #FFBA02;
            transition: border ease 0.4s, box-shadow ease 0.4s, background-color ease 1.2s;
        }
        .switch.switch-anim:checked:before {
            transition: left 0.3s;
        }
        footer{
            width: 100%;
            text-align: center;
            margin-top: 40px;
        }
        .bottom{
            width: 80%;
            background: #FFBA02;
            height: 40px;
            line-height: 40px;
            color: #ffffff;
            margin-left: 10%;
            margin-right: 10%;
            border-radius: 10px;
        }
        .layer_notice{
            background: #ffffff;
            width: 250px;
            border-radius: 5px;
            padding: 20px;
        }
        .layer_notice .title{
            text-align: center;
            font-size: 15px;
            font-weight: 700;
            color:black;
        }
        .layer_notice p{
            font-size: 13px;
            color: #777;
            margin: 0  !important;
        }
        .layer_notice .tip{
            color: black;
            font-size: 14px;
        }
        .layer_button{
            border-top:1px solid #FFBA02;
            color: #FFBA02;
            text-align: center;
            height: 40px;
            line-height: 40px;
        }
        .layui-m-layer1 .layui-m-layerchild {
            border: none;
            border-radius: 10px !important;
        }
        .score{
          margin-top: 10px;
            width: 200px;
        }

    </style>
@endsection
@section('content')
    <div class="content">
        <div class="order">
            <span class="line"></span>
            <span class="txt">请选择您要捐款的金额</span>
            <span class="line"></span>
        </div>
        <div class="checkMoney">
            <div class="money active">
                <span>10</span>元
                <img src="/images/wechat/check.png" alt="">
            </div>
            <div class="money">
                <span>20</span>元
                <img src="/images/wechat/check.png" alt="">
            </div>
            <div class="money">
                <span>50</span>元
                <img src="/images/wechat/check.png" alt="">
            </div>
        </div>
        <div class="checkMoney">
            <div class="money">
                <span>100</span>元
                <img src="/images/wechat/check.png" alt="">
            </div>
            <div class="money">
                <span>500</span>元
                <img src="/images/wechat/check.png" alt="">
            </div>
            <div class="money">
                <span>1000</span>元
                <img src="/images/wechat/check.png" alt="">
            </div>
            <input type="number" class="inputMoney form-control" placeholder="其他金额（元）" min="0">
        </div>
        <div class="integral">
            <span>积分抵扣<strong>共{{$num}}分，满1000分可用</strong><img src="/images/wechat/tip.png" alt=""></span>
            <input class="switch switch-anim pull-right" onchange="checkNum()"  type="checkbox" @if($num < 1000) disabled @endif  />
            <input type="number" class="score form-control" placeholder="请输入抵扣积分" style="display: none;">
        </div>
        <div class="anonymity">
            <span>匿名捐款</span>
            <input class="switch switch-anim pull-right" onchange="checkNum()" type="checkbox"  />
        </div>

        <footer>
            <div class="bottom">提交</div>
        </footer>
    </div>
    <div class="sweet" style="display: none;">
        <div class="layer_notice" >
            <p class="title">积分使用规则</p>
            <p class="tip">使用条件</p>
            <p>1.捐款金额大于10元（含）</p>
            <p>2.积分数量大于1000分（含）</p>
            <p class="tip">使用数量</p>
            <p>1.使用数量大于1000分（含）</p>
            <p>2.1000积分抵10元</p>
        </div>
        <div class="layer_button">我知道了</div>
    </div>
@endsection
@section('jscontent')
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/layer_mobile/layer.js"></script>
    <script>
        var money = 10;
        function checkNum(){
            if($('.switch-anim').prop('checked')){
                console.log("选中");
            }else{
                console.log("没选中");
            }
        }
        $('.integral img').click(function () {
            layer.open({
                type: 1,
                shade: true,
                title: false, //不显示标题,
                content: $('.sweet').html(), //捕获的元素，注意：最好该指定的元素要存放在body最外层，否则可能被其它的相对元素所影响
                cancel: function(){
                    layer.closeAll();
                }
            });
            $('.layui-m-layercont .layer_button').click(function(){
                layer.closeAll();
            })
        });

        $('.money').click(function(){
            $('.money').removeClass('active');
            $(this).addClass('active');
            money = $(this).find('span').text();
        })

        $(".inputMoney").blur(function(){
            if($(this).val() != ''){
                $('.money').removeClass('active');
                money = $(this).val();
            }else{
                $('.money:first').addClass('active');
                money = 10;
            }
        });

        $('.bottom').click(function(){
            $.ajax({
                type: 'POST',
                url: '/wechat/donation/add',
                data: {},
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success: function (m) {
                    window.location.href = '/wechat/public/activity';
                }
            });

        })






    </script>
@endsection