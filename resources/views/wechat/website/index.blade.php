@extends('wxbase')
@section('head')
    <title>关于我们</title>
    <style>
        .top-img{
            width: 100%;
            padding: 20px 40px 20px 40px;
        }
        .top-img img{
            width: 100%;
            height: auto;
        }
        .text-box{
            /*padding: 30px;*/
        }
        .text-box p{
            text-indent: 2em;
        }
       .bannerImg {
         width: 100%;
           height: 200px;
        }
        .introduce{
            padding: 20px;
        }
        .content{
            padding: 20px;
        }
        .title img{
            width: 24px;
            margin-top: -5px;
        }
        .title strong{
            padding:10px 5px 0 5px;
            display: inline-block;
            font-size: 17px;
            margin-bottom: 20px;
        }
        .title p{
            text-indent: 2em;
        }
    </style>
@endsection
@section('content')
    <div class="wrapper">
        <div class="">
            <div class="top-img">
                <img src="http://new.cohf.cn/index/images/logo.png">
            </div>
            <div class="banner">
                <img src="http://new.cohf.cn/uploads/20180704/08cc79837a13c9505b5ca1a036f90ec5.jpg" alt="" class="bannerImg">
            </div>
            <div class="introduce">
                <div class="text-box">
                    <p>
                        中国牙病防治基金会成立于1994年，是中国口腔医学领域唯一经民政部批准设立的国家级公募基金会，具有面向社会公众筹款的资格。是国家民政部认定的慈善组织，具有非营利公益组织免税资格和公益性捐赠税前抵扣资格。业务主管部门为国家卫生健康委员会。2017年12月成立第六届理事会，理事长葛立宏教授，监事长孙正教授。
                    </p>
                    <p>
                        基金会的宗旨是发展我国牙病防治工作，提高人民口腔健康水平。面向基层开展口腔医生规范化的基础理论、基本知识和基本技能培训，促进学术交流与国际合作。
                    </p>
                </div>
            </div>
            <div class="content">
                <div class="title">
                    <img src="/images/wechat/project.png" alt="">
                    <strong>我们的资质</strong>
                    <p>中国牙病防治基金会是中国口腔医学领域唯一经国家民政部批准设立，国家卫生健康委员会业务主管的国家级公募基金会。属于公共筹款型基金会，接受政府监督和指导，完全按照国家相关要求，规范操作和管理。具有公开透明、公信力、专业性的优势。</p>
                    <p>本基金会拥有以下资格：</p>
                    <p>（一）具有非营利公益组织免税资格，接受的捐赠资金免税。</p>
                    <p>（二）具有公益性捐赠税前抵扣资格，企业的捐赠款可在计算企业所得税前抵扣。</p>
                    <p>（三）获得民政部慈善组织认定资格，可在全国范围内向社会公开募捐。面向公众募捐的地域范围是：中国境内及许可本基金会募捐的国家地区。</p>
                </div>
            </div>
            <div class="content">
                <div class="title">
                    <img src="/images/wechat/project.png" alt="">
                    <strong>我们的业务范围</strong>
                    <p>（一）开展口腔医学领域慈善性公益活动；</p>
                    <p>（二）开展口腔健康教育和促进，提高全民口腔健康素养；</p>
                    <p>（三）开展规范化培训，提升口腔专业人员的基本防治能力；</p>
                    <p>（四）创新并推广口腔卫生服务的新技术、新疗法；</p>
                    <p>（五）推动口腔相关设备和产品的研发与使用；</p>
                    <p>（六）促进口腔医学科学研究，推动科研成果的转化与应用；</p>
                    <p>（七）资助表彰口腔疾病防治先进单位和个人；</p>
                    <p>（八）开展国内国际学术交流与合作。</p>
                </div>
            </div>
            <div class="content">
                <div class="title">
                    <img src="/images/wechat/project.png" alt="">
                    <strong>我们的联系方式</strong>
                    <p>邮 编： 100081</p>
                    <p>电 话： 010 - 82195554</p>
                    <p>传 真： 010 - 62150976</p>
                    <p>电子邮件： zgybfzjjh@163.com</p>
                    <p>地址：北京市 海淀 区中关村南大街 22 号 中关村科技发展大厦C座207室</p>
                    <p>乘车路线：965 、 967 、运通 105 、特 4 、 26 、 563 路汽车到中央民族大学站下车，城铁4 号线 国家图书馆 下 B口出 直行500米</p>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('jscontent')
    <script>

    </script>
@endsection