
@extends('wxbase')
@section('head')
    <meta charset="UTF-8">
    <meta name="_token" content="{{ csrf_token() }}"/>
    <meta http-equiv="X-UA-Compatible" content="IE=ie8,ie9">
    <meta name="viewport"
          content="initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="email=no">
    <meta name="format-detection" content="address=no;">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <title> 口腔自测 </title>
    <style type="text/css">
        /**index.wxss**/
        body{
            background-image: url(/images/wechat/bg.jpg) !important;
            background-size: 100vw 100vh !important;
        }
        .top {
            width: 80vw;
            line-height: 40px;
            color: #aaa;
            background: #eee;
            margin: 0 auto;
            text-align: center;
            margin-top: 7vw;
        }
        .body {
            margin-top: 30px;
        }
        .content {
            background-color: rgba(255,255,255,0.8);
            display: flex;
            margin: 15px 10px;
            padding: 10px 15px;
            border-radius: 3.5px;
            align-items: center;
            border: 1px solid #f3f3f3;
            font-size: 15px;
        }
        .content > img {
            width: 13vw;
            margin:10px;
        }
        .content div img {
            width: 20px;
        }
    </style>
    </head>
@endsection
@section('content')
    <div class="body">
        <div class="content" data-id="5bf254507633797d3b7b0084">
            <img src="/images/wechat/qqq.png">
            <div><img src="http://test.pw.aiyizhuyi.com/images/hy/icon.png" alt=""> 龋齿风险评估表（年龄0-6岁）</div>
        </div>
        <div class="content" data-id="5bf2585f7633797d36290b94">
            <img src="/images/wechat/bbb.png">
            <div> <img src="http://test.pw.aiyizhuyi.com/images/hy/icon.png" alt="">龋齿风险评估表（年龄＞6岁）</div>
        </div>
    </div>
@endsection
@section('jscontent')
    <script>
        $(function(){
            $('.content').on('click',function(){
                var id = $(this).data('id');
                window.location.href = '/wechat/survey?id='+id;
            })
        })
    </script>
@endsection