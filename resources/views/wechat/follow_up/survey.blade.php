@extends('wxbase')
@section('head')
    <title>口腔自测</title>
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/iCheck/skins/all.css">
    <link rel="stylesheet"
          href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css">
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/font-awesome/css/font-awesome.min.css">
    <link href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/mobiscroll/css/mobiscroll.custom-3.0.0-beta2.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ '/js/plugins/survey2/'. $survey2_type .'/survey2.css?time='.time() }}">
@endsection
@section('content')
    <div class="wrapper"></div>
@endsection
@section('jscontent')
    <script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/iCheck/jquery.icheck.min.js"></script>
    <script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/mobiscroll/js/mobiscroll.custom-3.0.0-beta2.min.js"></script>
    <script src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js" type="text/javascript" charset="utf-8"></script>
    <script src="/js/plugins/survey2/survey2.js?time={{ time() }}"></script>
    <script src="{{ '/js/plugins/survey2/'. $survey2_type .'/survey3.js?time='.time() }}"></script>
    <script>
        wx.config(<?php echo $js;?>);
    </script>
    <script>
        jQuery(document).ready(function () {
            $('.wrapper').survey2({
                data : "{{ $data }}",
                save : {
                    active:true,
                    id : "{{ $surveyRecord }}",
                    url : '/wechat/survey/save'
                }
            });
        });
    </script>
@endsection