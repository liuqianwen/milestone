@extends('wxbase')
@section('head')
    <title>活动详情</title>
    <style>
        .top {
            display: flex;
            flex-direction: column;
            padding: 10px;
        }
        .top img {
            width: calc(100vw - 20px);
            /*height: 50vw;*/
            border-radius: 3.5px;
            box-shadow: 0 0 10px 0 #ddd;
            margin-top: 10px;

        }
        .introduce {
            color: #8d8d8d;
            font-size: 0.9em;
            text-align: justify;
        }
        .top .title {
            font-size: 1.2em;
            line-height: 28px;
            padding: 10px;
        }
        .article {
            padding: 10px 15px;
            position: relative;
            text-align: justify;
            margin-bottom: 65px;
        }
        .sign-btn  {
            position: fixed;
            bottom: 0px;
            width: 100vw;
            background: #fff;
            cursor: pointer;
        }
        .sign-btn span {
            -webkit-border-radius: 3.5px;
            -moz-border-radius: 3.5px;
            border-radius: 3.5px;
            background: #f93b49;
            width: 80vw;
            height: 40px;
            line-height: 40px;
            display: inline-block;
            color: #fff;
            text-align: center;
            margin: 10px 10vw;
        }
        .article img {
            width: 100%;
        }
        .article{
            font-size: 15px;
        }
        .article p{
            text-indent: 2em;
        }
    </style>
@endsection
@section('content')
    <div class="content">
        <div class="top">
            <span class="title">{{$data['name']}}</span>
            <span class="introduce">{{$data['description']}}</span>
            <img src="{{$data['image']}}" alt="">
        </div>
        <div class="article over-flow article-height">
            @if($data['_id'] == '5dca3b4076337953aa69345b')
           {!! $data['content'] !!}
            @else
            <p><strong> 报名截止通告：</strong></p>
            <p>感谢各位朋友们关注《健康口腔大世界》， 积极参与 “健康口腔推广大使”的活动。鉴于本年度培训名额已满，活动报名从即日起结束，再次对大家的支持表示感谢！</p>
            <p>2020年《健康口腔大世界》将于明年10月在昆明举办。请您继续关注基金会公众号和8020平台，注意相关信息实时通报。欢迎您的热情参与！</p>
            <p>2020.10.昆明欢迎您！</p>
                @endif
        </div>
        @if($data['_id'] == '5dca3b4076337953aa69345b')
            <div class="sign-btn"><span>立即报名</span></div>
        @endif

    </div>
    <input type="hidden" id="act-id" value="{{$data['_id']}}">
@endsection

@section('jscontent')
    <script>
        var id = $('#act-id').val();
        $('.sign-btn').on('click', function() {
            window.location.href = '/wechat/signup/form/'+id;
        })
    </script>
@endsection