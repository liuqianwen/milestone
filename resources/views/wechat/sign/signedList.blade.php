@extends('wxbase')
@section('head')
    <title>{{$title}}</title>
    <style>
        .top {
            line-height: 28px;
            border-bottom: 1px solid #f1f1f1;
            background: #49b4ee;
            color: #fff;
            display: flex;
            justify-content: space-between;
            padding: 8px;
            align-items: center;
        }
        .top .info {
            display: flex;
            justify-content: space-around;
            width: 60vw;
        }
        .top .info > span {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }
        .table {
            font-size: 0.9em;
            width: 100vw;
            overflow: scroll;
        }
        .table .li {
            border-top: 1px solid #f1f1f1;
            padding: 10px 5px;
            line-height: 20px;
            /*width: 100%;*/
            min-width: 100vw;
        }
        .table .li .li-content {
            white-space: nowrap;
        }
        .table .li .li-content span {
            margin-right: 8px;
            width: 150px;
            display: inline-block;
            text-align: left;
            white-space: normal;
            vertical-align: middle;
        }
        .table .head {
            line-height: 28px;
            background: #f5f5f5;
            white-space: nowrap;
            padding: 10px 5px;
            min-width: 100vw;
        }
        .head span {
            width: 150px;
            display: inline-block;
            text-align: left;
            margin-right: 8px;
        }
        .ticket {
            background: #fff;
            color: #49b4ee;
            padding: 10px;
            border-radius: 3.5px;
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    <div class="top">
        @if($is_ticket)
        <div class="info">
            <span>
                <span>报名数</span>
                <span>{{$num}}</span>
            </span>
            <span>
                <span>总金额</span>
                <span>￥{{$totalPrice}}</span>
            </span>
        </div>
        <span class="">
            <span class="ticket" data-id="{{$id}}">发票信息</span>
        </span>
            @else
            <div class="info" style="width: 100vw;">
                <span>报名人数</span>
                <span>{{$num}}</span>
            </div>
            @endif
    </div>
    <div class="table">
        <div class="head" style="width: calc(20px + {{count($data[0]['value']) * 8}}px + {{$lengthTotal}}em);">
            @foreach ($data[0]['value'] as $v)
                <span style="width: {{$length[$v['number']]}}em;">{{$v['name']}}</span>
            @endforeach
        </div>
        @foreach ($data as $v)
            <div class="li" style="width: calc(20px + {{$lengthTotal}}em);">
                <div class="li-content">
                    @foreach ($v['value'] as $val)
                        <span style="width: {{$length[$val['number']]}}em;">{{$val['value']}}</span>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('jscontent')
    <script>
        $('.ticket').on('click', function () {
            var id = $(this).data('id');
            window.location.href = '/wechat/signup/pay/ticket?id='+id;
        })
    </script>
@endsection