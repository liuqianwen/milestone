@extends('wxbase')
@section('head')
    <title>发票信息</title>
    <style>
        body {
            background: #f5f5f5;
        }
        .content {
            padding: 10px;
        }
        .list {
            background: #fff;
            padding: 10px;
            margin-bottom: 10px;
        }
        .list .pay-info,.ticket-info .pay-info {
            border-bottom: 1px solid #c1c1c1;
            line-height: 28px;
            color: #ee7949;
        }
        .list .ticket-info {
            display: flex;
            flex-direction: column;
            line-height: 28px;
        }
        
        .list .ticket-info > span span{
            width: 5.5em;
            text-align: right;
            display: inline-block;
            margin-right: 10px;
            color: #000;
        }
        .no-data {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            margin-top: 20vw;
            color: #8d8d8d;
        }
        .no-data img {
            width: 50vw;
            margin-bottom: 10px;
        }
        /*发票*/
        .ticket-info {
            /*position: fixed;*/
            /*width: 80vw;*/
            /*top: 10vw;*/
            /*left: 10vw;*/
            padding: 10px;
            z-index: 100;
            background: #fff;
            border-radius: 3.5px;
        }
        .ticket-info input {
            margin: 10px;
            border: 1px solid #bfbfbf;
            border-radius: 3.5px;
            line-height: 28px;
            width: 90%;
            padding: 0 8px;
        }
        .ticket-btn {
            text-align: center;
            cursor: pointer;
        }
        .ticket-btn span{
            color: #fff;
            background: #49b4ee;
            border-radius: 3.5px;
            padding: 5px 15px;
        }
        .li {
            display: flex;
            align-items: center;
        }
        .li .title {
            width: 7em;
            text-align: right;
            display: inline-block;
        }
        .li input {
            width: calc(100% - 7em);
        }
    </style>
@endsection
@section('content')
    <div class="content">
        @foreach ($data as $v)
        {{--<div class="list">--}}
            {{--<div class="pay-info">--}}
                {{--<span>支付时间：{{$v['time']}}</span>--}}
            {{--</div>--}}
            {{--<div class="ticket-info">--}}
                {{--<span><span>发票抬头:</span>{{$v['ticket']['head']}}</span>--}}
                {{--<span><span>税号:</span>{{$v['ticket']['number']}}</span>--}}
                {{--<span><span>缴费人姓名:</span>{{$v['ticket']['name']}}</span>--}}
                {{--<span><span>开票金额:</span>￥{{$v['ticket']['money']}}</span>--}}
                {{--<span><span>手机:</span>{{$v['ticket']['phone']}}</span>--}}
                {{--<span><span>邮箱:</span>{{$v['ticket']['email']}}</span>--}}
            {{--</div>--}}
        {{--</div>--}}
        @endforeach
            <div class="ticket-info">
                <div class="pay-info">
                    <span>支付时间：{{$time}}</span>
                </div>
                {{--<div class="h4" style="text-align: center">发票信息</div>--}}
                {{--<hr>--}}
                <div class="li">
                    <span class="title">发票抬头:</span> <input type="text" name="head" placeholder="发票抬头" value="{{isset($data[0]) ? $data[0]['ticket']['head'] : ''}}">
                </div>
                <div class="li">
                    <span class="title">税号:</span><input type="text" name="number" placeholder="税号" value="{{isset($data[0]) ? $data[0]['ticket']['number'] : ''}}">
                </div>
                <div class="li">
                    <span class="title">缴费人姓名:</span><input type="text" name="name" placeholder="缴费人姓名" value="{{isset($data[0]) ? $data[0]['ticket']['name'] : ''}}">
                </div>
                <div class="li">
                    <span class="title">开票金额:</span><input type="number" name="money" placeholder="缴费金额" value="{{isset($data[0]) ? $data[0]['ticket']['money']: ''}}">
                </div>
                <div class="li">
                    <span class="title">手机:</span><input type="number" name="phone" placeholder="手机" value="{{isset($data[0]) ? $data[0]['ticket']['phone'] : ''}}">
                </div>
                <div class="li">
                    <span class="title">邮箱:</span><input type="text" name="email" placeholder="邮箱" value="{{isset($data[0]) ? $data[0]['ticket']['email'] : ''}}">
                </div>
                <hr>
                <div class="ticket-btn">
                    <span>确定</span>
                </div>
            </div>
        {{--@if(count($data) == 0)--}}
            {{--<div class="no-data">--}}
                {{--<img src="/images/wechat/no-data.png" alt="">--}}
                {{--<span>没有发票~</span>--}}
            {{--</div>--}}
            {{--@endif--}}
    </div>
    <input type="hidden" id="order_id" value="{{$id}}">
@endsection

@section('jscontent')
    <script>
        $('.ticket-btn').on('click', function() {
            var id = $('#order_id').val();
            var ticket = {};
            ticket.head = $('.ticket-info input[name=head]').val();
            ticket.number = $('.ticket-info input[name=number]').val();
            ticket.name  = $('.ticket-info input[name=name]').val();
            ticket.money = $('.ticket-info input[name=money]').val();
            ticket.phone = $('.ticket-info input[name=phone]').val();
            ticket.email  = $('.ticket-info input[name=email]').val();
            if (!ticket.head) {
                layer.open({
                    content: '发票抬头不能为空',
                    skin: 'msg',
                    time: 2
                })
                return false;
            }
            if (!ticket.number) {
                layer.open({
                    content: '税号不能为空',
                    skin: 'msg',
                    time: 2
                })
                return false;
            }
            if (!ticket.money) {
                layer.open({
                    content: '金额不能为空',
                    skin: 'msg',
                    time: 2
                })
                return false;
            }
            $.ajax({
                url: '/wechat/signup/ticket/save',
                method: 'post',
                data: {id:id,ticket:ticket},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name=_token]').attr('content')
                }
            }).done(function (res) {
                if (res.status == 0) {
                    layer.open({
                        content: '已保存',
                        skin: 'msg',
                        time: 2
                    })
                    // setTimeout(function(){
                    //     window.location.reload();
                    // },1000)
                }
            })
        })
    </script>
@endsection