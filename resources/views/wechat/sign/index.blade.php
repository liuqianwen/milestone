@extends('wxbase')
@section('head')
    <title>活动报名</title>
    <style>
        body {
            background: #f1f1f1;
        }
        .list {
            /*padding: 10px;*/
        }
        .tips {
            padding: 15px;
            margin-bottom: 10px;
            background: #fff;
        }
        .list .item {
            background: #fff;
            margin-bottom: 10px;
            padding: 10px 0;
            position: relative;
        }
        .list .item img {
            width: 90vw;
            /*height: 50vw;*/
            box-shadow: 0 0 10px 0 #ddd;
            margin: 0 5vw;
            display: inline-block;
            -webkit-border-radius: 3.5px;
            -moz-border-radius: 3.5px;
            border-radius: 3.5px;
            margin-bottom: 10px;
        }
        .list .item .right {
            display: flex;
            flex-direction: column;
            justify-content: center;
            margin-left: 20px;
            margin-right: 10px;
        }
        .list .item .right .name {
            font-size: 1.2em;
            line-height: 2em;
        }
        .list .item .right .span:not(.introduce) {
            font-size: 16px;
        }
        .list .item .right .introduce {
            overflow: hidden;
            -webkit-line-clamp: 2;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            font-size: 0.9em;
            color: #8d8d8d;
        }
        .button {
            position: absolute;
            top: 10px;
            right: 10px;
            display: flex;
            flex-direction: column;
        }
        .signed,.un-pay {
            padding: 3px 10px;
            background: #49b4ee;
            color: #fff;
            border-radius: 3.5px;
            cursor: pointer;
            margin-bottom: 10px;
        }
        .detail {
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    <div class="tips">
        <span>健康口腔推广能力培训通知(盖章版).pdf</span>
        <a href="http://www.cohf.cn/ueditor/php/upload/file/20191009/1570597692102828.pdf">下载</a>
    </div>
    <div class="list">
        @foreach ($data as $v)
        <div class="item detail" data-id="{{$v['_id']}}">
            <img src="{{$v['image']}}" alt="">
            <div class="right">
                <span class="name">{{$v['name']}}</span>
                <span class="introduce">{{$v['introduce']}}</span>
            </div>
            <div class="button">
                @if($v['signed'] > 0)
                    <span class="signed">已报名</span>
                @endif
                @if($v['un_pay'] > 0)
                    <span class="un-pay">待确认</span>
                @endif
            </div>
        </div>
            @endforeach
    </div>
@endsection

@section('jscontent')
    <script src="http://res.wx.qq.com/open/js/jweixin-1.4.0.js"></script>
    <script>
        wx.config(<?php echo $js; ?>);
        wx.ready(function() {
            wx.hideMenuItems({
                menuList: ["menuItem:copyUrl","menuItem:editTag","menuItem:delete","menuItem:originPage","menuItem:readMode", "menuItem:openWithQQBrowser", "menuItem:openWithSafari","menuItem:share:email","menuItem:share:brand","menuItem:share:qq","menuItem:share:QZone","menuItem:share:timeline","menuItem:share:appMessage","menuItem:favorite"] // 要隐藏的菜单项，只能隐藏“传播类”和“保护类”按钮
            });
        });
    </script>
    <script>
        var initFn = function () {
            init.detailFn();
            init.signed();
        }
        var init = {
            detailFn: function () {
                $('body').on('click','.detail',function(){
                    var id = $(this).data('id');
                    window.location.href = '/wechat/signup/detail/'+id;
                })
            },
            signed: function () {
                $('.signed').on('click', function(e) {
                    var id = $(this).parents('.detail').data('id');
                    e.stopPropagation();
                    window.location.href = '/wechat/signup/list/' + id+',1';
                })
                $('.un-pay').on('click', function(e) {
                    var id = $(this).parents('.detail').data('id');
                    e.stopPropagation();
                    window.location.href = '/wechat/signup/list/' + id+',2';
                })
            }
        }
        initFn();
    </script>
@endsection