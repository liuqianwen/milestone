@extends('wxbase')
@section('head')
    {{--<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">--}}
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>报名信息</title>
    <link rel="stylesheet" href="/css/wechat/form.css">
    <style>

    </style>
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-body">
                        <form role="form" class="">
                            @foreach ($form as $v)
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">{{$v['name']}}</label>
                                @if($v['is_must'] == 1)
                                <span class="warning">*必填</span>
                                @endif
                                @if($v['type'] == 1)
                                <span class="length-warning">可输{{$v['length']}}字</span>
                                @endif
                                <div class="col-sm-9">
                                    @if($v['type'] == 1)
                                    <input type="text" placeholder="{{$v['name']}}" data-required="{{$v['is_must']}}" data-number="{{$v['number']}}" data-length="{{$v['length']}}" data-name="{{$v['name']}}" class="form-control">
                                        @elseif($v['type'] == 2)
                                        <select  class="form-control" data-number="{{$v['number']}}" data-name="{{$v['name']}}">
                                            @foreach ($v['option'] as $v1)
                                            <option value="{{$v1}}">{{$v1}}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                            @endforeach
                        </form>
                        {{--<span class="warning active" style="display: flex;justify-content: flex-end;margin-right: 15px;margin-bottom: 10px;">*表单可重复修改</span>--}}
                        <div class="add-btn">
                            <span class="form-warning" style="margin-left: 15px">*表单可重复修改</span>
                            <button class="add btn btn-info">+添加到报名表</button>
                        </div>
                        @if($is_pay)
                        @if($pay_type == 3)
                            <hr>
                        <div class="pay_type">
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">选择支付方式</label>
                                <div class="col-sm-9" style="display: flex; align-items: center">
                                    <label class="radio-inline">
                                        <input type="radio" value="1" name="pay_type" checked> <span>微信支付</span>
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" value="2" name="pay_type"> <span>对公转账</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group pay_num display-none">
                                <div class="col-sm-9" style="font-size: 12px; color: #858585;background: #f7f4e6; padding: 10px;">
                                    <div>账号：0200 0076 0901 4456 112</div>
                                    <div>开户行：工商银行北京紫竹院支行</div>
                                    <div>账户名：中国牙病防治基金会</div>
                                </div>
                            </div>
                        </div>
                            @elseif($pay_type == 2)
                            <hr>
                            <div class="form-group pay_num display-none">
                                <div class="col-sm-9" style="font-size: 12px; color: #858585;background: #f7f4e6; padding: 10px;">
                                    <div>账号：0200 0076 0901 4456 112</div>
                                    <div>开户行：工商银行北京紫竹院支行</div>
                                    <div>账户名：中国牙病防治基金会</div>
                                </div>
                            </div>
                            @endif
                            @endif
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading-tabs num">共0条</div>
                    <div class="table">
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="bottom">
        @if($is_pay)
            <div class="pay-ticket">
                <span>发票</span>
            </div>
        @endif
        @if($pay_type == 1 || $pay_type == 3)
        <div class="pay-sign">
            <span>报名</span>
            @if($is_pay)
            <span class="price-total">￥{{$price}}</span>
                @endif
        </div>
            @elseif($pay_type == 2)
                <div class="sign-save">
                    <span>确定</span>
                    <span class="price-total">转账￥{{$price}}</span>
                </div>
            @else
                <div class="pay-sign">
                    <span>报名</span>
                </div>
            @endif
            <div class="sign-save display-none">
                <span>确定</span>
                <span class="price-total">￥{{$price}}</span>
            </div>
    </div>
    <div class="ticket-info display-none">
        <div class="h4" style="text-align: center">发票信息</div>
        <hr>
        <input type="text" name="head" placeholder="发票抬头">
        <input type="text" name="number" placeholder="税号">
        <input type="text" name="name" placeholder="缴费人姓名">
        <input type="number" name="money" placeholder="缴费金额">
        <input type="number" name="phone" placeholder="手机">
        <input type="text" name="email" placeholder="邮箱">
        <hr>
        <div class="ticket-btn">
            <button class="btn btn-primary">确定</button>
        </div>
    </div>
    <div class="shade display-none"></div>
    <input type="hidden" id="act-id" value="{{$id}}">
    <input type="hidden" id="price" value="{{$price}}">
    <input type="hidden" id="is_pay" value="{{$is_pay}}">
    <input type="hidden" id="enroll_pay_type" value="{{$pay_type}}">
@endsection

@section('jscontent')
    <script type="text/javascript" src="http://res2.wx.qq.com/open/js/jweixin-1.4.0.js?i={{time()}}"></script>
    <script src="/js/wechat/form.js"></script>
    <script>
        wx.config(<?php echo $js; ?>);
    </script>
    <script>
        $("input").on("blur",function(){
            window.scroll(0,1);//失焦后强制让页面归位
        });
        function isIphoneX(){
            if(/iphone/gi.test(window.navigator.userAgent)){
                /* iPhone X、iPhone XS */
                var x=(window.screen.width === 375 && window.screen.height === 812);
                /* iPhone XS Max */
                var xsMax=(window.screen.width === 414 && window.screen.height === 896);
                /* iPhone XR */
                var xR=(window.screen.width === 414 && window.screen.height === 896);
                if(x || xsMax || xR){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false
            }
        }


        /* 函数使用 */
        if(isIphoneX()){
            let selector = $('.bottom');
            selector.css('padding-bottom',"34px");
        }

        var win_h = $(window).height();//关键代码
        window.addEventListener('resize', function () {
            if($(window).height() < win_h){
                $('.bottom').hide();
            }else{
                $('.bottom').show();
            }
        });
        form.init()
    </script>
@endsection