@extends('base')
@section('head')
    <style>
        .core-box .core-content {
            margin-top: 0;
        }

        #_volumeBusiness, #_orders, #_agent {
            height: 300px;
        }

        .panel-heading {
            border-bottom-color: rgba(0, 0, 0, 0.1);
        }
    </style>
@endsection
@section('container')
    <div class="toolbar row">
        <div class="col-sm-6 hidden-xs">
            <div class="page-header">
                <h2>
                    首页
                    <small></small>
                </h2>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <a href="#" class="back-subviews">
                <i class="fa fa-chevron-left"></i> 返回
            </a>
            <a href="#" class="close-subviews">
                <i class="fa fa-times"></i> 关闭
            </a>
            <div class="toolbar-tools pull-right">
                <ul class="nav navbar-right"></ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">首页</a></li>
                <li class="active">首页</li>
            </ol>
        </div>
    </div>

@endsection
@section('subview')
@endsection
@section('js-container')
    <script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/echarts/build/dist/echarts.js"></script>
    <script src="/js/admin/index.js"></script>
    <script>
        $(function () {
            Main.init();
            index.init();
        })
    </script>
@endsection