@extends('base')
@section('head')
    <link rel="stylesheet"
          href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/Jcrop-master/css/jquery.Jcrop.min.css">
    <style>
        #_editDoctor, #_seeQrCode {
            display: none;
        }

        ._uploadHeadImgModel .modal-body {
            height: 30vh;
        }

        .image-replace {
            width: 30%;
        }

        #_doctorTable > thead > tr > th {
            text-align: center !important;
        }

        #_doctorTable > tbody > tr > td > img {
            width: 100px;
        }

        #_seeQrCode {
            width: 100%;
            height: 100%;
            text-align: center;
        }

        #_seeQrCode > img {
            width:200px;
        }
        .table1 td:first-child {
            width: 150px;
        }
        .table1 td:last-child {
            width: 200px;
        }

        .table2 td:first-child {
            width: 150px;
        }
        .table2 td:nth-child(2) {
            width: 200px;
        }
    </style>
@endsection
@section('container')
    <div class="toolbar row">
        <div class="col-sm-6 hidden-xs">
            <div class="page-header">
                <h2>医生管理
                    <small></small>
                </h2>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <a href="#" class="back-subviews">
                <i class="fa fa-chevron-left"></i> 返回
            </a>
            <a href="#" class="close-subviews">
                <i class="fa fa-times"></i> 关闭
            </a>
            <div class="toolbar-tools pull-right">
                <ul class="nav navbar-right">

                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">首页</a></li>
                <li class="active">医生管理</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div id="toolbar">
                        <button type="button" class="btn btn-blue tooltips edit-doctor" disabled>
                            修改信息
                        </button>
                    </div>
                    <table id="_doctorTable">
                        <thead>
                        <tr>
                            <th data-field="state" data-radio="true"></th>
                            <th data-field="name" data-align="left">姓名</th>
                            <th data-field="hospital" data-align="left">所属医院</th>
                            <th data-field="department" data-align="center">科室</th>
                            <th data-field="position" data-align="center">职务</th>
                            <th data-field="tel" data-align="left">电话</th>
                            <th data-field="hospital_status" data-align="left">医院状态</th>
                            <th data-field="status" data-align="left">状态</th>
                            <th data-field="createTime" data-align="center">创建日期</th>

                        </tr>
                        </thead>
                    </table>
                    <div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" class="hospital_id" value="{{$id}}">
@endsection
@section('subview')
    <div id="editDoctor" style="display: none;">
        <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
            <form class="form-editDoctor">
                <legend>编辑医生</legend>
                <div class="form-group">
                    <fieldset>
                        <div class="row col-lg-3 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">医生名称</label>
                                <div class="controls">
                                    <input type="text" class="form-control name" name="name">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-3 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">科室</label>
                                <div class="controls">
                                    <input type="text" class="form-control department" name="department">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-3 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">职位</label>
                                <div class="controls">
                                    <input type="text" class="form-control position" name="position">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-3 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">电话</label>
                                <div class="controls">
                                    <input type="text" class="form-control tel" name="tel">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-3 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">性别</label>
                                <div class="controls">
                                    <select name="sex" class="form-control sex">
                                        <option value="0">缺省</option>
                                        <option value="1">男</option>
                                        <option value="2">女</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">擅长</label>
                                <div class="controls">
                                    <textarea name="good" id="" class="form-control good"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">介绍</label>
                                <div class="controls">
                                    <textarea name="introduce" id="" class="form-control introduce"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">头像(点击图片进行修改)</label>
                            </div>
                            <img  src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/sfy/noHeadImg.jpg" class="img-thumbnail img" style="width:200px;margin-bottom: 20px;" alt="">
                            <input type="hidden" class="imgUrl" value="">
                        </div>
                    </fieldset>
                </div>
                <hr>
                <div class="pull-right">
                    <button type="button" class="btn btn-info close-subview-button">关闭</button>
                    <button class="btn btn-info save-note" type="submit">保存</button>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="publishModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">发布</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <style>
                    #clipArea {
                        width: 100%;
                        height: 400px;
                    }
                </style>
                <div class="modal-body">
                    <input type="file" name="file" class="form-control file" id="file">
                    <div class="photoClip">
                        <div id="clipArea"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="clipBtn">确定</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js-container')
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/hammer.min.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/iscroll-zoom-min.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/lrz.all.bundle.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/dist/Photoclip.min.js"></script>
    <script src="{{ asset('/js/admin/doctor.js') }}"></script>
    <script>
        jQuery(document).ready(function () {
            Main.init();
            FormElements.init();
            doctor.init();
        });
    </script>
@endsection