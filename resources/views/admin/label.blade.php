@extends('base')
@section('head')
@endsection
@section('container')
        <!-- start: PAGE -->
<div class="main-content">
    <!-- start: PANEL CONFIGURATION MODAL FORM -->
    <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Panel Configuration</h4>
                </div>
                <div class="modal-body">
                    Here will be a configuration form
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary">
                        Save changes
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- end: SPANEL CONFIGURATION MODAL FORM -->
    <div class="container">
        <!-- start: PAGE HEADER -->
        <!-- start: TOOLBAR -->
        <div class="toolbar row">
            <div class="col-sm-6 hidden-xs">
                <div class="page-header">
                    <h2>标签管理<small></small></h2>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <a href="#" class="back-subviews">
                    <i class="fa fa-chevron-left"></i> 返回
                </a>
                <a href="#" class="close-subviews">
                    <i class="fa fa-times"></i> 关闭
                </a>
                <div class="toolbar-tools pull-right">
                    <!-- start: TOP NAVIGATION MENU -->
                    <ul class="nav navbar-right">
                        <li>
                            <a href="#newlabe1" class="new-label">
                                <i class="fa fa-plus"></i> 添加标签
                            </a>
                        </li>
                    </ul>
                    <!-- end: TOP NAVIGATION MENU -->
                </div>
            </div>
        </div>
        <!-- end: TOOLBAR -->
        <!-- end: PAGE HEADER -->
        <!-- start: BREADCRUMB -->
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{ url('/') }}">首页</a></li>
                    <li class="active">标签管理</li>
                </ol>
            </div>
        </div>
        <!-- end: BREADCRUMB -->
        <!-- start: PAGE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div id="toolbar">
                            <button type="button" class="btn btn-blue tooltips edit-label" disabled>
                                编辑
                            </button>
                        </div>
                        <table id="labeltable">
                            <thead>
                            <tr>
                                <th data-field="state" data-radio="true"></th>
                                <th data-field="name" data-align="left">标签名称</th>
                                <th data-field="status" data-align="center">状态</th>
                                <th data-field="ctime" data-align="center">创建日期</th>
                            </tr>
                            </thead>
                        </table>
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end: PAGE CONTENT-->
        </div>
    </div>
    <!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->
@endsection
@section('subview')
        <!-- start: SUBVIEW SAMPLE CONTENTS -->
<!-- *** NEW label *** -->
<div id="newlabe1" style="display:none;">
    <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
        <legend>添加标签</legend>
        <form class="form-addlabel">
            <div class="form-group" style="margin-top: 30px;">
                <fieldset>
                    <div class="row col-lg-4 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                        <div class="form-group">
                            <label class="control-label">标签名称</label>
                            <div class="controls">
                                <input type="text" class="form-control name" name="name">
                            </div>
                        </div>
                    </div>
                    <div class="row col-lg-4 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                        <div class="form-group">
                            <label class="control-label">状态</label>
                            <select name="status" class="form-control status">
                                <option value="0">暂停</option>
                                <option value="1" selected>正常</option>
                            </select>
                        </div>
                    </div>
                </fieldset>
            </div>
            <hr>
            <div class="pull-right">
                <div class="btn-group">
                    <a href="#" class="btn btn-info close-subview-button">
                        关闭
                    </a>
                </div>
                <div class="btn-group">
                    <button class="btn btn-info save-note" type="submit">
                        保存
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- *** EDIT label *** -->
<div id="editlabel" style="display:none;">
    <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
        <legend>修改标签</legend>
        <form class="form-editlabel">
            <div class="form-group">
                <fieldset>
                    <div class="row col-lg-4 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">

                        <div class="form-group">
                            <label class="control-label">标签名称</label>
                            <div class="controls">
                                <input type="text" class="form-control name" name="name">
                                <input type="hidden" class="form-control bh" name="bh">
                            </div>
                        </div>
                    </div>
                    <div class="row col-lg-4 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                        <div class="form-group">
                            <label class="control-label">状态</label>
                            <select name="status" class="form-control status">
                                <option value="0">暂停</option>
                                <option value="1">正常</option>
                            </select>
                        </div>
                    </div>
                </fieldset>
            </div>
            <hr>
            <div class="pull-right">
                <div class="btn-group">
                    <a href="#" class="btn btn-info close-subview-button">
                        关闭
                    </a>
                </div>
                <div class="btn-group">
                    <button class="btn btn-info save-note" type="submit">
                        保存
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end: SUBVIEW SAMPLE CONTENTS -->
@endsection
@section('js-container')
    <script src="{{ asset('/js/admin/label.js') }}"></script>
    <!-- end: CORE JAVASCRIPTS  -->
    <script>
        jQuery(document).ready(function() {
            Main.init();
            label.init();
            $('.th-inner').parent().css('text-align', 'center');
        });
    </script>
@endsection