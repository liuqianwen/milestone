@extends('base')
@section('head')
    <style>
        .cure-config-body div {
            display: flex;
            flex-flow: row wrap;
            justify-content: flex-start;
            align-items: flex-start;
            margin-bottom: 15px;
        }

        .cure-config-body div > div {
            line-height: 1.5em;
            padding: 5px 10px 5px 10px;
            background: #cccccc;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            margin-left: 5px;
            margin-bottom: 5px;
        }

        .cure-config-body div > .current {
            background: #30c778;
            color: #fff;
        }

        .cure-config-body div > .plus {
            background: #fff;
        }
        .current {
            background: #30c778;
            color: #fff;
        }
    </style>
@endsection
@section('container')
    <div class="toolbar row">
        <div class="col-sm-6 hidden-xs">
            <div class="page-header">
                <h2> 视频管理
                    <small></small>
                </h2>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <a href="#" class="close-subviews"> <i class="fa fa-times"></i> 关闭 </a>

            <div class="toolbar-tools pull-right">
                <ul class="nav navbar-right">
                    <li>
                        <a href="javascript:void(0);" class="new-video"> <i class="fa fa-plus"></i> 添加视频 </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li><a href="{{ url('/admin') }}">首页</a></li>
                <li class="active">视频管理</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div id="toolbar">
                        <button class="btn btn-blue  edit">
                            编辑
                        </button>
                        <button class="btn btn-blue  video-top">
                            置顶
                        </button>
                        <button  class="btn btn-blue  unavailable">
                            禁用
                        </button>
                        <button  class="btn btn-blue available">
                            启用
                        </button>
                    </div>
                    <table id="video-table">
                        <thead>
                        <tr>
                            <th data-field="state" data-radio="true"></th>
                            {{--<th data-field="id" data-align="left">编号</th>--}}
                            <th data-field="image" data-align="center" data-width="8%">视频封面</th>
                            <th data-field="title" data-align="left">视频标题</th>
                            <th data-field="description" data-align="left">视频描述</th>
                            <th data-field="status" data-align="center">是否禁用</th>
                            <th data-field="top" data-align="center">排序</th>
                            <th data-field="created_at" data-align="center">创建时间</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('subview')
    <div id="newVideo" style="display:none">
        <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
            <form class="form-add-video">
                <legend>添加视频</legend>
                <div class="form-group">
                    <fieldset style="display: flex; flex-direction: column">
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">视频标题</label>
                                <div class="controls">
                                    <input type="text" class="form-control title" name="title">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">视频URL</label>
                                <div class="controls">
                                    <input type="text" class="form-control url" name="url">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label>标签</label>
                                <div class="cure-config-body">
                                    <div>
                                        @foreach($label as $v)
                                            <div class="system-malady" data-id="{{$v->_id}}">{{$v->name}}</div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">视频封面图</label>
                            </div>
                            <img  src="{{ empty($data->image) ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : $data->image }}" class="img-thumbnail img" style="width:40%;margin-bottom: 20px;" alt="">
                            <input type="hidden" class="imgUrl" value="">
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">视频介绍</label>

                                <div class="controls">
                                    <textarea name="introduce" id="" cols="30" rows="10" class="form-control  introduce" style=""></textarea>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <hr>
                <div class="pull-right">
                    <div class="btn-group">
                        <a href="#" class="btn btn-info close-subview-button"> 关闭 </a>
                    </div>
                    <div class="btn-group">
                        <button class="btn btn-info update-video-button" type="submit">
                            添加
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div id="updateVideo" style="display:none">
        <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
            <form class="form-update-video">
                <legend>修改视频</legend>
                <div class="form-group">
                    <fieldset style="display: flex; flex-direction: column">
                        <input type="hidden" class="dataId" value="">
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">视频标题</label>
                                <div class="controls">
                                    <input type="text" class="form-control title" name="title">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">视频URL</label>
                                <div class="controls">
                                    <input type="text" class="form-control url" name="url">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label>标签</label>
                                <div class="cure-config-body">
                                    <div>
                                        @foreach($label as $v)
                                            <div class="system-malady" data-id="{{$v->_id}}">{{$v->name}}</div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">视频封面图</label>
                            </div>
                            <img  src="{{ empty($data->image) ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : $data->image }}" class="img-thumbnail img" style="width:40%;margin-bottom: 20px;" alt="">
                            <input type="hidden" class="imgUrl" value="">
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">视频简介</label>

                                <div class="controls">
                                    <textarea name="introduce" id="" cols="30" rows="10" class="form-control  introduce" style=""></textarea>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <hr>
                <div class="pull-right">
                    <div class="btn-group">
                        <a href="#" class="btn btn-info close-subview-button"> 关闭 </a>
                    </div>
                    <div class="btn-group">
                        <button class="btn btn-info update-video-button" type="submit">
                            保存
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="publishModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">发布</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <style>
                    #clipArea {
                        width: 100%;
                        height: 400px;
                    }
                </style>
                <div class="modal-body">
                    <input type="file" name="file" class="form-control file" id="file">
                    <div class="photoClip">
                        <div id="clipArea"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="clipBtn">确定</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js-container')
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/hammer.min.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/iscroll-zoom-min.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/lrz.all.bundle.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/dist/Photoclip.min.js"></script>
    <script src="{{ asset('/js/admin/video.js') }}"></script>
    <script>
        jQuery(document).ready(function () {
            Main.init();
            video.init();
        });
    </script>
@endsection