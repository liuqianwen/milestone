@extends('base')
@section('container')
    <div class="main-content">
        <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">Panel Configuration</h4>
                    </div>
                    <div class="modal-body">
                        Here will be a configuration form
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Close
                        </button>
                        <button type="button" class="btn btn-primary">
                            Save changes
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="toolbar row">
                <div class="col-sm-6 hidden-xs">
                    <div class="page-header">
                        <h2>医院管理
                            <small></small>
                        </h2>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <a href="#" class="back-subviews"> <i class="fa fa-chevron-left"></i> 返回 </a>
                    <a href="#" class="close-subviews"> <i class="fa fa-times"></i> 关闭 </a>
                    <div class="toolbar-tools pull-right">
                        <ul class="nav navbar-right">
                            <li>
                                <a href="#newHospital" class="add-hospital"> <i class="fa fa-plus"></i> 添加医院 </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/') }}">首页</a></li>
                        <li class="active">医院管理</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-white">
                        <div class="panel-body">
                            <div id="toolbar">
                                <button type="button" class="btn btn-blue tooltips edit-hospital" >
                                    编辑医院信息
                                </button>
                                <button type="button" class="btn btn-blue tooltips add-doctor" >
                                    添加医生
                                </button>
                                <button type="button" class="btn btn-blue tooltips read-doctor" >
                                    查看医生
                                </button>
                            </div>
                            <table id="hospitalTable">
                                <thead>
                                <tr>
                                    <th data-field="state" data-radio="true"></th>
                                    <th data-field="name" data-align="left">医院名称</th>
                                    <th data-field="address" data-align="left">医院地址</th>
                                    <th data-field="status" data-align="left">状态</th>
                                </tr>
                                </thead>
                            </table>
                            <div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('subview')
    <div id="newHospital" style="display:none">
        <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
            <form class="form-addhospital">
                <legend>添加医院</legend>
                <div class="form-group">
                    <fieldset style="display: flex; flex-direction: column">
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">医院名称</label>
                                <div class="controls">
                                    <input type="text" class="form-control name" name="name">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">医院地址</label>
                                <div class="controls">
                                    <input type="text" class="form-control address" name="address">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">医院成就</label>
                                <div class="controls">
                                    <div class="checkbox">
                                        <label style="margin-right: 20px;">
                                            <input type="checkbox" value="5" class="grey badge" id="closeButton">
                                            五甲医院
                                        </label>
                                        <label style="margin-right: 20px;">
                                            <input type="checkbox" value="4" class="grey badge" id="closeButton">
                                            四甲医院
                                        </label>
                                        <label style="margin-right: 20px;">
                                            <input type="checkbox" value="3" class="grey badge" id="closeButton">
                                            三甲医院
                                        </label>
                                        <label style="margin-right: 20px;">
                                            <input type="checkbox" value="2" class="grey badge" id="closeButton">
                                            二甲医院
                                        </label>
                                        <label style="margin-right: 20px;">
                                            <input type="checkbox" value="1" class="grey badge" id="closeButton">
                                            一甲医院
                                        </label>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">医院logo</label>
                            </div>
                            <img  src="{{ empty($data->image) ? 'http://www.youbian.com/Images/201313/13653239592670.jpg' : $data->image }}" class="img-thumbnail img" style="width:30%;margin-bottom: 20px;" alt="">
                            <input type="hidden" class="imgUrl" value="">
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">医院介绍</label>

                                <div class="controls">
                                    <textarea name="introduce" id="" cols="30" rows="10" class="form-control  introduce" style=""></textarea>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div style="clear:both;"></div>
                <hr>
                <div class="pull-right">
                    <div class="btn-group">
                        <button class="btn btn-info save-note save-add" type="submit">
                            保存
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="editHospital" style="display:none">
        <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
            <form class="form-editHospital">
                <legend>编辑医院</legend>
                <div class="form-group">
                    <fieldset style="display: flex; flex-direction: column">
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">医院名称</label>
                                <div class="controls">
                                    <input type="text" class="form-control name" name="name">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">医院地址</label>
                                <div class="controls">
                                    <input type="text" class="form-control address" name="address">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">医院成就</label>
                                <div class="controls">
                                    <div class="checkbox">
                                        <label style="margin-right: 20px;">
                                            <input type="checkbox" value="5" class="grey badge" id="closeButton">
                                            五甲医院
                                        </label>
                                        <label style="margin-right: 20px;">
                                            <input type="checkbox" value="4" class="grey badge" id="closeButton">
                                            四甲医院
                                        </label>
                                        <label style="margin-right: 20px;">
                                            <input type="checkbox" value="3" class="grey badge" id="closeButton">
                                            三甲医院
                                        </label>
                                        <label style="margin-right: 20px;">
                                            <input type="checkbox" value="2" class="grey badge" id="closeButton">
                                            二甲医院
                                        </label>
                                        <label style="margin-right: 20px;">
                                            <input type="checkbox" value="1" class="grey badge" id="closeButton">
                                            一甲医院
                                        </label>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">医院logo</label>
                            </div>
                            <img  src="{{ empty($data->image) ? 'http://www.youbian.com/Images/201313/13653239592670.jpg' : $data->image }}" class="img-thumbnail img" data-type="1" style="width:30%;margin-bottom: 20px;" alt="">
                            <input type="hidden" class="imgUrl" value="">
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">医院介绍</label>

                                <div class="controls">
                                    <textarea name="introduce" id="" cols="30" rows="10" class="form-control  introduce" style=""></textarea>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div style="clear:both;"></div>
                <hr>
                <div class="pull-right">
                    <div class="btn-group">
                        <button class="btn btn-info save-note save-add" type="submit">
                            保存
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="addDoctor" style="display: none;">
        <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
            <form class="form-addDoctor">
                <legend>添加医生</legend>
                <div class="form-group">
                    <fieldset>
                        <div class="row col-lg-3 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">医生名称</label>
                                <div class="controls">
                                    <input type="text" class="form-control name" name="name">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-3 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">科室</label>
                                <div class="controls">
                                    <input type="text" class="form-control department" name="department">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-3 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">职位</label>
                                <div class="controls">
                                    <input type="text" class="form-control position" name="position">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-3 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">电话</label>
                                <div class="controls">
                                    <input type="text" class="form-control tel" name="tel">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-3 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">性别</label>
                                <div class="controls">
                                    <select name="sex" class="form-control sex">
                                        <option value="0">缺省</option>
                                        <option value="1">男</option>
                                        <option value="2">女</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">擅长</label>
                                <div class="controls">
                                    <textarea name="good" id="" class="form-control good"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">介绍</label>
                                <div class="controls">
                                    <textarea name="introduce" id="" class="form-control introduce"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">头像(点击图片进行修改)</label>
                            </div>
                            <img  src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/sfy/noHeadImg.jpg" class="img-thumbnail img" data-type="2" style="width:200px;margin-bottom: 20px;" alt="">
                            <input type="hidden" class="imgUrl" value="">
                        </div>
                    </fieldset>
                </div>
                <hr>
                <div class="pull-right">
                    <button type="button" class="btn btn-info close-subview-button">关闭</button>
                    <button class="btn btn-info save-note" type="submit">保存</button>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="publishModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">发布</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <style>
                    #clipArea {
                        width: 100%;
                        height: 400px;
                    }
                </style>
                <div class="modal-body">
                    <input type="file" name="file" class="form-control file" id="file">
                    <div class="photoClip">
                        <div id="clipArea"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="clipBtn">确定</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js-container')
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/hammer.min.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/iscroll-zoom-min.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/lrz.all.bundle.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/dist/Photoclip.min.js"></script>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=dUYryZ8iKFqQBiaK75fDMRey93Yer4pi"></script>
    <script src="{{ asset('/js/admin/hospital.js?time='.time()) }}"></script>
    <script>
        jQuery(document).ready(function (){
            Main.init();
            hospital.init();
            $('.th-inner').parent().css('text-align', 'center');
        });
    </script>
@endsection