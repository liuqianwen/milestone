@extends('base')
@section('head')
<link rel="stylesheet" type="text/css" href="/js/asset/simditor-2.3.19/styles/simditor.css" />
<style>
    .toolbars {
        border: 1px solid #ccc;
    }
    #editor {
        border-left: 1px solid #ccc;
        border-right: 1px solid #ccc;
        border-bottom: 1px solid #ccc;
        min-height: 300px;
        max-height: 500px;
        overflow: auto;
    }
    .help-block {
        color: rgba(219, 26, 26, 0.84);
    }
    .device {
        margin: auto;
        height: 736px;
        width: 414px;
        background: rgb(203, 203, 203);
        border-radius: 15px;
        box-shadow: 5px 6px 31px #ccc;
    }
    .iwindow,.iwindow1 {
        background: #fff;
        width: 100%;
        height: 100%;
        border-radius: 5px;
        padding: 15px 30px;
        overflow: auto;
    }
    .iwindow blockquote ,.iwindow1 blockquote{
        display: block;
        border-left: 8px solid #d0e5f2;
        padding: 5px 10px;
        margin: 10px 0;
        line-height: 1.4;
        font-size: 100%;
        background-color: #f1f1f1;
    }
    img{
        max-width: 100%;
    }
    .good{
        position: relative;
    }
    .delete{
        position: absolute;
        right:-20px;
        top:-20px;
    }

</style>
@endsection
@section('container')
    <div class="main-content">
        <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">Panel Configuration</h4>
                    </div>
                    <div class="modal-body">
                        Here will be a configuration form
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Close
                        </button>
                        <button type="button" class="btn btn-primary">
                            Save changes
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="toolbar row">
                <div class="col-sm-6 hidden-xs">
                    <div class="page-header">
                        <h2>厂家管理
                            <small></small>
                        </h2>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <a href="#" class="back-subviews"> <i class="fa fa-chevron-left"></i> 返回 </a>
                    <a href="#" class="close-subviews"> <i class="fa fa-times"></i> 关闭 </a>
                    <div class="toolbar-tools pull-right">
                        <ul class="nav navbar-right">
                            <li>
                                <a href="#newHospital" class="add-hospital"> <i class="fa fa-plus"></i> 添加厂家 </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/') }}">首页</a></li>
                        <li class="active">厂家管理</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-white">
                        <div class="panel-body">
                            <div id="toolbar">
                                <button type="button" class="btn btn-blue tooltips edit-hospital" >
                                    编辑厂家信息
                                </button>
                            </div>
                            <table id="hospitalTable">
                                <thead>
                                <tr>
                                    <th data-field="state" data-radio="true"></th>
                                    <th data-field="name" data-align="left">厂家名称</th>
                                    <th data-field="address" data-align="left">厂家地址</th>
                                    <th data-field="status" data-align="left">状态</th>
                                </tr>
                                </thead>
                            </table>
                            <div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('subview')
    <div id="newHospital" style="display:none">
        <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
            <form class="form-addhospital">
                <legend>添加厂家</legend>
                <div class="form-group">
                    <fieldset style="display: flex; flex-direction: column">
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">厂家名称</label>
                                <div class="controls">
                                    <input type="text" class="form-control name" name="name">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">厂家地址</label>
                                <div class="controls">
                                    <input type="text" class="form-control address" name="address">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">厂家logo</label>
                            </div>
                            <img  src="{{ empty($data->image) ? 'http://www.youbian.com/Images/201313/13653239592670.jpg' : $data->image }}" class="img-thumbnail img logoUrl" style="width:200px;margin-bottom: 20px;" alt="">
                            <input type="hidden" class="imgUrl logoVal" value="">
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">厂家介绍</label>

                                <div class="controls">
                                    <textarea name="introduce" id="" cols="20" rows="5" class="form-control  introduce" style=""></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">商品内容</label>
                                <div>
                                    <label class="radio-inline">
                                        <input type="radio" class="grey genre" value="0" name="gender" checked>
                                        无商品推荐或展示
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" class="grey genre" value="1" name="gender">
                                        有商品展示介绍
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" class="grey genre" value="2" name="gender">
                                        有商品推荐购买
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row showGoods col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1" style="display: none;">
                                <div class="col col-sm-12 col-xs-12 col-md-12 col-lg-8 col-xl-8">
                                    <div class="card">
                                        <div class="body">
                                            <div class="toolbars" id="toolbars"></div>
                                            <textarea id="editor" placeholder="输入内容" name="content" autofocus></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-sm-12 col-xs-12 col-md-12 col-lg-4 col-xl-4">
                                    <div class="card">
                                        <div class="body">
                                            <div class="device">
                                                <div class="iwindow">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        </div>
                        <div class="row recommend col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1" style="display: none;">
                                <div class="good" style="border: 1px solid #eeeeee;overflow: hidden;padding: 20px;">
                                    <div class="col col-sm-4 col-xs-12 col-md-3 col-lg-3 col-xl-3">
                                        <div class="form-group" >
                                            <label for="">商品主图</label>
                                        </div>
                                        <img  src="" class="img-thumbnail img" style="width:100%;margin-bottom: 20px;min-height: 100px" alt="">
                                        <input type="hidden" class="imgUrl" value="">
                                    </div>
                                    <div class="col col-sm-8 col-xs-12 col-md-9 col-lg-9 col-xl-9">
                                        <div class="form-group">
                                            <label>商品名称</label>
                                            <input type="text" class="form-control title"  name="title" value="">
                                        </div>
                                        <div class="form-group">
                                            <label>商品描述</label>
                                            <input type="text" class="form-control description" name="description" value="">
                                        </div>
                                        <div class="form-group">
                                            <label>商品购买链接</label>
                                            <input type="text" class="form-control url" name="url"  value="">
                                        </div>
                                        <i class="fa fa-times-circle fa-3x delete"></i>
                                    </div>
                                </div>
                                <a href="#" class="addGood btn btn-xs btn-blue tooltips pull-right" data-placement="top" data-original-title="添加商品" style="margin-top: 20px;"><i class="fa fa-plus fa-2x"></i></a>
                            </div>
                    </fieldset>
                </div>
                <div style="clear:both;"></div>
                <hr>
                <div class="pull-right">
                    <div class="btn-group">
                        <button class="btn btn-info save-note save-add" type="submit">
                            保存
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="editHospital" style="display:none">
        <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
            <form class="form-editHospital">
                <legend>编辑厂家</legend>
                <div class="form-group">
                    <fieldset style="display: flex; flex-direction: column">
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">厂家名称</label>
                                <div class="controls">
                                    <input type="text" class="form-control name" name="name">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">厂家地址</label>
                                <div class="controls">
                                    <input type="text" class="form-control address" name="address">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">厂家logo</label>
                            </div>
                            <img  src="{{ empty($data->image) ? 'http://www.youbian.com/Images/201313/13653239592670.jpg' : $data->image }}" class="img-thumbnail img logoUrl" style="width:200px;margin-bottom: 20px;" alt="">
                            <input type="hidden" class="imgUrl logoVal" value="">
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">厂家介绍</label>

                                <div class="controls">
                                    <textarea name="introduce" id="" cols="20" rows="5" class="form-control  introduce" style=""></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">商品内容</label>
                                <div>
                                    <label class="radio-inline">
                                        <input type="radio" class="grey genre" value="0" name="gender1" >
                                        无商品推荐或展示
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" class="grey genre" value="1" name="gender1">
                                        有商品展示介绍
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" class="grey genre" value="2" name="gender1">
                                        有商品推荐购买
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row showGoods col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1" style="display: none;">
                            <div class="col col-sm-12 col-xs-12 col-md-12 col-lg-8 col-xl-8">
                                <div class="card">
                                    <div class="body">
                                        <div class="toolbars" id="toolbars"></div>
                                        <textarea id="editor1" placeholder="输入内容" name="content" autofocus></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-sm-12 col-xs-12 col-md-12 col-lg-4 col-xl-4">
                                <div class="card">
                                    <div class="body">
                                        <div class="device">
                                            <div class="iwindow1">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row recommend col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-md-offset-1" style="display: none;">
                            <div class="good" style="border: 1px solid #eeeeee;overflow: hidden;padding: 20px;">
                                <div class="col col-sm-4 col-xs-12 col-md-3 col-lg-3 col-xl-3">
                                    <div class="form-group" >
                                        <label for="">商品主图</label>
                                    </div>
                                    <img  src="" class="img-thumbnail img" style="width:100%;margin-bottom: 20px;min-height: 100px" alt="">
                                    <input type="hidden" class="imgUrl" value="">
                                </div>
                                <div class="col col-sm-8 col-xs-12 col-md-9 col-lg-9 col-xl-9">
                                    <div class="form-group">
                                        <label>商品名称</label>
                                        <input type="text" class="form-control title"  name="title" value="">
                                    </div>
                                    <div class="form-group">
                                        <label>商品描述</label>
                                        <input type="text" class="form-control description" name="description" value="">
                                    </div>
                                    <div class="form-group">
                                        <label>商品购买链接</label>
                                        <input type="text" class="form-control url" name="url"  value="">
                                    </div>
                                    <i class="fa fa-times-circle fa-3x delete"></i>
                                </div>
                            </div>
                            <a href="#" class="addGood btn btn-xs btn-blue tooltips pull-right" data-placement="top" data-original-title="添加商品" style="margin-top: 20px;"><i class="fa fa-plus fa-2x"></i></a>
                        </div>
                    </fieldset>
                </div>
                <div style="clear:both;"></div>
                <hr>
                <div class="pull-right">
                    <div class="btn-group">
                        <button class="btn btn-info save-note save-add" type="submit">
                            保存
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="publishModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">发布</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <style>
                    #clipArea {
                        width: 100%;
                        height: 400px;
                    }
                </style>
                <div class="modal-body">
                    <input type="file" name="file" class="form-control file" id="file">
                    <div class="photoClip">
                        <div id="clipArea"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="clipBtn">确定</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js-container')
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/hammer.min.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/iscroll-zoom-min.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/lrz.all.bundle.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/dist/Photoclip.min.js"></script>
    <script type="text/javascript" src="/js/asset/editor/js/module.js"></script>
    <script type="text/javascript" src="/js/asset/editor/js/hotkeys.js"></script>
    <script type="text/javascript" src="/js/asset/editor/js/uploader.js"></script>
    <script type="text/javascript" src="/js/asset/editor/js/simditor.js"></script>
    <script type="text/javascript" src="{{ asset('/js/asset/editor/simditor-html-master/vendor/bower/js-beautify/js/lib/beautify-html.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/asset/editor/simditor-html-master/lib/simditor-html.js') }}"></script>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=dUYryZ8iKFqQBiaK75fDMRey93Yer4pi"></script>
    <script src="{{ asset('/js/admin/vender.js?time='.time()) }}"></script>
    <script>
        jQuery(document).ready(function (){
            Main.init();
            vender.init();
            $('.th-inner').parent().css('text-align', 'center');
        });
    </script>
@endsection