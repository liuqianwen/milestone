@extends('base')
@section('head')
    <link rel="stylesheet" type="text/css" href="/js/asset/simditor-2.3.19/styles/simditor.css" />
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jQuery-Tags-Input/jquery.tagsinput.css">
    <style>
        #_editUser, #_addUser, #_editPassword {
            display: none;
        }

        #_userTable > thead > tr > th {
            text-align:center !important;
        }

        .toolbars {
            border: 1px solid #ccc;
        }
        #editor {
            border-left: 1px solid #ccc;
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            min-height: 300px;
            max-height: 500px;
            overflow: auto;
        }
        .help-block {
            color: rgba(219, 26, 26, 0.84);
        }
        .device {
            margin: auto;
            height: 736px;
            width: 414px;
            background: rgb(203, 203, 203);
            border-radius: 15px;
            box-shadow: 5px 6px 31px #ccc;
        }
        .iwindow,.iwindow1 {
            background: #fff;
            width: 100%;
            height: 100%;
            border-radius: 5px;
            padding: 15px 30px;
            overflow: auto;
        }
        .iwindow blockquote ,.iwindow1 blockquote{
            display: block;
            border-left: 8px solid #d0e5f2;
            padding: 5px 10px;
            margin: 10px 0;
            line-height: 1.4;
            font-size: 100%;
            background-color: #f1f1f1;
        }
        img{
            max-width: 100%;
        }
        .cure-config-body div {
            display: flex;
            flex-flow: row wrap;
            justify-content: flex-start;
            align-items: flex-start;
            margin-bottom: 15px;
        }

        .cure-config-body div > div {
            line-height: 1.5em;
            padding: 5px 10px 5px 10px;
            background: #cccccc;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            margin-left: 5px;
            margin-bottom: 5px;
        }

        .cure-config-body div > .current {
            background: #30c778;
            color: #fff;
        }

        .cure-config-body div > .plus {
            background: #fff;
        }
        .departments-config-tag {
            display: flex;
            flex-flow: row wrap;
            border: 1px solid #ccc;
            padding: 15px 15px 10px 15px;
            border-radius: 10px;
        }
        #tags_1_tag,#tags_2_tag{
            min-width: 200px;
            border: none;
            background: #fff;
        }
        #tags_1_tagsinput,#tags_2_tagsinput{
            border: none !important;
        }
        .device{
            position: fixed;
            height: 75vh;
        }
        .current {
            background: #30c778;
            color: #fff;
        }
        .simditor-body{
            height: 30vh !important;
            overflow: scroll;
        }
    </style>
@endsection
@section('container')
    <div class="toolbar row">
        <div class="col-sm-6 hidden-xs">
            <div class="page-header">
                <h2>
                    文章管理
                    <small></small>
                </h2>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <a href="#" class="back-subviews">
                <i class="fa fa-chevron-left"></i> 返回
            </a>
            <a href="#" class="close-subviews">
                <i class="fa fa-times"></i> 关闭
            </a>
            <div class="toolbar-tools pull-right">
                <ul class="nav navbar-right">
                    <li>
                        <a href="javascript:;" class="user-add">
                            <i class="fa fa-plus"></i>添加
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">首页</a></li>
                <li class="active">文章管理</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div id="toolbar">
                        <button class="btn btn-blue user-edit">编辑</button>
                        <button class="btn btn-blue user-top">置顶</button>
                        <button  class="btn btn-blue  unavailable">
                            禁用
                        </button>
                        <button  class="btn btn-blue available">
                            启用
                        </button>
                    </div>
                    <table id="_userTable">
                        <thead>
                        <tr>
                            <th data-field="state" data-radio="true"></th>
                            <th data-field="name" data-align="left">标题</th>
                            <th data-field="description" data-align="left">描述</th>
                            <th data-field="status" data-align="center">状态</th>
                            <th data-field="top" data-align="center">排序</th>
                            <th data-field="createTime" data-align="center">创建时间</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('subview')
    <div id="_addUser" class="padding-15">
        <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
            <div class="row clearfix">
                <div class="col col-sm-12 col-xs-12 col-md-12 col-lg-8 col-xl-8">
                    <div class="card">
                        <div class="body">
                            <form id="addInformation">
                                <div class="form-group">
                                    <label>标题</label>
                                    <input type="text" class="form-control title" placeholder="标题" name="title" value="">
                                </div>
                                <div class="form-group">
                                    <label>详细说明</label>
                                    <input type="text" class="form-control description" name="description" placeholder="详细说明" value="">
                                </div>
                                <div class="form-group">
                                    <label>标签</label>
                                    <div class="cure-config-body">
                                        <div>
                                            @foreach($label as $v)
                                                <div class="system-malady" data-id="{{$v->_id}}">{{$v->name}}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                {{--<div class="form-group">--}}
                                {{--<label>可自定义标签</label>--}}
                                {{--<div class="cure-custon-body">--}}
                                {{--<div>--}}
                                {{--<div class="departments-config-tag">--}}
                                {{--<input id="tags_1" type="text" class="tags form-control" value="" style="display: none;">--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                <div class="form-group" >
                                    <label for="">封面图</label>
                                </div>
                                <img  src="{{ empty($data->image) ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : $data->image }}" class="img-thumbnail img" style="width:40%;margin-bottom: 20px;" alt="">
                                <input type="hidden" class="imgUrl" value="">
                                <div class="toolbars" id="toolbars"></div>
                                <textarea id="editor" placeholder="输入内容" autofocus></textarea>

                                <hr>
                                <button type="submit" class="btn btn-primary pull-right">提交</button>
                                <input type="hidden" class="id" name="id" value="">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col col-sm-12 col-xs-12 col-md-12 col-lg-4 col-xl-4">
                    <div class="card">
                        <div class="body">
                            <div class="device">
                                <div class="iwindow">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="_editUser" class="padding-15">
        <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
            <div class="row clearfix">
                <div class="col col-sm-12 col-xs-12 col-md-12 col-lg-8 col-xl-8">
                    <div class="card">
                        <div class="body">
                            <form id="editInformation">
                                <div class="form-group">
                                    <label>标题</label>
                                    <input type="text" class="form-control title" placeholder="标题" name="title" value="">
                                </div>
                                <div class="form-group">
                                    <label>详细说明</label>
                                    <input type="text" class="form-control description" name="description" placeholder="详细说明" value="">
                                </div>
                                <div class="form-group">
                                    <label>标签</label>
                                    <div class="cure-config-body">
                                        <div>
                                            @foreach($label as $v)
                                                <div class="system-malady" data-id="{{$v->_id}}">{{$v->name}}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                {{--<div class="form-group">--}}
                                {{--<label>可自定义标签</label>--}}
                                {{--<div class="cure-custon-body">--}}
                                {{--<div>--}}
                                {{--<div class="departments-config-tag">--}}
                                {{--<input id="tags_2" type="text" class="tags form-control" value="" style="display: none;">--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                <div class="form-group" >
                                    <label for="">封面图</label>
                                </div>
                                <img  src="{{ empty($data->image) ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : $data->image }}" class="img-thumbnail img" style="width:40%;margin-bottom: 20px;" alt="">
                                <input type="hidden" class="imgUrl" value="">
                                <div class="toolbars" id="toolbars"></div>
                                <textarea id="editor1" placeholder="输入内容" autofocus></textarea>

                                <hr>
                                <button type="submit" class="btn btn-primary pull-right">提交</button>
                                <input type="hidden" class="id" name="id" value="">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col col-sm-12 col-xs-12 col-md-12 col-lg-4 col-xl-4">
                    <div class="card">
                        <div class="body">
                            <div class="device">
                                <div class="iwindow1">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="publishModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">发布</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <style>
                    #clipArea {
                        width: 100%;
                        height: 400px;
                    }
                </style>
                <div class="modal-body">
                    <input type="file" name="file" class="form-control file" id="file">
                    <div class="photoClip">
                        <div id="clipArea"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="clipBtn">确定</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js-container')
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/hammer.min.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/iscroll-zoom-min.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/lrz.all.bundle.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/dist/Photoclip.min.js"></script>
    <script type="text/javascript" src="/js/asset/editor/js/module.js"></script>
    <script type="text/javascript" src="/js/asset/editor/js/hotkeys.js"></script>
    <script type="text/javascript" src="/js/asset/editor/js/uploader.js"></script>
    <script type="text/javascript" src="/js/asset/editor/js/simditor.js"></script>
    <script type="text/javascript" src="{{ asset('/js/asset/editor/simditor-html-master/vendor/bower/js-beautify/js/lib/beautify-html.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/asset/editor/simditor-html-master/lib/simditor-html.js') }}"></script>
    <script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
    <script src="/js/admin/article.js"></script>
    <script>
        $(function () {
            Main.init();
            article.init();
            let html = '{!! $data->content or '' !!}';
//            article.e().txt.html(html);
        })
    </script>
@endsection