@extends('base')
@section('head')
    <style>
        .select-option input {
            margin-bottom: 10px;
        }
        #information_table {
            table-layout: fixed;
        }
        #information_table tbody tr td {
            word-break: keep-all;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
    </style>
@endsection
@section('container')
    <div class="toolbar row">
        <div class="col-sm-6 hidden-xs">
            <div class="page-header">
                <h2>数据常量
                    <small></small>
                </h2>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <a href="#" class="back-subviews">
                <i class="fa fa-chevron-left"></i> 返回
            </a>
            <a href="#" class="close-subviews">
                <i class="fa fa-times"></i> 关闭
            </a>

            <div class="toolbar-tools pull-right">
                <ul class="nav navbar-right">
                    <li>
                        <a href="#newinformation" class="new-information"> <i class="fa fa-plus"></i> 添加常量 </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">首页</a></li>
                <li class="active">数据常量</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div id="toolbar" style="display:flex; flex-flow: row nowrap; justify-content: flex-start; align-items: center; ">
                        <button type="button" class="btn btn-blue tooltips edit-information">
                            详情
                        </button>
                        <p style="margin-left:15px; margin-bottom:0px; ">说明：用于获取用户的信息，如姓名、性别、出生年月等</p>
                    </div>
                    <table id="information_table">
                        <thead>
                        <tr>
                            <th data-field="state" data-radio="true"></th>
                            <th data-field="name" data-align="left">常量</th>
                            <th data-field="type" data-align="center">类型</th>
                            <th data-field="statusI" data-align="center">状态</th>
                        </tr>
                        </thead>
                    </table>
                    <div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('subview')
    <div id="newinformation" style="display:none">
        <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
            <form class="form-addinformation">
                <legend>添加常量</legend>
                <div class="form-group">
                    <fieldset>
                        <div class="row col-lg-3 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label"> 类型 </label>
                                <select name="type" class="form-control type">
                                    <option value="0">填空</option>
                                    <option value="1">下拉</option>
                                    <option value="2">日期</option>
                                </select>
                            </div>
                        </div>
                        <div class="row col-lg-9 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">名称</label>
                                <div class="controls">
                                    <input type="text" class="form-control name" name="name">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-9 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1" style="display:none;">
                            <div class="form-group">
                                <label class="control-label">下拉选项</label>
                                <div class="controls xuanxiang" style="">
                                    <input type="text" class="form-control option" name="option" value="" placeholder="请填写选项">
                                    <input type="text" class="form-control option" name="option" value="" placeholder="请填写选项" style="margin-top: 10px;">
                                    <button class="btn btn-info addxuanxiang" type="button" style="margin-top: 10px;">
                                        新增选项
                                    </button>
                                </div>
                            </div>
                        </div>

                    </fieldset>
                </div>
                <hr>
                <div class="pull-right">
                    <div class="btn-group">
                        <a href="#" class="btn btn-info close-subview-button"> 关闭 </a>
                    </div>
                    <div class="btn-group">
                        <button class="btn btn-info addinformation" type="button">
                            保存
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="editinformation" style="display:none">
        <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
            <form class="form-editinformation">
                <legend>常量详情</legend>
                <div class="form-group">
                    <fieldset>
                        <div class="row col-lg-3 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">类型 </label>
                                <select name="type" class="form-control type" disabled>
                                    <option value="0">填空</option>
                                    <option value="1">下拉</option>
                                    <option value="2">日期</option>
                                </select>
                            </div>
                        </div>
                        <div class="row col-lg-9 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">名称</label>
                                <div class="controls">
                                    <input type="text" class="form-control name" name="name" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-9 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">下拉选项</label>
                                <div class="controls xuanxiang" style="">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-3 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                            <div class="form-group">
                                <label class="control-label">状态 </label>
                                <select name="type" class="form-control status">
                                    <option value="0">停用</option>
                                    <option value="1">启用</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <hr>
                <div class="pull-right">
                    <div class="btn-group">
                        <a href="#" class="btn btn-info close-subview-button"> 关闭 </a>
                    </div>
                    <div class="btn-group">
                        <button class="btn btn-info editinformation" type="button">
                            保存
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js-container')
    <script src="{{ asset('/js/admin/follow_up/information.js') }}"></script>
    <script>
        jQuery(document).ready(function (){
            Main.init();
            FormElements.init();
            information.init();
        });
    </script>
@endsection