@extends('base')
@section('head')
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/select2/select2.css">
    <link rel="stylesheet" href="/js/plugins/survey2/survey2.css">
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jQuery-autoComplete-master/jquery.auto-complete.css">
    <style>
        .question-option input {
            margin-bottom: 10px;
        }
        .score input {
            margin-bottom: 10px;
        }
        #survey_table {
            table-layout: fixed;
        }
        /*#survey_table tbody tr td {*/
        /*word-break: keep-all;*/
        /*white-space: nowrap;*/
        /*overflow: hidden;*/
        /*text-overflow: ellipsis;*/
        /*}*/

        #toolbar {
            display: flex;
            flex-flow: row nowrap;
        }
        #toolbar > * {
            margin-right: 5px;
        }
    </style>
@endsection
@section('container')
    <div class="toolbar row">
        <div class="col-sm-6 hidden-xs">
            <div class="page-header">
                <h2>问卷管理
                    <small></small>
                </h2>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <a href="#" class="back-subviews">
                <i class="fa fa-chevron-left"></i>
                返回
            </a>
            <a href="#" class="close-subviews">
                <i class="fa fa-times"></i>
                关闭
            </a>
            <div class="toolbar-tools pull-right">
                <ul class="nav navbar-right">
                    <li>
                        <a href="javascript:;" class="add-survey">
                            <i class="fa fa-plus"></i>
                            添加问卷
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">首页</a>
                </li>
                <li class="active">问卷管理</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div id="toolbar">
                        <button type="button" class="btn btn-blue tooltips edit-survey" disabled>
                            编辑
                        </button>
                        <button type="button" class="btn btn-blue tooltips preview-survey" disabled>
                            预览
                        </button>
                    </div>
                    <table id="survey_table">
                        <thead>
                        <tr>
                            <th data-field="state" data-radio="true"></th>
                            <th data-field="title" data-align="left">标题</th>
                            <th data-field="type" data-align="center">类型</th>
                            <th data-field="status" data-align="center">状态</th>
                        </tr>
                        </thead>
                    </table>
                    <div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('subview')
    <div id="AddSurvey" style="display: none;"></div>
    <div id="PreviewSurvey" style="display: none;">
        <div class="row">
            <div class="col-lg-6" style="text-align: center;padding-top: 100px;">
                <iframe src="" class="preview-iframe" height="667" width="375" frameborder="0" style="border:1px solid #ccc;box-shadow: 0 2px 39px 11px;"></iframe>
            </div>
            <div class="col-lg-6" style="text-align: center;padding-top: 267px;">
                <img src="" class="preview-image" style="width: 333px;" alt="">
            </div>
        </div>
    </div>
@endsection
@section('js-container')
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/select2/select2.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jQuery-autoComplete-master/jquery.auto-complete.min.js"></script>
    {{--<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/survey2/v1.0.0/js/survey2.js"></script>--}}
    <script src="{{ asset('/js/plugins/survey2/survey3.js') }}"></script>
    <script src="{{ asset('/js/admin/follow_up/survey.js') }}"></script>
    <script>
        jQuery(document).ready(function (){
            Main.init();
            FormElements.init();
            survey.init();
        });
    </script>
@endsection