@extends('wxbase')
@section('head')
    <title>问卷调查</title>
    <link rel="stylesheet" href="/assets/plugins/iCheck/skins/all.css">
    <link rel="stylesheet"
          href="/assets/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css">
    <link rel="stylesheet"
          href="/assets/plugins/mobisscroll/css/mobiscroll.custom-3.0.0-beta2.min.css">
    <link rel="stylesheet"
          href="/assets/plugins/sweetalert/lib/sweet-alert.css"/>
    <link rel="stylesheet" href="/assets/plugins/fontawesome/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ '/js/plugins/survey2/'. $survey2_type .'/survey2.css?time='.time() }}">
@endsection
@section('container')
    <div class="wrapper"></div>
@endsection
@section('js-container')
    <script src="/assets/plugins/iCheck/jquery.icheck.min.js"></script>
    <script src="/assets/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <script src="/assets/plugins/sweetalert/lib/sweet-alert.min.js"></script>
    <script src="/assets/plugins/mobisscroll/js/mobiscroll.custom-3.0.0-beta2.min.js"></script>
    <script src="/assets/plugins/layui-v2.3.0/layui/layui.all.js"></script>
    <script src="/js/plugins/survey2//survey2.js?time={{ time() }}"></script>
    <script src="{{ '/js/plugins/survey2/'. $survey2_type .'/survey2.js?time='.time() }}"></script>
    <script>
        jQuery(document).ready(function () {
            $('.wrapper').survey2({
                data : "{{ $data }}",
                save : {
                    active:true,
                    id : "{{ $surveyRecord }}",
                    url : '/preview/save'
                }
            });
        });
    </script>
@endsection