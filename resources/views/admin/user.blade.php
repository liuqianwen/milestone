@extends('base')
@section('head')
    <style>
        #_editUser, #_addUser, #_editPassword {
            display: none;
        }

        #_userTable > thead > tr > th {
            text-align:center !important;
        }
    </style>
@endsection
@section('container')
    <div class="toolbar row">
        <div class="col-sm-6 hidden-xs">
            <div class="page-header">
                <h2>
                    管理员管理
                    <small></small>
                </h2>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <a href="#" class="back-subviews">
                <i class="fa fa-chevron-left"></i> 返回
            </a>
            <a href="#" class="close-subviews">
                <i class="fa fa-times"></i> 关闭
            </a>
            <div class="toolbar-tools pull-right">
                <ul class="nav navbar-right">
                    <li>
                        <a href="javascript:;" class="user-add">
                            <i class="fa fa-plus"></i>添加
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">首页</a></li>
                <li class="active">管理员管理</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div id="toolbar">
                        <button class="btn btn-blue user-edit">修改</button>
                        <button class="btn btn-blue user-edit-password">修改密码</button>
                    </div>
                    <table id="_userTable">
                        <thead>
                        <tr>
                            <th data-field="state" data-radio="true"></th>
                            <th data-field="email" data-align="left">登录名</th>
                            <th data-field="name" data-align="left">用户姓名</th>
                            <th data-field="is_active" data-align="center">状态</th>
                            <th data-field="createTime" data-align="center">创建时间</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('subview')
    <div id="_editUser" class="padding-15">
        <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
            <form class="form-editUser">
                <div class="form-group">
                    <fieldset>
                        <div class="row col-lg-8 col-md-8 col-sm-8 col-xs-8 col-xs-offset-2">
                            <div class="form-group">
                                <label class="control-label">姓名</label>
                                <div class="controls">
                                    <input type="text" class="form-control name" name="name">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <hr>
                <div class="pull-right">
                    <a href="#" class="btn btn-info close-subview-button">关闭</a>
                    <button class="btn btn-info save-note" type="submit">保存</button>
                </div>
            </form>
        </div>
    </div>
    <div id="_addUser" class="padding-15">
        <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
            <form class="form-addUser">
                <div class="form-group">
                    <fieldset>
                        <div class="row col-lg-8 col-md-8 col-sm-8 col-xs-8 col-xs-offset-2">
                            <div class="form-group">
                                <label class="control-label">姓名</label>
                                <div class="controls">
                                    <input type="text" class="form-control name" name="name">
                                </div>
                            </div>
                        </div>
                        <div class="row col-lg-8 col-md-8 col-sm-8 col-xs-8 col-xs-offset-2">
                            <div class="form-group">
                                <label class="control-label">登录名</label>
                                <div class="controls">
                                    <input type="email" class="form-control email" name="email">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <hr>
                <div class="pull-right">
                    <a href="#" class="btn btn-info close-subview-button">关闭</a>
                    <button class="btn btn-info save-note" type="submit">保存</button>
                </div>
            </form>
        </div>
    </div>
    <div id="_editPassword" class="padding-15">
        <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
            <form class="form-editPassword">
                <div class="form-group">
                    <fieldset>
                        <div class="row col-lg-8 col-md-8 col-sm-8 col-xs-8 col-xs-offset-2">
                            <div class="form-group">
                                <label class="control-label">密码</label>
                                <div class="controls">
                                    <input type="password" class="form-control password" name="password">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <hr>
                <div class="pull-right">
                    <a href="#" class="btn btn-info close-subview-button">关闭</a>
                    <button class="btn btn-info save-note" type="submit">保存</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js-container')
    <script src="/js/admin/user.js"></script>
    <script>
        $(function () {
            Main.init();
            user.init();
        })
    </script>
@endsection