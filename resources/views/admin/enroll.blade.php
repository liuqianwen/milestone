@extends('base')
@section('head')
    <link rel="stylesheet" type="text/css" href="/js/asset/simditor-2.3.19/styles/simditor.css" />
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jQuery-Tags-Input/jquery.tagsinput.css">
    <link rel="stylesheet" href="https://dangjian.hplyy.com/assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css">
    <style>
        #_editUser, #_addUser, #_editPassword {
            display: none;
        }

        #_userTable > thead > tr > th {
            text-align:center !important;
        }

        .toolbars {
            border: 1px solid #ccc;
        }
        #editor {
            border-left: 1px solid #ccc;
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            min-height: 300px;
            max-height: 500px;
            overflow: auto;
        }
        .help-block {
            color: rgba(219, 26, 26, 0.84);
        }
        .device {
            margin: auto;
            height: 736px;
            width: 414px;
            background: rgb(203, 203, 203);
            border-radius: 15px;
            box-shadow: 5px 6px 31px #ccc;
        }
        .iwindow,.iwindow1 {
            background: #fff;
            width: 100%;
            height: 100%;
            border-radius: 5px;
            padding: 15px 30px;
            overflow: auto;
        }
        .iwindow blockquote ,.iwindow1 blockquote{
            display: block;
            border-left: 8px solid #d0e5f2;
            padding: 5px 10px;
            margin: 10px 0;
            line-height: 1.4;
            font-size: 100%;
            background-color: #f1f1f1;
        }
        img{
            max-width: 100%;
        }
        .cure-config-body div {
            display: flex;
            flex-flow: row wrap;
            justify-content: flex-start;
            align-items: flex-start;
            margin-bottom: 15px;
        }

        .cure-config-body div > div {
            line-height: 1.5em;
            padding: 5px 10px 5px 10px;
            background: #cccccc;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            margin-left: 5px;
            margin-bottom: 5px;
        }

        .cure-config-body div > .current {
            background: #30c778;
            color: #fff;
        }

        .cure-config-body div > .plus {
            background: #fff;
        }
        .departments-config-tag {
            display: flex;
            flex-flow: row wrap;
            border: 1px solid #ccc;
            padding: 15px 15px 10px 15px;
            border-radius: 10px;
        }
        #tags_1_tag,#tags_2_tag{
           min-width: 200px;
            border: none;
            background: #fff;
        }
        #tags_1_tagsinput,#tags_2_tagsinput{
            border: none !important;
        }
        .device{
            position: fixed;
            height: 65vh;
        }
        .current {
            background: #30c778;
            color: #fff;
        }
        a.list-group-item {
            height: 40px;
        }
        ul li{list-style: none;}
        .user-list{
            overflow-x: scroll;
        }
        #order-table tr td:last-child {
            display: flex;
            align-items: center;
        }
        td .ticket-confirm {
            margin-left: 10px;
        }
        #order-table td {
            height: 80px;
        }
    </style>
@endsection
@section('container')
    <div class="toolbar row">
        <div class="col-sm-6 hidden-xs">
            <div class="page-header">
                <h2>
                    活动报名管理
                    <small></small>
                </h2>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <a href="#" class="back-subviews">
                <i class="fa fa-chevron-left"></i> 返回
            </a>
            <a href="#" class="close-subviews">
                <i class="fa fa-times"></i> 关闭
            </a>
            <div class="toolbar-tools pull-right">
                <ul class="nav navbar-right">
                    <li>
                        <a href="javascript:;" class="user-add">
                            <i class="fa fa-plus"></i>添加
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">首页</a></li>
                <li class="active">活动报名管理</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div id="toolbar">
                        <button class="btn btn-blue export-enroll">导出报名表</button>
                        <button class="btn btn-blue export-order">导出订单</button>
                        <button  class="btn btn-blue  unavailable">
                            禁用
                        </button>
                        <button  class="btn btn-blue available">
                            启用
                        </button>
                    </div>
                    <table id="_userTable">
                        <thead>
                        <tr>
                            <th data-field="state" data-radio="true"></th>
                            <th data-field="name" data-align="left">标题</th>
                            {{--<th data-field="description" data-align="left">描述</th>--}}
                            <th data-field="price" data-align="right">报名费用（元）</th>
                            <th data-field="user" data-align="center">报名数</th>
                            <th data-field="order" data-align="center">订单数</th>
                            <th data-field="confirm_order" data-align="center">待确认订单数</th>
                            <th data-field="status" data-align="center">状态</th>
                            <th data-field="createTime" data-align="center">创建时间</th>
                            <th data-field="caozuo" data-align="center">操作</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('subview')
    <div id="_addUser" class="padding-15">
        <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
            <div class="row">
            <div class="col-sm-12">
                <!-- start: FORM WIZARD PANEL -->
                <div class="panel panel-white" style="box-shadow: none;!important;">
                    <div class="panel-body">
                            <form action="#" role="form" class="smart-wizard form-horizontal" id="form">
                                <div id="wizard" class="swMain">
                                    <ul>
                                        <li>
                                            <a href="#step-1">
                                                <div class="stepNumber">
                                                    1
                                                </div>
                                                <span class="stepDesc"> 活动内容
																<br />
																<small></small> </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#step-2">
                                                <div class="stepNumber">
                                                    2
                                                </div>
                                                <span class="stepDesc"> 报名表单设计
																<br />
																<small></small> </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#step-3">
                                                <div class="stepNumber">
                                                    3
                                                </div>
                                                <span class="stepDesc"> 完成
																<br />
																<small></small> </span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="progress progress-xs transparent-black no-radius active">
                                        <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar partition-green step-bar">
                                            <span class="sr-only"> 0% Complete (success)</span>
                                        </div>
                                    </div>
                                    <div id="step-1">
                                        <h2 class="StepTitle"></h2>
                                        <div class="col col-sm-12 col-xs-12 col-md-12 col-lg-8 col-xl-8">
                                            <div class="form-group">
                                                <label>标题</label>
                                                <input type="text" class="form-control title"  name="title" value="">
                                            </div>
                                            <div class="form-group">
                                                <label>详细说明</label>
                                                <input type="text" class="form-control description" name="description"  value="">
                                            </div>
                                            <div class="form-group" >
                                                <label for="">封面图</label>
                                            </div>
                                            <img  src="{{ empty($data->image) ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : $data->image }}" class="img-thumbnail img" style="width:40%;margin-bottom: 20px;" alt="">
                                            <input type="hidden" class="imgUrl" value="">
                                            <div class="toolbars" id="toolbars"></div>
                                            <textarea id="editor" placeholder="输入内容" autofocus></textarea>
                                            <hr>
                                            <input type="hidden" class="enroll_id" name="enroll_id" id="enroll_id" value="">
                                            <div class="form-group" style="margin-top: 40px;">
                                                <div class="" style="text-align: right">
                                                    <button class="btn btn-blue next-step btn-block" style="width: 80px;float: right;margin-right: 30px">
                                                        下一步 <i class="fa fa-arrow-circle-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col-sm-12 col-xs-12 col-md-12 col-lg-4 col-xl-4">
                                            <div class="card">
                                                <div class="body">
                                                    <div class="device">
                                                        <div class="iwindow">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="step-2">
                                        {{--<h2 class="StepTitle">商品促销策略维护</h2>--}}
                                        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 99999;">
                                            <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1">
                                                <div class="row col-lg-3 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                                                    <div class="form-group">
                                                        <label>是否有报名费</label>
                                                        <select name="is_pay" class="form-control is_pay" id="">
                                                            <option value="0">否</option>
                                                            <option value="1">是</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row col-lg-3 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                                                    <div class="form-group">
                                                        <label>报名费金额</label>
                                                        <input type="number" class="form-control pay_price" name="pay_price" min="0">
                                                    </div>
                                                </div>
                                                <div class="row col-lg-3 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                                                    <div class="form-group">
                                                        <label>支付方式</label>
                                                        <select name="pay_type" class="form-control pay_type" id="">
                                                            <option value="1">仅微信支付</option>
                                                            <option value="2">仅对公打款</option>
                                                            <option value="3">微信支付和对公打款</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-offset-1">
                                                    <div class="form-group" style="margin-top: 20px">
                                                    <label for="">报名内容</label>
                                                    <table class="table uio-product">
                                                        <thead>
                                                        <tr>
                                                            <td>类型</td>
                                                            <td>字段名称</td>
                                                            <td class="change-tr">内容最长字数/选项</td>
                                                            <td>是否必填</td>
                                                            <td>操作</td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="additional-sales-info">
                                                            <td>
                                                                <select name="form_type" class="form-control form_type">
                                                                    <option value="1">文本框</option>
                                                                    <option value="2">下拉框</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <input type="text" value="" name="name" class="form-control name" placeholder="">
                                                            </td>
                                                            <td  style="width: 10em;">
                                                                <input type="number" value="5" name="length" class="form-control length" placeholder="最长字数,如英文数字一个算0.5字数">
                                                            </td>
                                                            <td style="display: none;">
                                                                <input type="text" name="option" class="form-control option" placeholder="选项之间请以&分隔">
                                                            </td>
                                                            <td>
                                                                <select name="is_must" class="form-control is_must">
                                                                    <option value="0">不必填</option>
                                                                    <option value="1" selected>必填</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <button class="btn btn-xs btn-info uio-product-add" type="button">增</button>
                                                                <button class="btn btn-xs btn-danger uio-product-delete" type="button">删</button>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                        <tfoot>
                                                    </table>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="">
                                            <div class="col-sm-2 col-sm-offset-3" style="margin-top: 40px;">
                                                <button class="btn btn-light-grey back-step btn-block">
                                                    <i class="fa fa-circle-arrow-left"></i> 上一步
                                                </button>
                                            </div>
                                            <div class="col-sm-2 col-sm-offset-3" style="margin-top: 40px;">
                                                <button class="btn btn-blue next-step btn-block" type="button">
                                                    确定发布 <i class="fa fa-arrow-circle-right"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="step-3">

                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <div id="_editUser" class="padding-15">
        <div class="noteWrap col-md-12 col-xs-12 ">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs tab-padding tab-space-3 tab-blue">
                        <li class="active">
                            <a href="#myTab2_order" data-toggle="tab">
                                订单列表
                            </a>
                        </li>
                        <li class="">
                            <a href="#myTab2_untreated" data-toggle="tab">
                                活动内容
                            </a>
                        </li>
                        <li>
                            <a href="#myTab2_unfinished" data-toggle="tab">
                                报名表单
                            </a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="myTab2_order" style="overflow: scroll">
                            <table id="order-table" class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                <tr>
                                    {{--<th data-field="state" data-radio="true"></th>--}}
                                    <th data-field="base" data-align="left">基地名称</th>
                                    <th data-field="order_number" data-align="left">系统订单号</th>
                                    <th data-field="pay_type" data-align="right">支付方式</th>
                                    <th data-field="wx_order" data-align="center">微信支付订单号</th>
                                    <th data-field="price" data-align="center">单价</th>
                                    <th data-field="num" data-align="center">数量</th>
                                    <th data-field="total_price" data-align="center">总支付金额</th>
                                    <th data-field="ticket_money" data-align="left">开票金额</th>
                                    <th data-field="ticket_confirm_money" data-align="left">实际开票金额</th>
                                    <th data-field="is_ticket">是否开票</th>
                                    {{--<th data-field="ticket_number" data-align="left">发票税号</th>--}}
                                    <th data-field="ticket_link" data-align="left">发票联系人</th>
                                    <th data-field="ticket_tel" data-align="left">发票联系电话</th>
                                    <th data-field="ticket_email" data-align="left">发票发送邮箱</th>
                                    <th data-field="status" data-align="center">状态</th>
                                    <th data-field="remark" data-align="center">备注</th>
                                    <th data-field="createTime" data-align="center">创建时间</th>
                                    <th data-field="caozuo" data-align="center">操作</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                            <div>
                            </div>
                        </div>
                        <div class="tab-pane fade  in" id="myTab2_untreated">
                            <div class="row clearfix">
                            <div class="col col-sm-12 col-xs-12 col-md-12 col-lg-8 col-xl-8">
                                <div class="card">
                                    <div class="body">
                                        <form id="editInformation">
                                            <div class="form-group">
                                                <label>标题</label>
                                                <input type="text" class="form-control title" placeholder="标题" name="title" value="">
                                            </div>
                                            <div class="form-group">
                                                <label>详细说明</label>
                                                <input type="text" class="form-control description" name="description" placeholder="详细说明" value="">
                                            </div>
                                            <div class="form-group" >
                                                <label for="">封面图</label>
                                            </div>
                                            <img  src="{{ empty($data->image) ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : $data->image }}" class="img-thumbnail img" style="width:40%;margin-bottom: 20px;" alt="">
                                            <input type="hidden" class="imgUrl" value="">
                                            <div class="toolbars1" id="toolbars1"></div>
                                            <textarea id="editor1" placeholder="输入内容" autofocus></textarea>

                                            <hr>
                                            {{--<button type="submit" class="btn btn-primary pull-right">提交</button>--}}
                                            <input type="hidden" class="id" name="id" value="">
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-sm-12 col-xs-12 col-md-12 col-lg-4 col-xl-4">
                                <div class="card">
                                    <div class="body">
                                        <div class="device">
                                            <div class="iwindow1">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>

                        <div class="tab-pane fade" id="myTab2_unfinished">
                            <div class="row">
                                <div class=" col-lg-5 col-md-5 col-sm-6 col-xs-10 col-xs-offset-1 form-info">

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
        </div>
    </div>

    <div id="infoOrder" style="display:none;">
        <form class="form-untreated">
            <div class="list-group">
                <a class="list-group-item active" style="text-align: center;background-color: #ddd;border-color:#ddd;color:black;" href="#">
                    订单信息
                </a>
                <a class="list-group-item" href="#">
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: right;" >系统订单编号：</p>
                    </div>
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: left;" class="order_number"></p>
                    </div>
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: right;">微信支付订单号：</p>
                    </div>
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: left;" class="wx_order"></p>
                    </div>
                </a>
                <a class="list-group-item" href="#">
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: right;" >支付方式：</p>
                    </div>
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: left;" class="pay_types"></p>
                    </div>
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: right;">订单状态：</p>
                    </div>
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: left;" class="order_status"></p>
                    </div>
                </a>
                <a class="list-group-item" href="#">
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: right;">下单时间：</p>
                    </div>
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: left;" class="date"></p>
                    </div>
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: right;">总价格：</p>
                    </div>
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: left;" class="total_price"></p>
                    </div>
                </a>
            </div>
            <div class="list-group">
                <a class="list-group-item active" style="text-align: center;background-color: #ddd;border-color:#ddd;color:black;" href="#">
                    发票信息
                </a>
                <a class="list-group-item" href="#">
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: right;">抬头：</p>
                    </div>
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: left;" class="rise"></p>
                    </div>
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: right;">税号：</p>
                    </div>
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: left;" class="paragraph"></p>
                    </div>
                </a>
                <a class="list-group-item" href="#">
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: right;">缴费人员姓名：</p>
                    </div>
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: left;" class="invoice_name"></p>
                    </div>
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: right;">开票金额：</p>
                    </div>
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: left;" class="invoice_price"></p>
                    </div>
                </a>
                <a class="list-group-item" href="#">
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: right;">手机：</p>
                    </div>
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: left;" class="tel"></p>
                    </div>
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: right;">邮箱：</p>
                    </div>
                    <div class="row col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p style="text-align: left;" class="email"></p>
                    </div>
                </a>
            </div>
            <div class="list-group goods-div">
                <a class="list-group-item active" style="text-align: center;background-color: #ddd;border-color:#ddd;color:black;" href="#">
                    报名信息
                </a>
                <table class="table user-list">
                    <thead>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <center>
                <hr>
                <div class="pull-right" style="margin-right: 200px;margin-bottom: 40px;">
                    <div class="btn-group">
                        <a href="#" class="btn btn-info close-subview-button"> 关闭 </a>
                    </div>
                    <div class="btn-group btn-save">
                        <a href="#" class="btn btn-info save-order"> 审核通过 </a>
                    </div>
                </div>
            </center>
        </form>
    </div>
    <div class="modal fade" id="publishModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">发布</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <style>
                    #clipArea {
                        width: 100%;
                        height: 400px;
                    }
                </style>
                <div class="modal-body">
                    <input type="file" name="file" class="form-control file" id="file">
                    <div class="photoClip">
                        <div id="clipArea"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="clipBtn">确定</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="preview-enroll" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">预览</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="text-align: center">
                    <iframe src="" class="preview-iframe" height="667" width="375" frameborder="0" style="border:1px solid #ccc;box-shadow: 0 2px 39px 11px;"></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirm-ticket" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">开出发票确认</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="">发票税号</label>
                            <input type="text" class="form-control ticket-number" value="12310101425026409J" disabled>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">联系人</label>
                            <input type="text" class="form-control ticket-link" value="张三" disabled>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">联系电话</label>
                            <input type="text" class="form-control ticket-tel" value="123101014" disabled>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">邮箱</label>
                            <input type="text" class="form-control ticket-email" value="12310@126.com" disabled>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">实际支付金额</label>
                            <input type="text" class="form-control money" value="1000" disabled>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">发票金额</label>
                            <input type="text" class="form-control ticket-money" value="1000" disabled>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">实际开票金额</label>
                            <input type="number" class="form-control confirm-money" value="">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="">备注</label>
                            <textarea name="" id="" class="form-control ticket-remark" cols="30" rows="5"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="ticketBtn">确认</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js-container')
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/hammer.min.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/iscroll-zoom-min.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/lrz.all.bundle.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/dist/Photoclip.min.js"></script>
    <script type="text/javascript" src="/js/asset/editor/js/module.js"></script>
    <script type="text/javascript" src="/js/asset/editor/js/hotkeys.js"></script>
    <script type="text/javascript" src="/js/asset/editor/js/uploader.js"></script>
    <script type="text/javascript" src="/js/asset/editor/js/simditor.js"></script>
    <script type="text/javascript" src="{{ asset('/js/asset/editor/simditor-html-master/vendor/bower/js-beautify/js/lib/beautify-html.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/asset/editor/simditor-html-master/lib/simditor-html.js') }}"></script>
    <script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <script src="https://dangjian.hplyy.com/assets/bundles/datatablescripts.bundle.js"></script>
    <script src="/js/admin/enroll.js?time={{time()}}"></script>
    <script>
        $(function () {
            Main.init();
            article.init();
            let html = '{!! $data->content or '' !!}';
//            article.e().txt.html(html);
        })
    </script>
@endsection