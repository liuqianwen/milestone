@extends('base')
@section('head')
@endsection
@section('container')
        <!-- start: PAGE -->
<div class="main-content">
    <!-- start: PANEL CONFIGURATION MODAL FORM -->
    <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Panel Configuration</h4>
                </div>
                <div class="modal-body">
                    Here will be a configuration form
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary">
                        Save changes
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- end: SPANEL CONFIGURATION MODAL FORM -->
    <div class="container">
        <!-- start: PAGE HEADER -->
        <!-- start: TOOLBAR -->
        <div class="toolbar row">
            <div class="col-sm-6 hidden-xs">
                <div class="page-header">
                    <h2>抽奖设置<small></small></h2>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <a href="#" class="back-subviews">
                    <i class="fa fa-chevron-left"></i> 返回
                </a>
                <a href="#" class="close-subviews">
                    <i class="fa fa-times"></i> 关闭
                </a>
                <div class="toolbar-tools pull-right">
                    <!-- start: TOP NAVIGATION MENU -->
                    <ul class="nav navbar-right">
                        <li>
                            <a href="#newlabe1" class="new-label">
                                <i class="fa fa-plus"></i> 添加
                            </a>
                        </li>
                    </ul>
                    <!-- end: TOP NAVIGATION MENU -->
                </div>
            </div>
        </div>
        <!-- end: TOOLBAR -->
        <!-- end: PAGE HEADER -->
        <!-- start: BREADCRUMB -->
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{ url('/') }}">首页</a></li>
                    <li class="active">抽奖设置</li>
                </ol>
            </div>
        </div>
        <!-- end: BREADCRUMB -->
        <!-- start: PAGE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div id="toolbar">
                            <button type="button" class="btn btn-blue tooltips edit-label" disabled>
                                编辑
                            </button>
                        </div>
                        <table id="labeltable">
                            <thead>
                            <tr>
                                <th data-field="state" data-radio="true"></th>
                                <th data-field="name" data-align="left">名称</th>
                                <th data-field="type" data-align="center">类型</th>
                                <th data-field="sort" data-align="center">排位</th>
                            </tr>
                            </thead>
                        </table>
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end: PAGE CONTENT-->
        </div>
    </div>
    <!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->
@endsection
@section('subview')
        <!-- start: SUBVIEW SAMPLE CONTENTS -->
<!-- *** NEW label *** -->
<div id="newlabe1" style="display:none;">
    <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
        <form class="form-addlabel">
            <div class="form-group" style="margin-top: 30px;">
                <fieldset>
                    <div class="row col-lg-4 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                        <div class="form-group">
                            <label class="control-label">名称</label>
                            <div class="controls">
                                <input type="text"  class="form-control name" name="name">
                            </div>
                        </div>
                    </div>
                    <div class="row col-lg-4 col-md-5 col-sm-5 col-xs-10 col-xs-offset-1">
                        <div class="form-group">
                            <label class="control-label"> 类型 </label>
                            <select name="type" class="form-control type">
                                <option value="1">积分</option>
                                <option value="2">礼包</option>
                                <option value="0">谢谢参与</option>
                            </select>
                        </div>
                    </div>
                    <div class="row col-lg-4 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                        <div class="form-group">
                            <label class="control-label">积分数量</label>
                            <div class="controls">
                                <input type="number" min="0" class="form-control integral" name="integral">
                            </div>
                        </div>
                    </div>
                    <div class="row col-lg-4 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                        <div class="form-group">
                            <label class="control-label">排位（1-8）</label>
                            <div class="controls">
                                <input type="number" min="1" max="8" class="form-control sort" name="sort">
                            </div>
                        </div>
                    </div>
                    <div class="row col-lg-4 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                        <div class="form-group">
                            <label class="control-label">抽中概率（请填写整数，数值越大概率越大）</label>
                            <div class="controls">
                                <input type="number" min="0"  class="form-control odds" name="odds">
                            </div>
                        </div>
                    </div>
                    <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1">
                        <div class="form-group" >
                            <label for="">icon图片</label>
                        </div>
                        <img  src="" class="img-thumbnail img" style="width:200px;margin-bottom: 20px;height: 150px" alt="">
                        <input type="hidden" class="imgUrl" value="">
                    </div>
                </fieldset>
            </div>
            <hr>
            <div class="pull-right">
                <div class="btn-group">
                    <a href="#" class="btn btn-info close-subview-button">
                        关闭
                    </a>
                </div>
                <div class="btn-group">
                    <button class="btn btn-info save-note" type="submit">
                        保存
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- *** EDIT label *** -->
<div id="editlabel" style="display:none;">
    <div class="noteWrap col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
        <form class="form-editlabel">
            <div class="form-group">
                <fieldset>
                    <div class="row col-lg-4 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                        <div class="form-group">
                            <label class="control-label">名称</label>
                            <div class="controls">
                                <input type="text"  class="form-control name" name="name">
                            </div>
                        </div>
                    </div>
                    <div class="row col-lg-4 col-md-5 col-sm-5 col-xs-10 col-xs-offset-1">
                        <div class="form-group">
                            <label class="control-label"> 类型 </label>
                            <select name="type" class="form-control type">
                                <option value="1">积分</option>
                                <option value="2">礼包</option>
                                <option value="0">谢谢参与</option>
                            </select>
                        </div>
                    </div>
                    <div class="row col-lg-4 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                        <div class="form-group">
                            <label class="control-label">积分数量</label>
                            <div class="controls">
                                <input type="number" min="0" class="form-control integral" name="integral">
                            </div>
                        </div>
                    </div>
                    <div class="row col-lg-4 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                        <div class="form-group">
                            <label class="control-label">排位（1-8）</label>
                            <div class="controls">
                                <input type="number" min="1" max="8" class="form-control sort" name="sort">
                            </div>
                        </div>
                    </div>
                    <div class="row col-lg-4 col-md-4 col-sm-5 col-xs-10 col-xs-offset-1">
                        <div class="form-group">
                            <label class="control-label">抽中概率（请填写整数，数值越大概率越大）</label>
                            <div class="controls">
                                <input type="number" min="0"  class="form-control odds" name="odds">
                            </div>
                        </div>
                    </div>
                    <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1">
                        <div class="form-group" >
                            <label for="">icon图片</label>
                        </div>
                        <img  src="" class="img-thumbnail img" style="width:200px;margin-bottom: 20px;height: 150px" alt="">
                        <input type="hidden" class="imgUrl" value="">
                    </div>
                </fieldset>
            </div>
            <hr>
            <div class="pull-right">
                <div class="btn-group">
                    <a href="#" class="btn btn-info close-subview-button">
                        关闭
                    </a>
                </div>
                <div class="btn-group">
                    <button class="btn btn-info save-note" type="submit">
                        保存
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end: SUBVIEW SAMPLE CONTENTS -->
        <div class="modal fade" id="publishModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content ">
                    <div class="modal-header">
                        <h5 class="modal-title">发布</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <style>
                        #clipArea {
                            width: 100%;
                            height: 400px;
                        }
                    </style>
                    <div class="modal-body">
                        <input type="file" name="file" class="form-control file" id="file">
                        <div class="photoClip">
                            <div id="clipArea"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="clipBtn">确定</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('js-container')
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/hammer.min.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/iscroll-zoom-min.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/demo/js/lrz.all.bundle.js"></script>
    <script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/PhotoClip.js-master/dist/Photoclip.min.js"></script>
    <script src="{{ asset('/js/admin/prize.js') }}"></script>
    <!-- end: CORE JAVASCRIPTS  -->
    <script>
        jQuery(document).ready(function() {
            Main.init();
            prize.init();
            $('.th-inner').parent().css('text-align', 'center');
        });
    </script>
@endsection