<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.2 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- start: HEAD -->

<head>
<!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="_token" content="{{ csrf_token() }}"/>
    <meta name="description" content="mst">
    <meta name="author" content="Belync Inc.">
    <meta name="keyword" content="口腔">
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    {{--<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>--}}
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap/css/bootstrap.min.css">
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/datepicker/css/datepicker.css">
<!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/sweetalert/lib/sweet-alert.css"/>
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/iCheck/skins/all.css">
    <link href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/mobiscroll/css/mobiscroll.custom-3.0.0-beta2.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/layer_mobile/need/layer.css">
    @yield('head')
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<style>
    .self-container{
        margin-top:2vh;
    }
</style>

<body>
<div class="main-wrapper">
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        @yield('content')
    </div>
</div>
<!--[if gte IE 9]><!-->
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jQuery/jquery-2.1.1.min.js"></script>
<!--<![endif]-->
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/blockUI/jquery.blockUI.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/iCheck/jquery.icheck.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/datepicker/js/bootstrap-datepicker.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/sweetalert/lib/sweet-alert.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/mobiscroll/js/mobiscroll.custom-3.0.0-beta2.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/layer-v3.1.1/layer/layer.js"></script>

@yield('jscontent')
</body>
<!-- end: BODY -->
</html>