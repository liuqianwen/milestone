<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]>
<html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <title>迈世通后台管理</title>
    <meta charset="utf-8"/>
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,IE=9,IE=8,chrome=1"/>
    <![endif]-->
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="_token" content="{{ csrf_token() }}"/>
    <meta name="description" content="北京爱医助医公司提供支持">
    <meta name="author" content="爱医助医">
    <meta name="keyword" content="随访易">
    <link rel="stylesheet"
          href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/iCheck/skins/all.css">
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/animate.css/animate.min.css">
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-table/dist/bootstrap-table.css">
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/toastr/toastr.min.css">
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/owl-carousel/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/owl-carousel/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/owl-carousel/owl-carousel/owl.transitions.css">
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-modal/css/bootstrap-modal.css"/>
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"/>
    {{--<link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/toastr/toastr.min.css">--}}
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/datepicker/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/lightbox2/css/lightbox.css">
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/sweetalert/lib/sweet-alert.css"/>
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/css/styles.css">
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/css/styles-responsive.css">
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/css/plugins.css">
    <link rel="stylesheet" href="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/css/themes/theme-style8.css" type="text/css" id="skin_color">
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/layer_mobile/need/layer.css">
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-daterangepicker-master/daterangepicker.css">
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/mobiscroll/css/mobiscroll.custom-3.0.0-beta2.min.css">
    <link rel="shortcut icon" href="/favicon.ico"/>
    <style>
        .main-wrapper {
            height: 80vh;
        }
    </style>
    @yield('head')
</head>
<body>
<div class="main-wrapper">
    <header class="topbar navbar navbar-inverse navbar-fixed-top inner">
        <div class="container">
            <div class="navbar-header">
                <a class="sb-toggle-left hidden-md hidden-lg" href="#main-navbar"> <i class="fa fa-bars"></i> </a>
                <a class="navbar-brand" href="{{ asset('/') }}"></a>
            </div>
            <div class="topbar-tools">
                <!-- start: TOP NAVIGATION MENU -->
                <ul class="nav navbar-right">
                    <!-- start: USER DROPDOWN -->
                    <li class="dropdown current-user">
                        <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true"
                           href="#">
                            <img src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/images/headimg.png" width="30"
                                 height="30" class="img-circle" alt="">
                            <span class="username hidden-xs">{{ Auth::user()->name }}</span>
                            <i class="fa fa-caret-down "></i> </a>
                        <ul class="dropdown-menu dropdown-dark">
                            <li>
                                {{--<a href="{{ url('/logout') }}"> 退出系统 </a>--}}
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    退出系统
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                    <li class="right-menu-toggle">
                        <a href="#" class="sb-toggle-right">
                            <i class="fa fa-globe toggle-icon"></i>
                            <i class="fa fa-caret-right"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <a class="closedbar inner hidden-sm hidden-xs" href="#"> </a>
    <nav id="pageslide-left" class="pageslide inner">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation left-wrapper transition-left">
                <div class="navigation-toggler hidden-sm hidden-xs">
                    <a href="#main-navbar" class="sb-toggle-left"> </a>
                </div>
                <div class="user-profile border-top padding-horizontal-10 block">
                    <div class="inline-block">
                        <img src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/images/headimg.png" width="50"
                             height="50" alt="">
                    </div>
                    <div class="inline-block">
                        <h5 class="no-margin"> 欢迎 </h5>
                        <h4 class="no-margin hyname"> {{ Auth::user()->name }} </h4>
                    </div>
                </div>

                <ul class="main-navigation-menu">
                    <li>
                        <a href="{{ url('/') }}">
                            <i class="glyphicon glyphicon-home"></i>
                            <span class="title"> 首页 </span>
                        </a>
                    </li>
{{--                    <li>--}}
{{--                        <a href="{{ url('/im/chat') }}">--}}
{{--                            <i class="glyphicon glyphicon-home"></i>--}}
{{--                            <span class="title"> 咨询 </span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                    <li>
                        <a href="{{ url('/label') }}">
                            <i class="glyphicon glyphicon-home"></i>
                            <span class="title"> 标签管理 </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/article') }}">
                            <i class="glyphicon glyphicon-home"></i>
                            <span class="title"> 文章管理 </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/video') }}">
                            <i class="glyphicon glyphicon-home"></i>
                            <span class="title"> 视频管理 </span>
                        </a>
                    </li>
{{--                    <li>--}}
{{--                        <a href="{{ url('/volunteer') }}">--}}
{{--                            <i class="glyphicon glyphicon-home"></i>--}}
{{--                            <span class="title"> 公益活动 </span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a href="{{ url('/hospital') }}">--}}
{{--                            <i class="fa  fa-hospital-o"></i>--}}
{{--                            <span class="title"> 医院管理 </span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a href="{{ url('/doctor') }}">--}}
{{--                            <i class="fa fa-users"></i>--}}
{{--                            <span class="title"> 医生管理 </span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a href="{{ url('/vender') }}">--}}
{{--                            <i class="glyphicon glyphicon-home"></i>--}}
{{--                            <span class="title"> 厂家管理 </span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a href="{{ url('/follow_up/survey') }}">--}}
{{--                            <i class="glyphicon glyphicon-home"></i>--}}
{{--                            <span class="title"> 问卷管理 </span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a href="{{ url('/follow_up/information') }}">--}}
{{--                            <i class="glyphicon glyphicon-home"></i>--}}
{{--                            <span class="title"> 常量管理 </span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                    <li>
                        <a href="{{url('/user')}}">
                            <i class="fa fa-user"></i>
                            <span class="title"> 管理员管理 </span>
                        </a>
                    </li>
{{--                    <li>--}}
{{--                        <a href="{{ url('/prize') }}">--}}
{{--                            <i class="fa fa-table"></i>--}}
{{--                            <span--}}
{{--                                    class="title"> 抽奖设置 </span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a href="{{ url('/enroll') }}">--}}
{{--                            <i class="glyphicon glyphicon-home"></i>--}}
{{--                            <span class="title"> 活动报名 </span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                </ul>
            </div>
        </div>
        <div class="slide-tools">
            <div class="col-xs-6 text-left no-padding">
                <a class="btn btn-sm status" href="#">
                    状态
                    <i class="fa fa-dot-circle-o text-green"></i>
                    <span>在线</span>
                </a>
            </div>
            <div class="col-xs-6 text-right no-padding">
                <a class="btn btn-sm log-out text-right" href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i class="fa fa-power-off"></i>
                    退出系统 </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </nav>
    <div id="pageslide-right" class="pageslide slide-fixed inner">
        <div class="right-wrapper ps-container">
            <ul class="nav nav-tabs nav-justified" id="sidebar-tab">
                <li class="active">
                    <a href="javascript:void(0)" role="tab" data-toggle="tab"><i class="fa fa-gear"></i> 样式设置 </a>
                </li>
            </ul>
            <div class="hidden-xs" id="style_selector">
                <div id="style_selector_container">
                    <div class="pageslide-title">
                        样式选择
                    </div>
                    <div class="box-title">
                        选择你的布局样式
                    </div>
                    <div class="input-box">
                        <div class="input">
                            <select name="layout" class="form-control">
                                <option value="default">默认</option>
                                <option value="boxed">盒子型</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-title">
                        选择你的头部样式
                    </div>
                    <div class="input-box">
                        <div class="input">
                            <select name="header" class="form-control">
                                <option value="fixed">固定</option>
                                <option value="default">可活动</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-title">
                        选择你的侧边栏样式
                    </div>
                    <div class="input-box">
                        <div class="input">
                            <select name="sidebar" class="form-control">
                                <option value="fixed">固定</option>
                                <option value="default">可活动</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-title">
                        选择你的尾部样式
                    </div>
                    <div class="input-box">
                        <div class="input">
                            <select name="footer" class="form-control">
                                <option value="default">可活动</option>
                                <option value="fixed">固定</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-title">
                        10 种预定义颜色方案
                    </div>
                    <div class="images icons-color">
                        <a href="#" id="default">
                            <img src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/images/color-1.png" alt=""
                                 class="active">
                        </a>
                        <a href="#" id="style2">
                            <img src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/images/color-2.png" alt="">
                        </a>
                        <a href="#" id="style3">
                            <img src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/images/color-3.png" alt="">
                        </a>
                        <a href="#" id="style4">
                            <img src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/images/color-4.png" alt="">
                        </a>
                        <a href="#" id="style5">
                            <img src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/images/color-5.png" alt="">
                        </a>
                        <a href="#" id="style6">
                            <img src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/images/color-6.png" alt="">
                        </a>
                        <a href="#" id="style7">
                            <img src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/images/color-7.png" alt="">
                        </a>
                        <a href="#" id="style8">
                            <img src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/images/color-8.png" alt="">
                        </a>
                        <a href="#" id="style9">
                            <img src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/images/color-9.png" alt="">
                        </a>
                        <a href="#" id="style10">
                            <img src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/images/color-10.png" alt="">
                        </a>
                    </div>
                    <div class="box-title">
                        盒子型背景
                    </div>
                    <div class="images boxed-patterns">
                        <a href="#" id="bg_style_1">
                            <img src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/images/bg.png" alt="">
                        </a>
                        <a href="#" id="bg_style_2">
                            <img src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/images/bg_2.png" alt="">
                        </a>
                        <a href="#" id="bg_style_3">
                            <img src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/images/bg_3.png" alt="">
                        </a>
                        <a href="#" id="bg_style_4">
                            <img src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/images/bg_4.png" alt="">
                        </a>
                        <a href="#" id="bg_style_5">
                            <img src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/images/bg_5.png" alt="">
                        </a>
                    </div>
                    <div class="style-options">
                        <a href="#" class="clear_style"> 清除样式 </a>
                        <a href="#" class="save_style"> 保存样式 </a>
                    </div>
                </div>
                <div class="style-toggle open"></div>
            </div>
        </div>
    </div>
    <div class="main-container inner">
        <div class="main-content">
            <div class="container">
                @yield('container')
            </div>
            <div class="subviews">
                <div class="subviews-container"></div>
            </div>
        </div>
    </div>
    <footer class="inner">
        <div class="footer-inner">
            <div class="pull-left">
                2018 &copy; 北京爱医助医科技有限公司
            </div>
            <div class="pull-right">
                <span class="go-top"><i class="fa fa-chevron-up"></i></span>
            </div>
        </div>
    </footer>
    @yield('subview')
</div>
<!--[if lt IE 9]>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/respond.min.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/excanvas.min.js"></script>
<script type="text/javascript"
        src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jQuery/jquery-1.11.1.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jQuery/jquery-2.1.1.min.js"></script>
<!--<![endif]-->
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/blockUI/jquery.blockUI.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/iCheck/jquery.icheck.min.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootbox/bootbox.min.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jquery.appear/jquery.appear.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jquery-cookie/jquery.cookie.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/velocity/jquery.velocity.min.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-table/bootstrap-table-zh-CN.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/toastr/toastr.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/truncate/jquery.truncate.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/autosize/jquery.autosize.min.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/datepicker/js/bootstrap-datepicker.zh-CN.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/moment/min/moment.min.js"></script>
<script type="text/javascript" src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-daterangepicker-master/moment1.min.js"></script>
<script type="text/javascript" src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-daterangepicker-master/daterangepicker.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
{{--<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/ckeditor/ckeditor.js"></script>--}}
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/ckeditor/adapters/jquery.js"></script>
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/sweetalert/lib/sweet-alert.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/layer_mobile/layer.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/mobiscroll/js/mobiscroll.custom-3.0.0-beta2.min.js"></script>
{{--<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/toastr/toastr.js"></script>--}}
<script src="/js/main.js"></script>
<script src="/js/form-elements.js"></script>
<script src="/js/subview.js"></script>
@yield('js-container')
</body>
</html>
