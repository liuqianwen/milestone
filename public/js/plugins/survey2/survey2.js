/**
 * @author liu_xu_dong
 * version: 1.0.0
 */

!function ($){
    'use strict';
    // TOOLS DEFINITION
    // ======================
    /**
     * 生成基础form
     * @param field 字段
     * @param text 文本label
     * @param input_type input / textarea
     * @param isHasId 是否设置id
     * @returns {string}
     */

    var form_handle              = function (field, text, input_type, isHasId){
        var input_type = arguments[2] ? arguments[2] : 'input';
        var isHasId    = arguments[3] ? arguments[3] : false;
        var idString   = '';
        if ( isHasId ) idString = ' id="' + field + '" ';
        var title_form = [
            '<div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">',
            '<div class="form-group">',
            '<label class="control-label">' + text + '</label>',
            '<div class="controls">',
            input_type == 'input'
                ? '<input type="text" class="form-control ' + field + '" ' + idString + ' name="' + field + '">'
                : '<textarea class="form-control ' + field + '" ' + idString + ' name="' + field + '" rows="8"></textarea>',
            '</div>',
            '</div>',
            '</div>'
        ].join('');
        return title_form;
    };
    /**
     * 左侧问题分类
     * @returns {string}
     */
    var question_type_handle     = function (){
        var question_type = [
            { name: '单选题', icon: 'fa fa-dot-circle-o', id: 1 },
            { name: '多选题', icon: 'fa fa-check-square-o', id: 2 },
            { name: '填空文本题', icon: 'fa fa-list-ul', id: 3 },
            { name: '填空数值题', icon: 'fa fa-list-ol', id: 9 },
            { name: '填空日期题', icon: 'fa fa-calendar', id: 10 },
            { name: '描述题', icon: 'fa fa-file-text-o', id: 4 },
            { name: '图片题', icon: 'fa fa-picture-o', id: 5 },
            { name: '说明题', icon: 'fa fa-file-text', id: 6 },
            { name: '星级题', icon: 'fa fa-star', id: 7 },
            { name: '多选描述题', icon: 'fa fa-check-square', id: 8 }
        ];
        var div           = '';
        $.each(question_type, function (index, value){
            div += '<div data-type="' + value.id + '" class="choose-question survey2-left"><i class="' + value.icon + ' icon-width"></i>  ' + value.name + '</div>';
        });
        return div;
    };
    /**
     * 定义question_body, scale_data , scale_array ,question_all, tmp_value,tmp_value_description,survey2_tmp_object,click_sort;
     */
    var question_body, scale_data, scale_array, question_all, tmp_value, tmp_value1, tmp_value2, survey2_tmp_object,
        survey2_tmp_object1, survey2_tmp_object2, survey2_tmp_object3, click_sort;
    /**
     * 收集页面所有的数据
     * @param type
     * @param options
     * @returns {*}
     */
    var getAllData               = function (type, options){
        survey2_tmp_object = {};
        if ( type == 'save' && options.handle.action == 'edit' ) {
            if ( $('.survey2-container .dataId').val() == '' ) return false;
            survey2_tmp_object.id = $('.survey2-container .dataId').val();
        }
        if ( options.title == true ) {
            if ( $('.survey2-container .title').val() == '' ) return false;
            survey2_tmp_object.title = $('.survey2-container .title').val();
        }
        if ( options.description == true ) survey2_tmp_object.description = $('.survey2-container .description').val();
        if ( options.remark == true ) survey2_tmp_object.remark = $('.survey2-container .remark').val();
        if ( options.information == true ) survey2_tmp_object.information = $('.survey2-container #information').val();
        if ( options.type == true ) survey2_tmp_object.type = $('.survey2-container .type').val();
        if ( options.self_description == true ) survey2_tmp_object.self_description = $('.survey2-container .self_description').val();
        survey2_tmp_object.question = [];
        tmp_value1                  = $('.survey2-container .question-body').length;
        if ( tmp_value1 == 0 ) return false;
        $('.survey2-container .question-body').each(function (index, value){
            survey2_tmp_object1         = {};
            survey2_tmp_object1.number  = index + 1;
            survey2_tmp_object1.content = $(this).find('.question-input').text();
            if ( survey2_tmp_object1.content == '' || survey2_tmp_object1.content.trim() == '点击输入问题' ) return false;
            survey2_tmp_object1.type               = $(this).data('type');
            survey2_tmp_object1.option             = [];
            survey2_tmp_object1.option_description = [];
            if ( survey2_tmp_object1.type == 1 || survey2_tmp_object1.type == 2 || survey2_tmp_object1.type == 8 ) {
                tmp_value2 = $(this).find('.question-option-single').length;
                $(this).find('.question-option-single').each(function (index1, value1){
                    if ( $(this).text() == '' || $(this).text().trim() == '点击输入选项' ) return false;
                    survey2_tmp_object1.option.push($(this).text());
                });
                if ( tmp_value2 > survey2_tmp_object1.option.length ) return false;
                if ( survey2_tmp_object1.type == 8 ) {
                    $(this).find('.question-option-description').each(function (index1, value1){
                        if ( $(this).text() == '' || $(this).text().trim() == '点击输入描述' ) return false;
                        survey2_tmp_object1.option_description.push($(this).text());
                    });
                    if ( tmp_value2 > survey2_tmp_object1.option_description.length ) return false;
                }
            }
            survey2_tmp_object1.required = $(this).find('.question-btn-action-detail').data('required');
            tmp_value                    = $(this).find('.question-btn-action-detail').data('jump');
            if ( tmp_value == '' ) {
                survey2_tmp_object1.jump = [tmp_value1 == index + 1 ? 'end' : index + 2];
            } else {
                survey2_tmp_object2 = tmp_value.split(',');
                survey2_tmp_object3 = [];
                $.each(survey2_tmp_object2, function (index1, value1){
                    survey2_tmp_object3.push(value1 == '' ? tmp_value1 == index + 1 ? 'end' : index + 2 : value1)
                });
                survey2_tmp_object1.jump = survey2_tmp_object3;
            }
            survey2_tmp_object.question.push(survey2_tmp_object1);
        });
        if ( tmp_value1 > survey2_tmp_object.question.length ) return false;
        return survey2_tmp_object;
    };
    /**
     * 点击问题操作
     * @param compile
     */
    var clickQuestion            = function (compile){
        clickQuestionTitle(compile);
        clickQuestionOption(compile);
        clickAddOption(compile);
        clickSortAndRemoveOption(compile);
        clickSortQuestion(compile);
        clickRemoveQuestion(compile);
        clickMoreQuestion(compile);
    };
    /**
     * 点击输入问题
     * @param compile
     */
    var clickQuestionTitle       = function (compile){
        compile.find('.question-input').off().on('focus', function (){
            if ( $(this).text().trim() === '点击输入问题' ) {
                $(this).text('');
            }
        }).on('focusout', function (){
            if ( $(this).text().trim() === '' ) {
                $(this).text('点击输入问题');
            }
        });
    };
    /**
     * 点击输入选项
     * @param compile
     */
    var clickQuestionOption      = function (compile){
        compile.find('.question-option-single').off().on('focus', function (){
            if ( $(this).text().trim() === '点击输入选项' ) {
                $(this).text('');
            }
        }).on('focusout', function (){
            if ( $(this).text().trim() === '' ) {
                $(this).text('点击输入选项');
            }
        });
        compile.find('.question-option-description').off().on('focus', function (){
            if ( $(this).text().trim() === '点击输入描述' ) {
                $(this).text('');
            }
        }).on('focusout', function (){
            if ( $(this).text().trim() === '' ) {
                $(this).text('点击输入描述');
            }
        });
    };
    /**
     * 点击添加问题选项
     * @param compile
     */
    var clickAddOption           = function (compile){
        compile.find('.question-option-add').off().on('click', function (){
            question_body = $(this).parents('.question-body');
            if ( question_body.data('type') == 8 ) {
                question_body.find('.question-option-main').append(question_element.$option_description);
            } else {
                if ( question_body.data('type') == 1 ) {
                    question_body.find('.question-btn-action-detail').data('jump', question_body.find('.question-btn-action-detail').data('jump') + ',');
                }
                question_body.find('.question-option-main').append(question_element.$option);
            }
            clickQuestionOption(compile);
            clickSortAndRemoveOption(compile);
        });
    };
    /**
     * 问题选项顺序和删除选项
     * @param compile
     */
    var clickSortAndRemoveOption = function (compile){
        compile.find('.question-option-btn').off().on('click', function (){
            question_body = $(this).parents('.question-body');
            tmp_value     = $(this).parents('.question-option').find('.question-option-single').text();
            if ( question_body.data('type') == '8' ) survey2_tmp_object3 = $(this).parents('.question-option').find('.question-option-description').text();
            ;
            switch ( $(this).data('btn') ) {
                case 'up':
                    survey2_tmp_object = $(this).parents('.question-option').prev();
                    if ( typeof survey2_tmp_object[0] != 'undefined' ) {
                        $(this).parents('.question-option').find('.question-option-single').text(survey2_tmp_object.find('.question-option-single').text());
                        survey2_tmp_object.find('.question-option-single').text(tmp_value);
                        if ( question_body.data('type') == 8 ) {
                            $(this).parents('.question-option').find('.question-option-description').text(survey2_tmp_object.find('.question-option-description').text());
                            survey2_tmp_object.find('.question-option-description').text(survey2_tmp_object3);
                        }
                        if ( question_body.data('type') == 1 ) {
                            tmp_value1                          = question_body.find('.question-option').index($(this).parents('.question-option'));
                            survey2_tmp_object1                 = question_body.find('.question-btn-action-detail').data('jump').split(',');
                            survey2_tmp_object2                 = survey2_tmp_object1[tmp_value1];
                            survey2_tmp_object1[tmp_value1]     = survey2_tmp_object1[tmp_value1 - 1];
                            survey2_tmp_object1[tmp_value1 - 1] = survey2_tmp_object2;
                            question_body.find('.question-btn-action-detail').data('jump', survey2_tmp_object1.join(','));
                        }
                    }
                    break;
                case 'down':
                    survey2_tmp_object = $(this).parents('.question-option').next();
                    if ( typeof survey2_tmp_object[0] != 'undefined' ) {
                        $(this).parents('.question-option').find('.question-option-single').text(survey2_tmp_object.find('.question-option-single').text());
                        survey2_tmp_object.find('.question-option-single').text(tmp_value);
                        tmp_value1 = question_body.find('.question-option').index($(this).parents('.question-option'));
                        if ( question_body.data('type') == 8 ) {
                            $(this).parents('.question-option').find('.question-option-description').text(survey2_tmp_object.find('.question-option-description').text());
                            survey2_tmp_object.find('.question-option-description').text(survey2_tmp_object3);
                        }
                        if ( question_body.data('type') == 1 ) {
                            tmp_value1                          = question_body.find('.question-option').index($(this).parents('.question-option'));
                            survey2_tmp_object1                 = question_body.find('.question-btn-action-detail').data('jump').split(',');
                            survey2_tmp_object2                 = survey2_tmp_object1[tmp_value1];
                            survey2_tmp_object1[tmp_value1]     = survey2_tmp_object1[tmp_value1 + 1];
                            survey2_tmp_object1[tmp_value1 + 1] = survey2_tmp_object2;
                            question_body.find('.question-btn-action-detail').data('jump', survey2_tmp_object1.join(','));
                        }
                    }
                    break;
                case 'remove':
                    if ( question_body.find('.question-option').length > 2 ) {
                        if ( question_body.data('type') == 1 ) {
                            tmp_value1          = question_body.find('.question-option').index($(this).parents('.question-option'));
                            survey2_tmp_object  = question_body.find('.question-btn-action-detail').data('jump').split(',');
                            survey2_tmp_object1 = [];
                            $.each(survey2_tmp_object, function (index, value){
                                if ( index != tmp_value1 ) survey2_tmp_object1.push(value);
                            });
                            question_body.find('.question-btn-action-detail').data('jump', survey2_tmp_object1.join(','));
                        }
                        $(this).parents('.question-option').remove();
                    }
                    break;
            }
        });
    };
    /**
     * 移除问题
     * @param compile
     */
    var clickRemoveQuestion      = function (compile){
        compile.find('.question-btn-action-remove').off().on('click', function (){
            question_body = $(this).parents('.question-body');
            question_all  = question_body.parent().find('.question-body');
            tmp_value     = parseInt(question_body.find('.question-title').text().split('Q')[1].split('.')[0]);
            question_body.remove();
            survey2_tmp_object = question_all.length;
            $.each(question_all, function (index, value){
                tmp_value1 = index + 1;
                if ( tmp_value1 != tmp_value ) {
                    $(this).find('.question-btn-action-detail').data('jump', doRemoveReplace($(this).find('.question-btn-action-detail').data('jump'), tmp_value));
                }
                if ( tmp_value1 > tmp_value ) {
                    $(this).find('.question-title').text('Q' + index + '.');
                }
            });
        });
    };
    var doRemoveReplace          = function (jumpString, replace_number){
        var tmp = [], tmp1 = [];
        if ( jumpString != '' ) tmp = jumpString.split(',');
        $.each(tmp, function (index, value){
            if ( value < replace_number ) tmp1.push(value);
            if ( value == replace_number ) tmp1.push('');
            if ( value > replace_number ) tmp1.push(value - 1);
        });
        tmp = tmp1.join(',');
        return tmp;
    };
    /**
     * 切换问题顺序
     * @param compile
     */
    var clickSortQuestion        = function (compile){
        compile.find('.question-btn-action-sort').off().on('click', function (){
            question_body      = $(this).parents('.question-body');
            click_sort         = $(this).data('sort');
            survey2_tmp_object = {};
            if ( click_sort == 'up' ) survey2_tmp_object = question_body.prev();
            if ( click_sort == 'down' ) survey2_tmp_object = question_body.next();
            if ( typeof survey2_tmp_object[0] != 'undefined' ) {
                tmp_value = question_body.find('.question-title').text();
                question_body.find('.question-title').text(survey2_tmp_object.find('.question-title').text());
                survey2_tmp_object.find('.question-title').text(tmp_value);
                tmp_value1 = parseInt(tmp_value.split('Q')[1].split('.')[0]);
                if ( click_sort == 'up' ) {
                    tmp_value           = doSortReplace(survey2_tmp_object.find('.question-btn-action-detail').data('jump'), tmp_value1);
                    survey2_tmp_object1 = question_body.prev();
                }
                if ( click_sort == 'down' ) {
                    tmp_value1 += 1;
                    tmp_value = survey2_tmp_object.find('.question-btn-action-detail').data('jump');
                    question_body.find('.question-btn-action-detail').data('jump', doSortReplace(question_body.find('.question-btn-action-detail').data('jump'), tmp_value1));
                    survey2_tmp_object1 = question_body.next();
                }
                survey2_tmp_object.remove();
                if ( click_sort == 'up' ) {
                    question_body.after(survey2_tmp_object1);
                    survey2_tmp_object = question_body.next();
                }
                if ( click_sort == 'down' ) {
                    question_body.before(survey2_tmp_object1);
                    survey2_tmp_object = question_body.prev();
                }
                survey2_tmp_object.find('.question-btn-action-detail').data('jump', tmp_value);
                clickQuestion(compile);
            }
        });
    };
    var doSortReplace            = function (string, number){
        var tmp = [], tmp1 = [];
        if ( string != '' ) tmp = string.split(',');
        $.each(tmp, function (index, value){
            if ( value == number ) {
                tmp1.push('');
            } else {
                tmp1.push(value);
            }
        });
        return tmp1.join(',');
    };
    /**
     * 是否必填 与 跳转逻辑
     * @param compile
     */
    var clickMoreQuestion        = function (compile){
        compile.find('.question-btn-action-detail').off().on('click', function (){
            $.blockUI({
                message: '加载数据中...'
            });
            question_body       = $(this).parents('.question-body');
            survey2_tmp_object  = question_body.nextAll();
            survey2_tmp_object3 = {};
            $.each(survey2_tmp_object, function (index, value){
                survey2_tmp_object3[$(this).find('.question-title').text().split('Q')[1].split('.')[0]] = $(this).find('.question-title').text() + ($(this).find('.question-input').text().trim() === '点击输入问题' ? '' : $(this).find('.question-input').text().trim());
            });
            survey2_tmp_object3['end'] = '结束页面';
            survey2_tmp_object         = '';
            $.each(survey2_tmp_object3, function (index, value){
                survey2_tmp_object += '<option value="' + index + '">' + value + '</option>';
            });
            tmp_value  = question_body.find('.question-title').text();
            tmp_value1 = question_body.find('.question-input').text().trim() === '点击输入问题' ? '' : question_body.find('.question-input').text().trim();
            tmp_value  = tmp_value + tmp_value1 + '的详情';
            $('.survey2-jump-modal .jump-question-content').text(tmp_value);
            $('.survey2-jump-modal .form-jump').empty();
            survey2_tmp_object1 = '';
            if ( question_body.data('type') == 1 ) {
                question_body.find('.question-option').each(function (index, value){
                    tmp_value = index + 1;
                    survey2_tmp_object1 += '<div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1"> ' +
                        '<div class="form-group"> ' +
                        '<label class="control-label">' + ($(this).find('.question-option-single').text().trim() === '点击输入选项' ? '第' + tmp_value + '个选项:' : $(this).find('.question-option-single').text().trim() + ':') + '跳转题目</label> ' +
                        '<div class="controls"> ' +
                        '<select name="jump" class="form-control jump">' +
                        survey2_tmp_object +
                        '</select>' +
                        '</div> ' +
                        '</div> ' +
                        '</div>';
                });
            } else {
                survey2_tmp_object1 = '<div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1"> ' +
                    '<div class="form-group"> ' +
                    '<label class="control-label">跳转题目</label> ' +
                    '<div class="controls"> ' +
                    '<select name="jump" class="form-control jump">' +
                    survey2_tmp_object +
                    '</select>' +
                    '</div> ' +
                    '</div> ' +
                    '</div>';
            }
            survey2_tmp_object2 = [
                '<div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1"> ' +
                '<div class="form-group"> ' +
                '<label class="control-label">是否必填</label> ' +
                '<div class="controls"> ' +
                '<select name="required" class="form-control required">' +
                '<option value="1">是</option>' +
                '<option value="0">否</option>' +
                '</select>' +
                '</div> ' +
                '</div> ' +
                '</div>',
                survey2_tmp_object1
            ].join('');
            $('.survey2-jump-modal .form-jump').append(survey2_tmp_object2);
            $('.survey2-jump-modal .form-jump .required').val($(this).data('required'));
            survey2_tmp_object = [];
            if ( question_body.data('type') == 1 ) {
                survey2_tmp_object = $(this).data('jump').split(',');
            } else {
                survey2_tmp_object.push($(this).data('jump'));
            }
            $.each(survey2_tmp_object, function (index, value){
                if ( value != '' ) $('.survey2-jump-modal .form-jump .jump:eq(' + index + ')').val(value);
            });
            $.unblockUI();
            $('.survey2-jump-modal').modal('show');
            clickSaveModel(question_body);
        });
    };
    /**
     * 保存modal数据
     * @param question
     */
    var clickSaveModel           = function (question){
        $('.survey2-jump-modal .btn-set-jump').off().on('click', function (){
            question.find('.question-btn-action-detail').data('required', $('.survey2-jump-modal .form-jump .required').val());
            tmp_value = [];
            $.each($('.survey2-jump-modal .form-jump .jump'), function (){
                tmp_value.push($(this).val());
            });
            tmp_value = tmp_value.join(',');
            question.find('.question-btn-action-detail').data('jump', tmp_value);
            $('.survey2-jump-modal').modal('hide');
        });
    };
    /**
     * 问题元素
     * @type {{$start: string, $option: string, $option_description: string, $left_btn: string, $right_btn: string}}
     */
    var question_element         = {
        $start: '<div> ' +
        '<div class="question-title">question_title</div> ' +
        '<div class="question-input" contenteditable="true">点击输入问题</div>' +
        '</div>',
        $option: '<div class="question-option">' +
        '<div class="question-option-single" contenteditable="true">点击输入选项</div>' +
        '<div class="question-option-btn" data-btn="up"><i class="fa fa-lg fa-arrow-up"></i></div> ' +
        '<div class="question-option-btn" data-btn="down"><i class="fa fa-lg fa-arrow-down"></i></div> ' +
        '<div class="question-option-btn" data-btn="remove"><i class="fa fa-lg fa-remove"></i></div> ' +
        '<div class="clear-both"></div>' +
        '</div>',
        $option_description: '<div class="question-option">' +
        '<div class="question-option-single" contenteditable="true">点击输入选项</div>' +
        '<div class="question-option-btn" data-btn="up"><i class="fa fa-lg fa-arrow-up"></i></div> ' +
        '<div class="question-option-btn" data-btn="down"><i class="fa fa-lg fa-arrow-down"></i></div> ' +
        '<div class="question-option-btn" data-btn="remove"><i class="fa fa-lg fa-remove"></i></div> ' +
        '<div class="question-option-description" contenteditable="true">点击输入描述</div>' +
        '<div class="clear-both"></div>' +
        '</div>',
        $left_btn: '<div class="left-btn"> ' +
        '<div class="question-option-add">' +
        '<i class="fa fa-2x fa-plus"></i> ' +
        '</div> ' +
        '</div> ',
        $right_btn_remove: '<div class="question-btn-action question-btn-action-remove"> ' +
        '<i class="fa fa-2x fa-trash-o"></i>  ' +
        '</div> ',
        $right_btn_detail_single: '<div class="question-btn-action question-btn-action-detail" data-required="1" data-jump=","> ' +
        '<i class="fa fa-2x fa-align-justify"></i>  ' +
        '</div> ',
        $right_btn_detail_other: '<div class="question-btn-action question-btn-action-detail" data-required="1" data-jump=""> ' +
        '<i class="fa fa-2x fa-align-justify"></i>  ' +
        '</div> ',
        $right_btn_sort: '<div class="question-btn-action question-btn-action-sort" data-sort="down"> ' +
        '<i class="fa fa-2x fa-hand-o-down"></i>  ' +
        '</div> ' +
        '<div class="question-btn-action question-btn-action-sort" data-sort="up"> ' +
        '<i class="fa fa-2x fa-hand-o-up"></i>  ' +
        '</div> '
    };
    /**
     * 问题html
     * @type {{1: string, 2: string}}
     */
    var question_html            = {
        1: '<div class="row question-body" data-type="1"> ' +
        '   <div class="question-body-main"> ' +
        question_element.$start +
        '       <div class="clear-both"></div>' +
        '       <div class="question-option-main"> ' +
        question_element.$option +
        question_element.$option +
        '       </div>' +
        '       <div class="clear-both"></div>' +
        '       <div> ' +
        '           <div class="question-btn"> ' +
        question_element.$left_btn +
        '               <div class="right-btn"> ' +
        question_element.$right_btn_remove +
        question_element.$right_btn_detail_single +
        question_element.$right_btn_sort +
        '                   <div class="question-btn-no-action"> 单选题 ' +
        '                   </div> ' +
        '               </div> ' +
        '           </div> ' +
        '       </div> ' +
        '   </div> ' +
        '</div>',
        2: '<div class="row question-body" data-type="2"> ' +
        '   <div class="question-body-main"> ' +
        question_element.$start +
        '       <div class="clear-both"></div>' +
        '       <div class="question-option-main"> ' +
        question_element.$option +
        question_element.$option +
        '       </div>' +
        '       <div class="clear-both"></div>' +
        '       <div> ' +
        '           <div class="question-btn"> ' +
        question_element.$left_btn +
        '               <div class="right-btn"> ' +
        question_element.$right_btn_remove +
        question_element.$right_btn_detail_other +
        question_element.$right_btn_sort +
        '                   <div class="question-btn-no-action"> 多选题 ' +
        '                   </div> ' +
        '               </div> ' +
        '           </div> ' +
        '       </div> ' +
        '   </div> ' +
        '</div>',
        3: '<div class="row question-body" data-type="3"> ' +
        '   <div class="question-body-main"> ' +
        question_element.$start +
        '       <div class="clear-both"></div>' +
        '       <div> ' +
        '           <div class="question-btn"> ' +
        '               <div class="right-btn"> ' +
        question_element.$right_btn_remove +
        question_element.$right_btn_detail_other +
        question_element.$right_btn_sort +
        '                   <div class="question-btn-no-action"> 填空文本题 ' +
        '                   </div> ' +
        '               </div> ' +
        '           </div> ' +
        '       </div> ' +
        '   </div> ' +
        '</div>',
        4: '<div class="row question-body" data-type="4"> ' +
        '   <div class="question-body-main"> ' +
        question_element.$start +
        '       <div class="clear-both"></div>' +
        '       <div> ' +
        '           <div class="question-btn"> ' +
        '               <div class="right-btn"> ' +
        question_element.$right_btn_remove +
        question_element.$right_btn_detail_other +
        question_element.$right_btn_sort +
        '                   <div class="question-btn-no-action"> 描述题 ' +
        '                   </div> ' +
        '               </div> ' +
        '           </div> ' +
        '       </div> ' +
        '   </div> ' +
        '</div>',
        5: '<div class="row question-body" data-type="5"> ' +
        '   <div class="question-body-main"> ' +
        question_element.$start +
        '       <div class="clear-both"></div>' +
        '       <div> ' +
        '           <div class="question-btn"> ' +
        '               <div class="right-btn"> ' +
        question_element.$right_btn_remove +
        question_element.$right_btn_detail_other +
        question_element.$right_btn_sort +
        '                   <div class="question-btn-no-action"> 图片题 ' +
        '                   </div> ' +
        '               </div> ' +
        '           </div> ' +
        '       </div> ' +
        '   </div> ' +
        '</div>',
        6: '<div class="row question-body" data-type="6"> ' +
        '   <div class="question-body-main"> ' +
        question_element.$start +
        '       <div class="clear-both"></div>' +
        '       <div> ' +
        '           <div class="question-btn"> ' +
        '               <div class="right-btn"> ' +
        question_element.$right_btn_remove +
        question_element.$right_btn_detail_other +
        question_element.$right_btn_sort +
        '                   <div class="question-btn-no-action"> 说明题 ' +
        '                   </div> ' +
        '               </div> ' +
        '           </div> ' +
        '       </div> ' +
        '   </div> ' +
        '</div>',
        7: '<div class="row question-body" data-type="7"> ' +
        '   <div class="question-body-main"> ' +
        question_element.$start +
        '       <div class="clear-both"></div>' +
        '       <div> ' +
        '           <div class="question-btn"> ' +
        '               <div class="right-btn"> ' +
        question_element.$right_btn_remove +
        question_element.$right_btn_detail_other +
        question_element.$right_btn_sort +
        '                   <div class="question-btn-no-action"> 星级题 ' +
        '                   </div> ' +
        '               </div> ' +
        '           </div> ' +
        '       </div> ' +
        '   </div> ' +
        '</div>',
        8: '<div class="row question-body" data-type="8"> ' +
        '   <div class="question-body-main"> ' +
        question_element.$start +
        '       <div class="clear-both"></div>' +
        '       <div class="question-option-main"> ' +
        question_element.$option_description +
        question_element.$option_description +
        '       </div>' +
        '       <div class="clear-both"></div>' +
        '       <div> ' +
        '           <div class="question-btn"> ' +
        question_element.$left_btn +
        '               <div class="right-btn"> ' +
        question_element.$right_btn_remove +
        question_element.$right_btn_detail_other +
        question_element.$right_btn_sort +
        '                   <div class="question-btn-no-action"> 多选描述题 ' +
        '                   </div> ' +
        '               </div> ' +
        '           </div> ' +
        '       </div> ' +
        '   </div> ' +
        '</div>',
        9: '<div class="row question-body" data-type="9"> ' +
        '   <div class="question-body-main"> ' +
        question_element.$start +
        '       <div class="clear-both"></div>' +
        '       <div> ' +
        '           <div class="question-btn"> ' +
        '               <div class="right-btn"> ' +
        question_element.$right_btn_remove +
        question_element.$right_btn_detail_other +
        question_element.$right_btn_sort +
        '                   <div class="question-btn-no-action"> 填空数值题 ' +
        '                   </div> ' +
        '               </div> ' +
        '           </div> ' +
        '       </div> ' +
        '   </div> ' +
        '</div>',
        10: '<div class="row question-body" data-type="10"> ' +
        '   <div class="question-body-main"> ' +
        question_element.$start +
        '       <div class="clear-both"></div>' +
        '       <div> ' +
        '           <div class="question-btn"> ' +
        '               <div class="right-btn"> ' +
        question_element.$right_btn_remove +
        question_element.$right_btn_detail_other +
        question_element.$right_btn_sort +
        '                   <div class="question-btn-no-action"> 填空日期题 ' +
        '                   </div> ' +
        '               </div> ' +
        '           </div> ' +
        '       </div> ' +
        '   </div> ' +
        '</div>'
    };
    /**
     * 设置问题
     * @param compile
     * @param data
     * @returns {boolean}
     */
    var setQuestion              = function (compile, data){
        compile.empty();
        $.each(data, function (index, value){
            if ( index == 'id' ) $('.survey2-container .dataId').val(value);
            if ( index == 'title' || index == 'description' || index == 'remark' || index == 'type' || index == 'self_description' ) $('.survey2-container .' + index).val(value);
            if ( index == 'information' ) {
                $('.survey2-container #information').val(value);
                callSelect2($('.survey2-container #information'));
            }
            if ( index == 'question' ) {
                $.each(value, function (index1, value1){
                    question_body = '<div class="row question-body" data-type="' + value1.type + '">';
                    question_body += '<div class="question-body-main">';
                    question_body += question_element.$start;
                    question_body += '<div class="clear-both"></div>';
                    switch ( parseInt(value1.type) ) {
                        case 1:
                            question_body += '<div class="question-option-main">';
                            $.each(value1.option, function (){
                                question_body += question_element.$option;
                            });
                            question_body += '</div>';
                            question_body += '<div class="clear-both"></div>';
                            break;
                        case 2:
                            question_body += '<div class="question-option-main">';
                            $.each(value1.option, function (){
                                question_body += question_element.$option;
                            });
                            question_body += '</div>';
                            question_body += '<div class="clear-both"></div>';
                            break;
                        case 8:
                            question_body += '<div class="question-option-main">';
                            $.each(value1.option, function (){
                                question_body += question_element.$option_description;
                            });
                            question_body += '</div>';
                            question_body += '<div class="clear-both"></div>';
                            break;
                    }
                    question_body += '       <div> ';
                    question_body += '           <div class="question-btn">';
                    if ( value1.type == 1 || value1.type == 2 || value1.type == 8 ) question_body += question_element.$left_btn;
                    question_body += '               <div class="right-btn"> ' +
                        question_element.$right_btn_remove +
                        question_element.$right_btn_detail_other +
                        question_element.$right_btn_sort;
                    question_body += '<div class="question-btn-no-action">';
                    switch ( value1.type ) {
                        case '1':
                            question_body += '单选题';
                            break;
                        case '2':
                            question_body += '多选题';
                            break;
                        case '3':
                            question_body += '填空文本题';
                            break;
                        case '4':
                            question_body += '描述题';
                            break;
                        case '5':
                            question_body += '图片题';
                            break;
                        case '6':
                            question_body += '说明题';
                            break;
                        case '7':
                            question_body += '星级题';
                            break;
                        case '8':
                            question_body += '多选描述题';
                            break;
                        case '9':
                            question_body += '填空数值题';
                            break;
                        case '10':
                            question_body += '填空日期题';
                            break;
                    }
                    question_body += '      </div> ' +
                        '               </div> ' +
                        '           </div> ' +
                        '       </div> ' +
                        '   </div> ' +
                        '</div>';
                    compile.append(question_body);
                    // 设置值
                    question_body = compile.find('.question-body:last');
                    question_body.find('.question-title').text('Q' + value1.number + '.');
                    question_body.find('.question-input').text(value1.content);
                    if ( value1.type == 1 || value1.type == 2 || value1.type == 8 ) {
                        $(value1.option).each(function (index2, value2){
                            question_body.find('.question-option-single:eq(' + index2 + ')').text(value2);
                        });
                        if ( value1.type == 8 ) {
                            $(value1.option_description).each(function (index2, value2){
                                question_body.find('.question-option-description:eq(' + index2 + ')').text(value2);
                            });
                        }
                    }
                    question_body.find('.question-btn-action-detail').data('required', value1.required);
                    question_body.find('.question-btn-action-detail').data('jump', value1.jump);
                });
            }
        });
        clickQuestion(compile);
        return true;
    };
    /**
     * 添加问题
     * @param compile
     * @param type
     * @constructor
     */
    var AddQuestion              = function (compile, type){
        var question       = '';
        var question_title = compile.find('.question-body').length + 1;
        question_title     = 'Q' + question_title + '.';
        if ( typeof question_html[type] != 'undefined' ) {
            question = question_html[type];
            question = question.replace('question_title', question_title);
            compile.append(question);
            scrollBottom(compile);
            clickQuestion(compile);
        }
    };
    /**
     * 滑动到底
     * @param compile
     */
    var scrollBottom             = function (compile){
        var mainContainer     = compile,
            scrollToContainer = mainContainer.find('.question-body:last');
        mainContainer.animate({
            scrollTop: scrollToContainer.offset().top - mainContainer.offset().top + mainContainer.scrollTop()
        }, 1000);
    };
    // SURVEY2 CLASS DEFINITION
    // ======================
    var survey2                               = function (el, options){
        this.options = options;
        this.$el     = $(el);
        this.$el_    = this.$el.clone();
        this.init();
    };
    survey2.DEFAULTS                          = {
        legend: '问卷详情',
        form_class: 'form-survey',
        title: true,
        description: true,
        remark: false,
        information: true,
        type: false,
        self_description: true,
        scale: [],
        handle: {
            action: 'add',
            data: {}
        },
        template: {
            show: false,
            data: [],
            url: ''
        },
        save: {
            show: true,
            url: ''
        },
        preview: {
            show: false,
            url: ''
        },
        tableId: 'survey_table'
    };
    survey2.prototype.init                    = function (){
        this.initEmptyEl();
        this.initFormContent();
        this.initTemplate();
        this.initAddQuestion();
        this.initContainer();
        this.initHideSubview();
        this.initClickLeft();
        this.initChangeTemplate();
        this.initSelect2();
        this.initModal();
        this.initResize();
        this.initClickPreviewAndSave();
        this.initSetDefaultQuestion();
    };
    survey2.prototype.initEmptyEl             = function (){
        this.$el.empty();
        $('.survey2-model').remove();
    };
    survey2.prototype.initFormContent         = function (){
        this.$formContent = '';
        if ( this.options.title ) this.$formContent += form_handle('title', '名称', 'input');
        // if ( this.options.type ) this.$formContent += form_handle('type', '类型', 'input');
        // if ( this.options.self_description ) this.$formContent += form_handle('self_description', '备注', 'textarea');
        if ( this.options.description ) this.$formContent += form_handle('description', '说明', 'textarea');
        // if ( this.options.remark ) this.$formContent += form_handle('remark', '微信推送消息提醒内容', 'textarea');
        if ( this.options.information ) this.$formContent += form_handle('information', '信息量表', 'input', true);
        if ( this.options.handle.action == 'edit' ) this.$formContent += '<input type="hidden" class="dataId" name="dataId">';
    };
    survey2.prototype.initTemplate            = function (){
        this.$template = '';
        if ( this.options.template.show ) {
            this.$template   = '<div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            this.$template += '<div class="form-group">' +
                '<label class="control-label">选择模版</label>' +
                '<div class="controls">';
            this.$template += '<select class="form-control template" name="template">';
            this.$template += '<option value="0">请选择</option>';
            var thisTemplate = this.$template;
            $.each(this.options.template.data, function (index, value){
                thisTemplate += '<option value="' + value.id + '">' + value.name + '</option>';
            });
            this.$template = thisTemplate;
            this.$template += '</select></div></div></div>';
        }
    };
    survey2.prototype.initAddQuestion         = function (){
        this.$mainContent = [
            '<div class="row">',
            '   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">',
            '       <div class="survey2-left-main">',
            question_type_handle(),
            '       </div>',
            '   </div>',
            '   <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 survey-compile">',
            '   </div>',
            '</div>'
        ].join('');
    };
    survey2.prototype.initContainer           = function (){
        this.$container = $([
            '<div class="noteWrap col-md-10 col-xs-12 col-md-offset-1 col-xs-offset-1 survey2-container">',
            '<form class="' + this.options.form_class + '">',
            '<legend>' + this.options.legend + '</legend>',
            '<div class="form-group">',
            '<fieldset>',
            this.$formContent,
            this.$template,
            this.$mainContent,
            '</fieldset>',
            '</div>',
            '<hr>',
            '<div class="pull-right">',
            '<div class="btn-group">',
            (this.options.preview.show == true ? '<a href="javascript:void(0);" class="btn btn-info close-subview-button survey2-btn-preview"> 预览 </a>' : ''),
            (this.options.save.show == true ? '<a href="javascript:void(0);" class="btn btn-info close-subview-button survey2-btn-save"> 保存 </a>' : ''),
            '</div>',
            '</div>',
            '</form>',
            '</div>'
        ].join(''));
        this.$el.append(this.$container);
        this.$compile = this.$el.find('.survey-compile');
        this.$el.find('.form-control').parent().parent().parent().css('width', $('.survey2-container .survey2-left-main').parent().parent().width() + 16 + 'px');
    };
    survey2.prototype.initHideSubview         = function (){
        $('.survey2-container .survey2-btn-close').off().on('click', function (){
            $.hideSubview();
        });
    };
    survey2.prototype.initSetDefaultQuestion  = function (){
        if ( this.options.handle.action == 'edit' ) {
            //console.log(this.options.handle.data);
            setQuestion(this.$compile, this.options.handle.data);
        }
    };
    survey2.prototype.initClickLeft           = function (){
        this.chooseQuestion = this.$el.find('.choose-question');
        var compile         = this.$compile;
        this.chooseQuestion.on('click', function (){
            AddQuestion(compile, $(this).data('type'));
        });
    };
    survey2.prototype.initChangeTemplate      = function (){
        var templateUrl = this.options.template.url;
        var compile     = this.$compile;
        this.$el.find('.template').off().on('change', function (){
            var templateValue = $(this).val();
            if ( templateValue != 0 ) {
                $.ajax({
                    type: 'POST',
                    url: templateUrl,
                    data: { id: templateValue },
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    success: function (json){
                        setQuestion(compile, json.data);
                    },
                    error: function (){
                        console.log('模版返回数据错误！');
                    }
                });
            }
        });
    };
    survey2.prototype.initSelect2             = function (){
        scale_data  = this.options.scale;
        scale_array = {};
        $.each(scale_data, function (index, value){
            scale_array[value.id] = value;
        });
        this.$information = this.$el.find('.information');
        this.$description = this.$el.find('.description');
        this.$information.removeClass('form-control');
        callSelect2(this.$information);
        this.$information = this.$el.find('.information');
        this.$information.css('width', this.$description.css('width'));
    };
    survey2.prototype.initModal               = function (){
        this.$jump_modal = [
            '<div class="survey2-model survey2-jump-modal modal extended-modal fade no-display modal-overflow in" tabindex="-1"' +
            'data-width="760" aria-hidden="false" style="display: none;">',
            '   <div class="modal-dialog">',
            '       <div class="modal-content">',
            '           <div class="modal-header">',
            '               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">',
            '                   ×     ',
            '               </button>',
            '               <h4 class="modal-title jump-question-content"></h4>',
            '           </div>',
            '           <div class="modal-body">',
            '               <div class="row">',
            '                   <div class="form-group form-jump">',
            '                       <fieldset>',
            '                       </fieldset>',
            '                   </div>',
            '               </div>',
            '           </div>',
            '           <div class="modal-footer">',
            '               <button type="button" data-dismiss="modal" class="btn btn-light-grey shut-jump">',
            '                   关闭',
            '               </button>',
            '               <button type="button" class="btn btn-info btn-set-jump">',
            '                   保存',
            '               </button>',
            '           </div>',
            '       </div>',
            '   </div>',
            '</div>'
        ].join('');
        this.$el.parent().append(this.$jump_modal);
        if ( this.options.preview.show == true ) {
            this.$preview_modal = [
                '<div class="survey2-model survey2-preview-modal modal extended-modal fade no-display modal-overflow in" tabindex="-1"' +
                'data-width="500" aria-hidden="false" style="display: none;">',
                '   <div class="modal-dialog">',
                '       <div class="modal-content">',
                '           <div class="modal-header">',
                '               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">',
                '                   ×     ',
                '               </button>',
                '               <h4 class="modal-title">扫码手机预览</h4>',
                '           </div>',
                '           <div class="modal-body" style="text-align: center;">',
                '               <img src="" class="preview-img" style="width:400px;height:400px;" alt="">',
                '           </div>',
                '           <div class="modal-footer">',
                '               <button type="button" data-dismiss="modal" class="btn btn-light-grey shut-preview">',
                '                   关闭',
                '               </button>',
                '           </div>',
                '       </div>',
                '   </div>',
                '</div>'
            ].join('');
            this.$el.parent().append(this.$preview_modal);
        }
    };
    survey2.prototype.initResize              = function (){
        var thisInformation = this.$information;
        var thisDescription = this.$description;
        $('.survey2-container .survey2-left-main').css('width', $('.survey2-container .survey2-left-main').parent().width() + 'px');
        $(window).resize(function (){
            thisInformation.css('width', thisDescription.css('width'));
            $('.survey2-container .survey2-left-main').css('width', $('.survey2-container .survey2-left-main').parent().width() + 'px');
            $('.survey2-container .form-control').parent().parent().parent().css('width', $('.survey2-container .survey2-left-main').parent().parent().width() + 16 + 'px');
        });
        $(window).scroll(function (){
            countPositionTop();
        });
        $('.subviews').off().on('scroll', function (){
            countPositionTop();
        });
    };
    survey2.prototype.initClickPreviewAndSave = function (){
        var thisOptions    = this.options;
        var thisPreviewUrl = this.options.preview.url;
        var thisSaveUrl    = this.options.save.url;
        var returnData;
        this.$el.find('.survey2-btn-preview').off().on('click', function (){
            returnData = getAllData('preview', thisOptions);
            if ( returnData ) {
                $.ajax({
                    type: 'POST',
                    url: thisPreviewUrl,
                    data: { data: returnData },
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    success: function (json){
                        if ( json.status == 0 ) {
                            $('.survey2-preview-modal .preview-img').attr('src', json.src);
                            $('.survey2-preview-modal').modal('show');
                        } else {
                            swal('数据不正确！', '', 'warning');
                        }
                    },
                    error: function (){
                        console.log('预览返回数据错误！');
                    }
                });
            } else {
                swal('数据不正确！', '', 'warning');
            }
        });
        var thisTableId = this.options.tableId;
        this.$el.find('.survey2-btn-save').off().on('click', function (){
            returnData = getAllData('save', thisOptions);
            if ( returnData ) {
                $.ajax({
                    type: 'POST',
                    url: thisSaveUrl,
                    data: { data: returnData },
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    success: function (json){
                        if ( json.status == 0 ) {
                            swal('保存成功！', '', 'success');
                            $.hideSubview();
                            $('#' + thisTableId).bootstrapTable('refresh');
                        } else {
                            swal('数据不正确！', '', 'warning');
                        }
                    },
                    error: function (){
                        console.log('保存返回数据错误！');
                    }
                });
            } else {
                swal('数据不正确！', '', 'warning');
            }
        });
    };
    var callSelect2                           = function (container){
        container.select2({
            placeholder: "请选择需要填写信息",
            minimumInputLength: 0,
            multiple: true,
            separator: "^",
            maximunSelectionSize: 10,
            initSelection: function (element, callback){   // 初始化时设置默认值
                var data = [];
                $(element.val().split("^")).each(function (){
                    if ( typeof scale_array[this] !== 'undefined' ) {
                        data.push(scale_array[this]);
                    }
                });
                callback(data);
            },
            formatSelection: function (item){
                return item.show;
            },  // 选择结果中的显示
            formatResult: function (item){
                return item.show;
            },
            data: scale_data
        });
    };
    /**
     * 计算顶部高度
     */
    var countPositionTop                      = function (){
        if ( $('.survey2-container .survey-compile').offset().top < 121 ) {
            $('.survey2-container .survey2-left-main').css('position', 'fixed').css('top', '121px');
        } else {
            $('.survey2-container .survey2-left-main').css('position', '').css('top', '');
        }
    };
    /**
     * 方法控制
     * @param el
     * @param method_name
     * @returns {{}|*}
     */
    var survey2Method                         = function (el, method_name){
        survey2_tmp_object = {};
        switch ( method_name ) {
            case 'save' :
                survey2_tmp_object = survey2Save(el);
                break;
        }
        return survey2_tmp_object;
    };
    /**
     * 举例 保存方法
     * @param el
     * @returns {string[]}
     */
    var survey2Save                           = function (el){
        return ['aaa', 'bbb', 'ccc'];
    };
    // SURVEY2 PLUGIN DEFINITION
    // =======================
    var allowedMethods = [];
    $.fn.survey2       = function (option){
        // SURVEY2 METHODS
        if ( typeof option == 'string' && $.inArray(option, allowedMethods) >= 0 ) {
            return survey2Method(this, option);
        }
        // SURVEY2 INIT
        if ( typeof (option) == 'object' ) {
            new survey2(this, $.extend(survey2.DEFAULTS, option));
        }
    };
}(jQuery);