/**
 * @author liu_xu_dong
 * wechat classic 经典版微信问卷
 * version: 1.0.0
 * depend: iCheck,jQuery-Smart-Wizard,mobiscroll,sweetalert,wx_jssdk
 */

!function ($) {

    /**
     * 变量定义
     */

    var survey2_tmp_object, survey2_tmp_object1, survey2_this, survey2_tmp_object2, survey2_tmp_object3, save_url,
        step_number, step_bool, step_i, survey_i, thisNextStep, thisNowElement;

    // TOOLS DEFINITION
    // ======================


    // SURVEY2 CLASS DEFINITION
    // ======================

    var survey2 = function (el, options) {
        this.options = options;
        this.$el = $(el);
        this.$el_ = this.$el.clone();
        this.init();
    };

    survey2.DEFAULTS = {
        data: '',
        save: {
            active: false,
            id: 0,
            url: ''
        }
    };

    survey2.prototype.init = function () {
        this.initJsonStringToObject();
        this.initEmptyEl();
        this.initStepHtml();
        this.initContainer();
        this.initUploadImage();
        this.initJump();
        this.initWizardElement();
        this.initEventEvent();
        this.initWizard();
    };

    survey2.prototype.initJsonStringToObject = function () {
        survey2_tmp_object = this.options.data;
        survey2_tmp_object = survey2_tmp_object.replace(/\n/g, "<br>").replace(/\r/g, "\\\\r").replace(/&quot;/g, '"');
        survey2_tmp_object = JSON.parse(survey2_tmp_object);
        this.options.data = survey2_tmp_object;
    };
    survey2.prototype.initEmptyEl = function () {
        this.$el.empty();
    };
    survey2.prototype.initStepHtml = function () {
        this.$step = [
            '<section id="step-1" data-type="indexes">',
            '<div class="-survey-body-title">',
            this.options.data.title,
            '</div>',
            '<div class="-survey-body-content">',
            this.options.data.description,
            '</div>',
            '</section>'
        ].join('');
        step_number = 1;
        if (this.options.data.information.length > 0) {
            survey2_tmp_object = '';
            $.each(this.options.data.information, function (index, value) {
                switch (value.type) {
                    case 0:
                        survey2_tmp_object += '<div class="form-group"> ' +
                            '<input type="text" class="form-control" placeholder="' + value.name + '" data-name="' + value.name + '"/> ' +
                            '</div>';
                        break;
                    case 1:
                        survey2_tmp_object += '<div class="form-group select"> ' +
                            '<select name="" id="" data-name="' + value.name + '" class="form-control">';
                        $.each(value.option, function (index2, value2) {
                            survey2_tmp_object += '<option value="' + value2 + '">' + value2 + '</option>'
                        });
                        survey2_tmp_object += '</select></div>';
                        break;
                    case 2:
                        survey2_tmp_object += '<div class="form-group"> ' +
                            '<input type="text" class="form-control date" placeholder="' + value.name + '" data-name="' + value.name + '"/> ' +
                            '</div>';
                        break;
                }
            });
            this.$step += [
                '<section id="step-2" data-type="information">',
                '<div class="-survey-body-subtitle">填写个人信息 </div>',
                '<div class="-survey-body-content content">',
                survey2_tmp_object,
                '</div>',
                '</section>'
            ].join('');
            step_number = 2;
        }
        if (this.options.data.question.length > 0) {
            step_bool = false;
            survey2_tmp_object = this.$step;
            $.each(this.options.data.question, function (index, value) {
                step_number += 1;
                survey2_tmp_object += [
                    '<section id="step-' + step_number + '" data-number="' + value.number + '" data-type="' + value.type + '" data-required="' + value.required + '">',
                    '<div class="-survey-body-subtitle">',
                    value.content,
                    '</div>',
                    '<div class="-survey-body-content content">',
                    '<div class="jump" data-jump="' + value.jump[0] + '"></div>'
                ].join('');
                switch ('' + value.type) {
                    case '1': // 单选
                        $.each(value.option, function (index1, value1) {
                            survey2_tmp_object += '<label> ' +
                                '<input type="radio" name="radio' + value.number + '" data-jump="' + value.jump[index1] + '"> ' + value1 +
                                '</label>';
                        });
                        break;
                    case '2': // 多选
                        $.each(value.option, function (index1, value1) {
                            survey2_tmp_object += '<label> ' +
                                '<input type="checkbox" name="checkbox' + value.number + '"> ' + value1 +
                                '</label>';
                        });
                        break;
                    case '3': // 填空文本
                        survey2_tmp_object += '<label> <input type="text" name="text" class="form-control" placeholder=""> </label>';
                        break;
                    case '4': // 描述
                        survey2_tmp_object += '<label> <textarea name="" id="" class="form-control"></textarea> </label>';
                        break;
                    case '5': // 图片
                        survey2_tmp_object += '<div class="-upload-img upload-img"> ' +
                            '<div> ' +
                            '<input type="hidden" class=""> ' +
                            '</div> ' +
                            '<div> ' +
                            '<input type="hidden" class=""> ' +
                            '</div> ' +
                            '<div> ' +
                            '<input type="hidden" class=""> ' +
                            '</div> </div>';
                        break;
                    case '6': // 说明
                        break;
                    case '7': // 星级
                        survey2_tmp_object += '<div class="-star star"> ' +
                            '<i class="fa fa-star-o fa-2x" data-id="1"></i> ' +
                            '<i class="fa fa-star-o fa-2x" data-id="2"></i> ' +
                            '<i class="fa fa-star-o fa-2x" data-id="3"></i> ' +
                            '<i class="fa fa-star-o fa-2x" data-id="4"></i> ' +
                            '<i class="fa fa-star-o fa-2x" data-id="5"></i> ' +
                            '</div>';
                        break;
                    case '8': // 多选描述
                        $.each(value.option, function (index1, value1) {
                            survey2_tmp_object += '<label> ' +
                                '<input type="checkbox" name="checkbox' + value.number + '" data-jumpdesc="' + value.option_description[index1] + '"> ' + value1 +
                                '</label>';
                        });
                        step_bool = true;
                        break;
                    case '9': // 填空数值
                        survey2_tmp_object += '<label> <input type="number" name="text" class="form-control" placeholder=""> </label>';
                        break;
                    case '10': // 填空日期
                        survey2_tmp_object += '<label> <input type="text" name="text" class="form-control date" placeholder=""> </label>';
                        break;
                }
                survey2_tmp_object += '</div>';
                survey2_tmp_object += '</section>';
                if (step_bool) {
                    step_number += 1;
                    step_bool = false;
                    survey2_tmp_object += '<section id="step-' + step_number + '" data-type="subcheckbox"> ' +
                        '<div class="jump" data-jump="' + value.jump[0] + '"></div> ' +
                        '<div class="-checkbox-description description"></div> ' +
                        '</section>';
                }
            });
            this.$step = survey2_tmp_object;
        }
        step_number += 1;
        this.$step += '<section id="step-' + step_number + '" data-type="endPage">' +
            '<div class="-end-description">' +
            '<img src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/bbjkjl/survey/images/smile.png" alt="">' +
            '<div>非常感谢你的参与</div>' +
            '</div>' +
            '</section>';
        this.$step += '<ul>';
        for (step_i = 1; step_i <= step_number; step_i++) {
            this.$step += '<li><a href="#step-' + step_i + '"></a></li>';
        }
        this.$step += '</ul>';
    };
    survey2.prototype.initContainer = function () {
        this.$el.addClass('survey2-container').addClass('-survey');
        this.$container = $([
            '<div class="-survey-heading"></div>',
            '<div class="-survey-body swMain" id="wizard">',
            this.$step,
            '</div>',
            '<div class="-survey-footer"> ',
            '<button class="btn btn-info next-step">参加本调查</button> ',
            '</div>'
        ].join(''));
        this.$el.append(this.$container);
    };
    survey2.prototype.initUploadImage = function () {
        $('.upload-img div').off().on('click', function () {
            var _this = $(this);
            var input = $(this).find('input');
            wx.chooseImage({
                count: 1,
                sourceType: ['album', 'camera'],
                success: function (resChoose) {
                    var localIds = resChoose.localIds;
                    _this.find('img').remove();
                    _this.append('<img src="' + localIds[0] + '" />');
                    if (_this.data('number') > 0 && _this.data('number') != '3') {
                        _this.parent().parent().parent().find('.upload-inp' + (_this.data('number') + 1)).find('img').css('display', 'block')
                    }
                    wx.uploadImage({
                        localId: localIds[0].toString(),
                        isShowProgressTips: 1,
                        success: function (resUpload) {
                            input.val(resUpload.serverId);
                        }
                    });
                }
            });
        });
    };
    survey2.prototype.initJump = function () {
        survey2_tmp_object = [];
        $('.swMain>ul>li').each(function (i, v) {
            survey2_tmp_object.push($(v).find('a').attr('href'));
        });
        this.$jump = [];
        for (survey_i in survey2_tmp_object) {
            if (typeof $(survey2_tmp_object[survey_i]).data('number') !== 'undefined') {
                var key = survey2_tmp_object[survey_i].substr(6);
                this.$jump[key] = $(survey2_tmp_object[survey_i]).data('number');
            }
        }
        this.$swMainLength = $('.swMain > ul > li').length;
        this.$jump[this.$swMainLength] = 'end';
    };

    survey2.prototype.initWizardElement = function () {
        this.$wizard = $('#wizard');
        this.$nextStep = $(".next-step");
        this.$information = [];
        this.$question = [];
    };

    survey2.prototype.initEventEvent = function () {
        $(".star").find('i').on('click', function () {
            var element = $('.star');
            var id = $(this).data('id');
            element.find('i').removeClass('fa-star activeted').addClass('fa-star-o');
            $(this).addClass('activeted');
            element.find('i').each(function (i, v) {
                if ($(v).data('id') <= id) {
                    $(v).removeClass('fa-star-o').addClass('fa-star');
                }
            });
        });
        $('select').mobiscroll().select({
            theme: 'mobiscroll',
            display: 'bottom',
            lang: 'zh'
        });
        $('.date').mobiscroll().date({
            theme: 'mobiscroll',
            display: 'bottom',
            lang: 'zh'
        });
        $('input').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%'
        });
        survey2_this = this;
        this.$nextStep.off().on('click', function (e) {
            e.preventDefault();
            survey2_this.validateSteps();
        });
    };

    survey2.prototype.initWizard = function () {
        survey2_tmp_object = this;
        this.$wizard.smartWizard({
            selected: 0,
            keyNavigation: false,
            // onLeaveStep: leaveAStepCallback,
            onLeaveStep: null,
            onShowStep: survey2_tmp_object.onShowStep
        });
    };

    survey2.prototype.leaveAStepCallback = function (obj, context) {
        return this.validateSteps(context.fromStep, context.toStep);
    };

    survey2.prototype.onShowStep = function (obj, context) {
        thisNextStep = $('.next-step');
        thisNowElement = $(obj.attr('href'));
        switch (thisNowElement.data('type')) {
            case 'indexes':       // 首页
                thisNextStep.html('参加本调查');
                break;
            case 'information':   // 量表
                thisNextStep.html('正式答题');
                break;
            case 1: // 单选
            case 2: // 多选
            case 3: // 填空文本
            case 4: // 描述
            case 5: // 图片
            case 6: // 说明
            case 7: // 星级
            case 9:
            case 10:
                thisNextStep.html('下一题');
                break;
            case 8: // 多选描述
                thisNextStep.html('查看说明');
                break;
            case 'subcheckbox':   // 副多选
                thisNextStep.html('下一题');
                break;
            case 'endPage':       // 结束页
                thisNextStep.html('提交');
                break;
        }
        return true;
    };

    survey2.prototype.validateSteps = function (stepnumber, nextstep) {
        this.$inputElement = '';
        this.$selectElement = '';
        this.$jump_one = this.$jump.indexOf(thisNowElement.find('.jump').data('jump'));
        survey2_tmp_object = {};
        survey2_tmp_object.number = thisNowElement.data('number');
        survey2_tmp_object.type = thisNowElement.data('type');
        switch ('' + thisNowElement.data('type')) {
            case 'indexes':       // 首页
                this.$wizard.smartWizard('goForward');
                break;
            case 'information':   // 量表
                this.$inputElement = thisNowElement.find('.form-group');
                survey2_tmp_object2 = [];
                this.$inputElement.each(function (i, v) {
                    survey2_tmp_object1 = {};
                    if($(v).hasClass('select')) {
                        survey2_tmp_object1.name = $(v).find('select').data('name');
                    } else {
                        survey2_tmp_object1.name = $(v).find('input').data('name');
                    }
                    survey2_tmp_object1.value = $(v).find('input').val();
                    survey2_tmp_object2.push(survey2_tmp_object1);
                });
                this.$information = survey2_tmp_object2;
                this.$wizard.smartWizard('goForward');
                break;
            case '1':  // 单选
                this.$inputElement = thisNowElement.find('.content').find('.checked>input');
                this.$jump_single = this.$jump.indexOf(this.$inputElement.data('jump'));
                if (-1 === this.$jump_single) {
                    swal('失败', '请选择', 'warning');
                } else {
                    survey2_tmp_object.answer = thisNowElement.find('.content').find('input').index(this.$inputElement);
                    this.$question.push(survey2_tmp_object);
                    this.$wizard.smartWizard('goToStep', this.$jump_single);
                }
                break;
            case '2': // 多选
                this.$inputElement = thisNowElement.find('.content').find('.checked>input');
                survey2_tmp_object.answer = [];
                this.$inputElement.each(function (i, v) {
                    survey2_tmp_object.answer.push(thisNowElement.find('.content').find('input').index($(v)));
                });
                this.$question.push(survey2_tmp_object);
                this.$wizard.smartWizard('goToStep', this.$jump_one);
                break;
            case '3': // 填空文本
                this.$inputElement = thisNowElement.find('.content').find('input');
                if (thisNowElement.data('required') == 1 && this.$inputElement.val() == '') {
                    swal('请填写！', '', 'warning');
                } else {
                    survey2_tmp_object.answer = this.$inputElement.val();
                    this.$question.push(survey2_tmp_object);
                    this.$wizard.smartWizard('goToStep', this.$jump_one);
                }
                break;
            case '4': // 描述
                this.$inputElement = thisNowElement.find('.content').find('textarea');
                if (thisNowElement.data('required') == 1 && this.$inputElement.val() == '') {
                    swal('请填写！', '', 'warning');
                } else {
                    survey2_tmp_object.answer = this.$inputElement.val();
                    this.$question.push(survey2_tmp_object);
                    this.$wizard.smartWizard('goToStep', this.$jump_one);
                }
                break;
            case '5': // 图片
                this.$inputElement = thisNowElement.find('.content').find('input');
                survey2_tmp_object.answer = [];
                this.$inputElement.each(function (i, v) {
                    survey2_tmp_object.answer.push($(v).val());
                });
                if (thisNowElement.data('required') == 1 && survey2_tmp_object.answer[0] == '' && survey2_tmp_object.answer[1] == '' && survey2_tmp_object.answer[2] == '') {
                    swal('请上传！', '', 'warning');
                } else {
                    this.$question.push(survey2_tmp_object);
                    this.$wizard.smartWizard('goToStep', this.$jump_one);
                }
                break;
            case '6': // 说明
                this.$wizard.smartWizard('goToStep', this.$jump_one);
                break;
            case '7': // 星级
                this.$inputElement = thisNowElement.find('.content').find('.star .activeted');
                survey2_tmp_object.answer = (typeof this.$inputElement.data('id') === 'undefined') ? 0 : this.$inputElement.data('id');
                if (survey2_tmp_object.answer == '' && thisNowElement.data('required') == 1) {
                    swal('请选择！', '', 'warning');
                } else {
                    this.$question.push(survey2_tmp_object);
                    this.$wizard.smartWizard('goToStep', this.$jump_one);
                }
                break;
            case '8': // 多选描述
                this.$inputElement = thisNowElement.find('.content').find('.checked>input');
                var html = '';
                survey2_tmp_object.answer = [];
                this.$inputElement.each(function (i, v) {
                    html += '<p>' + $(v).data('jumpdesc') + '</p>';
                    survey2_tmp_object.answer.push(thisNowElement.find('.content').find('input').index($(v)));
                });
                this.$question.push(survey2_tmp_object);
                if (html === '') {
                    this.$wizard.smartWizard('goToStep', this.$jump_one);
                } else {
                    thisNowElement.next().find('.description').html(html);
                    this.$wizard.smartWizard('goForward');
                }
                break;
            case '9' :
                this.$inputElement = thisNowElement.find('.content').find('input');
                if (thisNowElement.data('required') == 1 && this.$inputElement.val() == '') {
                    swal('请填写！', '', 'warning');
                } else {
                    survey2_tmp_object.answer = this.$inputElement.val();
                    this.$question.push(survey2_tmp_object);
                    this.$wizard.smartWizard('goToStep', this.$jump_one);
                }
                break;
            case '10' :
                this.$inputElement = thisNowElement.find('.content').find('input');
                if (thisNowElement.data('required') == 1 && this.$inputElement.val() == '') {
                    swal('请填写！', '', 'warning');
                } else {
                    survey2_tmp_object.answer = this.$inputElement.val();
                    this.$question.push(survey2_tmp_object);
                    this.$wizard.smartWizard('goToStep', this.$jump_one);
                }
                break;
            case 'subcheckbox':   // 副多选
                this.$wizard.smartWizard('goToStep', this.$jump_one);
                break;
            case 'endPage':// 结束页
                if (this.options.save.active) {
                    var survey2_tmp_object3 = {};
                    survey2_tmp_object3.question = this.$question;
                    survey2_tmp_object3.information = this.$information;
                    survey2_tmp_object3.record_id = this.options.save.id;
                    save_url = this.options.save.url;
                    var option = {
                        type: 'POST',
                        url: save_url,
                        data: {data: survey2_tmp_object3},
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    };
                    $.ajax(option).done(function (m) {
                        switch (m.status) {
                            case 0:
                                WeixinJSBridge.call('closeWindow');
                                break;
                            default:
                                swal('失败', '提交失败了。', 'warning');
                        }

                    });
                } else {
                    WeixinJSBridge.call('closeWindow');
                }
                break;
        }
        return true;
    };


    // SURVEY2 DEFAULT METHOD
    // ======================

    /**
     * 方法
     * @param el
     * @param method_name
     * @returns {{}|*}
     */
    var survey2Method = function (el, method_name) {
        survey2_tmp_object = {};
        switch (method_name) {
            case 'save' :
                survey2_tmp_object = survey2Save(el);
                break;
        }
        return survey2_tmp_object;
    };

    /**
     * 举例保存方法
     * @param el
     * @returns {string[]}
     */
    var survey2Save = function (el) {

        return ['aaa', 'bbb', 'ccc'];
    };

    // SURVEY2 PLUGIN DEFINITION
    // =======================

    var allowedMethods = [];

    $.fn.survey2 = function (option) {
        // SURVEY2 METHODS
        if (typeof option == 'string' && $.inArray(option, allowedMethods) >= 0) {
            return survey2Method(this, option);
        }
        // SURVEY2 INIT
        if (typeof (option) == 'object') {
            new survey2(this, $.extend(survey2.DEFAULTS, option));
        }
    };
}(jQuery);