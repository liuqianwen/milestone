var id = $('#act-id').val();
var num = 0;
var price = Number($('#price').val());
var is_pay = $('#is_pay').val();
var enroll_pay_type = $('#enroll_pay_type').val();
var pay_type = (enroll_pay_type == 1 || enroll_pay_type == 2) ? enroll_pay_type : 1;
var form = {
    init: function() {
        form.eventFn();
        form.checkInput();
        form.signSave();
    },
    eventFn: function(){
        //选择支付方式
        $('input[type=radio][name=pay_type]').on('change', function(){
            pay_type = $(this).val();
            if(pay_type == 1){
                $('.pay_num').addClass('display-none')
                $('.pay-sign').removeClass('display-none');
                $('.sign-save').addClass('display-none');
            }else {
                $('.pay_num').removeClass('display-none')
                $('.pay-sign').addClass('display-none');
                $('.sign-save').removeClass('display-none');
            }
        })
        //添加到列表
        $('body').on('click','.add.active', function() {
            var li = '<div class="li">\n' +
                '                            <div class="li-content">\n' ;
            // $('.form-group input[type=text]').each(function() {
            $('.form-group .form-control').each(function() {
                var text = $(this).val();
                var number = $(this).data('number');
                var name = $(this).data('name');
                li += '                                <span data-number="'+number+'" data-name="'+name+'">'+text+'</span>\n' ;
            })
            li += '                            </div>\n' +
                '                            <div class="delete">\n' +
                '                                移除\n' +
                '                            </div>\n' +
                '                        </div>';
            $('.table').append(li);
            num += 1;
            form.checkNum();
        })
        $('.add').on('click', function() {
            form.checkAdd();
        })
        //移除
        $('.table').on('click','.delete',function() {
            $(this).parent().remove();
            num -= 1;
            form.checkNum();
        })
        //发票信息确定
        $('.ticket-btn').on('click', function() {
            $('.ticket-info').addClass('display-none');
            $('.shade').addClass('display-none');
        })
        $('.pay-ticket').on('click', function(e) {
            // e.stopPropagation();
            $('.ticket-info').removeClass('display-none');
            $('.shade').removeClass('display-none');
        })
    },
    checkInput: function() {
        $('.form-group input[type=text]').on('blur', function() {
            var text = $(this).val();
            var len  = text.length;
            if ($(this).data('required') && !text) {
                $(this).parent().siblings('.warning').addClass('active');
            }else {
                $(this).parent().siblings('.warning').removeClass('active');
            }
            form.checkForm();
        })
        $('.form-group input[type=text]').bind('input propertychange',function () {
            var text = $(this).val();
            var len  = text.length;
            var max_len = $(this).data('length');
            if (len > max_len) {
                var newText = text.substr(0,max_len);
                $(this).val(newText);
            }
        })
    },
    checkAdd: function() {
        var check = true;
        $('.form-group').each(function() {
            var text = $(this).find('input').val();
            var is_required = $(this).find('input').data('required');
            if (!text && is_required) {
                check = false;
                $(this).find('.warning').addClass('active');
            }
        })
    },
    checkForm: function() {
        var check = true;
        $('.form-group').each(function() {
            var text = $(this).find('input').val();
            var is_required = $(this).find('input').data('required');
            if (!text && is_required) {
                check = false;
            }
        })
        if(check) {
            $('.add').addClass('active');
        }else {
            $('.add').removeClass('active');
        }
    },
    checkNum: function() {
        $('.num').text('共'+num+'条');
        if (price && num >= 1) {
            $('.price-total').text('￥'+price * num);
        }else if (num < 1) {
            $('.price-total').text('￥'+price);
        }
        if (num >= 1) {
            $('.pay-sign').addClass('active');
            $('.sign-save').addClass('active');
        }else {
            $('.pay-sign').removeClass('active');
            $('.sign-save').removeClass('active');
        }
    },
    signSave: function() {
        $('.bottom').on('click','.pay-sign.active,.sign-save.active',function() {
            var arr = [];
            $('.table .li').each(function(i,v) {
                var obj = {};
                var li = [];
                var that = $(this);
                that.find('.li-content span').each(function(j,val) {
                    var tmp = {};
                    tmp.name = $(this).data('name');
                    tmp.number = $(this).data('number');
                    tmp.value = $(this).text();
                    li.push(tmp);
                })
                obj.value = li;
                arr.push(obj);
            })
            var ticket = {};
            ticket.head = $('.ticket-info input[name=head]').val();
            ticket.number = $('.ticket-info input[name=number]').val();
            ticket.name  = $('.ticket-info input[name=name]').val();
            ticket.money = $('.ticket-info input[name=money]').val();
            ticket.phone = $('.ticket-info input[name=phone]').val();
            ticket.email  = $('.ticket-info input[name=email]').val();

            $.ajax({
                url: '/wechat/signup/save',
                method: 'post',
                data: {data: arr,id:id,price: price,num:num, ticket:ticket,pay_type: pay_type},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name=_token]').attr('content')
                }
            }).done(function(result) {
                if ((result.status == 0) && is_pay == 1 && pay_type == 1) {
                    wx.chooseWXPay({
                        timestamp: result.pay.timestamp,
                        nonceStr: result.pay.nonceStr,
                        package: result.pay.package,
                        signType: result.pay.signType,
                        paySign: result.pay.paySign, // 支付签名
                        success: function (res) {
                            // 支付成功后的回调函数
                            layer.open({
                                content: '报名成功'
                                ,skin: 'msg'
                                ,time: 2 //2秒后自动关闭
                            });
                            setTimeout(function() {
                                window.location.href = '/wechat/signup';
                            },1000)
                        }
                    });
                }else if(result.status == 0 && is_pay == 1 && pay_type == 2) {
                    layer.open({
                        content: '转账后系统确认报名，请耐心等待'
                        ,skin: 'msg'
                        ,time: 2 //2秒后自动关闭
                    });
                    setTimeout(function() {
                        window.location.href = '/wechat/signup';
                    },1000)
                }else if(result.status == 0 ) {
                    layer.open({
                        content: '报名成功'
                        ,skin: 'msg'
                        ,time: 2 //2秒后自动关闭
                    });
                    setTimeout(function() {
                        window.location.href = '/wechat/signup';
                    },100)
                }
            })
        })
        $('.bottom').on('click','.pay-sign,.sign-save',function(){
            if (!$(this).hasClass('active')) {
                layer.open({
                    content: '请先添加到报名表'
                    ,skin: 'msg'
                    ,time: 2 //2秒后自动关闭
                });
            }
        })
    }
};