//分类切换隐藏显示
function all() {
    init.lunboFn();
    init.tabFn();
    init.searchFn();
    init.moreFn();
    init.detailFn();
}
var init = {
    lunboFn: function () {
        //轮播图
        $('.wrap .pic li:first').addClass('on');
        var wrap=$('.wrap'),
            pic=$('.wrap .pic li'),
            list=$('.wrap .cercle span'),
            index=0,
            timer=null,
            moveEndX,startX,startY,moveEndY,X,Y;
        changePic(index);
        // 定义并调用自动播放函数
        timer = setInterval(autoPlay, 4000);

        // 触摸整个容器时停止自动播放
        wrap.on('touchstart', function () {
            clearInterval(timer);
        })

        // 触摸结束时继续播放至下一张
        wrap.on('touchend', function () {
            timer = setInterval(autoPlay, 4000);
        })

        function autoPlay () {
            if (++index >= pic.length) index = 0;
            changePic(index);
        }

        // 定义图片切换函数
        function changePic (curIndex) {
            pic.each(function(index,value){
                $(this).css('display','none');
                list.each(function(len,v) {
                    if(index == len) {
                        $(this).removeClass('on');
                    }
                    if(len == curIndex) {
                        $(this).addClass('on');
                    }
                })
                if(index == curIndex) {
                    $(this).show();
                    $(this).find('.text').show();
                }
            })
        }
        $(".wrap li").on("touchstart", function(e) {
            // 判断默认行为是否可以被禁用
            if (e.cancelable) {
                // 判断默认行为是否已经被禁用
                if (!e.defaultPrevented) {
                    e.preventDefault();
                }
            }
            startX = e.originalEvent.changedTouches[0].pageX,
                startY = e.originalEvent.changedTouches[0].pageY;
        });
        $(".wrap li").on("touchend", function(e) {
            // 判断默认行为是否可以被禁用
            if (e.cancelable) {
                // 判断默认行为是否已经被禁用
                if (!e.defaultPrevented) {
                    e.preventDefault();
                }
            }
            moveEndX = e.originalEvent.changedTouches[0].pageX,
                moveEndY = e.originalEvent.changedTouches[0].pageY,
                X = moveEndX - startX,
                Y = moveEndY - startY;
            //左滑
            if ( X > 10 ) {
                if(index == 0) {
                    index = pic.length -1;
                }else{
                    index = index -1;
                }
                changePic(index);
            }
            //右滑
            else if ( X < -10 ) {
                if(index == pic.length -1) {
                    index = 0;
                }else{
                    index = index + 1;
                }
                changePic(index);
            } else {
                var id = $(this).data('id');
                window.location.href = '/wechat/edu/article?id=' + id;
            }
        });
    },
    tabFn: function () {
        $('.type .type-name').on('click', function () {
            var type = $(this).data('type');
            $('#type').val(type);
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            switch (type) {
                case 1:
                    window.location.href = '/wechat/education';
                    break;
                case 2:
                    window.location.href = '/wechat/edu/video/list';
                    break;
                case 3:
                    window.location.href = 'https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzIzMzAwMTU1MQ==&scene=124#wechat_redirect';
                    break;
            }
        })
    },
    searchFn: function () {
        //点击输入框显示
        $('.search input').on('focus',function() {
            $('.top .cancel').removeClass('display-none');
            $('.search').addClass('search-a');
            var type = $('#type').val();
            switch(type) {
                case '1':
                    $('.before-search1').removeClass('display-none');
                    break;
                case '2':
                    $('.before-search2').removeClass('display-none');
                    break;
            }
            $('.content').addClass('display-none');
        })
        //点击取消显示
        $('.cancel').on('click',function () {
            var type = $('#type').val();
            switch(type) {
                case '1':
                    window.location.href = '/wechat/education';
                    break;
                case '2':
                    window.location.href = '/wechat/edu/video/list'
                    break;
            }
        })
        //搜索内容 获取键盘搜索按钮事件
        $(".search input").on('keypress', function(e) {
            var keycode = e.keyCode;
            //获取搜索框的值
            var searchText = $(this).val();
            if (keycode == '13') {
                e.preventDefault();
                //请求搜索接口
                if (searchText == '') {
                    alert('请输入检索内容！');
                } else {
                    var type = $('#type').val();
                    var data = {};
                    var search = $('.search input').val();
                    window.location.href = '/wechat/education?type='+type+'&search='+search;
                }
            }
        });
        //热门标签搜索
        $('.before-search .rm-tag').on('click', function (e) {
            var id = $(this).data('id');
            var type = $('#type').val();
            var data = {};
            data.id = id;
            data.type = type;
            window.location.href = '/wechat/education?type=' +type + '&id=' + id;
        })
    },
    moreFn: function () {
        $('.top-title .more').on('click',function () {
            var type = $('#type').val();
                window.location.href = '/wechat/edu/all?type= ' + type;
                // window.location.href = '/wechat/edu/video/list/more?type=' + type;
            // }

        })
    },
    detailFn: function () {
        //文章详情
        $('body').on('click','.type-1 .item',function (e) {
            var id = $(this).data('id');
            window.location.href = '/wechat/edu/article?id=' + id;
        })
        //视频详情
        $('body').on('click','.type-2 .item,.type-3 .item',function () {
            var id = $(this).data('id');
            window.location.href = '/wechat/edu/video?id= ' + id;
        })
    }

}
all();