var doctor = function () {
    "use strict";
    var tableObj, doctorId, jcropApi, imgObj;
    var tableEle = $('#_doctorTable');
    var formEditEle = $('.form-editDoctor');
    // 表格插件
    var table = function () {
        tableEle.bootstrapTable({
            method: 'get',
            url: '/doctor/data',
            toolbar: "#toolbar",
            cache: false,
            striped: true,
            selectItemName: "radioName",
            pagination: true,
            pageList: [10, 20, 50, 100],
            pageSize: 10,
            pageNumber: 1,
            search: true,
            sidePagination: 'server',
            showColumns: true,
            clickToSelect: true,
            queryParams: function (params) {
                $('.edit-doctor,.edit-doctor-activate,.edit-doctor-logout,.info-doctor').prop('disabled', true);
                params.id = $('.hospital_id').val();
                return params;
            }
        });
        tableEle.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
            tableObj = tableEle.bootstrapTable('getSelections')[0];
            doctorId = tableObj.id;
            $('.edit-doctor').prop('disabled', !tableEle.bootstrapTable('getSelections').length);
        });
    };

    // 点击事件
    var event = function () {

        $('.edit-doctor').off().on("click", function () {
            $.subview({
                content: '#editDoctor',
                startFrom: "right",
                onShow: function () {
                    $('#editDoctor .name').val(tableObj.name);
                    $('#editDoctor .tel').val(tableObj.tel);
                    $('#editDoctor .department').val(tableObj.department);
                    $('#editDoctor .position').val(tableObj.position);
                    $('#editDoctor .img-thumbnail').attr('src',tableObj.imgUrl);
                    $('#editDoctor .imgUrl').val(tableObj.image);
                    $('#editDoctor .good').val(tableObj.good);
                    $('#editDoctor .introduce').val(tableObj.introduce);
                },
                onClose: function () {
                    formEditEle[0].reset();
                    $.hideSubview();
                },
                onHide: function () {
                }
            });
        });
        $(".close-subview-button").off().on("click", function (e) {
            $(".close-subviews").trigger("click");
            e.preventDefault();
        });
    };
    // 修改
    var editDoctorForm = function () {
        var errorHandler1 = $('.errorHandler', formEditEle);
        var successHandler1 = $('.successHandler', formEditEle);
        formEditEle.validate({
            errorElement: "span",
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                element.parent().parent().find('label').append(error);
            },
            ignore: "",
            rules: {
                name: { required: true },
                position: { required: true},
                good: { required: true},
                introduce: { required: true }
            },
            messages: {
                name: { required: ' *不能为空' },
                position: { required: ' *不能为空'},
                good: { required: ' *不能为空'},
                introduce: { required: ' *不能为空' }
            },
            submitHandler: function (form) {
                errorHandler1.hide();
                $.blockUI({
                    message: '<i class="fa fa-spinner fa-spin"></i> 正在提交数据...'
                });
                var obj     = new Object;
                obj.name    = $('.form-editDoctor .name').val();
                obj.department = $('.form-editDoctor .department').val();
                obj.position = $('.form-editDoctor .position').val();
                obj.tel = $('.form-editDoctor .tel').val();
                obj.sex = $('.form-editDoctor .sex').val();
                obj.good = $('.form-editDoctor .good').val();
                obj.introduce = $('.form-editDoctor .introduce').val();
                obj.headimg = $('.form-editDoctor .imgUrl').val();
                console.log(obj.headimg);
                $.ajax({
                    type: 'POST',
                    url: '/doctor/edit',
                    data: {data: obj, id: doctorId},
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    success: function (m) {
                        switch (m.status) {
                            case 0:
                                $(form)[0].reset();
                                tableEle.bootstrapTable('refresh');
                                $.hideSubview();
                                break;
                            default:
                                swal('失败', m.message, 'warning');
                        }
                        $.unblockUI();
                    }
                });
            },
            invalidHandler: function (event, validator) {//display error alert on form submit
                errorHandler1.show();
            }
        });
    };
    var ajaxLoadImage     = function (){
        $('.img-thumbnail').on('click', function (){
            var that = $(this);
            $("#file").val('');
            $('#clipArea').find('div').remove();
            $('#publishModal').modal('show');
            new PhotoClip('#clipArea', {
                size: [300, 300],   //剪裁完成的图片height，width
                outputSize: [1000, 1000], //剪裁框的height，width
                file: "#file",     //文件上传框ID
                view: "#view",     //预览div的ID
                ok: "#clipBtn",    //剪裁开始按钮ID
                outputType: 'jpg', //指定输出图片的类型，可选 "jpg" 和 "png" 两种种类型，默认为 "jpg"。
                //img: 'img/mm.jpg',
                loadStart: function (){
                    console.log('开始读取照片');
                },
                loadComplete: function (){
                    console.log('照片读取完成');
                },
                done: function (dataURL){
                    $.ajax({
                        type: "POST",
                        url: '/image/upload',
                        dataType: 'json',
                        data: { img: dataURL,path:'8020/headImg' },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        success: function (m){
                            switch ( m.status ) {
                                case 0:
                                    that.attr('src', m.data.route);
                                    that.parent().find('.imgUrl').val(m.data.url);
                                    $('#publishModal').modal('hide');
                                    break;
                                default:
                            }
                        }
                    });
                },
                fail: function (msg){
                    console.log(msg);
                }
            });
        });
    };

    return {
        init: function () {
            table();
            event();
            editDoctorForm();
            ajaxLoadImage();
        }
    };
}();