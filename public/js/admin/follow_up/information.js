var information = function () {
    "use strict";
    var table;
    // 表格部分
    var subViewElement, subViewContent;
    var informationname = $('.username').text();
    var jQueryTable = $('#information_table');
    var informationTable = function () {
        jQueryTable.bootstrapTable({
            method: 'get',
            url: '/follow_up/information/data',
            toolbar: "#toolbar",
            cache: false,
            striped: true,
            selectItemName: "radioName",
            pagination: true,
            pageList: [10, 20, 50, 100],
            pageSize: 20,
            pageNumber: 1,
            search: true,
            sidePagination: 'server',
            queryParams: queryParams,
            showColumns: true,
            clickToSelect: true
        });
        $('#large-columns-table').next().click(function () {
            $(this).hide();
            buildTable(jQueryTable, 50, 50);
        });
        jQueryTable.on('check.bs.table uncheck.bs.table ' + 'check-all.bs.table uncheck-all.bs.table', function () {
            $('.edit-information').prop('disabled', !jQueryTable.bootstrapTable('getSelections').length);
        });
        $('.th-inner').parent().css('text-align', 'center');
    };
    var queryParams = function (params) {
        $('.edit-information').prop('disabled', true);
        return params;
    };
    var checkAddinformation = function () {
        bootbox.confirm({
            buttons: {cancel: {label: "取消"}, confirm: {label: "确认"}},
            message: "您没有保存该记录，确定关闭吗?",
            callback: function (result) {
                if (result) {
                    $('.form-addinformation')[0].reset();
                    $.hideSubview();
                }
            }
        });
    };
    var runSubview = function () {
        $('.new-information').off().on("click", function () {
            subViewElement = $(this);
            subViewContent = subViewElement.attr('href');
            $.subview({
                content: subViewContent,
                startFrom: "right",
                onShow: function () {
                    $('.form-addinformation .xuanxiang .bubi').remove();
                    $('.form-addinformation .xuanxiang').parent().parent().hide();

                },
                onClose: function () {
                    checkAddinformation();
                },
                onHide: function () {
                }
            });
        });
        $(".close-subview-button").off().on("click", function (e) {
            $(".close-subviews").trigger("click");
            e.preventDefault();
        });
        $('.edit-information').on('click', function () {
            var status = jQueryTable.bootstrapTable('getSelections')[0].status;
            $("#editinformation .name").val('');
            $("#editinformation .xuanxiang input").remove();
            $("#editinformation .type").val('');
            $("#editinformation .status").val(status);
            $.subview({
                content: '#editinformation',
                startFrom: "right",
                onShow: function () {
                    var data = jQueryTable.bootstrapTable('getSelections')['0'];
                    $("#editinformation .name").val(data.name);
                    $("#editinformation .type").val(data.types);
                    if (data.types == 1) {
                        for (var i = 0; i < data.option.length; i++) {
                            var option = '<input type="text" class="form-control option" name="option" value="' + data.option[i] + '" disabled style="margin-bottom: 10px;">'
                            $('#editinformation .xuanxiang').append(option);
                        }
                    }

                    if (data.types == 1) {
                        $('#editinformation .xuanxiang').parent().parent().show();
                    } else {
                        $('#editinformation .xuanxiang').parent().parent().hide();
                    }
                },
                onClose: function () {
                    $.hideSubview();
                },
                onHide: function () {
                }
            });
        });
    };
    var event = function () {
        $('.addinformation').click(function () {
            var type = $('.form-addinformation .type option:selected').val();
            var name = $('.form-addinformation .name').val();
            if (name == '') {
                swal('请填写常量名称');
                return false;
            }
            var option = [];
            if (type == 1) {
                $('.form-addinformation .option').each(function () {
                    if ($(this).val() != '') {
                        option.push($.trim($(this).val()));
                    }
                })
                if (option.length < 2) {
                    swal('请填写选项');
                    return false;
                }
            }
            var informationToAdd = new Object;
            informationToAdd.type = type;
            informationToAdd.name = name;
            informationToAdd.option = option;
            $.blockUI({
                message: '<i class="fa fa-spinner fa-spin"></i> 正在提交数据'
            });
            $.ajax({
                type: 'POST',
                url: '/follow_up/information/add',
                data: {postdata: informationToAdd},
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success: function (json) {
                    if (json.say == 'ok') {
                        $('.form-addinformation')[0].reset();
                        $.hideSubview();
                        toastr.success(informationname + '添加一个常量！');
                        jQueryTable.bootstrapTable('refresh');
                    }
                    $.unblockUI();
                }
            });
        });

        $('.editinformation').click(function () {
            var id = jQueryTable.bootstrapTable('getSelections')[0].id;
            var status = $('#editinformation .status').val();
            $.blockUI({
                message: '<i class="fa fa-spinner fa-spin"></i> 正在提交数据'
            });
            $.ajax({
                type: 'POST',
                url: '/follow_up/information/edit',
                data: {id: id, status: status},
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success: function (json) {
                    if (json.say == 'ok') {
                        $('.form-editinformation')[0].reset();
                        $.hideSubview();
                        toastr.success(informationname + '修改一个常量！');
                        jQueryTable.bootstrapTable('refresh');
                    }
                    $.unblockUI();
                }
            });
        });


        $('.type').change(function () {
            var type = $('.type option:selected').val();
            if (type == 1) {
                $('.option').parent().parent().parent().show();
            } else {
                $('.option').parent().parent().parent().hide();
            }
        })


        $('.addxuanxiang').click(function () {
            var add = true;
            $('.form-addinformation .xuanxiang input').each(function () {
                if ($(this).val() == '') {
                    swal('选项不能为空');
                    add = false;
                    return false;
                }
            });
            if (add == true) {
                var option = '   <input type="text" class="form-control option bubi" name="option" value="" placeholder="请填写选项" style="margin-top: 10px;">';
                $(this).before(option);
            }
        });
    };


    return {
        init: function () {
            informationTable();
            runSubview();
            event();
        }
    }
}();