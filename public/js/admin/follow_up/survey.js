var survey = function (){
    "use strict";
    var allScale, allTemplate;
    var table, close   = true;
    var jQueryTable    = $('#survey_table');
    var type_id        = 0;
    var surveyTable    = function (){
        jQueryTable.bootstrapTable({
            method: 'get',
            url: '/follow_up/survey/data',
            toolbar: "#toolbar",
            cache: false,
            striped: true,
            selectItemName: "radioName",
            pagination: true,
            pageList: [10, 20, 50, 100],
            pageSize: 20,
            pageNumber: 1,
            search: true,
            sidePagination: 'server',
            queryParams: queryParams,
            showColumns: true,
            clickToSelect: true
        });
        jQueryTable.on('check.bs.table uncheck.bs.table ' + 'check-all.bs.table uncheck-all.bs.table', function (){
            $('.edit-survey').prop('disabled', !jQueryTable.bootstrapTable('getSelections').length);
            $('.preview-survey').prop('disabled', !jQueryTable.bootstrapTable('getSelections').length);
            $('.detail-survey').prop('disabled', !jQueryTable.bootstrapTable('getSelections').length);
            $('.stop-survey').prop('disabled', !jQueryTable.bootstrapTable('getSelections').length);
        });
        $('.th-inner').parent().css('text-align', 'center');
    };
    var queryParams    = function (params){
        $('.edit-survey').prop('disabled', true);
        $('.preview-survey').prop('disabled', true);
        $('.detail-survey').prop('disabled', true);
        $('.stop-survey').prop('disabled', true);
        params.type_id = type_id;
        return params;
    };
    var typeChange     = function (){
        $('#type').off().on('change', function (){
            type_id = $(this).val();
            jQueryTable.bootstrapTable('refresh');
        });
    };
    var autoComplete   = function (){
        $(".type").off().autoComplete({
            minChars: 0,
            source: function (term, response){
                $.getJSON('/follow_up/survey/autoComplete',
                    { q: term },
                    function (data){
                        response(data);
                    });
            }
        });
    };
    var getTemplate    = function (){
        $.ajax({
            type: 'GET',
            url: '/follow_up/survey/template/list',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            success: function (json){
                allTemplate = json;
            }
        });
    };
    var getScale       = function (){
        $.ajax({
            type: 'GET',
            url: '/follow_up/information/search',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            success: function (json){
                allScale = json;
            }
        });
    };
    var runSubview     = function (){
        $('.add-survey').off().on("click", function (){
            if ( typeof (allScale) == 'object' && typeof (allTemplate) == 'object' ) {
                $.subview({
                    content: '#AddSurvey',
                    startFrom: "right",
                    onShow: function (){
                        $('#AddSurvey').survey3({
                            form_class: "form-add-survey",
                            scale: allScale,
                            legend: '添加问卷',
                            template: {
                                show: true,
                                data: allTemplate[1],
                                url: '/follow_up/survey/template'
                            },
                            handle: {
                                action: 'add',
                                data: {}
                            },
                            preview: {
                                show: true,
                                url: '/follow_up/class/preview'
                            },
                            save: {
                                show: true,
                                url: '/follow_up/survey/save'
                            },
                            tableId: 'survey_table',
                            type: 1,
                            question_type:[
                                { name: '单选题', icon: 'fa fa-dot-circle-o', id: 1 },
                                // { name: '多选题', icon: 'fa fa-check-square-o', id: 2 },
                            ]
                        });
                        autoComplete();
                    },
                    onClose: function (){
                        checkAddSurvey();
                    },
                    onHide: function (){
                    }
                });
            }
        });
        $('.edit-survey').off().on('click', function (){
            table = jQueryTable.bootstrapTable('getSelections')[0];
            // if ( table.end != '' ) {
            //     swal('停用问卷，不可修改！', '', 'warning');
            //     return;
            // } else {
            //     if ( table.start != '' ) swal('问卷正在使用，只可修改问卷说明，微信消息内容！');
                $.ajax({
                    type: 'POST',
                    url: '/follow_up/survey/edit/get',
                    data: { id: table.dataId },
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    success: function (json){
                        $.subview({
                            content: '#AddSurvey',
                            startFrom: "right",
                            onShow: function (){
                                $('#AddSurvey').survey3({
                                    form_class: "form-edit-survey",
                                    scale: allScale,
                                    legend: '修改问卷',
                                    handle: {
                                        action: 'edit',
                                        data: json.data
                                    },
                                    template: {
                                        show: json.template,
                                        data: allTemplate[1],
                                        url: '/follow_up/survey/template'
                                    },
                                    preview: {
                                        show: true,
                                        url: '/follow_up/survey/preview'
                                    },
                                    save: {
                                        show: true,
                                        url: '/follow_up/survey/save'
                                    },
                                    tableId: 'survey_table',
                                    type: json.data.type,
                                    question_type:[
                                        { name: '单选题', icon: 'fa fa-dot-circle-o', id: 1 },
                                        // { name: '多选题', icon: 'fa fa-check-square-o', id: 2 },
                                    ]
                                });
                                autoComplete();
                            },
                            onClose: function (){
                                checkAddSurvey();
                            },
                            onHide: function (){
                            }
                        });
                    },
                    error: function (){
                        console.log('修改返回数据错误！');
                    }
                });
            // }
        });
        $('.preview-survey').off().on('click', function (){
            table = jQueryTable.bootstrapTable('getSelections')[0];
            $.subview({
                content: '#PreviewSurvey',
                startFrom: "right",
                onShow: function (){
                    $.ajax({
                        type: 'POST',
                        url: '/follow_up/class/preview/two',
                        data: { id: table.dataId },
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        success: function (json){
                            if ( json.say == 'ok' ) {
                                $('.preview-iframe').attr('src', json.url);
                                $('.preview-image').attr('src', json.src);
                            }
                        },
                        error: function (){
                            console.log('预览返回数据错误！');
                        }
                    });
                },
                onClose: function (){
                    $.hideSubview();
                },
                onHide: function (){
                }
            });
        });
        $('.detail-survey').off().on('click', function (){
            table = jQueryTable.bootstrapTable('getSelections')[0];
            $.ajax({
                type: 'POST',
                url: '/follow_up/survey/edit/get',
                data: { id: table.dataId },
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success: function (json){
                    $.subview({
                        content: '#AddSurvey',
                        startFrom: "right",
                        onShow: function (){
                            $('#AddSurvey').survey2({
                                form_class: "form-detail-survey",
                                scale: allScale,
                                legend: '问卷详情',
                                handle: {
                                    action: 'edit',
                                    data: json.data
                                },
                                template: {
                                    show: false,
                                    data: allTemplate,
                                    url: '/follow_up/survey/template'
                                },
                                preview: {
                                    show: false,
                                    url: '/follow_up/survey/preview'
                                },
                                save: {
                                    show: false,
                                    url: '/follow_up/survey/save'
                                },
                                tableId: 'survey_table'
                            });
                        },
                        onClose: function (){
                            $.hideSubview();
                        },
                        onHide: function (){
                        }
                    });
                },
                error: function (){
                    console.log('修改返回数据错误！');
                }
            });
        });
        $('.stop-survey').off().on('click', function (){
            table = jQueryTable.bootstrapTable('getSelections')[0];
            if ( table.end != '' ) {
                swal('已经结束');
            } else {
                bootbox.confirm({
                    buttons: { cancel: { label: "取消" }, confirm: { label: "确认" } },
                    message: "您确定结束吗?停用后关于此问卷的策略全部停止，停用动作不可撤消。",
                    callback: function (result){
                        if ( result ) {
                            $.ajax({
                                type: 'POST',
                                url: '/follow_up/survey/stop',
                                data: { id: table.dataId },
                                dataType: 'json',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                                },
                                success: function (json){
                                    jQueryTable.bootstrapTable("refresh");
                                    $.hideSubview();
                                },
                                error: function (){
                                    console.log('修改返回数据错误！');
                                }
                            });
                        }
                    }
                });
            }
        });
        $(".close-subview-button").off().on("click", function (e){
            $(".close-subviews").trigger("click");
            e.preventDefault();
        });
    };
    var checkAddSurvey = function (){
        bootbox.confirm({
            buttons: { cancel: { label: "取消" }, confirm: { label: "确认" } },
            message: "您没有保存该记录，确定关闭吗?",
            callback: function (result){
                if ( result ) {
                    $.hideSubview();
                }
            }
        });
    };
    return {
        init: function (){
            surveyTable();
            getScale();
            getTemplate();
            runSubview();
            autoComplete();
            typeChange();
        }
    }
}();