var volunteer = function () {
    // 表格
    var clickTime = 0;
    var userTableObj = null;
    var userId = null;
    var addForm     = $('#addInformation');
    var editForm     = $('#editInformation');
    var editor,editor1;
    var userTable = function () {
        var tableEle = $('#_userTable');
        tableEle.bootstrapTable({
            method: 'get',
            url: '/volunteer/data',
            toolbar: "#toolbar",
            cache: true,
            striped: true,
            selectItemName: "radioName",
            pagination: true,
            pageList: [10, 20, 50, 100],
            pageSize: 20,
            pageNumber: 1,
            search: true,
            sidePagination: 'server',
            queryParams: function (params) {
                $('.available').attr('disabled', true);
                $('.user-edit').attr('disabled', true);
                $('.unavailable').attr('disabled', true);
                return params
            },
            showColumns: true,
            clickToSelect: true
        });
        tableEle.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
            userTableObj = tableEle.bootstrapTable('getSelections')[0];
            $('.available').attr('disabled', !tableEle.length);
            $('.user-edit').attr('disabled', !tableEle.length);
            $('.unavailable').attr('disabled', !tableEle.length);
            userId = userTableObj._id;
        });
    };

    var editorFunc = function(){
         editor =  new Simditor({
            textarea: $('#editor'),
            toolbar: ['title', 'bold', 'italic', 'underline', 'strikethrough','fontScale', 'color', '|', 'ol', 'ul', 'blockquote', 'code', 'table', '|', 'link', 'image', 'hr', '|', 'indent', 'outdent', 'alignment', '|', 'html'],
            upload: {
                url: '/image/uploads',
                params: null,
                fileKey: 'upload_file',
                connectionCount: 3,
                leaveConfirm: '文件上传中，关闭此页面将取消上传。'
            },
            pasteImage: true,
            tabIndent: true,
            allowedTags:['div','br', 'span', 'a', 'img', 'b', 'strong', 'i', 'strike', 'u', 'font', 'p', 'ul', 'ol', 'li', 'blockquote', 'pre', 'code', 'h1', 'h2', 'h3', 'h4', 'hr'],
            allowedStyles:{
                div:['font-size','font-family','color','float','display','flex-direction','flex-wrap','flex-flow','justify-content','align-items','align-content','width','height'],
                table:['table']
            }
        });
        editor.on('valuechanged',function(e){
            var ele = $('.iwindow');
            ele.html(editor.getValue());
        })
        editor1 =  new Simditor({
            textarea: $('#editor1'),
            toolbar: ['title', 'bold', 'italic', 'underline', 'strikethrough','fontScale', 'color', '|', 'ol', 'ul', 'blockquote', 'code', 'table', '|', 'link', 'image', 'hr', '|', 'indent', 'outdent', 'alignment', '|', 'html'],
            upload: {
                url: '/image/uploads',
                params: null,
                fileKey: 'upload_file',
                connectionCount: 3,
                leaveConfirm: '文件上传中，关闭此页面将取消上传。'
            },
            pasteImage: true,
            tabIndent: true,
            allowedTags:['div','br', 'span', 'a', 'img', 'b', 'strong', 'i', 'strike', 'u', 'font', 'p', 'ul', 'ol', 'li', 'blockquote', 'pre', 'code', 'h1', 'h2', 'h3', 'h4', 'hr'],
            allowedStyles:{
                div:['font-size','font-family','color','float','display','flex-direction','flex-wrap','flex-flow','justify-content','align-items','align-content','width','height'],
                table:['table']
            }
        });
        editor1.on('valuechanged',function(e){
            var ele = $('.iwindow1');
            ele.html(editor1.getValue());
        })
    };


    var uploadImg   = function (){
        $('.img-thumbnail').on('click', function (){
            var that = $(this);
            $("#file").val('');
            $('#clipArea').find('div').remove();
            $('#publishModal').modal('show');
            new PhotoClip('#clipArea', {
                size: [525, 360],   //剪裁完成的图片height，width
                outputSize: [1800, 1000], //剪裁框的height，width
                file: "#file",     //文件上传框ID
                view: "#view",     //预览div的ID
                ok: "#clipBtn",    //剪裁开始按钮ID
                outputType: 'jpg', //指定输出图片的类型，可选 "jpg" 和 "png" 两种种类型，默认为 "jpg"。
                //img: 'img/mm.jpg',
                loadStart: function (){
                    console.log('开始读取照片');
                },
                loadComplete: function (){
                    console.log('照片读取完成');
                },
                done: function (dataURL){
                    $.ajax({
                        type: "POST",
                        url: '/image/upload',
                        dataType: 'json',
                        data: { img: dataURL },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        success: function (m){
                            switch ( m.status ) {
                                case 0:
                                    that.attr('src', m.data.route);
                                    that.parent().find('.imgUrl').val(m.data.url);
                                    $('#publishModal').modal('hide');
                                    break;
                                default:
                            }
                        }
                    });
                },
                fail: function (msg){
                    console.log(msg);
                }
            });
        });
    };

    var event = function () {
        $('.user-add').off().on('click', function () {
            preventMultipleClicks(function () {
                $.subview({
                    content: "#_addUser",
                    startFrom: "right",
                    onShow: function () {
                        $(addForm).find('.system-malady').removeClass('current');
                        $('#tags_1').importTags('');
                        $(addForm).find('.img-thumbnail').attr('src','https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537254584770&di=0400b2dea6923ae9928287d5b935dec3&imgtype=0&src=http%3A%2F%2Fbpic.wotucdn.com%2F15%2F93%2F69%2F15936965-806042199ced6a02ee158d0cfc2d0124-1.jpg');
                        $(addForm).find('.imgUrl').val('');
                        editor.setValue('');

                    },
                    onClose: function () {
                        $.hideSubview();
                    },
                    onHide: function () {
                    }
                });
            });
        });
        $('.user-edit').off().on('click', function () {
            preventMultipleClicks(function () {
                $.subview({
                    content: "#_editUser",
                    startFrom: "right",
                    onShow: function () {
                        $(editForm).find('.id').val(userTableObj.id);
                        $(editForm).find('.title').val(userTableObj.name);
                        $(editForm).find('.description').val(userTableObj.description);
                        $(editForm).find('.imgUrl').val(userTableObj.image);
                        $(editForm).find('.img-thumbnail').attr('src',userTableObj.imgUrl);
                        editor1.setValue(userTableObj.content);
                        $('.iwindow1').html(userTableObj.content);
                        $(editForm).find('.system-malady').removeClass('current');
                        $(editForm).find('.system-malady').each(function(){
                            var id = $(this).data('id');
                            if($.inArray(id,userTableObj.type_id) !=-1){
                                $(this).addClass('current');
                            }
                        })

                    },
                    onClose: function () {
                        $.hideSubview();
                    },
                    onHide: function () {
                    }
                });
            });
        });
        $('.unavailable').on('click', function () {
            var id = $('#_userTable').bootstrapTable('getSelections')[0]['id'];
            var data = new Object();
            data.id = id;
            data.status = 1;
            $.ajax({
                url:'/volunteer/edit',
                method:'post',
                data:{data:JSON.stringify(data)},
                dataType:'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success:function (res) {
                    $('#_userTable').bootstrapTable('refresh');
                }
            });
        });
        $('.available').on('click', function () {
            var id = $('#_userTable').bootstrapTable('getSelections')[0]['id'];
            var data = new Object();
            data.id = id;
            data.status = 0;
            $.ajax({
                url:'/volunteer/edit',
                method:'post',
                data:{data:JSON.stringify(data)},
                dataType:'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success:function (res) {
                    $('#_userTable').bootstrapTable('refresh');
                }
            });
        });
        $('.tags').tagsInput({
            width: 'auto',
            defaultText: '点击输入，敲回车键添加',
        });

        //治疗项目点击事件
        $('.system-malady').off().on('click',function(){
            var id = $(this).data('id');
            if($(this).hasClass('current')){
                $(this).removeClass('current');
            }else{
                $(this).addClass('current');
            }
        });
    };

    //
    // 添加文章
    var addValidate = function () {
        var errorHandler1 = $('.errorHandler', addForm);
        var successHandler1 = $('.successHandler', addForm);
        addForm.validate({
            errorElement: "span",
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                element.parent().find('label').append(error);
            },
                    ignore: "",
                    rules: {
                        title: {
                            required: true,
                        }
                    },
                    messages: {
                        title: {
                            required: ' * 标题不能为空'
                        }
                    },
            submitHandler: function (form) {
                errorHandler1.hide();
                $.blockUI({
                    message: '<i class="fa fa-spinner fa-spin"></i> 正在提交数据...'
                });
                            var obj         = {};
                            obj.title       = $('.title', form).val();
                            obj.description = $('.description', form).val();
                            obj.content     = editor.getValue();
                            obj.image       = $('.imgUrl').val();
                            obj.id          = $('.id', form).val();
                            var oldLabel = [];
                            $('.current').each(function(){
                                oldLabel.push($(this).data('id'));
                            });
                            //obj.newLabel = $('#tags_1').val();
                            obj.oldLabel = oldLabel;
                            $.ajax({
                                type: 'POST',
                                url: '/volunteer/save',
                                data: { data: JSON.stringify(obj) },
                                dataType: 'json',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                                },
                                success: function (m){
                                    switch ( m.status ) {
                                        case 0:
                                            $('#_userTable').bootstrapTable('refresh');
                                            $(form)[0].reset();
                                            $.hideSubview();
                                            break;
                                        default:
                                            swal('失败', m.message, 'warning');
                                    }
                                    $.unblockUI();
                                }
                            });
            },
            invalidHandler: function (event, validator) {//display error alert on form submit
                errorHandler1.show();
            }
        });
    };

    // 編輯文章
    var editValidate = function () {
        var errorHandler1 = $('.errorHandler', editForm);
        var successHandler1 = $('.successHandler', editForm);
        editForm.validate({
            errorElement: "span",
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                element.parent().find('label').append(error);
            },
            ignore: "",
            rules: {
                title: {
                    required: true,
                }
            },
            messages: {
                title: {
                    required: ' * 标题不能为空'
                }
            },
            submitHandler: function (form) {
                errorHandler1.hide();
                $.blockUI({
                    message: '<i class="fa fa-spinner fa-spin"></i> 正在提交数据...'
                });
                var obj         = {};
                obj.title       = $('.title', form).val();
                obj.description = $('.description', form).val();
                obj.content     = editor1.getValue();
                obj.image       = $('.imgUrl').val();
                obj.id          = $('.id', form).val();
                var oldLabel = [];
                $('.current').each(function(){
                    oldLabel.push($(this).data('id'));
                });
                //obj.newLabel = $('#tags_1').val();
                obj.oldLabel = oldLabel;
                $.ajax({
                    type: 'POST',
                    url: '/volunteer/save',
                    data: { data: JSON.stringify(obj) },
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    success: function (m){
                        switch ( m.status ) {
                            case 0:
                                $('#_userTable').bootstrapTable('refresh');
                                $(form)[0].reset();
                                $.hideSubview();
                                break;
                            default:
                                swal('失败', m.message, 'warning');
                        }
                        $.unblockUI();
                    }
                });
            },
            invalidHandler: function (event, validator) {//display error alert on form submit
                errorHandler1.show();
            }
        });
    };

    // 连续点击事件没有超过0.5秒，限制触发。
    var preventMultipleClicks = function (func) {
        var time = new Date();
        var t = time.getTime();
        if (t > (clickTime + 500)) {
            func();
            clickTime = t;
        }
    };
    return {
        init: function () {
            userTable();
            event();
            addValidate();
            editValidate();
            uploadImg();
            editorFunc();
        }
    }
}();