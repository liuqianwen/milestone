var hospital = function () {
    "use strict";
    var hId,close = true;
    var hosptitalTable = function () {
        $('#hospitalTable').bootstrapTable({
            method: 'get',
            url: '/hospital/data',
            toolbar: "#toolbar",
            cache: false,
            striped: true,
            selectItemName: "radioName",
            pagination: true,
            pageList: [10, 20, 50, 100],
            pageSize: 10,
            pageNumber: 1,
            search: true,
            sidePagination: 'server',
            queryParams: queryParams,
            showColumns: true,
            clickToSelect: true
        });
        $('#large-columns-table').next().click(function () {
            $(this).hide();
            buildTable($('#hosptitalTable'), 50, 50);
        });
        $('#hospitalTable').on('check.bs.table uncheck.bs.table ' +
            'check-all.bs.table uncheck-all.bs.table', function () {
            $('.edit-hospital').prop('disabled', !$('#hospitalTable').bootstrapTable('getSelections').length);
            $('.add-doctor').prop('disabled', !$('#hospitalTable').bootstrapTable('getSelections').length);
            $('.read-doctor').prop('disabled', !$('#hospitalTable').bootstrapTable('getSelections').length);
        });

    };
    var queryParams = function (params) {
        $('.edit-hospital').prop('disabled', true);
        $('.add-doctor').prop('disabled', true);
        $('.read-doctor').prop('disabled', true);
        return params
    };
    var ajaxLoadImage     = function (){
        $('.img-thumbnail').on('click', function (){
            var that = $(this);
            var type = that.data('type');
            var size = [500,300];
            var outputSize = [1000,600];
            if(type == 2){
                 size = [300,300];
                 outputSize = [1000,1000];
            }
            $("#file").val('');
            $('#clipArea').find('div').remove();
            $('#publishModal').modal('show');
            new PhotoClip('#clipArea', {
                size: size,   //剪裁完成的图片height，width
                outputSize: outputSize, //剪裁框的height，width
                file: "#file",     //文件上传框ID
                view: "#view",     //预览div的ID
                ok: "#clipBtn",    //剪裁开始按钮ID
                outputType: 'jpg', //指定输出图片的类型，可选 "jpg" 和 "png" 两种种类型，默认为 "jpg"。
                //img: 'img/mm.jpg',
                loadStart: function (){
                    console.log('开始读取照片');
                },
                loadComplete: function (){
                    console.log('照片读取完成');
                },
                done: function (dataURL){
                    $.ajax({
                        type: "POST",
                        url: '/image/upload',
                        dataType: 'json',
                        data: { img: dataURL,path:'8020/headImg' },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        success: function (m){
                            switch ( m.status ) {
                                case 0:
                                    that.attr('src', m.data.route);
                                    that.parent().find('.imgUrl').val(m.data.url);
                                    $('#publishModal').modal('hide');
                                    break;
                                default:
                            }
                        }
                    });
                },
                fail: function (msg){
                    console.log(msg);
                }
            });
        });
    };

    var submitAddHospitalForm = function (){
        var AddHospitalForm = $('.form-addhospital');
        var errorHandler1   = $('.errorHandler', AddHospitalForm);
        var successHandler1 = $('.successHandler', AddHospitalForm);
        AddHospitalForm.validate({
                errorElement: "span",
                errorClass: 'help-block',
                errorPlacement: function (error, element){
                    element.parent().parent().find('label').append(error);
                },
                ignore: "",
                rules: {
                    name: { required: true },
                    address: { required: true},
                    introduce: { required: true }
                },
                messages: {
                    name: { required: ' *不能为空' },
                    address: { required: ' *不能为空'},
                    introduce: { required: ' *不能为空' }
                },
                submitHandler: function (form){
                    errorHandler1.hide();
                    $.blockUI({
                        message: '<i class="fa fa-spinner fa-spin"></i> 正在提交数据...'
                    });
                    var obj     = new Object;
                    obj.name    = $('.form-addhospital .name').val();
                    obj.address = $('.form-addhospital .address').val();
                    obj.introduce = $('.form-addhospital .introduce').val();
                    obj.logo = $('.form-addhospital .imgUrl').val();
                    var badge = [];
                    $('.form-addhospital .checkbox .checked').each(function(){
                        badge.push($(this).find('input').val());
                    });
                    obj.badge = badge;
                    var map = new BMap.Map();
                    //console.log(map);
                    var localSearch = new BMap.LocalSearch(map);
                    localSearch.search(obj.address);
                    localSearch.setSearchCompleteCallback(function (searchResult) {
                        if(searchResult) {
                            var poi = searchResult.getPoi(0);
                            if (poi != undefined) {
                                obj.pointlat = poi.point.lat;
                                obj.pointlng = poi.point.lng;
                            } else {
                                obj.pointlat = '';
                                obj.pointlng = '';
                            }
                            obj.city = searchResult.city;
                        }else{
                            obj.pointlat = '';
                            obj.pointlng = '';
                            obj.city = '';
                        }

                        var option   = {
                            type: 'POST',
                            url: '/hospital/add',
                            data: { data: JSON.stringify(obj) },
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        };
                        $.ajax(option).done(function (msg){
                            switch ( msg.status ) {
                                case 0:
                                    $(form)[0].reset();
                                    $('#hospitalTable').bootstrapTable('refresh');
                                    $.hideSubview();
                                    break;
                                default:
                                    swal('', msg.message);
                                    $.hideSubview();
                            }
                            $.unblockUI();
                        });
                    });
                },
                invalidHandler: function (event, validator){//display error alert on form submit
                    errorHandler1.show();
                }
            }
        );
    };

    var hospitalEditForm  = function (){
        var editHospitalForm = $('.form-editHospital');
        var errorHandler1    = $('.errorHandler', editHospitalForm);
        var successHandler1  = $('.successHandler', editHospitalForm);
        editHospitalForm.validate({
            errorElement: "span",
            errorClass: 'help-block',
            errorPlacement: function (error, element){
                element.parent().parent().find('label').append(error);
            },
            ignore: "",
            rules: {
                name: { required: true },
                email: { required: true, email: true },
                number: { required: true }
            },
            messages: {
                name: { required: ' *不能为空' },
                email: { required: ' *不能为空', email: '输入正确的email格式' },
                number: { required: ' *不能为空' }
            },
            submitHandler: function (form){
                errorHandler1.hide();
                $.blockUI({
                    message: '<i class="fa fa-spinner fa-spin"></i> 正在提交数据...'
                });
                var obj     = new Object;
                obj.name    = $('.form-editHospital .name').val();
                obj.address = $('.form-editHospital .address').val();
                obj.introduce = $('.form-editHospital .introduce').val();
                obj.logo = $('.form-editHospital .imgUrl').val();
                var badge = [];
                $('.form-editHospital .badge:checked').each(function(){
                    //console.log($(this).val())
                    badge.push($(this).val());
                });
                obj.badge = badge;
                var map = new BMap.Map();
                //console.log(map);
                var localSearch = new BMap.LocalSearch(map);
                localSearch.search(obj.address);
                localSearch.setSearchCompleteCallback(function (searchResult) {
                    if(searchResult) {
                        var poi = searchResult.getPoi(0);
                        if (poi != undefined) {
                            obj.pointlat = poi.point.lat;
                            obj.pointlng = poi.point.lng;

                        } else {
                            obj.pointlat = '';
                            obj.pointlng = '';
                        }
                        obj.city = searchResult.city;
                    }else{
                        obj.pointlat = '';
                        obj.pointlng = '';
                        obj.city = '';
                    }
                    var option   = {
                        type: 'POST',
                        url: '/hospital/edit',
                        data: { data: obj, hId: hId },
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    };
                    $.ajax(option).done(function (msg){
                        switch ( msg.status ) {
                            case 0:
                                $(form)[0].reset();
                                $('#hospitalTable').bootstrapTable('refresh');
                                $.hideSubview();
                                break;
                            default:
                                swal('', msg.message);
                                $.hideSubview();
                        }
                        $.unblockUI();
                    });
                });
            },
            invalidHandler: function (event, validator){//display error alert on form submit
                errorHandler1.show();
            }
        });
    };

    var AddDoctorForm = function (){
        var AddDoctorForm = $('.form-addDoctor');
        var errorHandler1   = $('.errorHandler', AddDoctorForm);
        var successHandler1 = $('.successHandler', AddDoctorForm);
        AddDoctorForm.validate({
                errorElement: "span",
                errorClass: 'help-block',
                errorPlacement: function (error, element){
                    element.parent().parent().find('label').append(error);
                },
                ignore: "",
                rules: {
                    name: { required: true },
                    position: { required: true},
                    good: { required: true},
                    introduce: { required: true }
                },
                messages: {
                    name: { required: ' *不能为空' },
                    position: { required: ' *不能为空'},
                    good: { required: ' *不能为空'},
                    introduce: { required: ' *不能为空' }
                },
                submitHandler: function (form){
                    errorHandler1.hide();
                    $.blockUI({
                        message: '<i class="fa fa-spinner fa-spin"></i> 正在提交数据...'
                    });
                    var id = $('#hospitalTable').bootstrapTable('getSelections')[0]['id'];
                    var obj     = new Object;
                    obj.hospital_id = id;
                    obj.name    = $('.form-addDoctor .name').val();
                    obj.department = $('.form-addDoctor .department').val();
                    obj.position = $('.form-addDoctor .position').val();
                    obj.tel = $('.form-addDoctor .tel').val();
                    obj.sex = $('.form-addDoctor .sex').val();
                    obj.good = $('.form-addDoctor .good').val();
                    obj.introduce = $('.form-addDoctor .introduce').val();
                    obj.headimg = $('.form-addDoctor .imgUrl').val();
                    var option   = {
                            type: 'POST',
                            url: '/doctor/add',
                            data: { data: JSON.stringify(obj) },
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        };
                        $.ajax(option).done(function (msg){
                            switch ( msg.status ) {
                                case 0:
                                    $(form)[0].reset();
                                    $('#hospitalTable').bootstrapTable('refresh');
                                    $.hideSubview();
                                    break;
                                default:
                                    swal('', msg.message);
                                    $.hideSubview();
                            }
                            $.unblockUI();
                        });
                },
                invalidHandler: function (event, validator){//display error alert on form submit
                    errorHandler1.show();
                }
            }
        );
    };

    var runSubview        = function (){
        $('.add-hospital').on('click', function (){
            $.subview({
                content: '#newHospital',
                startFrom: "right",
                onShow: function (){
                    $('#newHospital').find('.img-thumbnail').attr('src','http://www.youbian.com/Images/201313/13653239592670.jpg');
                    $('#newHospital').find('.imgUrl').val('');
                    $('#newHospital').find('.system-malady').removeClass('current');
                },
                onClose: function (){
                    checkAddHospital(close);
                },
                onHide: function (){
                }
            });
        });
        $('.edit-hospital').on('click', function (){
            $.subview({
                content: '#editHospital',
                startFrom: "right",
                onShow: function (){
                    var hospital = $('#hospitalTable').bootstrapTable('getSelections')[0];
                    hId = hospital.id;
                    $('.form-editHospital .img-thumbnail').attr('src',hospital['imgUrl']);
                    $('.form-editHospital .imgUrl').val(hospital['img']);
                    $('.form-editHospital .name').val(hospital['name']);
                    $('.form-editHospital .address').val(hospital['address']);
                    $('.form-editHospital .introduce').val(hospital['introduce']);
                    $('.form-editHospital').find('.badge').attr('checked',false);
                    $('.form-editHospital').find('.badge').each(function(){
                        var id = $(this).val();
                        if($.inArray(String(id),hospital['badge']) !=-1){
                            $(this).iCheck('check');
                        }
                    })

                },
                onClose: function (){
                    checkEditHospital(close);
                },
                onHide: function (){
                }
            });
        });

        $('.add-doctor').on('click', function (){
            $.subview({
                content: '#addDoctor',
                startFrom: "right",
                onShow: function (){
                    $('#addDoctor').find('.img-thumbnail').attr('src','https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/sfy/noHeadImg.jpg');
                    $('#addDoctor').find('.imgUrl').val('');
                },
                onClose: function (){
                    checkAddDoctor(close);
                },
                onHide: function (){
                }
            });
        });

        $('.read-doctor').on('click', function (){
            var id        = $('#hospitalTable').bootstrapTable('getSelections')['0']['id'];
            window.location = '/doctor?id=' + id;
        });
    };
    var checkEditHospital = function (bool){
        if ( bool ) {
            close = false;
            bootbox.confirm({
                buttons: { cancel: { label: "取消" }, confirm: { label: "确认" } },
                message: "您没有保存该记录，确定关闭吗?",
                callback: function (result){
                    close = true;
                    if ( result ) {
                        $('.form-editHospital')[0].reset();
                        $.hideSubview();
                    }
                }
            });
        }
    };
    var checkAddHospital      = function (bool){
        if ( bool ) {
            close = false;
            bootbox.confirm({
                buttons: { cancel: { label: "取消" }, confirm: { label: "确认" } },
                message: "您没有保存该记录，确定关闭吗?",
                callback: function (result){
                    close = true;
                    if ( result ) {
                        $('.form-addhospital')[0].reset();
                        $.hideSubview();
                    }
                }
            });
        }
    };
    var checkAddDoctor     = function (bool){
        if ( bool ) {
            close = false;
            bootbox.confirm({
                buttons: { cancel: { label: "取消" }, confirm: { label: "确认" } },
                message: "您没有保存该记录，确定关闭吗?",
                callback: function (result){
                    close = true;
                    if ( result ) {
                        $('.form-addDoctor')[0].reset();
                        $.hideSubview();
                    }
                }
            });
        }
    };
    var submitForm        = function (){
        $('.save-edit').on('click', function (){
            $('.form-editHospital').submit();
        });
    };
    return {
        init: function () {
            hosptitalTable();
            ajaxLoadImage();
            runSubview();
            submitForm();
            hospitalEditForm();
            submitAddHospitalForm();
            AddDoctorForm();
        }
    };
}();

