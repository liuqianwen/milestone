var video = function () {
    "use strict";
    var table, close = true, image, rawHeadImg, cropper = true, croppers = true;
    var tableFunction = function () {
        $('#video-table').bootstrapTable({
            method: 'get',
            url: '/video/data',
            toolbar: "#toolbar",
            cache: false,
            striped: true,
            search: true,
            selectItemName: "radioName",
            pagination: true,
            sidePagination: 'server',
            queryParams: queryParams,
            showColumns: true,
            clickToSelect: true,
        });
        $('#large-columns-table').next().click(function () {
            $(this).hide();
            buildTable($('#video-table'), 50, 50);
        });
        $('#video-table').on('check.bs.table uncheck.bs.table ' + 'check-all.bs.table uncheck-all.bs.table', function () {
            var tableObj = $('#video-table').bootstrapTable('getSelections')[0];
            $('.unavailable').prop('disabled', !$('#video-table').bootstrapTable('getSelections').length);
            $('.available').prop('disabled', !$('#video-table').bootstrapTable('getSelections').length);
            $('.edit').prop('disabled', !$('#video-table').bootstrapTable('getSelections').length);
            if(tableObj.tops == 1){
                $('.video-top').text('取消置顶');
            }else{
                $('.video-top').text('置顶');
            }
            $('.video-top').prop('disabled', !$('#video-table').bootstrapTable('getSelections').length);
        });
        $('.th-inner').parent().css('text-align', 'center');
    };
    var queryParams = function (params) {
        $('.unavailable').prop('disabled', true);
        $('.available').prop('disabled', true);
        $('.edit').prop('disabled', true);
        $('.video-top').prop('disabled', true);
        return params;
    };

    var uploadImg   = function (){
        $('.img-thumbnail').on('click', function (){
            var that = $(this);
            $("#file").val('');
            $('#clipArea').find('div').remove();
            $('#publishModal').modal('show');
            new PhotoClip('#clipArea', {
                size: [500, 300],   //剪裁完成的图片height，width
                outputSize: [1800, 1000], //剪裁框的height，width
                file: "#file",     //文件上传框ID
                view: "#view",     //预览div的ID
                ok: "#clipBtn",    //剪裁开始按钮ID
                outputType: 'jpg', //指定输出图片的类型，可选 "jpg" 和 "png" 两种种类型，默认为 "jpg"。
                //img: 'img/mm.jpg',
                loadStart: function (){
                    console.log('开始读取照片');
                },
                loadComplete: function (){
                    console.log('照片读取完成');
                },
                done: function (dataURL){
                    $.ajax({
                        type: "POST",
                        url: '/image/upload',
                        dataType: 'json',
                        data: { img: dataURL },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        success: function (m){
                            switch ( m.status ) {
                                case 0:
                                    that.attr('src', m.data.route);
                                    that.parent().find('.imgUrl').val(m.data.url);
                                    $('#publishModal').modal('hide');
                                    break;
                                default:
                            }
                        }
                    });
                },
                fail: function (msg){
                    console.log(msg);
                }
            });
        });
    };

    var addVideo = function () {
        var form = $('.form-add-video');
        var errorHandler1 = $('.errorHandler', form);
        var successHandler1 = $('.successHandler', form);
        form.validate({
            errorElement: "span",
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                element.parent().parent().find('label').append(error);
            },
            ignore: "",
            rules: {
                title:{required: true},
                url:{required: true},
                introduce:{required:true},

            },
            messages: {
                title: {required: '* 不能为空'},
                url: {required: '* 不能为空'},
                introduce: {required: '* 不能为空'},
            },
            submitHandler: function (form) {
                errorHandler1.hide();
                $.blockUI({
                    message: '<i class="fa fa-spinner fa-spin"></i> 正在提交数据...'
                });
                var data = new Object();
                data.title = $('.title', form).val();
                data.introduce = $('.introduce', form).val();
                data.image = $('.imgUrl', form).val();
                var label = [];
                $('.current', form).each(function(){
                    label.push($(this).data('id'));
                });
                data.label = label;
                data.url = $('.url',form).val();
                var option = {
                    type: 'POST',
                    url: '/video/add',
                    data: {data: JSON.stringify(data)},
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                };
                $.ajax(option).done(function (msg) {
                    if (msg.say == 'ok') {
                        $('#video-table').bootstrapTable('refresh');
                        $(form)[0].reset();
                    }
                    $.hideSubview();
                    $.unblockUI();
                });
            },
            invalidHandler: function (event, validator) {//display error alert on form submit
                errorHandler1.show();
            }
        });
    };

    var UpdateVideo = function () {
        var form = $('.form-update-video');
        var errorHandler1 = $('.errorHandler', form);
        var successHandler1 = $('.successHandler', form);
        form.validate({
            errorElement: "span",
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                element.parent().parent().find('label').append(error);
            },
            ignore: "",
            rules: {
                title:{required: true},
                url:{required: true},
                introduce:{required:true},

            },
            messages: {
                title: {required: '* 不能为空'},
                url: {required: '* 不能为空'},
                introduce: {required: '* 不能为空'},
            },
            submitHandler: function (form) {
                errorHandler1.hide();
                $.blockUI({
                    message: '<i class="fa fa-spinner fa-spin"></i> 正在提交数据...'
                });
                var data = new Object();
                data.id = $('.dataId',form).val();
                data.title = $('.title', form).val();
                data.introduce = $('.introduce', form).val();
                data.image = $('.imgUrl', form).val();
                var label = [];
                $('.current', form).each(function(){
                    label.push($(this).data('id'));
                });
                data.label = label;
                data.url = $('.url',form).val();
                var option = {
                    type: 'POST',
                    url: '/video/edit',
                    data: {data: JSON.stringify(data)},
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                };
                $.ajax(option).done(function (msg) {
                    if (msg.say == 'ok') {
                        $('#video-table').bootstrapTable('refresh');
                        $(form)[0].reset();
                    }
                    $.hideSubview();
                    $.unblockUI();
                });
            },
            invalidHandler: function (event, validator) {//display error alert on form submit
                errorHandler1.show();
            }
        });
    };
    var runSubview = function () {
        $('.new-video').off().on("click", function () {
            $.subview({
                content: "#newVideo",
                startFrom: "right",
                onShow: function () {
                    $('.form-add-video').val('');
                    $('.system-malady').removeClass('current');
                    $('#newVideo').find('.img-thumbnail').attr('src','https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg');
                    $('#newVideo').find('.imgUrl').val('');
                },
                onClose: function () {
                    checkAddVideo(close);
                },
                onHide: function () {
                }
            });
        });
        $('.unavailable').on('click', function () {
            var id = $('#video-table').bootstrapTable('getSelections')[0]['id'];
            var data = new Object();
            data.id = id;
            data.status = 1;
            $.ajax({
                url:'/video/edit',
                method:'post',
                data:{data:JSON.stringify(data)},
                dataType:'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success:function (res) {
                    $('#video-table').bootstrapTable('refresh');
                }
            });
        });
        $('.available').on('click', function () {
            var id = $('#video-table').bootstrapTable('getSelections')[0]['id'];
            var data = new Object();
            data.id = id;
            data.status = 0;
            $.ajax({
                url:'video/edit',
                method:'post',
                data:{data:JSON.stringify(data)},
                dataType:'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success:function (res) {
                    $('#video-table').bootstrapTable('refresh');
                }
            });
        });
        $('.video-top').on('click', function () {
            var id = $('#video-table').bootstrapTable('getSelections')[0]['id'];
            var data = new Object();
            data.id = id;
            data.status = 0;
            $.ajax({
                url:'video/top',
                method:'post',
                data:{data:JSON.stringify(data)},
                dataType:'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success:function (res) {
                    $('#video-table').bootstrapTable('refresh');
                }
            });
        });
        $('.edit').on('click', function () {
            $.subview({
                content: '#updateVideo',
                startFrom: "right",
                onShow: function () {
                    table = $('#video-table').bootstrapTable('getSelections')[0];
                    $('.form-update-video .dataId').val(table['id']);
                    $('.form-update-video .title').val(table['title']);
                    $('.form-update-video .introduce').val(table['description']);
                    $('.form-update-video .url').val(table['url']);
                    $('.form-update-video .img-thumbnail').attr('src',table['imgUrl']);
                    $('.form-update-video .imgUrl').val(table['img']);
                    $('.form-update-video').find('.system-malady').removeClass('current');
                    $('.form-update-video').find('.system-malady').each(function(){
                        var id = $(this).data('id');
                        if($.inArray(id,table.type_id) !=-1){
                            $(this).addClass('current');
                        }
                    })
                },
                onClose: function () {
                    checkUpdateVideo(close);
                },
                onHide: function () {
                }
            });
        });
        $(".close-subview-button").off().on("click", function (e) {
            $(".close-subviews").trigger("click");
            e.preventDefault();
        });


        //标签点击事件
        $('.system-malady').off().on('click',function(){
            var id = $(this).data('id');
            if($(this).hasClass('current')){
                $(this).removeClass('current');
            }else{
                $(this).addClass('current');
            }
        });
    };
    var checkAddVideo = function (bool) {
        if (bool) {
            close = false;
            bootbox.confirm({
                buttons: {cancel: {label: "取消"}, confirm: {label: "确认"}},
                message: "您没有保存该记录，确定关闭吗?",
                callback: function (result) {
                    close = true;
                    if (result) {
                        $('.form-add-video')[0].reset();
                        $.hideSubview();
                    }
                }
            });
        }
    };
    var checkUpdateVideo = function (bool) {
        if (bool) {
            close = false;
            bootbox.confirm({
                buttons: {cancel: {label: "取消"}, confirm: {label: "确认"}},
                message: "您没有保存该记录，确定关闭吗?",
                callback: function (result) {
                    close = true;
                    if (result) {
                        $('.form-update-video')[0].reset();
                        $.hideSubview();
                    }
                }
            });
        }
    };
    return {
        init: function () {
            tableFunction();
            runSubview();
            addVideo();
            uploadImg();
            UpdateVideo();
        }
    }
}();
