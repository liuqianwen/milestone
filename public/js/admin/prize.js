var prize = function () {
    "use strict";
    var subViewElement, subViewContent;
    var username = $('.username').text();
    var labeltable = function (){
        $('#labeltable').bootstrapTable({
            method:'get',
            url: '/prize/data',
            toolbar:"#toolbar",
            cache: false,
            striped: true,
            selectItemName:"radioName",
            pagination: true,
            pageList: [10,20,50,100],
            pageSize:10,
            pageNumber:1,
            search: true,
            sidePagination:'server',
            queryParams: queryParams,
            showColumns: true,
            clickToSelect: true
        });
        $('#large-columns-table').next().click(function () {
            $(this).hide();
            buildTable($('#labeltable'), 50, 50);
        });
        $('#labeltable').on('check.bs.table uncheck.bs.table ' +
            'check-all.bs.table uncheck-all.bs.table', function () {
            $('.edit-label').prop('disabled', !$('#labeltable').bootstrapTable('getSelections').length);
        });
    };
    var queryParams = function(params) {
        $('.edit-label').prop('disabled',true);
        return params
    };
    var checkAddlabel = function() {
        bootbox.confirm({
            buttons:{cancel:{label:"取消"}, confirm:{label:"确认"}},
            message:"您没有保存该记录，确定关闭吗?",
            callback:function(result) {
                if (result) {
                    $('.form-addlabel')[0].reset();
                    $.hideSubview();
                }
            }
        });
    };
    var checkEditlabel = function () {
        bootbox.confirm({
            buttons:{cancel:{label:"取消"}, confirm:{label:"确认"}},
            message:"您没有保存该记录，确定关闭吗?",
            callback:function(result) {
                if (result) {
                    $('.form-editlabel')[0].reset();
                    $.hideSubview();
                }
            }
        });
    };
    var runSubviews = function () {
        $('.new-label').off().on("click",function() {
            subViewElement = $(this);
            subViewContent = subViewElement.attr('href');
            $.subview({
                content: subViewContent,
                startFrom: "right",
                onShow: function() {
                    $('#newlabe1 .img').attr('src','');
                    $('#newlabe1 .type').trigger('change');
                },
                onClose: function() {
                    checkAddlabel();
                },
                onHide: function() {

                }
            });
        });
        $('.edit-label').off().on("click",function() {
            subViewElement = $(this);
            subViewContent = '#editlabel';
            $.subview({
                content: subViewContent,
                startFrom: "right",
                onShow: function() {
                    $.blockUI({
                        message:'<i class="fa fa-spinner fa-spin"></i> 加载数据中...'
                    });
                    doEditlabelSth();
                },
                onClose: function() {
                    checkEditlabel();
                },
                onHide: function() {

                }
            });
        });
        $(".close-subview-button").off().on("click", function(e) {
            $(".close-subviews").trigger("click");
            e.preventDefault();
        });
    };
    var doEditlabelSth = function () {
        var arr = $('#labeltable').bootstrapTable('getSelections')[0];
        $('.form-editlabel .name').val(arr.name);
        $('.form-editlabel .type').val(arr.typeVal);
        if(arr.typeVal == 1){
            $('.form-editlabel .integral').val(arr.integral);
        }else{
            $('.form-editlabel .integral').val('');
        }
        $('.form-editlabel .sort').val(arr.sort);
        $('.form-editlabel .odds').val(arr.odds);
        $('.form-editlabel .img').attr('src',arr.image);
        $.unblockUI();
    };
    var runAddlabelFormValidation = function () {
        var formAddlabel = $('.form-addlabel');
        var errorHandler1 = $('.errorHandler',formAddlabel);
        var successHandler1 = $('.successHandler', formAddlabel);
        formAddlabel.validate({
            errorElement: "span",
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                element.parent().parent().find('label').append(error);
            },
            ignore: "",
            rules: {
                name : {
                    required : true
                },
                odds : {
                    required : true,
                    min:0
                },
                integral : {
                    required : function(){
                        var type = $('.type',formAddlabel).val();
                        if(type == 1){
                            return true
                        }else{
                            return false;
                        }
                    },
                    min:0
                },
                sort : {
                    required : true,
                    number:true,
                    min:1,
                    max:8,
                },
            },
            messages: {
                name : {
                    required : ' *不能为空'
                },
                odds : {
                    required : ' *不能为空',
                    min:'不能为负数'
                },
                integral:{
                    required : ' *不能为空',
                    min:'不能为负数'
                },
                sort : {
                    required : ' *不能为空',
                    min:'最小为1',
                    max:'最大为8',
                },
            },
            submitHandler : function(form) {
                errorHandler1.hide();
                $.blockUI({
                    message:'<i class="fa fa-spinner fa-spin"></i> 正在提交标签数据...'
                });
                var labelToAdd = new Object;
                labelToAdd.name = $('.form-addlabel .name').val();
                labelToAdd.type = $('.form-addlabel .type').val();
                labelToAdd.integral = $('.form-addlabel .integral').val();
                labelToAdd.sort = $('.form-addlabel .sort').val();
                labelToAdd.odds = $('.form-addlabel .odds').val();
                labelToAdd.image = $('.form-addlabel .imgUrl').val();
                $.ajax({
                    type:'POST',
                    url:'/prize/add',
                    data:{postdata:labelToAdd},
                    dataType:'json',
                    headers:{
                        'X-CSRF-TOKEN':$('meta[name="_token"]').attr('content')
                    },
                    success: function(json) {
                        if(json.status == 0){
                            $('.form-addlabel')[0].reset();
                            $.hideSubview();
                            $('#labeltable').bootstrapTable('refresh');
                        }
                        $.unblockUI();
                    }
                });
            },
            invalidHandler : function(event, validator) {//display error alert on form submit
                errorHandler1.show();
            }
        });
    };
    var runEditlabelFormValidation = function () {
        var formEditlabel = $('.form-editlabel');
        var errorHandler2 = $('.errorHandler',formEditlabel);
        var successHandler2 = $('.successHandler', formEditlabel);
        formEditlabel.validate({
            errorElement: "span",
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                element.parent().parent().find('label').append(error);
            },
            ignore: "",
            rules: {
                name : {
                    required : true
                },
                odds : {
                    required : true,
                    min:0
                },
                integral : {
                    required : function(){
                        var type = $('.type',formEditlabel).val();
                        if(type == 1){
                            return true
                        }else{
                            return false;
                        }
                    },
                    min:0
                },
                sort : {
                    required : true,
                    number:true,
                    min:1,
                    max:8,
                },
            },
            messages: {
                name : {
                    required : ' *不能为空'
                },
                odds : {
                    required : ' *不能为空',
                    min:'不能为负数'
                },
                integral:{
                    required : ' *不能为空',
                    min:'不能为负数'
                },
                sort : {
                    required : ' *不能为空',
                    min:'最小为1',
                    max:'最大为8',
                },
            },
            invalidHandler: function(event, validator) {
                successHandler2.hide();
                errorHandler2.show();
            },
            submitHandler:function(form) {
                successHandler2.show();
                errorHandler2.hide();
                $.blockUI({
                    message: '<i class="fa fa-spinner fa-spin"></i> 正在提交标签数据...'
                });
                var arr = $('#labeltable').bootstrapTable('getSelections')[0];
                var labelToEdit = new Object();
                labelToEdit.id = arr.id;
                labelToEdit.name = $('.form-editlabel .name').val();
                labelToEdit.type = $('.form-editlabel .type').val();
                labelToEdit.integral = $('.form-editlabel .integral').val();
                labelToEdit.sort = $('.form-editlabel .sort').val();
                labelToEdit.odds = $('.form-editlabel .odds').val();
                labelToEdit.image = $('.form-editlabel .imgUrl').val();
                $.ajax({
                    type:'POST',
                    url:'/prize/edit',
                    data:{postdata:labelToEdit},
                    dataType:'json',
                    headers:{
                        'X-CSRF-TOKEN':$('meta[name="_token"]').attr('content')
                    },
                    success: function(json){
                        if(json.say == 'ok'){
                            $.hideSubview();
                            $('.form-editlabel')[0].reset();
                            $('#labeltable').bootstrapTable('refresh');
                        }
                        $.unblockUI();
                    }
                });
            }
        });
    };

    var uploadImg   = function (){
        $('.img-thumbnail').on('click', function (){
            var that = $(this);
            $("#file").val('');
            $('#clipArea').find('div').remove();
            $('#publishModal').modal('show');
            new PhotoClip('#clipArea', {
                size: [200, 150],   //剪裁完成的图片height，width
                outputSize: [400, 300], //剪裁框的height，width
                file: "#file",     //文件上传框ID
                view: "#view",     //预览div的ID
                ok: "#clipBtn",    //剪裁开始按钮ID
                outputType: 'jpg', //指定输出图片的类型，可选 "jpg" 和 "png" 两种种类型，默认为 "jpg"。
                //img: 'img/mm.jpg',
                loadStart: function (){
                    console.log('开始读取照片');
                },
                loadComplete: function (){
                    console.log('照片读取完成');
                },
                done: function (dataURL){
                    $.ajax({
                        type: "POST",
                        url: '/image/upload',
                        dataType: 'json',
                        data: { img: dataURL },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        success: function (m){
                            switch ( m.status ) {
                                case 0:
                                    that.attr('src', m.data.route);
                                    that.parent().find('.imgUrl').val(m.data.url);
                                    $('#publishModal').modal('hide');
                                    break;
                                default:
                            }
                        }
                    });
                },
                fail: function (msg){
                    console.log(msg);
                }
            });
        });
    };

    var clickFunction = function () {
        $('.type').on('change',function(){
            var type = $(this).val();
            if(type == 1){
                $(this).parents('form').find('.integral').parents('.row').show();
            }else{
                $(this).parents('form').find('.integral').parents('.row').hide();
            }
        })
    }
    return {
        init: function () {
            labeltable();
            runSubviews();
            runAddlabelFormValidation();
            runEditlabelFormValidation();
            uploadImg();
            clickFunction();
        }
    };
}();
