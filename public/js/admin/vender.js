var vender = function () {
    "use strict";
    var hId,close = true;
    var editor,editor1;
    var photo;
    var hosptitalTable = function () {
        $('#hospitalTable').bootstrapTable({
            method: 'get',
            url: '/vender/data',
            toolbar: "#toolbar",
            cache: false,
            striped: true,
            selectItemName: "radioName",
            pagination: true,
            pageList: [10, 20, 50, 100],
            pageSize: 10,
            pageNumber: 1,
            search: true,
            sidePagination: 'server',
            queryParams: queryParams,
            showColumns: true,
            clickToSelect: true
        });
        $('#large-columns-table').next().click(function () {
            $(this).hide();
            buildTable($('#hosptitalTable'), 50, 50);
        });
        $('#hospitalTable').on('check.bs.table uncheck.bs.table ' +
            'check-all.bs.table uncheck-all.bs.table', function () {
            $('.edit-hospital').prop('disabled', !$('#hospitalTable').bootstrapTable('getSelections').length);
            $('.add-doctor').prop('disabled', !$('#hospitalTable').bootstrapTable('getSelections').length);
            $('.read-doctor').prop('disabled', !$('#hospitalTable').bootstrapTable('getSelections').length);
        });

    };
    var queryParams = function (params) {
        $('.edit-hospital').prop('disabled', true);
        $('.add-doctor').prop('disabled', true);
        $('.read-doctor').prop('disabled', true);
        return params
    };


    var editorFunc = function(){
        editor =  new Simditor({
            textarea: $('#editor'),
            toolbar: ['title', 'bold', 'italic', 'underline', 'strikethrough','fontScale', 'color', '|', 'ol', 'ul', 'blockquote', 'code', 'table', '|', 'link', 'image', 'hr', '|', 'indent', 'outdent', 'alignment', '|', 'html'],
            upload: {
                url: '/image/uploads',
                params: null,
                fileKey: 'upload_file',
                connectionCount: 3,
                leaveConfirm: '文件上传中，关闭此页面将取消上传。'
            },
            pasteImage: true,
            tabIndent: true,
            allowedTags:['div','br', 'span', 'a', 'img', 'b', 'strong', 'i', 'strike', 'u', 'font', 'p', 'ul', 'ol', 'li', 'blockquote', 'pre', 'code', 'h1', 'h2', 'h3', 'h4', 'hr'],
            allowedStyles:{
                div:['font-size','font-family','color','float','display','flex-direction','flex-wrap','flex-flow','justify-content','align-items','align-content','width','height'],
                table:['table']
            }
        });
        editor.on('valuechanged',function(e){
            var ele = $('.iwindow');
            ele.html(editor.getValue());
        })
        editor1 =  new Simditor({
            textarea: $('#editor1'),
            toolbar: ['title', 'bold', 'italic', 'underline', 'strikethrough','fontScale', 'color', '|', 'ol', 'ul', 'blockquote', 'code', 'table', '|', 'link', 'image', 'hr', '|', 'indent', 'outdent', 'alignment', '|', 'html'],
            upload: {
                url: '/image/uploads',
                params: null,
                fileKey: 'upload_file',
                connectionCount: 3,
                leaveConfirm: '文件上传中，关闭此页面将取消上传。'
            },
            pasteImage: true,
            tabIndent: true,
            allowedTags:['div','br', 'span', 'a', 'img', 'b', 'strong', 'i', 'strike', 'u', 'font', 'p', 'ul', 'ol', 'li', 'blockquote', 'pre', 'code', 'h1', 'h2', 'h3', 'h4', 'hr'],
            allowedStyles:{
                div:['font-size','font-family','color','float','display','flex-direction','flex-wrap','flex-flow','justify-content','align-items','align-content','width','height'],
                table:['table']
            }
        });
        editor1.on('valuechanged',function(e){
            var ele = $('.iwindow1');
            ele.html(editor1.getValue());
        })
    };
    var ajaxLoadImage     = function (){
        $('.img-thumbnail').off("click").on("click",function(){
            var that = $(this);
            var type = that.data('type');
            var size = [500,300];
            var outputSize = [1000,600];
            if(type == 2){
                 size = [300,300];
                 outputSize = [1000,1000];
            }
            $("#file").val('');
            $('#clipArea').find('div').remove();
            $('#publishModal').modal('show');
            photo =  new PhotoClip('#clipArea', {
                size: size,   //剪裁完成的图片height，width
                outputSize: outputSize, //剪裁框的height，width
                file: "#file",     //文件上传框ID
                view: "#view",     //预览div的ID
                ok: "#clipBtn",    //剪裁开始按钮ID
                outputType: 'jpg', //指定输出图片的类型，可选 "jpg" 和 "png" 两种种类型，默认为 "jpg"。
                //img: 'img/mm.jpg',
                loadStart: function (){
                    console.log('开始读取照片');
                },
                loadComplete: function (){
                    console.log('照片读取完成');
                },
                done: function (dataURL){
                    $.ajax({
                        type: "POST",
                        url: '/image/upload',
                        dataType: 'json',
                        data: { img: dataURL,path:'8020/headImg' },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        success: function (m){
                            switch ( m.status ) {
                                case 0:
                                    that.attr('src', m.data.route);
                                    that.parent().find('.imgUrl').val(m.data.url);
                                    $('#publishModal').modal('hide');
                                    break;
                                default:
                            }
                            photo.destroy();
                        }
                    });
                },
                fail: function (msg){
                    console.log(msg);
                    photo.destroy();
                }
            });
        });
    };

    var submitAddHospitalForm = function (){
        var AddHospitalForm = $('.form-addhospital');
        var errorHandler1   = $('.errorHandler', AddHospitalForm);
        var successHandler1 = $('.successHandler', AddHospitalForm);
        AddHospitalForm.validate({
                errorElement: "span",
                errorClass: 'help-block',
                errorPlacement: function (error, element){
                    element.parent().parent().find('label').append(error);
                },
                ignore: "",
                rules: {
                    name: { required: true },
                    address: { required: true},
                    introduce: { required: true },
                },
                messages: {
                    name: { required: ' *不能为空' },
                    address: { required: ' *不能为空'},
                    introduce: { required: ' *不能为空' },
                },
                submitHandler: function (form){
                    errorHandler1.hide();
                    var type = $('.form-addhospital input[name=gender]:checked').val();
                    var obj     = new Object;
                    if(type == 1){
                        if(editor.getValue() == ''){
                            swal('请填写商品展示内容');
                            return false;
                        }else{
                            obj.content = editor.getValue();
                        }
                    }
                    var t = true;
                    if(type == 2){
                        var good = [];
                        $('.good',form).each(function(){
                            var arr = new   Object();
                            var title = $(this).find('.title').val();
                            var description = $(this).find('.description').val();
                            var url = $(this).find('.url').val();
                            var image = $(this).find('.imgUrl').val();
                            console.log(title,description,url,image);
                            if(title == '' || description == '' || url == '' || image == ''){
                                t = false;
                            }else{
                                arr.title = title;
                                arr.description = description;
                                arr.url = url;
                                arr.image = image;
                                good.push(arr);
                            }

                        });
                        if(!t){
                            swal('请将商品信息填写完整');
                            return false;
                        }else{
                            obj.goods = good;
                        }
                    }

                    obj.name    = $('.form-addhospital .name').val();
                    obj.address = $('.form-addhospital .address').val();
                    obj.introduce = $('.form-addhospital .introduce').val();
                    obj.logo = $('.form-addhospital .logoVal').val();
                    obj.genre = type;
                    var map = new BMap.Map();
                    //console.log(map);
                    var localSearch = new BMap.LocalSearch(map);
                    localSearch.search(obj.address);
                    localSearch.setSearchCompleteCallback(function (searchResult) {
                            if(searchResult) {
                                var poi = searchResult.getPoi(0);
                                if (poi != undefined) {
                                    obj.pointlat = poi.point.lat;
                                    obj.pointlng = poi.point.lng;
                                } else {
                                    obj.pointlat = '';
                                    obj.pointlng = '';
                                }
                                obj.city = searchResult.city;
                            }else{
                                obj.pointlat = '';
                                obj.pointlng = '';
                                obj.city = '';
                            }
                    $.blockUI({
                        message: '<i class="fa fa-spinner fa-spin"></i> 正在提交数据...'
                    });
                        var option   = {
                            type: 'POST',
                            url: '/vender/add',
                            data: { data: JSON.stringify(obj) },
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        };
                        $.ajax(option).done(function (msg){
                            switch ( msg.status ) {
                                case 0:
                                    $(form)[0].reset();
                                    $('#hospitalTable').bootstrapTable('refresh');
                                    $.hideSubview();
                                    break;
                                default:
                                    swal('', msg.message);
                                    $.hideSubview();
                            }
                            $.unblockUI();
                        });
                  });
                },
                invalidHandler: function (event, validator){//display error alert on form submit
                    errorHandler1.show();
                }
            }
        );
    };

    var hospitalEditForm  = function (){
        var editHospitalForm = $('.form-editHospital');
        var errorHandler1    = $('.errorHandler', editHospitalForm);
        var successHandler1  = $('.successHandler', editHospitalForm);
        editHospitalForm.validate({
            errorElement: "span",
            errorClass: 'help-block',
            errorPlacement: function (error, element){
                element.parent().parent().find('label').append(error);
            },
            ignore: "",
            rules: {
                name: { required: true },
                address: { required: true},
                introduce: { required: true },
            },
            messages: {
                name: { required: ' *不能为空' },
                address: { required: ' *不能为空'},
                introduce: { required: ' *不能为空' },
            },
            submitHandler: function (form){
                errorHandler1.hide();
                var type = $('.form-editHospital input[name=gender1]:checked').val();
                var obj     = new Object;
                if(type == 1){
                    if(editor1.getValue() == ''){
                        swal('请填写商品展示内容');
                        return false;
                    }else{
                        obj.content = editor1.getValue();
                    }
                }
                var t = true;
                if(type == 2){
                    var good = [];
                    $('.good',form).each(function(){
                        var arr = new   Object();
                        var title = $(this).find('.title').val();
                        var description = $(this).find('.description').val();
                        var url = $(this).find('.url').val();
                        var image = $(this).find('.imgUrl').val();
                        if(title == '' || description == '' || url == '' || image == ''){
                            t = false;
                        }else{
                            arr.title = title;
                            arr.description = description;
                            arr.url = url;
                            arr.image = image;
                            good.push(arr);
                        }

                    });
                    if(!t){
                        swal('请将商品信息填写完整');
                        return false;
                    }else{
                        obj.goods = good;
                    }
                }

                obj.name    = $('.form-editHospital .name').val();
                obj.address = $('.form-editHospital .address').val();
                obj.introduce = $('.form-editHospital .introduce').val();
                obj.logo = $('.form-editHospital .logoVal').val();
                obj.genre = type;
                var map = new BMap.Map();
                //console.log(map);
                var localSearch = new BMap.LocalSearch(map);
                localSearch.search(obj.address);
                localSearch.setSearchCompleteCallback(function (searchResult) {
                        if(searchResult) {
                            var poi = searchResult.getPoi(0);
                            if (poi != undefined) {
                                obj.pointlat = poi.point.lat;
                                obj.pointlng = poi.point.lng;
                            } else {
                                obj.pointlat = '';
                                obj.pointlng = '';
                            }
                            obj.city = searchResult.city;
                        }else{
                            obj.pointlat = '';
                            obj.pointlng = '';
                            obj.city = '';
                        }
                $.blockUI({
                    message: '<i class="fa fa-spinner fa-spin"></i> 正在提交数据...'
                });

                    var option   = {
                        type: 'POST',
                        url: '/vender/edit',
                        data: { data: obj, hId: hId },
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    };
                    $.ajax(option).done(function (msg){
                        switch ( msg.status ) {
                            case 0:
                                $(form)[0].reset();
                                $('#hospitalTable').bootstrapTable('refresh');
                                $.hideSubview();
                                break;
                            default:
                                swal('', msg.message);
                                $.hideSubview();
                        }
                        $.unblockUI();
                    });
                });
            },
            invalidHandler: function (event, validator){//display error alert on form submit
                errorHandler1.show();
            }
        });
    };


    var runSubview        = function (){
        $('.add-hospital').on('click', function (){
            $.subview({
                content: '#newHospital',
                startFrom: "right",
                onShow: function (){
                    $('#newHospital').find('.logoUrl').attr('src','http://www.youbian.com/Images/201313/13653239592670.jpg');
                    $('#newHospital').find('.imgUrl').val('');
                    $('#newHospital').find('.system-malady').removeClass('current');
                },
                onClose: function (){
                    checkAddHospital(close);
                },
                onHide: function (){
                }
            });
        });
        $('.edit-hospital').on('click', function (){
            $.subview({
                content: '#editHospital',
                startFrom: "right",
                onShow: function (){
                    var hospital = $('#hospitalTable').bootstrapTable('getSelections')[0];
                    hId = hospital.id;
                    $('.form-editHospital .logoUrl').attr('src',hospital['imgUrl']);
                    $('.form-editHospital .logoUrl').next().val(hospital['img']);
                    $('.form-editHospital .name').val(hospital['name']);
                    $('.form-editHospital .address').val(hospital['address']);
                    $('.form-editHospital .introduce').val(hospital['introduce']);
                    $('.form-editHospital').find('.genre').iCheck('uncheck');
                    $('.form-editHospital').find('.genre').each(function(){
                        var id = $(this).val();
                        if(id == hospital['genre']){
                            $(this).iCheck('check');
                        }
                    })
                    $('.form-editHospital .good').remove();
                    editor1.setValue('');
                    var str = '  <div class="good" style="border: 1px solid #eeeeee;overflow: hidden;padding: 20px;">'+
                        '<div class="col col-sm-4 col-xs-12 col-md-3 col-lg-3 col-xl-3">'+
                        '<div class="form-group" >'+
                        '<label for="">商品主图</label>'+
                        '</div>'+
                        '<img  src="" class="img-thumbnail img" style="width:100%;margin-bottom: 20px;min-height: 100px" alt="">'+
                        '<input type="hidden" class="imgUrl" value="">'+
                        '</div>'+
                        '<div class="col col-sm-8 col-xs-12 col-md-9 col-lg-9 col-xl-9">'+
                        '<div class="form-group">'+
                        '<label>商品名称</label>'+
                        '<input type="text" class="form-control title"  name="title" value="">'+
                        '</div>'+
                        '<div class="form-group">'+
                        '<label>商品描述</label>'+
                        '<input type="text" class="form-control description" name="description" value="">'+
                        '</div>'+
                        '<div class="form-group">'+
                        '<label>商品购买链接</label>'+
                        '<input type="text" class="form-control url" name="url"  value="">'+
                        '</div>'+
                        '<i class="fa fa-times-circle fa-3x delete"></i>'+
                        '</div>'+
                        '</div>';
                    if(hospital['genre'] == 1){
                        editor1.setValue(hospital['content']);
                    }else if(hospital['genre'] == 2){
                        var str = '';
                        $.each(hospital['goods'],function(index,value){
                             str += '  <div class="good" style="border: 1px solid #eeeeee;overflow: hidden;padding: 20px;">'+
                                '<div class="col col-sm-4 col-xs-12 col-md-3 col-lg-3 col-xl-3">'+
                                '<div class="form-group" >'+
                                '<label for="">商品主图</label>'+
                                '</div>'+
                                '<img  src="'+ value['imgUrl'] +'" class="img-thumbnail img" style="width:100%;margin-bottom: 20px;min-height: 100px" alt="">'+
                                '<input type="hidden" class="imgUrl" value="'+ value['image'] +'">'+
                                '</div>'+
                                '<div class="col col-sm-8 col-xs-12 col-md-9 col-lg-9 col-xl-9">'+
                                '<div class="form-group">'+
                                '<label>商品名称</label>'+
                                '<input type="text" class="form-control title"  name="title" value="'+ value['title'] +'">'+
                                '</div>'+
                                '<div class="form-group">'+
                                '<label>商品描述</label>'+
                                '<input type="text" class="form-control description" name="description" value="'+ value['description'] +'">'+
                                '</div>'+
                                '<div class="form-group">'+
                                '<label>商品购买链接</label>'+
                                '<input type="text" class="form-control url" name="url"  value="'+ value['url'] +'">'+
                                '</div>'+
                                '<i class="fa fa-times-circle fa-3x delete"></i>'+
                                '</div>'+
                                '</div>';
                        });
                    }
                    $('.form-editHospital').find('.addGood').before(str);
                    ajaxLoadImage();
                    deFunction();

                },
                onClose: function (){
                    checkEditHospital(close);
                },
                onHide: function (){
                }
            });
        });

        $('.genre').on('ifChecked', function(event){
            if($(this).val() == 1){
                $(this).parents('form').find('.showGoods').show();
                $(this).parents('form').find('.recommend').hide();
            }else if($(this).val() == 2){
                $(this).parents('form').find('.showGoods').hide();
                $(this).parents('form').find('.recommend').show();
            }else{
                $(this).parents('form').find('.showGoods').hide();
                $(this).parents('form').find('.recommend').hide();
            }
        });
        $('.addGood').click(function(){
            var str = '  <div class="good" style="border: 1px solid #eeeeee;overflow: hidden;padding: 20px;">'+
                '<div class="col col-sm-4 col-xs-12 col-md-3 col-lg-3 col-xl-3">'+
                '<div class="form-group" >'+
                '<label for="">商品主图</label>'+
                '</div>'+
                '<img  src="" class="img-thumbnail img" style="width:100%;margin-bottom: 20px;min-height: 100px" alt="">'+
                '<input type="hidden" class="imgUrl" value="">'+
                '</div>'+
                '<div class="col col-sm-8 col-xs-12 col-md-9 col-lg-9 col-xl-9">'+
                '<div class="form-group">'+
                '<label>商品名称</label>'+
                '<input type="text" class="form-control title"  name="title" value="">'+
                '</div>'+
                '<div class="form-group">'+
                '<label>商品描述</label>'+
                '<input type="text" class="form-control description" name="description" value="">'+
                '</div>'+
                '<div class="form-group">'+
                '<label>商品购买链接</label>'+
                '<input type="text" class="form-control url" name="url"  value="">'+
                '</div>'+
                '<i class="fa fa-times-circle fa-3x delete"></i>'+
                '</div>'+
                '</div>';
            $(this).parent().find('.addGood').before(str);
            ajaxLoadImage();
            deFunction();
        })
    };
    var deFunction = function(){
        $('.delete').click(function(){
            var length = $(this).parents('.form-addhospital').find('.good').length;
            if(length == 1){
                return false;
            }else{
                $(this).parents('.good').remove();
            }
        });
    }
    var checkEditHospital = function (bool){
        if ( bool ) {
            close = false;
            bootbox.confirm({
                buttons: { cancel: { label: "取消" }, confirm: { label: "确认" } },
                message: "您没有保存该记录，确定关闭吗?",
                callback: function (result){
                    close = true;
                    if ( result ) {
                        $('.form-editHospital')[0].reset();
                        $.hideSubview();
                    }
                }
            });
        }
    };
    var checkAddHospital      = function (bool){
        if ( bool ) {
            close = false;
            bootbox.confirm({
                buttons: { cancel: { label: "取消" }, confirm: { label: "确认" } },
                message: "您没有保存该记录，确定关闭吗?",
                callback: function (result){
                    close = true;
                    if ( result ) {
                        $('.form-addhospital')[0].reset();
                        $.hideSubview();
                    }
                }
            });
        }
    };

    var submitForm        = function (){
        $('.save-edit').on('click', function (){
            $('.form-editHospital').submit();
        });
    };
    return {
        init: function () {
            hosptitalTable();
            ajaxLoadImage();
            runSubview();
            submitForm();
            hospitalEditForm();
            submitAddHospitalForm();
            editorFunc();
            deFunction();
        }
    };
}();

