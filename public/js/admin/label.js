var label = function () {
    "use strict";
    var subViewElement, subViewContent;
    var username = $('.username').text();
    var labeltable = function (){
        $('#labeltable').bootstrapTable({
            method:'get',
            url: '/label/data',
            toolbar:"#toolbar",
            cache: false,
            striped: true,
            selectItemName:"radioName",
            pagination: true,
            pageList: [10,20,50,100],
            pageSize:10,
            pageNumber:1,
            search: true,
            sidePagination:'server',
            queryParams: queryParams,
            showColumns: true,
            clickToSelect: true
        });
        $('#large-columns-table').next().click(function () {
            $(this).hide();
            buildTable($('#labeltable'), 50, 50);
        });
        $('#labeltable').on('check.bs.table uncheck.bs.table ' +
            'check-all.bs.table uncheck-all.bs.table', function () {
            $('.edit-label').prop('disabled', !$('#labeltable').bootstrapTable('getSelections').length);
        });
    };
    var queryParams = function(params) {
        $('.edit-label').prop('disabled',true);
        return params
    };
    var checkAddlabel = function() {
        bootbox.confirm({
            buttons:{cancel:{label:"取消"}, confirm:{label:"确认"}},
            message:"您没有保存该记录，确定关闭吗?",
            callback:function(result) {
                if (result) {
                    $('.form-addlabel')[0].reset();
                    $.hideSubview();
                }
            }
        });
    };
    var checkEditlabel = function () {
        bootbox.confirm({
            buttons:{cancel:{label:"取消"}, confirm:{label:"确认"}},
            message:"您没有保存该记录，确定关闭吗?",
            callback:function(result) {
                if (result) {
                    $('.form-editlabel')[0].reset();
                    $.hideSubview();
                }
            }
        });
    };
    var runSubviews = function () {
        $('.new-label').off().on("click",function() {
            subViewElement = $(this);
            subViewContent = subViewElement.attr('href');
            $.subview({
                content: subViewContent,
                startFrom: "right",
                onShow: function() {
                },
                onClose: function() {
                    checkAddlabel();
                },
                onHide: function() {

                }
            });
        });
        $('.edit-label').off().on("click",function() {
            subViewElement = $(this);
            subViewContent = '#editlabel';
            $.subview({
                content: subViewContent,
                startFrom: "right",
                onShow: function() {
                    $.blockUI({
                        message:'<i class="fa fa-spinner fa-spin"></i> 加载数据中...'
                    });
                    doEditlabelSth();
                },
                onClose: function() {
                    checkEditlabel();
                },
                onHide: function() {

                }
            });
        });
        $(".close-subview-button").off().on("click", function(e) {
            $(".close-subviews").trigger("click");
            e.preventDefault();
        });
    };
    var doEditlabelSth = function () {
        var arr = $('#labeltable').bootstrapTable('getSelections');
        var status;
        switch(arr[0].status){
            case '暂停': status = '0'; break;
            case '正常': status = '1'; break;
        }
        $('.form-editlabel .bh').val(arr[0].bh);
        $('.form-editlabel .name').val(arr[0].name);
        $('.form-editlabel .status').val(status);
        $.unblockUI();
    };
    var runAddlabelFormValidation = function () {
        var formAddlabel = $('.form-addlabel');
        var errorHandler1 = $('.errorHandler',formAddlabel);
        var successHandler1 = $('.successHandler', formAddlabel);
        formAddlabel.validate({
            errorElement: "span",
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                element.parent().parent().find('label').append(error);
            },
            ignore: "",
            rules: {
                name : {
                    required : true
                }
            },
            messages: {
                name : {
                    required : ' *不能为空'
                }
            },
            submitHandler : function(form) {
                errorHandler1.hide();
                $.blockUI({
                    message:'<i class="fa fa-spinner fa-spin"></i> 正在提交标签数据...'
                });
                var labelToAdd = new Object;
                labelToAdd.name = $('.form-addlabel .name').val();
                labelToAdd.status = $('.form-addlabel .status').val();
                $.ajax({
                    type:'POST',
                    url:'/label/add',
                    data:{postdata:labelToAdd},
                    dataType:'json',
                    headers:{
                        'X-CSRF-TOKEN':$('meta[name="_token"]').attr('content')
                    },
                    success: function(json) {
                        if(json.say == 'ok'){
                            $('.form-addlabel')[0].reset();
                            $.hideSubview();
                            toastr.success( username + '添加一个标签！');
                            $('#labeltable').bootstrapTable('refresh');
                        }
                        if(json.say == 'no'){
                            swal('标签已存在','','warning');
                        }
                        $.unblockUI();
                    }
                });
            },
            invalidHandler : function(event, validator) {//display error alert on form submit
                errorHandler1.show();
            }
        });
    };
    var runEditlabelFormValidation = function () {
        var formEditlabel = $('.form-editlabel');
        var errorHandler2 = $('.errorHandler',formEditlabel);
        var successHandler2 = $('.successHandler', formEditlabel);
        formEditlabel.validate({
            errorElement: "span",
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                element.parent().parent().find('label').append(error);
            },
            ignore: "",
            rules: {
                name :{required:true}
            },
            messages:{
                name:{required:' *不能为空'}
            },
            invalidHandler: function(event, validator) {
                successHandler2.hide();
                errorHandler2.show();
            },
            submitHandler:function(form) {
                successHandler2.show();
                errorHandler2.hide();
                $.blockUI({
                    message: '<i class="fa fa-spinner fa-spin"></i> 正在提交标签数据...'
                });
                var labelToEdit = new Object;
                labelToEdit.bh = $('.form-editlabel .bh').val(),
                    labelToEdit.name = $('.form-editlabel .name').val();
                labelToEdit.status = $('.form-editlabel .status').val();
                $.ajax({
                    type:'POST',
                    url:'/label/edit',
                    data:{postdata:labelToEdit},
                    dataType:'json',
                    headers:{
                        'X-CSRF-TOKEN':$('meta[name="_token"]').attr('content')
                    },
                    success: function(json){
                        if(json.say == 'ok'){
                            $.hideSubview();
                            toastr.success( username + '修改一条标签信息');
                            $('.form-editlabel')[0].reset();
                            $('#labeltable').bootstrapTable('refresh');
                        }
                        $.unblockUI();
                    }
                });
            }
        });
    };
    return {
        init: function () {
            labeltable();
            runSubviews();
            runAddlabelFormValidation();
            runEditlabelFormValidation();
        }
    };
}();
