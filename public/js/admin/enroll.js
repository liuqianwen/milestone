var article = function () {
    // 表格
    var clickTime = 0;
    var tableObj = null;
    var tableOrderObj = null;
    var userId = null;
    var orderId = null;
    var addForm     = $('#addInformation');
    var editForm     = $('#editInformation');
    var editor,editor1;
    var wizardContent = $('#wizard');
    var wizardForm = $('#form');
    var numberOfSteps = $('.swMain > ul > li').length;
    var tableOrder = $('#order-table');
    var dataTable;
    var userTable = function () {
        var tableEle = $('#_userTable');
        tableEle.bootstrapTable({
            method: 'get',
            url: '/enroll/data',
            toolbar: "#toolbar",
            cache: true,
            striped: true,
            selectItemName: "radioName",
            pagination: true,
            pageList: [10, 20, 50, 100],
            pageSize: 20,
            pageNumber: 1,
            search: true,
            sidePagination: 'server',
            queryParams: function (params) {
                $('.export-enroll').attr('disabled', true);
                $('.export-order').attr('disabled', true);
                $('.unavailable').attr('disabled', true);
                $('.available').attr('disabled', true);
                return params
            },
            showColumns: true,
            clickToSelect: true
        });
        tableEle.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
            tableObj = tableEle.bootstrapTable('getSelections')[0];
            if(tableObj.statusVal == 1){
                $('.export-enroll').attr('disabled', !tableEle.length);
                $('.export-order').attr('disabled', !tableEle.length);
                $('.unavailable').attr('disabled', !tableEle.length);
                $('.available').attr('disabled', tableEle.length);
            }else if(tableObj.statusVal == 0){
                $('.export-enroll').attr('disabled', tableEle.length);
                $('.export-order').attr('disabled', tableEle.length);
                $('.unavailable').attr('disabled', tableEle.length);
                $('.available').attr('disabled', tableEle.length);
            }else{
                $('.export-enroll').attr('disabled', !tableEle.length);
                $('.export-order').attr('disabled', !tableEle.length);
                $('.unavailable').attr('disabled', tableEle.length);
                $('.available').attr('disabled', !tableEle.length);
            }

            userId = tableObj.id;
            tableOrder.bootstrapTable('refresh');
            OrderAjax();
        });
    };

    var OrderAjax = function () {
        $.ajax({
            type: "post",
            url: '/enroll/order/table',
            dataType: 'json',
            data: { id: userId },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            success: function (m){
                switch ( m.status ) {
                    case 0:
                        dataTable.destroy();
                        dataTable.clear();
                        var str = '';
                        $.each(m.rows,function (index,value) {
                            str += '<tr>';
                            str += '<td>'+ value.base +'</td>';
                            str += '<td>'+ value.order_number +'</td>';
                            str += '<td>'+ value.pay_type +'</td>';
                            str += '<td>'+ value.wx_order +'</td>';
                            str += '<td>'+ value.price +'</td>';
                            str += '<td>'+ value.num +'</td>';
                            str += '<td>'+ value.total_price +'</td>';
                            str += '<td>' + value.ticket_money + '</td>';
                            str += '<td class="ticket-confirm-money">' + value.ticket_confirm_money + '</td>';
                            str += '<td class="ticket-confirm-money">' + value.is_ticket + '</td>';
                            // str += '<td>'+ value.ticket_number +'</td>';
                            str += '<td>'+ value.ticket_link +'</td>';
                            str += '<td>'+ value.ticket_tel +'</td>';
                            str += '<td>'+ value.ticket_email +'</td>';
                            str += '<td class="status">'+ value.status +'</td>';
                            str += '<td class="remark">'+ value.remark +'</td>';
                            str += '<td>'+ value.createTime +'</td>';
                            str += '<td>'+ value.caozuo +'</td>';
                            str += '</tr>';
                        })
                        $('#order-table tbody').empty();
                        $('#order-table tbody').append(str);
                        orderTable();
                        break;
                    default:
                }
            }
        });
    }
    var orderTable = function () {
        // tableOrder.bootstrapTable({
        //     method: 'get',
        //     url: '/enroll/order/data',
        //     toolbar: "#toolbar1",
        //     cache: true,
        //     striped: true,
        //     selectItemName: "radioName",
        //     pagination: true,
        //     pageList: [10, 20, 50, 100],
        //     pageSize: 20,
        //     pageNumber: 1,
        //     search: true,
        //     sidePagination: 'server',
        //     queryParams: function (params) {
        //         params.id = userId;
        //         return params
        //     },
        //     showColumns: true,
        //     clickToSelect: true
        // });
        // tableOrder.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
        //     tableOrderObj = tableOrder.bootstrapTable('getSelections')[0];
        //     orderId = tableOrderObj.id;
        // });
       dataTable =   $('#order-table').DataTable({
            pageLength:25,
            "ordering": true,
            "language": {
                "processing": '<div class="panel-overlay-content unselectable"><span class="panel-overlay-icon text-dark"><i class="fa fa-spinner fa-2x fa-spin"></i></span><h4 class="panel-overlay-title">数据加载中...</h4></div>',
                "lengthMenu": "显示 _MENU_ 项结果",
                "zeroRecords": "没有匹配结果",
                "info": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                "infoEmpty": "显示第 0 至 0 项结果，共 0 项",
                "infoFiltered": "(由 _MAX_ 项结果过滤)",
                "infoPostFix": "",
                "search": "搜索:",
                "searchPlaceholder": "搜索...",
                "iDisplayLength":25,
                "url": "",
                "emptyTable": "暂无数据",
                "loadingRecords": "载入中...",
                "infoThousands": ",",
                "paginate": {
                    "first": "首页",
                    "previous": "上页",
                    "next": "下页",
                    "last": "末页"
                },
                "aria": {
                    paginate: {
                        first: '首页',
                        previous: '上页',
                        next: '下页',
                        last: '末页'
                    }
                },
            }
        });
    };

    var editorFunc = function(){
        editor =  new Simditor({
            textarea: $('#editor'),
            toolbar: ['title', 'bold', 'italic', 'underline', 'strikethrough','fontScale', 'color', '|', 'ol', 'ul', 'blockquote', 'code', 'table', '|', 'link', 'image', 'hr', '|', 'indent', 'outdent', 'alignment', '|', 'html'],
            upload: {
                url: '/image/uploads',
                params: null,
                fileKey: 'upload_file',
                connectionCount: 3,
                leaveConfirm: '文件上传中，关闭此页面将取消上传。'
            },
            pasteImage: true,
            tabIndent: true,
            allowedTags:['div','br', 'span', 'a', 'img', 'b', 'strong', 'i', 'strike', 'u', 'font', 'p', 'ul', 'ol', 'li', 'blockquote', 'pre', 'code', 'h1', 'h2', 'h3', 'h4', 'hr'],
            allowedStyles:{
                div:['font-size','font-family','color','float','display','flex-direction','flex-wrap','flex-flow','justify-content','align-items','align-content','width','height'],
                table:['table']
            }
        });
        editor.on('valuechanged',function(e){
            var ele = $('.iwindow');
            ele.html(editor.getValue());
        })
        editor1 =  new Simditor({
            textarea: $('#editor1'),
            toolbar: ['title', 'bold', 'italic', 'underline', 'strikethrough','fontScale', 'color', '|', 'ol', 'ul', 'blockquote', 'code', 'table', '|', 'link', 'image', 'hr', '|', 'indent', 'outdent', 'alignment', '|', 'html'],
            upload: {
                url: '/image/uploads',
                params: null,
                fileKey: 'upload_file',
                connectionCount: 3,
                leaveConfirm: '文件上传中，关闭此页面将取消上传。'
            },
            pasteImage: true,
            tabIndent: true,
            allowedTags:['div','br', 'span', 'a', 'img', 'b', 'strong', 'i', 'strike', 'u', 'font', 'p', 'ul', 'ol', 'li', 'blockquote', 'pre', 'code', 'h1', 'h2', 'h3', 'h4', 'hr'],
            allowedStyles:{
                div:['font-size','font-family','color','float','display','flex-direction','flex-wrap','flex-flow','justify-content','align-items','align-content','width','height'],
                table:['table']
            }
        });
        editor1.on('valuechanged',function(e){
            var ele = $('.iwindow1');
            ele.html(editor1.getValue());
        })
    };


    var uploadImg   = function (){
        $('.img-thumbnail').on('click', function (){
            var that = $(this);
            $("#file").val('');
            $('#clipArea').find('div').remove();
            $('#publishModal').modal('show');
            new PhotoClip('#clipArea', {
                size: [500, 280],   //剪裁完成的图片height，width
                outputSize: [1000, 560], //剪裁框的height，width
                file: "#file",     //文件上传框ID
                view: "#view",     //预览div的ID
                ok: "#clipBtn",    //剪裁开始按钮ID
                outputType: 'jpg', //指定输出图片的类型，可选 "jpg" 和 "png" 两种种类型，默认为 "jpg"。
                //img: 'img/mm.jpg',
                loadStart: function (){
                    console.log('开始读取照片');
                },
                loadComplete: function (){
                    console.log('照片读取完成');
                },
                done: function (dataURL){
                    $.ajax({
                        type: "POST",
                        url: '/image/upload',
                        dataType: 'json',
                        data: { img: dataURL },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        success: function (m){
                            switch ( m.status ) {
                                case 0:
                                    that.attr('src', m.data.route);
                                    that.parent().find('.imgUrl').val(m.data.url);
                                    $('#publishModal').modal('hide');
                                    break;
                                default:
                            }
                        }
                    });
                },
                fail: function (msg){
                    console.log(msg);
                }
            });
        });
    };

    var event = function () {
        $('.user-add').off().on('click', function () {
            preventMultipleClicks(function () {
                $.subview({
                    content: "#_addUser",
                    startFrom: "right",
                    onShow: function () {
                        $('#enroll_id').val('');
                        $('#wizard .anchor  a').not('#wizard .anchor  a:first').removeClass().addClass('disabled');
                        $(addForm).find('.img-thumbnail').attr('src','https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg');
                        $(addForm).find('.imgUrl').val('');
                        editor.setValue('');

                    },
                    onClose: function () {
                        $.hideSubview();
                    },
                    onHide: function () {
                    }
                });
            });
        });
        $('body').on('click','.info-enroll', function (){
            preventMultipleClicks(function () {
                $.subview({
                    content: "#_editUser",
                    startFrom: "right",
                    onShow: function () {
                        $(editForm).find('.id').val(tableObj.id);
                        $(editForm).find('.title').val(tableObj.name);
                        $(editForm).find('.description').val(tableObj.description);
                        $(editForm).find('.imgUrl').val(tableObj.image);
                        $(editForm).find('.img-thumbnail').attr('src',tableObj.imgUrl);
                        editor1.setValue(tableObj.content);
                        $('.iwindow1').html(tableObj.content);
                        var str = '';
                        $.each(tableObj.form,function (index,value) {
                            if(value.type == 1){
                                str +=
                                    '                                                <div class="form-group">\n' +
                                    '                                                    <label>'+ value.name +'</label>\n' +
                                    '                                                    <input type="number" class="form-control" name="" >\n' +
                                    '                                                </div>'
                            }else if(value.type == 2){
                                str += '     <div class="form-group">\n' +
                                    '                                                    <label>'+ value.name +'</label>\n' +
                                    '                                                    <select name="" class="form-control" id="">\n';

                                $.each(value.option,function (index1,value1) {
                                    str += '<option value="0">'+ value1 +'</option>';
                                });
                                   str+= '                                                    </select>\n' +
                                    '                                                </div>\n';
                            }
                        })
                        $('.form-info').empty();
                        $('.form-info').append(str);

                    },
                    onClose: function () {
                        $.hideSubview();
                    },
                    onHide: function () {
                    }
                });
            });
        });
        $('body').on('click','.info-order', function (){
            orderId = $(this).data('id');
            console.log(orderId);
            preventMultipleClicks(function () {
                $.subview({
                    content: "#infoOrder",
                    startFrom: "right",
                    onShow: function () {
                        $.ajax({
                            url:'/enroll/order/info',
                            method:'post',
                            data:{id:orderId},
                            dataType:'json',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            },
                            success:function (res) {
                                if(res.status == 1){
                                    $('.order_number').text(res.order.order_number);
                                    $('.wx_order').text(res.order.wx_order);
                                    if(res.order.pay_type == 1){
                                        $('.pay_types').text('微信支付');
                                    }else{
                                        $('.pay_types').text('对公打款');
                                    }
                                    if(res.order.status == 0){
                                        $('.order_status').text('待审核');
                                        $('.btn-save').show();
                                    }else{
                                        $('.order_status').text('支付成功');
                                        $('.btn-save').hide();
                                    }
                                    $('.date').text(res.order.created_at);
                                    $('.total_price').text(res.order.total_price);
                                    $('.total_price').text(res.order.total_price);
                                    $('.total_price').text(res.order.total_price);
                                    $('.rise').text(res.order.pay_ticket['head']);
                                    $('.paragraph').text(res.order.pay_ticket['number']);
                                    $('.invoice_name').text(res.order.pay_ticket['name']);
                                    $('.invoice_price').text(res.order.pay_ticket['money']);
                                    $('.tel').text(res.order.pay_ticket['phone']);
                                    $('.email').text(res.order.pay_ticket['email']);
                                    var str = '<tr>';
                                    $.each(res.arr,function (index,value) {
                                        str+=  ' <th>'+value+'</th>';
                                    })

                                    str +=    '                </tr>';
                                    var user = '';
                                    $.each(res.user,function (index,value) {
                                        user += '<tr>';
                                        $.each(value.value,function (index1,value1) {
                                            user += '<td>'+value1.value+'</td>';
                                        });

                                        str +=    '                </a>';
                                    })
                                    $('.goods-div table thead').empty();
                                    $('.goods-div table tbody').empty();
                                    $('.goods-div table thead').append(str);
                                    $('.goods-div table tbody').append(user);
                                }
                            }
                        });
                    },
                    onClose: function () {
                        $.hideSubview();
                    },
                    onHide: function () {
                    }
                });
            });
        });
        $('.unavailable').on('click', function () {
            var data = new Object();
            data.id = userId;
            data.status = 2;
            $.ajax({
                url:'/enroll/edit',
                method:'post',
                data:{data:JSON.stringify(data)},
                dataType:'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success:function (res) {
                    $('#_userTable').bootstrapTable('refresh');
                }
            });
        });
        $(".close-subview-button").off().on("click", function (e) {
            bootbox.confirm({
                buttons: {cancel: {label: "取消"}, confirm: {label: "确认"}},
                message: "您确定要关闭此页面吗?",
                callback: function (result) {
                    if (result) {
                        $(".close-subviews").trigger("click");
                        e.preventDefault();
                    }
                }
            });
        });
        $('.available').on('click', function () {
            var data = new Object();
            data.id = userId;
            data.status = 1;
            $.ajax({
                url:'/enroll/edit',
                method:'post',
                data:{data:JSON.stringify(data)},
                dataType:'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success:function (res) {
                    $('#_userTable').bootstrapTable('refresh');
                }
            });
        });

        $('.save-order').click(function () {
            $.ajax({
                url:'/enroll/order/edit',
                method:'post',
                data:{id:orderId},
                dataType:'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success:function (res) {

                    $('#order-table').bootstrapTable('refresh');
                    $.hideSubview();
                }
            });
        })


        //编辑促销
        $('body').on('click','.edit-enroll', function (){
            $.subview({
                content: "#_addUser", //对应子视图id
                startFrom: "right",
                onShow: function (){
                    $('#enroll_id').val(tableObj.id);
                    $('.title').val(tableObj.name);
                    $('.description').val(tableObj.description);
                    $('#_addUser').find('.imgUrl').val(tableObj.image);
                    $('#_addUser').find('.img-thumbnail').attr('src',tableObj.imgUrl);
                    editor.setValue(tableObj.content);
                    $('.iwindow').html(tableObj.content);
                    // $('#_addUser #wizard ul a').not('#_addUser #wizard ul a:first').removeClass().addClass('disabled');
                    wizardContent.smartWizard('goToStep', (tableObj.is_step+1));
                    $('.is_pay').trigger('change');
                },
                onClose: function (){
                    $.hideSubview();
                    wizardForm[0].reset();
                    wizardContent.smartWizard('goToStep', 1);
                    $('.form-group').removeClass('has-success').find('.symbol').removeClass('ok');
                    $('#wizard .anchor a').removeClass().addClass('disabled');
                },
                onHide: function (){
                    $('#wizard .anchor a').removeClass().addClass('disabled');
                }
            });
        });


        $('.is_pay').change(function () {
            var type = $(this).val();
            if(type == 1){
                $('.pay_price').parent().show();
                $('.pay_type').parent().show();
            }else{
                $('.pay_price').parent().hide();
                $('.pay_type').parent().hide();
            }
        })
        $('.is_pay').trigger('change');

        // //预览
        // $('body').on('click','.info-enroll', function (){
        //     $('.preview-iframe').attr('src','/wechat/signup/detail/'+userId );
        //     $('#preview-enroll').modal('show');
        // });

        //导出
        $('.export-enroll').click(function () {
            window.location.href = '/enroll/export?id=' + userId;
        })
        $('.export-order').click(function () {
            window.location.href = '/enroll/export/order?id=' + userId;
        })

        $('body').on('change','.form_type',function () {
            var type = $(this).val();
            if(type == 1){
                $(this).parents('tr').find('.length').parent().show();
                $(this).parents('tr').find('.option').parent().hide();
            }else{
                $('.change-tr').css('width','15em')
                $(this).parents('tr').find('.length').parent().hide();
                $(this).parents('tr').find('.option').parent().show();
            }
        })

    };

    var initWizard = function () {
        // function to initiate Wizard Form
        wizardContent.smartWizard({
            selected: 0,
            keyNavigation: false,
            onLeaveStep: leaveAStepCallback,
            onShowStep: onShowStep,
        });
        var numberOfSteps = 0;
        animateBar();
        initValidator();
    };
    var animateBar = function (val) {
        if ((typeof val == 'undefined') || val == "") {
            val = 1;
        };

        var valueNow = Math.floor(100 / numberOfSteps * val);
        $('.step-bar').css('width', valueNow + '%');
    };
    var validateCheckRadio = function (val) {
        $("input[type='radio'], input[type='checkbox']").on('ifChecked', function(event) {
            $(this).parent().closest(".has-error").removeClass("has-error").addClass("has-success").find(".help-block").remove().end().find('.symbol').addClass('ok');
        });
    };
    var initValidator = function () {
        $.validator.addMethod("cardExpiry", function () {
            //if all values are selected
            if ($("#card_expiry_mm").val() != "" && $("#card_expiry_yyyy").val() != "") {
                return true;
            } else {
                return false;
            }
        }, 'Please select a month and year');
        $.validator.addMethod("compareDate", function (value, element, param) {
            var startDate = jQuery(param).val();
            var date1 = new Date(Date.parse(startDate.replace(/\-/g, "/")));
            var date2 = new Date(Date.parse(value.replace(/\-/g, "/")));
            return date1 <= date2;
        });
        $.validator.setDefaults({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
                    error.appendTo($(element).closest('.form-group').children('div'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            ignore: ':hidden',
            rules: {
                title: {
                    required: true,
                }
            },
            messages: {
                title: {
                    required: ' * 标题不能为空'
                }
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            }
        });
    };
    var displayConfirm = function () {
        $('.display-value', form).each(function () {
            var input = $('[name="' + $(this).attr("data-display") + '"]', form);
            if (input.attr("type") == "text" || input.attr("type") == "email" || input.is("textarea")) {
                $(this).html(input.val());
            } else if (input.is("select")) {
                $(this).html(input.find('option:selected').text());
            } else if (input.is(":radio") || input.is(":checkbox")) {

                $(this).html(input.filter(":checked").closest('label').text());
            } else if ($(this).attr("data-display") == 'card_expiry') {
                $(this).html($('[name="card_expiry_mm"]', form).val() + '/' + $('[name="card_expiry_yyyy"]', form).val());
            }
        });
    };
    var onShowStep = function (obj, context) {
        if(context.toStep == numberOfSteps){
            $('.anchor').children("li:nth-child(" + context.toStep + ")").children("a").removeClass('wait');
            displayConfirm();
        }
        $(".next-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goForward");
        });
        $(".back-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goBackward");
        });
        $(".finish-step").unbind("click").click(function (e) {
            e.preventDefault();
            onFinish(obj, context);
        });
    };
    var leaveAStepCallback = function (obj, context) {
        return validateSteps(context.fromStep, context.toStep);
        // return false to stay on step and true to continue navigation
    };
    var onFinish = function (obj, context) {
        if (validateAllSteps()) {
            $('.anchor').children("li").last().children("a").removeClass('wait').removeClass('selected').addClass('done').children('.stepNumber').addClass('animated tada');
            //wizardForm.submit();
        }
    };
    var validateSteps = function (stepnumber, nextstep) {
        var isStepValid = false;
        if (numberOfSteps >= nextstep && nextstep > stepnumber) {
            // cache the form element selector
            if (stepnumber == 2 || (stepnumber!= 2 && wizardForm.valid())) { // validate the form
                wizardForm.validate().focusInvalid();
                for (var i=stepnumber; i<=nextstep; i++){
                    $('.anchor').children("li:nth-child(" + i + ")").not("li:nth-child(" + nextstep + ")").children("a").removeClass('wait').addClass('done').children('.stepNumber').addClass('animated tada');
                }
                switch (stepnumber) {
                    case 1:
                        step1Submit();
                        // resetForm();
                        break;
                    case 2:
                        var object = [];
                        $('.uio-product tbody tr').each(function(index,value){
                            var name = $(this).find('.name').val();
                            var type = $(this).find('.form_type').val();
                            var is_must = $(this).find('.is_must').val();
                            var length = $(this).find('.length').val();
                            var option = $(this).find('.option').val();
                            if(type == 1){
                                if((name && length)){
                                    var arr = {};
                                    arr.number = index + 1;
                                    arr.name = name;
                                    arr.type = type;
                                    arr.is_must = is_must;
                                    arr.length = length;
                                    arr.option = '';
                                    object.push(arr);
                                }
                            }else{
                                if(name && option ){
                                    var arr = {};
                                    arr.number = index + 1;
                                    arr.name = name;
                                    arr.type = type;
                                    arr.is_must = is_must;
                                    arr.length = '';
                                    arr.option = option;
                                    object.push(arr);
                                }
                            }

                        });
                        if(object.length == 0){
                            swal('请填写完整');
                            return false;
                        }
                        var is_pay = $('.is_pay').val();
                        var pay_price = $('.pay_price').val();

                        if(is_pay == 1){
                            if(!pay_price || parseFloat(pay_price) <= 0){
                                swal('请填写报名费用');
                                return false;
                            }
                        }
                        finishSave();
                        break;
                    case 3:

                        break;
                    case 4:
                        break
                }
                //focus the invalid fields
                animateBar(nextstep);
                isStepValid = true;
                return true;
            };
        } else if (nextstep < stepnumber) {
            for (i=nextstep; i<=stepnumber; i++){
                $('.anchor').children("li:nth-child(" + i + ")").children("a").addClass('wait').children('.stepNumber').removeClass('animated tada');
            }

            animateBar(nextstep);
            return true;
        }
    };
    var validateAllSteps = function () {
        var isStepValid = true;
        // all step validation logic
        return isStepValid;
    };


    // 保存第一步
    var step1Submit = function () {
        var obj         = {};
        obj.id = $('#enroll_id').val();
        obj.title       = $('#_addUser .title').val();
        obj.description = $('#_addUser .description').val();
        obj.content     = editor.getValue();
        obj.image       = $('.imgUrl').val();
        $.blockUI({
            message: '<i class="fa fa-spinner fa-spin"></i> 正在加载数据...'
        });
        $.ajax({
            type: 'POST',
            url: '/enroll/step1Save',
            data: { data: JSON.stringify(obj) },
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            success: function (m){
                switch ( m.status ) {
                    case 0:
                        $('#enroll_id').val(m.id);
                        $('#_userTable').bootstrapTable('refresh');
                        $.unblockUI();
                        break;
                    default:
                        swal('失败', m.message, 'warning');
                }
            }
        });
    };

    var finishSave  = function () {
        var id = $('#enroll_id').val();
        if(id){
            var object = [];
            $('.uio-product tbody tr').each(function(index,value){
                var name = $(this).find('.name').val();
                var type = $(this).find('.form_type').val();
                var is_must = $(this).find('.is_must').val();
                var length = $(this).find('.length').val();
                var option = $(this).find('.option').val();
                if(type == 1){
                    if((name && length)){
                        var arr = {};
                        arr.number = index + 1;
                        arr.name = name;
                        arr.type = type;
                        arr.is_must = is_must;
                        arr.length = length;
                        arr.option = '';
                        object.push(arr);
                    }
                }else{
                    if(name && option ){
                        var arr = {};
                        arr.number = index + 1;
                        arr.name = name;
                        arr.type = type;
                        arr.is_must = is_must;
                        arr.length = '';
                        arr.option = option.split('&');
                        object.push(arr);
                    }
                }
            });
            if(object.length == 0){
                swal('请设计报名表单');
                return false;
            }
            var is_pay = $('.is_pay').val();
            var pay_price = $('.pay_price').val();
            var pay_type = $('.pay_type').val();

            if(is_pay == 1){
                if(!pay_price || parseFloat(pay_price) <= 0){
                    swal('请填写报名费用');
                    return false;
                }
            }
            var data = {};
            data.id = id;
            data.is_pay = is_pay;
            data.pay_price = pay_price;
            data.pay_type = pay_type;
            data.form = object;
            $.blockUI({
                message: '<i class="fa fa-spinner fa-spin"></i> 正在提交数据...'
            });
            $.ajax({
                type: 'POST',
                url: '/enroll/save',
                data: {data:data},
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success: function (m){
                    switch ( m.status ) {
                        case 0:
                            $('#_userTable').bootstrapTable('refresh');
                            swal({title:"发布成功！",
                                text:"3秒后自动返回首页或点下面按钮立即返回。",
                                type:"success"},function(){ window.location.reload();})
                            setTimeout(function(){
                                window.location.reload();
                            }, 3000);
                            break;
                        default:
                            swal('失败', m.message, 'warning');
                    }
                }
            });
        }
    };

    var plus = function(){
        $('.uio-product-add').off().on('click',function(){
            var str = ' <tr class="additional-sales-info">\n' +
                '                                                            <td>\n' +
                '                                                                <select name="form_type" class="form-control form_type">\n' +
                '                                                                    <option value="1">文本框</option>\n' +
                '                                                                    <option value="2">下拉框</option>\n' +
                '                                                                </select>\n' +
                '                                                            </td>\n' +
                '                                                            <td>\n' +
                '                                                                <input type="text" value="" name="number" class="form-control name">\n' +
                '                                                            </td>\n' +
                '                                                             <td  style="width: 10em;">\n' +
                '                                                                <input type="text" value="5" name="length" class="form-control length" 吗，>\n' +
                '                                                            </td>'+
                '    <td style="display: none;">\n' +
                '                                                                <input type="text" name="option" class="form-control option" placeholder="选项之间请以&分隔">\n' +
                '                                                            </td>'+
                '                                                            <td>\n' +
                '                                                                <select name="is_must" class="form-control is_must">\n' +
                '                                                                    <option value="0">不必填</option>\n' +
                '                                                                    <option value="1" selected>必填</option>\n' +
                '                                                                </select>\n' +
                '                                                            </td>\n' +
                '                                                            <td>\n' +
                '                                                                <button class="btn btn-xs btn-info uio-product-add" type="button">增</button>\n' +
                '                                                                <button class="btn btn-xs btn-danger uio-product-delete" type="button">删</button>\n' +
                '                                                            </td>\n' +
                '                                                        </tr>';
            $(this).parents('tbody').append(str);
            plus();
        });
        $('.uio-product-delete').off().on('click',function(){
            var length = $(this).parents('tbody').find('.additional-sales-info').length;
            if(length > 1){
                $(this).parents('tr').remove();
            }
        })
    }
    var ticketConfirm = function() {
        var tr = null;
        $('body').on('click', '.ticket-confirm', function () {
            tr = $(this).parents('tr');
            orderId = $(this).data('id');
            $.ajax({
                url:'/enroll/order/info',
                method:'post',
                data:{id:orderId},
                dataType:'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success:function (res) {
                    if(res.status == 1){
                        var ele = $('#confirm-ticket');
                        ele.find('.ticket-number').val(res.order.pay_ticket['number']);
                        ele.find('.ticket-link').val(res.order.pay_ticket['name']);
                        ele.find('.ticket-tel').val(res.order.pay_ticket['phone']);
                        ele.find('.ticket-money').val(res.order.pay_ticket['money']);
                        ele.find('.ticket-email').val(res.order.pay_ticket['email']);
                        ele.find('.ticket-remark').val(res.order.pay_ticket['remark']);
                        ele.find('.confirm-money').val(res.order.pay_ticket['ticket_money']);
                        ele.find('.money').val(res.order.total_price);
                    }
                }
            });
            $('#confirm-ticket').modal('show');
        })
        $('#ticketBtn').on('click', function () {
            var money = $('#confirm-ticket .confirm-money').val();
            var remark = $('#confirm-ticket .ticket-remark').val();
            if (!money) {
                layer.open({
                    content:'请填写实际开票金额',
                    skin: 'msg',
                    time: 2
                })
                return false;
            }
            $.ajax({
                url: '/enroll/order/ticket',
                method:'post',
                dataType: 'json',
                data: {data:{money: money,remark: remark,orderId: orderId}},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            }).done(function(res){
                if(res.status == 0) {
                    tr.find('.ticket-confirm-money').text(money);
                    var remark1 = tr.find('.remark').text();
                    tr.find('.remark').html(remark1 +'<br>'+remark);
                    tr.find('.status').html('<span style="color: #16d250;">已开票</span>');
                    $('#confirm-ticket').modal('hide');
                }
            })
        })
    }
    // 连续点击事件没有超过0.5秒，限制触发。
    var preventMultipleClicks = function (func) {
        var time = new Date();
        var t = time.getTime();
        if (t > (clickTime + 500)) {
            func();
            clickTime = t;
        }
    };
    return {
        init: function () {
            userTable();
            event();
            uploadImg();
            initWizard();
            validateCheckRadio();
            plus();
            editorFunc();
            orderTable();
            ticketConfirm();
        }
    }
}();