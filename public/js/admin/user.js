var user = function () {
    // 表格
    var clickTime = 0;
    var userTableObj = null;
    var userId = null;
    var userTable = function () {
        var tableEle = $('#_userTable');
        tableEle.bootstrapTable({
            method: 'get',
            url: '/user/data',
            toolbar: "#toolbar",
            cache: true,
            striped: true,
            selectItemName: "radioName",
            pagination: true,
            pageList: [10, 20, 50, 100],
            pageSize: 20,
            pageNumber: 1,
            search: true,
            sidePagination: 'server',
            queryParams: function (params) {
                $('.user-add').attr('disabled', true);
                $('.user-edit').attr('disabled', true);
                $('.user-edit-password').attr('disabled', true);
                return params
            },
            showColumns: true,
            clickToSelect: true
        });
        tableEle.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
            userTableObj = tableEle.bootstrapTable('getSelections')[0];
            $('.user-add').attr('disabled', !tableEle.length);
            $('.user-edit').attr('disabled', !tableEle.length);
            $('.user-edit-password').attr('disabled', !tableEle.length);
            userId = userTableObj._id;
        });
    };
    var event = function () {
        $('.user-add').off().on('click', function () {
            preventMultipleClicks(function () {
                $.subview({
                    content: "#_addUser",
                    startFrom: "right",
                    onShow: function () {

                    },
                    onClose: function () {
                        $.hideSubview();
                    },
                    onHide: function () {
                    }
                });
            });
        });
        $('.user-edit').off().on('click', function () {
            preventMultipleClicks(function () {
                $.subview({
                    content: "#_editUser",
                    startFrom: "right",
                    onShow: function () {
                        $('.name', '.form-editUser').val(userTableObj.name);
                    },
                    onClose: function () {
                        $.hideSubview();
                    },
                    onHide: function () {
                    }
                });
            });
        });
        $('.user-edit-password').off().on('click', function () {
            preventMultipleClicks(function () {
                $.subview({
                    content: "#_editPassword",
                    startFrom: "right",
                    onShow: function () {

                    },
                    onClose: function () {
                        $.hideSubview();
                    },
                    onHide: function () {
                    }
                });
            });
        });
    };
    // 添加用户
    var addUserForm = function () {
        var formObj = $('.form-addUser');
        var errorHandler1 = $('.errorHandler', formObj);
        var successHandler1 = $('.successHandler', formObj);
        formObj.validate({
            errorElement: "span",
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                element.parent().parent().find('label').append(error);
            },
            ignore: "",
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                name: {
                    required: '* 不能为空'
                },
                email: {
                    required: '* 不能为空',
                    email: '登录名必须是E-mail格式',
                }
            },
            submitHandler: function (form) {
                errorHandler1.hide();
                $.blockUI({
                    message: '<i class="fa fa-spinner fa-spin"></i> 正在提交数据...'
                });
                var obj = {};
                obj.name = $('.name', form).val();
                obj.email = $('.email', form).val();
                $.ajax({
                    type: 'POST',
                    url: '/user/add',
                    data: {data: obj},
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    success: function (m) {
                        switch (m.status) {
                            case 0:
                                $('#_userTable').bootstrapTable('refresh');
                                $(form)[0].reset();
                                $.hideSubview();
                                break;
                            case 2:
                                swal('失败', '登录名重复', 'warning');
                                break;
                            default:
                                swal('失败', m.message, 'warning');
                        }
                        $.unblockUI();
                    }
                });
            },
            invalidHandler: function (event, validator) {//display error alert on form submit
                errorHandler1.show();
            }
        });
    };
    // 修改指定用户的姓名
    var editUserForm = function () {
        var formObj = $('.form-editUser');
        var errorHandler1 = $('.errorHandler', formObj);
        var successHandler1 = $('.successHandler', formObj);
        formObj.validate({
            errorElement: "span",
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                element.parent().parent().find('label').append(error);
            },
            ignore: "",
            rules: {
                name: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: ' *不能为空'
                }
            },
            submitHandler: function (form) {
                errorHandler1.hide();
                $.blockUI({
                    message: '<i class="fa fa-spinner fa-spin"></i> 正在提交数据...'
                });
                var obj = {};
                obj.name = $('.name', form).val();
                console.log(userId);
                $.ajax({
                    type: 'POST',
                    url: '/user/edit',
                    data: {data: obj, id: userId},
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    success: function (m) {
                        switch (m.status) {
                            case 0:
                                $('#_userTable').bootstrapTable('refresh');
                                $(form)[0].reset();
                                $.hideSubview();
                                break;
                            default:
                                swal('失败', m.message, 'warning');
                        }
                        $.unblockUI();
                    }
                });
            },
            invalidHandler: function (event, validator) {//display error alert on form submit
                errorHandler1.show();
            }
        });
    };
    // 修改指定用户的密码
    var editPasswordForm = function () {
        var formObj = $('.form-editPassword');
        var errorHandler1 = $('.errorHandler', formObj);
        var successHandler1 = $('.successHandler', formObj);
        formObj.validate({
            errorElement: "span",
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                element.parent().parent().find('label').append(error);
            },
            ignore: "",
            rules: {
                name: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: ' *不能为空'
                }
            },
            submitHandler: function (form) {
                errorHandler1.hide();
                $.blockUI({
                    message: '<i class="fa fa-spinner fa-spin"></i> 正在提交数据...'
                });
                var obj = {};
                obj.password = $('.password', form).val();
                $.ajax({
                    type: 'POST',
                    url: '/user/edit',
                    data: {data: obj, id: userId},
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    success: function (m) {
                        switch (m.status) {
                            case 0:
                                $(form)[0].reset();
                                $('#_userTable').bootstrapTable('refresh');
                                $.hideSubview();
                                break;
                            default:
                                swal('失败', m.message, 'warning');
                        }
                        $.unblockUI();
                    }
                });
            },
            invalidHandler: function (event, validator) {//display error alert on form submit
                errorHandler1.show();
            }
        });
    };
    // 连续点击事件没有超过0.5秒，限制触发。
    var preventMultipleClicks = function (func) {
        var time = new Date();
        var t = time.getTime();
        if (t > (clickTime + 500)) {
            func();
            clickTime = t;
        }
    };
    return {
        init: function () {
            userTable();
            event();
            addUserForm();
            editUserForm();
            editPasswordForm();
        }
    }
}();