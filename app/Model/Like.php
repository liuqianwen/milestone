<?php

namespace App\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Like extends Eloquent
{
    protected $table      = 'like';
    protected $collection = 'like';

    public static function add( $data )
    {
        extract($data);
        $self                = new self;
        $self->openId        = $openId;
        $self->type          = $type;
        $self->source_id     = $source_id;
        $self->like_status   = $like_status;
        $self->collect_status = $collect_status;
        $self->is_active     = 1;
        $self->timestamp = time();
        $self->save();
        return $self->_id;
    }

    public static function edit($self,$data){
        if (!is_object($self))
            $self = self::find($self);
        foreach ($data as $k => $v) $self->$k = $v;
        $self->save();

        return null;
    }

}
