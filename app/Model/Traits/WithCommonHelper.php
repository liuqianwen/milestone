<?php

namespace App\Model\Traits;


/**
 * 模型公共功能方法
 *
 * Trait WithCommonHelper
 * @package App\Models\Traits
 */
trait WithCommonHelper
{
    /**
     * 追加过滤条件
     *
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', '1');
    }

    /**
     * 追加过滤条件
     *
     * @param $query
     * @return mixed
     */
    public function scopeStatus($query)
    {
        return $query->where('status', '1');
    }
}
