<?php
/**
 * 后台bootstrap-table扩展
 */

namespace App\Model\Traits;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;

trait BootstrapTable
{
    /**
     * 获取bootstrap-table需要获取的数据
     *
     * @param \Closure|NULL $queryFunc   where 条件
     * @param array         $searchField 需要搜索的字段
     * @param array         $field       需要获取的字段
     * @param array         $orderBy     排序字段
     * @param bool         $isOrderTiem 是否根据时间排序
     * @return array
     */
    public static function getRows( \Closure $queryFunc = NULL, $searchField = [], $field = ['*'], $orderBy = ['created_at', 'desc'],$isOrderTime = false )
    {
        $resources = new static();
        list($sort, $order) = $orderBy;
        $offset = Request::get('offset');
        $limit  = Request::get('limit');
        $order  = Request::get('order') ?? $order;
        $sort   = Request::get('sort') ?? $sort;
        $search = Request::get('search');
        is_null($queryFunc) || $resources = $resources->where($queryFunc);
        if ( $search != '' && !empty($searchField) ) {
            $resources->where(function( $query ) use ( $searchField, $search ) {
                $search = '%' . $search . '%';
                foreach ( $searchField as $key => $val ) {
                    ( 0 == $key ) ?
                            $query->where($val, 'LIKE', $search)
                            : $query->orwhere($val, 'LIKE', $search);
                }
            });
        }
        empty($sort) || $resources = $resources->orderBy($sort, $order);
        if ($isOrderTime) $resources = $resources->orderBy('created_at','desc');
        $total = $resources->count();
        if ( $total == $offset ) $offset = $offset - $limit;
        $rows = $resources->skip($offset)->limit($limit)->select($field)->get()->toArray();

        return compact('rows', 'total');
    }
}
