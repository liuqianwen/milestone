<?php
/**
 * 后台datatables扩展
 */
namespace App\Model\Traits;
use Illuminate\Support\Facades\Request;

trait DataTables
{
    /**
     * 获取datatables需要获取的数据
     * @param Closure $queryfunc where手动添加查询条件
     * @param Closure $searchfunc 切换搜索条件
     * @return array
     */
    public static function datatablesAjax(\Closure $queryfunc = null,\Closure $searchfunc = null)
    {
        $self = (new static);
        $start = Request::get('offset');
        $end = Request::get('limit');
//        $order = Request::get('order')[0];
//        $columns = Request::get('columns'); // 列值
//        $column = $order['column']; // 排序字段
//        $dir = $order['dir']; // 升序还是降序

//        $data['recordsTotal'] = $self::count();
        //查询结果

        $resouces = $self::where(function($query) use($queryfunc) {
            //附加条件
            if (!empty($queryfunc)) {
                $queryfunc($query);
            }
        })->where(function($query) use($searchfunc) {
            //datatables查询条件
            $value = Request::get('searching');
            if($value){//有条件搜索
                $columns = Request::get('columns'); // 列值
                foreach ($columns as $key => $v) {
                    if ($v['data'] && $v['searchable'] == 'true') {
                        if(!empty($searchfunc)){
                            $searchfunc($query,$v['data'],$value);
                        }else{
                            $query->orwhere($v['data'],'LIKE','%'.$value.'%');
                        }
                    }
                }
            }
        });
        $data['total'] = $resouces->count();
//        $data['draw'] = Request::get('draw');
//        $data['data'] = $resouces->orderBy($columns[$column]['data'],$dir)->skip($start)->limit($end)->get()->toArray();
        $data['rows'] = $resouces->orderBy('created_at','desc')->skip($start)->limit($end)->get()->toArray();
        return $data;
    }
}
