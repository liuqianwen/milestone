<?php

namespace App\Model;
use App\Http\Controllers\Weixin\CodeController;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Member extends Eloquent
{
    protected $connection = 'mongo';
    protected $table = 'member';
    protected $fillable = [];

    static  function add($data)
    {
        $number            = intval(self::max('number')) + 1;
        extract($data);
        $self                   = new self;
        $self->_id              = $openId;
        $self->name             = '';         // 真实姓名
        $self->nickname           = $nickname;       // 微信名
        $self->sex              = $sex;          // 性别 0 缺省 1 男 2 女
        $self->headimgurl       = $headimgurl;          // 头像
        $self->tel              = '';          // 电话号码
        $self->brithday         = '';
        $self->timestamp        = time();// 创建时间戳
        $self->subscribe        = 0;
        $self->is_active        = isset($data['is_active']) ? $data['is_active']: 1;
        $self->number = $number;
        $code = new CodeController();
        $card_no = $code->encodeID($number+1000,3);
        $card_vc = substr(md5($card_no),0,2);
        $card_vc = strtoupper($card_vc);
        $self->card = $card_vc.$card_no;;
        $self->save();
        return true;
    }

}