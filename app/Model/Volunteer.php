<?php

namespace App\Model;

use App\Log\InfoLog;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Auth;

class Volunteer extends Eloquent
{
    protected $connection = 'mongo';
    protected $table = 'volunteer';
    protected $fillable = [];


    /**
     * 获取数据
     * @param $limit
     * @param $offset
     * @param $search
     * @return array
     */
    public static function getData($limit,$offset,$search,$where){

        $query = self::orderBy('created_at','asc')->where('is_active',1);
        foreach($where as $k=>$v){
            $query->where($k,$v);
        }
        if($search != ''){
            $GLOBALS['newsearch'] = '%' . $search . '%';
            $query = $query->where(function ($query) {
                $newsearch = $GLOBALS['newsearch'];
                $query->where('_id', 'like', $newsearch)
                    ->orwhere('name', 'like', $newsearch);
            });
        }
        $total = $query->count();
        if ($total == $offset) {
            $offset = $offset - $limit;
        }
        $label = $query->skip($offset)->limit($limit)->get();
        $data = ['total'=>$total,'rows'=>$label];
        return $data;
    }

    /**
     * 添加标签
     * @param $arr
     * @return bool
     */
    public static function addData($arr){
        $findOne = self::where('name', $arr['name'])->count();
        if ($findOne > 0) {
            return false;
        } else {
            $newid = intval(self::max('_id')) + 1;
            $label = new self;
            $label->_id = $newid;
            $label->is_active = 1;
            $label->name = $arr['name'];
            $label->status = intval($arr['status']);
            $label->timestamp = time();
            $label->save();
//            InfoLog::addLog(Auth::user()->email . '添加了一条数据', ['user' => Auth::user()->email, 'table' => 'illnessLabel', 'data' => $arr]);
            return true;
        }
    }

    /**
     * 修改标签
     * @param $arr
     * @return bool
     */
    public static function editData($arr){
        $labelid = intval($arr['bh']);
        $label = self::find($labelid);
        if(is_null( $label)){
            return false;
        }else{
            $label->name = $arr['name'];
            $label->status = intval($arr['status']);
            $label->save();
//            InfoLog::addLog(Auth::user()->email . '修改了一条数据', ['user' => Auth::user()->email, 'table' => ' illnesslabel', 'data' => $arr]);
            return true;
        }
    }
}
