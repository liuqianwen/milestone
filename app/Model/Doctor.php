<?php

namespace App\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Doctor extends Eloquent
{
    protected $table      = 'doctor';
    protected $collection = 'doctor';

    public static function add( $data )
    {
        extract($data);
        $self                = new self;
        $self->name          = $name;
        $self->sex           = $sex;
        $self->hospital_id   = $hospital_id;
        $self->position           = isset($position) ? $position : '';
        $self->department = isset($department) ? $department : '';
        $self->tel           = isset($tel) ? $tel : '';
        $self->headimg    = isset($headimg) ? $headimg : '';
        $self->introduce     = isset($introduce) ? $introduce : '';
        $self->good     = isset($good) ? $good : '';
        $self->status        = 1;
        $self->is_active     = 1;
        $self->timestamp = time();
        $self->save();
        return $self->_id;
    }

    /**
     * 获取数据
     * @param $limit
     * @param $offset
     * @param $search
     * @return array
     */
    public static function getData($limit,$offset,$search,$where){
        $query = self::orderBy('created_at','asc')->where('is_active',1);
        foreach($where as $k=>$v){
            $query->where($k,$v);
        }
        if($search != ''){
            $GLOBALS['newsearch'] = '%' . $search . '%';
            $query = $query->where(function ($query) {
                $newsearch = $GLOBALS['newsearch'];
                $query->where('_id', 'like', $newsearch)
                    ->orwhere('name', 'like', $newsearch);
            });
        }
        $total = $query->count();
        if ($total == $offset) {
            $offset = $offset - $limit;
        }
        $label = $query->skip($offset)->limit($limit)->get();
        $data = ['total'=>$total,'rows'=>$label];
        return $data;
    }


    public static function edit($self,$data){
        if (!is_object($self))
            $self = self::find($self);
        foreach ($data as $k => $v) $self->$k = $v;
        $self->save();

        return null;
    }

}
