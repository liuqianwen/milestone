<?php

namespace App\Model;

use App\Log\InfoLog;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Auth;

class EnrollOrder extends Eloquent
{
    protected $connection = 'mongo';
    protected $table = 'enroll_order';
    protected $fillable = [];

    public static function add($data)
    {
        $self = new self;
        $self->order_number  = self::orderCode();
        $self->num       = $data['num'];
        $self->price     = $data['price'];
        $self->total_price = $data['total_price'];
        $self->wx_order  = $data['wx_order'];
        $self->source_id = $data['source_id'];
        $self->member    = $data['member'];
        $self->pay_ticket = $data['pay_ticket'];
        $self->pay_type   = $data['pay_type'];
        $self->pay_time  = '';
        $self->timestamp = time();
        $self->status    = $data['status'] ?? 0; //未付款 1 付款
        $self->is_active = 1;
        $self->save();
        return ['_id' => $self->_id,'order_number' => $self->order_number];
    }
    protected static function orderCode()
    {
        $str = date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
        return $str;
    }


    /**
     * 获取数据
     * @param $limit
     * @param $offset
     * @param $search
     * @return array
     */
    public static function getData($limit,$offset,$search,$where){

        $query = self::orderBy('status','asc')->orderBy('created_at','desc')->where('is_active',1);
        foreach($where as $k=>$v){
            $query->where($k,$v);
        }
        if($search != ''){
            $GLOBALS['newsearch'] = '%' . $search . '%';
            $query = $query->where(function ($query) {
                $newsearch = $GLOBALS['newsearch'];
                $query->where('_id', 'like', $newsearch)
                    ->orwhere('name', 'like', $newsearch);
            });
        }

        $query = $query->where(function ($query) {
            $query = $query->where(function ($query) {
//                $query->where('pay_type', 1)
                   $query->where('status', 1);
            })->orWhere('pay_type',2);
        });
        $total = $query->count();
        if ($total == $offset) {
            $offset = $offset - $limit;
        }
        $label = $query->skip($offset)->limit($limit)->get();
//        $label = $query->get();
        $data = ['total'=>$total,'rows'=>$label];
        return $data;
    }
}
