<?php

namespace App\Model;

use App\Log\InfoLog;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Hospital extends Eloquent
{
    protected $connection = 'mongo';
    protected $table = 'hospital';
    protected $collection = 'hospital';
    protected $fillable = [];

    public static function add($data)
    {
        extract($data);
        $self = new self;
        $self->type = 1;
        $self->name = $name; // 医院名称
        $self->address = $address; // 医院地址
        $self->introduce = $introduce; // 医院地址
        $self->pointlat = $pointlat; // 地址纬度
        $self->pointlng = $pointlng; // 地址经度
        $self->city = $city; // 地址经度
        $self->logo = isset($logo) ? $logo : ''; // 医院头像
        $self->badge = isset($badge) ? $badge : ''; // 医院徽章
        $self->status = 1;                                  // 医院状态
        $self->is_active = 1;
        $self->timestamp = time();
        $self->save();
        return $self->_id;
    }

    /**
     * 获取数据
     * @param $limit
     * @param $offset
     * @param $search
     * @return array
     */
    public static function getData($limit,$offset,$search,$where){

        $query = self::orderBy('created_at','asc')->where('is_active',1);
        foreach($where as $k=>$v){
            $query->where($k,$v);
        }
        if($search != ''){
            $GLOBALS['newsearch'] = '%' . $search . '%';
            $query = $query->where(function ($query) {
                $newsearch = $GLOBALS['newsearch'];
                $query->where('_id', 'like', $newsearch)
                    ->orwhere('name', 'like', $newsearch);
            });
        }
        $total = $query->count();
        if ($total == $offset) {
            $offset = $offset - $limit;
        }
        $label = $query->skip($offset)->limit($limit)->get();
        $data = ['total'=>$total,'rows'=>$label];
        return $data;
    }
    public static function edit($self,$data){
        if (!is_object($self))
            $self = self::find($self);
        foreach ($data as $k => $v){
            if($k == 'genre'){
                $self->$k = intval($v);
            }else{
                $self->$k = $v;
            }

        }
        $self->save();


        return null;
    }

}
