<?php

namespace App\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
class Video extends Eloquent
{
    protected $table = 'video';
    protected $collection = 'video';
    protected $fillable = [];

    public static function add($data)
    {
        $video = new self;
        return $video;
        $video->_id = intval(self::max('_id'))+1;
        $video->name = $data['title'];
        $video->type_id = $data['label'];
        $video->type = 2;
        $video->video_url = $data['url'];
        $video->description = $data['introduce'];
        $video->image = $data['image'];
        $video->timestamp = time();
        $video->status = 1;
        $video->read = 0;
        $video->is_active = 1;
        $video->save();
        return true;
    }

    /**
     * 获取数据
     * @param $limit
     * @param $offset
     * @param $search
     * @return array
     */
    public static function getData($limit,$offset,$search,$where){

        $query = self::orderBy('created_at','asc')->where('is_active',1);
        dd($query->get());
        foreach($where as $k=>$v){
            $query->where($k,$v);
        }
        if($search != ''){
            $GLOBALS['newsearch'] = '%' . $search . '%';
            $query = $query->where(function ($query) {
                $newsearch = $GLOBALS['newsearch'];
                $query->where('_id', 'like', $newsearch)
                    ->orwhere('name', 'like', $newsearch);
            });
        }
        $total = $query->count();
        if ($total == $offset) {
            $offset = $offset - $limit;
        }
        $label = $query->skip($offset)->limit($limit)->get();
        $data = ['total'=>$total,'rows'=>$label];
        return $data;
    }
}
