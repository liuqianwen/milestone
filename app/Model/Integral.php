<?php

namespace App\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Integral extends Eloquent
{
    protected $connection = 'mongo';
    protected $table      = 'integral';
    protected $collection = [];

    public static function add( $data )
    {
        extract($data);
        $self  = new self;
        $self->type          = $type;
        $self->openId        = $openId;
        $self->num          = $num;
        $self->description     = $description;
        $self->math     = $math;
        $self->day     = isset($day) ? $day : '';
        $self->is_active     = 1;
        $self->timestamp = time();
        $self->save();
        return true;
    }

}
