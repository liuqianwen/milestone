<?php

namespace App\Model;

use App\Log\InfoLog;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Auth;

class EnrollData extends Eloquent
{
    protected $connection = 'mongo';
    protected $table = 'enroll_data';
    protected $fillable = [];

    public static function add($data)
    {
        $self = new self;
        $self->member = $data['member'];
        $self->order_id = $data['order_id'];
        $self->order_number = $data['order_number'];
        $self->source_id = $data['source_id'];
        $self->pay_type  = $data['pay_type']; //1微信 2 对公
        $self->value = $data['value'];
        $self->timestamp = time();
        $self->status = $data['status'] ?? 0; // 1报名成功 0 未付款
        $self->is_active = 1;
        $self->save();
        return $self->_id;
    }
}
