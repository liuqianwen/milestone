<?php

namespace App\Model\FollowUp;

use App\Model\Model;
use Auth;

class Information extends Model
{
    protected $table      = 'information';
    protected $collection = 'information';

    public static function add( $data )
    {
        extract($data);
        $self                 = new self;
        $self->name = $name;
        $self->type         = $type;
        $self->option         = $option;
        $self->status         = 1;
        $self->timestamp      = time();
        $self->is_active      = 1;
        $self->save();

        return $self->_id;
    }
}