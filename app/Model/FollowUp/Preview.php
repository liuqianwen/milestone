<?php

namespace App\Model\FollowUp;

use App\Model\Model;

class Preview extends Model
{
    protected $table = 'preview';
    protected $collection = 'preview';

    public static function add($data)
    {
        extract($data);
        $self = new self;
        $self->title = $title;
        $self->type = intval($type);
        $self->description = $description;
        $self->information = $information;
        $self->question = $question;
        $self->timestamp = time();
        $self->is_active = 1;  // 1 正常 0 ....
        $self->status = 1;     // 1 正常 0 ....
        $self->save();

        return $self->_id;
    }
}