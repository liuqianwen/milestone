<?php

namespace App\Model\FollowUp;

use App\Model\Model;
use Auth;

    class RecordInfo extends Model
{
    protected $table      = 'record_info';
    protected $collection = 'record_info';

    public static function add( $data )
    {
        extract($data);
        $self = new self;
        $self->record_id = $record_id;
        $self->survey_id = $survey_id;
        $self->question_number = $question_number;
        $self->type = $type;
        $self->answer = $answer;
        if(isset($other)){
            $self->other = $other;
        }
        $self->timestamp = time();
        $self->is_active = 1;  // 1 正常 0 ....
        $self->save();

        return $self->_id;
    }

    public function InstitutionInfo()
    {
        return $this->hasOne('App\Model\Institution', '_id', 'institution_id');
    }
}