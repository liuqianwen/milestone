<?php

namespace App\Model\FollowUp;

use App\Model\Model;
use Auth;

class Survey extends Model
{
    protected $table      = 'survey';
    protected $collection = 'survey';

    public static function add( $data )
    {
        extract($data);
        $self = new self;
        $self->title = $title;
        $self->type = intval($type);
        $self->description = $description;
        $self->information = $information;
        $self->question = $question;
        $self->timestamp = time();
        $self->is_active = 1;  // 1 正常 0 ....
        $self->status = 1;     // 1 正常 0 ....
        $self->save();

        return $self->_id;
    }

    public function InstitutionInfo()
    {
        return $this->hasOne('App\Model\Institution', '_id', 'institution_id');
    }
}
