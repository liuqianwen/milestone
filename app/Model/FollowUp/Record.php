<?php

namespace App\Model\FollowUp;

use App\Model\Model;
use Auth;

    class Record extends Model
{
    protected $table      = 'record';
    protected $collection = 'record';

    public static function add( $data )
    {
        extract($data);
        $self = new self;
        $self->openId = $openId;
        $self->survey_id = $survey_id;
        $self->information = $information;
        $self->timestamp = time();
        $self->is_active = 1;  // 1 正常 0 ....
        $self->save();

        return $self->_id;
    }

    public function InstitutionInfo()
    {
        return $this->hasOne('App\Model\Institution', '_id', 'institution_id');
    }
}