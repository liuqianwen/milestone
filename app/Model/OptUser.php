<?php

namespace App\Model;

use App\Model\Traits\WithCommonHelper;
use Illuminate\Database\Eloquent\Model;
use App\Model\Traits\BootstrapTable;

class OptUser extends Model
{
    use WithCommonHelper;
    use Traits\DataTables;

    protected $table = 'optuser';
    protected $fillable
                     = [
                    'id', 'name', 'phone', 'openid', 'type', 'area', 'sex', 'image', 'store', 'IDcard', 'status', 'in_job', 'is_active'
            ];
    use BootstrapTable;

    /**
     * 获取督导
     *
     * @return array
     */
    static public function getCouncilorName()
    {
        $Obj  = static::where('is_active', 1)->where('status', 1)->whereIn('type', [1, 2])->get(['id', 'name']);
        $data = [];
        foreach ( $Obj as $v ) {
            $data[$v->id] = $v->name;
        }

        return $data;
    }

    /**
     * 获取用户
     *
     * @param null $id
     * @return array
     */
    static public function getName( $id = NULL )
    {
        $obj = static::where('is_active', 1);
        if ( !is_null($id) ) $obj = is_array($id) ? $obj->whereIn('id', $id) : $obj->where('id', $id);
        $obj = $obj->get(['id', 'name']);

        $data = [];
        foreach ( $obj as $v ) {
            $data[$v->id] = $v->name;
        }

        return $data;
    }

    /**
     * 多对多
     *
     * @return BelongsToMany
     */
    public function systems()
    {
        return $this->belongsToMany(
                'App\Model\DicSystem',
                'opt_user_system',
                'uid',
                'sid'
        );
    }

    /**
     * 一对多
     *
     * @return
     */
    public function series()
    {
        return $this->belongsToMany(
            'App\Model\OptSeries',
            'opt_user_series',
            'uid',
            'sid'
        );
    }

    public function syncSystem( ...$categorys )
    {
        $this->systems()->detach();

        return $this->giveSystemTo($categorys);
    }

    public function syncSeries( ...$categorys )
    {
        $this->series()->detach();

        return $this->giveSeriesTo($categorys);
    }

    public function giveSystemTo( ...$systems )
    {
        $systems = collect($systems)
                ->flatten()
                ->map(function( $system ) {
                    return app(DicSystem::class)->find(intval($system));
                })
                ->all();

        $this->systems()->saveMany($systems);

        return $this;
    }
    public function giveSeriesTo( ...$series )
    {
        $series = collect($series)
            ->flatten()
            ->map(function( $s ) {
                return app(OptSeries::class)->find(intval($s));
            })
            ->all();
        $this->series()->saveMany($series);

        return $this;
    }

    public function hasOneCmsuser()
    {
        return $this->hasOne('App\Model\CMSUser', 'uid', 'id');
    }


}
