<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Weixin\InterfaceController;
use App\Model\FollowUp\Information;
use App\Model\FollowUp\Preview;
use App\Model\FollowUp\Record;
use App\Model\FollowUp\RecordInfo;
use App\Model\FollowUp\Survey;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;

use App\User;

class PushController extends Controller
{
    public function preview( $id )
    {
        // 查询问卷内容
        $data     = Preview::find($id);
        if($data->information == '' || $data->information == []){
            $data->information = [];
        }else{
            $information = explode('^',$data->information);
            $in = Information::whereIn('_id',$information)->get();
            $infor = [];
            foreach($in as $v){
                $arr = [];
                $arr['id'] = $v->_id;
                $arr['name'] = $v->name;
                $arr['type'] = $v->type;
                $arr['option'] = $v->option;
                $infor[$v->_id] = $arr;
            }
            $aaa = [];
            foreach($information as $v){
                $aaa[] = $infor[$v];
            }
            $data->information = $aaa;

        }
        $js       = InterfaceController::js();
        $argument = [
            'data'         => $data,
            'surveyRecord' => $id,
            'js'           => $js,
            'is_wechat'    => true,
            'survey2_type' => 'blue'
        ];

        return view('admin.follow_up.preview')->with($argument);
    }

    public function previewTwo( $id )
    {

        // 查询问卷内容
        $data     = Survey::find($id);
        if($data->information == '' || $data->information == []){
            $data->information = [];
        }else{
            $information = explode('^',$data->information);
            $in = Information::whereIn('_id',$information)->get();
            $infor = [];
            foreach($in as $v){
                $arr = [];
                $arr['id'] = $v->_id;
                $arr['name'] = $v->name;
                $arr['type'] = $v->type;
                $arr['option'] = $v->option;
                $infor[$v->_id] = $arr;
            }
            $aaa = [];
            foreach($information as $v){
                $aaa[] = $infor[$v];
            }
            $data->information = $aaa;

        }
        $js       = InterfaceController::js();
        $argument = [
            'data'         => $data,
            'surveyRecord' => $id,
            'js'           => $js,
            'is_wechat'    => true,
            'survey2_type' => 'blue'
        ];

        return view('admin.survey.preview')->with($argument);
    }

    public function previewPage( $id )
    {
        // 查询问卷内容
        $data     = Survey::find($id);
        if($data->information == '' || $data->information == []){
            $data->information = [];
        }else{
            $information = explode('^',$data->information);
            $in = Information::whereIn('_id',$information)->get();
            $infor = [];
            foreach($in as $v){
                $arr = [];
                $arr['id'] = $v->_id;
                $arr['name'] = $v->name;
                $arr['type'] = $v->type;
                $arr['option'] = $v->option;
                $infor[$v->_id] = $arr;
            }
            $aaa = [];
            foreach($information as $v){
                $aaa[] = $infor[$v];
            }
            $data->information = $aaa;

        }
        $js       = InterfaceController::js();
        $argument = [
            'data'         => $data,
            'surveyRecord' => $id,
            'js'           => $js,
            'is_wechat'    => true,
            'survey2_type' => 'blue'
        ];

        return view('admin.survey.preview')->with($argument);
    }

    public function previewSave(){
        return response()->json(['status'=>0]);
    }

    public function classSave(){
        $input        = Input::get('data');
        $right = [];
        foreach($input['question'] as $v){
            $right[] = intval($v['right']);
        }
        $result = max($right);
        extract($input);
        $survey = Survey::find($record_id);
        return response()->json([ 'status' => 0, 'message' => '添加成功','title'=>$survey->title,'result'=>$result,'id'=>$record_id]);
    }

    public function classPreview( $id )
    {
        // 查询问卷内容
        $data     = Preview::find($id);
        if($data->information == '' || $data->information == []){
            $data->information = [];
        }else{
            $information = explode('^',$data->information);
            $in = Information::whereIn('_id',$information)->get();
            $infor = [];
            foreach($in as $v){
                $arr = [];
                $arr['id'] = $v->_id;
                $arr['name'] = $v->name;
                $arr['type'] = $v->type;
                $arr['option'] = $v->option;
                $infor[$v->_id] = $arr;
            }
            $aaa = [];
            foreach($information as $v){
                $aaa[] = $infor[$v];
            }
            $data->information = $aaa;

        }
        $js       = InterfaceController::js();
        $argument = [
            'data'         => $data,
            'surveyRecord' => $id,
            'js'           => $js,
            'is_wechat'    => true,
            'survey2_type' => 'blue'
        ];

        return view('admin.follow_up.classPreview')->with($argument);
    }
    public function twoclassPreview( $id )
    {
        // 查询问卷内容
        $data     = Survey::find($id);
        if($data->information == '' || $data->information == []){
            $data->information = [];
        }else{
            $information = explode('^',$data->information);
            $in = Information::whereIn('_id',$information)->get();
            $infor = [];
            foreach($in as $v){
                $arr = [];
                $arr['id'] = $v->_id;
                $arr['name'] = $v->name;
                $arr['type'] = $v->type;
                $arr['option'] = $v->option;
                $infor[$v->_id] = $arr;
            }
            $aaa = [];
            foreach($information as $v){
                $aaa[] = $infor[$v];
            }
            $data->information = $aaa;

        }
        $js       = InterfaceController::js();
        $argument = [
            'data'         => $data,
            'surveyRecord' => $id,
            'js'           => $js,
            'is_wechat'    => true,
            'survey2_type' => 'blue'
        ];

        return view('admin.follow_up.classPreview')->with($argument);
    }
    public function score(){
        $data = Input::all();
        return view('wechat.surveyResult')->with($data);
    }

}
