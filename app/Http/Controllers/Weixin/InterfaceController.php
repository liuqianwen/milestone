<?php

namespace App\Http\Controllers\Weixin;

use EasyWeChat\Factory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Input;

class InterfaceController extends Controller
{
    /**
     * 初始化
     */
    public static function init()
    {
        $options = Config::get('wechat');
        $app = Factory::officialAccount($options['official_account']['default']);
        return $app;
    }

    /**
     * 发送模版消息
     *
     * @param $userId
     * @param $data
     * @param $url
     * @param $type
     * @return mixed
     */
//    public static function notice( $userId, $data, $url, $template )
//    {
//        $notice       = self::init()->template_message;
//        $configNotice = Config::get('notice');
//        $templateId   = $configNotice[$template];
//
//        $noticeId = $notice->send([
//            'touser'      => $userId,
//            'template_id' => $templateId,
//            'url'         => $url,
//            'data'        => $data,
//        ]);
//
//        return $noticeId;
//    }

    /**
     * jssdk 数据确认
     *
     * @return \EasyWeChat\Js\Js
     */
    public static function js()
    {
        $js = self::init()->jssdk;
        $returnJs = $js->buildConfig([ 'chooseImage', 'previewImage', 'uploadImage', 'downloadImage', 'getLocation', 'chooseWXPay', 'scanQRCode','onMenuShareTimeline','onMenuShareAppMessage','updateAppMessageShareData','updateTimelineShareData','hideMenuItems' ],false, false, true);

        return $returnJs;
    }

    /**
     * 菜单
     */
    public function menu()
    {
        $dns = Config::get('dns');
        $buttons = [
            ["type" => "view",
                "name" => "消毒知识",
                "url"  => $dns . "/wechat/education"
            ],
//            [
//                "name"=>"个人中心",
//                "sub_button" => [
//                    ["type" => "view",
//                        "name" => "我",
//                        "url"  => $dns . "/wechat/person"
//                    ],
//                    ["type" => "view",
//                        "name" => "基金会简介",
//                        "url"  => $dns . "/wechat/website"
//                    ],
//                    ['type' => 'view',
//                        'name' => '活动报名',
//                        'url'  => $dns . '/wechat/signup'
//                    ]
//                ]
//            ]


        ];
        self::init()->menu->create($buttons);
        echo 'OK';
    }

    /**
     * 把资源写入服务器
     *
     * @param $mediaid
     * @return string
     */
//    public static function temporary( $mediaid,$directory = 'avatar' )
//    {
//        $app = self::init();
//        $content = $app->media->get($mediaid);
//        $imgSrc = self::uploadMedia($content,$directory);
//        return $imgSrc;
//    }

//    public static function uploadMedia( $content, $directory )
//    {
//        $timer = date('Ymd', time());
//        $extension = 'jpg';
//        $str = str_random(32);
//        $newName = $str . '.' . $extension;
////        $content=$base64;
//        Storage::disk('local')->put($directory.'/'.$timer.'/'.$newName,$content);
//        return $directory.'/'.$timer.'/'.$newName;
//    }


    /**
     * 二维码
     */
//    public function foreverQrCode( $str )
//    {
//        // 永久二维码
//        $qrcode = self::init()->qrcode;
//        $result = $qrcode->forever($str);
//        $url = $result['url'];
//        return $url;
//    }

//    public function temporaryQrCode( $int, $time )
//    {
//        // 临时二维码
//        $qrcode = self::init()->qrcode;
//        $result = $qrcode->temporary($int, $time);
//        $url = $result['url'];
//        return $url;
//    }

//    public function addHospitalCode()
//    {
//        $data = Input::all();
//        if ( isset( $data['hId'] ) ) {
//            $hId = $data['hId'];
//            $doctorStr = 'docto' . $hId;
//            $staffStr = 'staff' . $hId;
//            $doctorUrl = $this->foreverQrCode($doctorStr);
//            $staffUrl = $this->foreverQrCode($staffStr);
//
//            // 保存到数据库
//            return response()->json(['doctor_qrcode' => $doctorUrl, 'staff_qrcode' => $staffUrl]);
//        }
//
//        return response()->json([]);
//    }

//    public function setDoctorCode()
//    {
//        $data = Input::all();
//        if ( isset( $data['hId'] ) && isset( $data['dId'] ) ) {
//            $hId = $data['hId'];
//            $dId = $data['dId'];
//            $doctorUrl = $this->foreverQrCode($dId . $hId);
//
//            return $doctorUrl;
//        }
//
//        return '';
//    }
  public static function getToken()
    {
        $option = Config::get('wechat')['official_account']['default'];
        dd($option);
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,"https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_HEADER,0);
        $ouput = curl_exec($ch);
        if ($ouput == false) {
            $res =  curl_error($ch);
        } else {
            $res =  $ouput;
        }
        curl_close($ch);
        return $res;
    }
}
