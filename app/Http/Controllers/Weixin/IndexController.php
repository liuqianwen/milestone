<?php

namespace App\Http\Controllers\Weixin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use EasyWeChat\Factory;
use Illuminate\Support\Facades\Config;

class IndexController extends Controller
{

    public function index()
    {//        Log::info('request arrived.');
        $options = Config::get('wechat');
//        $dns     = Config::get('dns')
        $app = Factory::officialAccount($options['official_account']['default']);
        $app->server->push(function ($message) {
            switch ($message['MsgType']) {
                case 'event':
                    $event = new EventController($message);
                    return $event->index();
                    break;
                case 'text':
                    return '收到文字消息';
                    break;
            }

            // ...
        });
        $response = $app->server->serve();
        $response->send();
        return $response;
    }
}
