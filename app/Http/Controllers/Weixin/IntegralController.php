<?php

namespace App\Http\Controllers\Weixin;

use App\Http\Controllers\Controller;
use App\Model\Integral;
use Illuminate\Support\Facades\Input;

class IntegralController extends Controller
{
    public function get()
    {
        $openId = Input::get('openId');
        $integral = Integral::where('openId',$openId)->where('is_active',1)->get();
        $num = 0;
        foreach ($integral as $v) {
            if ($v->math == 0) {
                $num += $v->num;
            } else if ($v->math == 1) {
                $num -= $v->num;
            }
        }
        return response()->json(['num' => $num]);
    }
    public function add()
    {
        $data = Input::get('data');
        $data = json_decode($data,true);
        $res = Integral::add($data);
        if ($res) {
            return response()->json(['status' => 0]);
        }
    }

}
