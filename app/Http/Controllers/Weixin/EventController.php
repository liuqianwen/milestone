<?php

namespace App\Http\Controllers\Weixin;

use App\Model\Integral;
use App\Model\Member;
use App\User;
use EasyWeChat\Factory;
use Illuminate\Support\Facades\Config;
use EasyWeChat\Kernel\Messages\Text;
use EasyWeChat\Kernel\Messages\News;
use EasyWeChat\Kernel\Messages\NewsItem;

class EventController
{
    private $openId;
    private $event;
    private $EventKey;
    private $member;
    private $user;
//    private $tag;
    private $dns;
//    private $arg;
//    private $image;

    public function __construct( $message )
    {
        $this->openId   = $message['FromUserName'];
        $this->event    = $message['Event'];
        $this->EventKey = isset($message['EventKey']) ? $message['EventKey'] : '';
        $this->dns      = Config::get('dns');
    }

    public function index()
    {
        $result = $this->memberInit();
        if ( $result !== false ) {
            $this->member = $result;
            switch ( $this->event ) {
                case 'subscribe':
                    # 关注事件
                    return $this->subscribeMessage();
                case 'CLICK':
                    # 菜单点击事件
                    return $this->clickMessage();
                case 'unsubscribe':
                    #取消关注事件
                    return $this->unSubscribeMessage();
                case 'SCAN':
                    #已关注时扫码事件
                    return $this->SCANMessage();
                #其他事件
                default:
                    return '';
            }
        } else {
            return '网络连接失败，请重新操作！';
        }
    }

    /**
     *  获取微信用户的信息
     */
    private function getWeChatInfo()
    {
        $options     = Config::get('wechat');
        $app         = Factory::officialAccount($options['official_account']['default']);
        $userService = $app->user;
        $this->user  = $userService->get($this->openId);
        $this->tag   = $app->user_tag;
    }

    /**
     * 初始化member
     */
    private function memberInit()
    {
        $member = Member::where('_id',$this->openId)->first();
        if ( is_null($member) ) {
            $this->getWeChatInfo();

            $data = [
                'openId' => $this->openId,
                'headimgurl' => $this->user['headimgurl'],
                'nickname' => $this->user['nickname'],
                'sex' => intval($this->user['sex'])
            ];
            try {
                $bool  = Member::add($data);
            } catch ( \Exception $e ) {
                $bool = false;
            }
        } else {
            $bool = true;
        }
        if ( $bool ) {
            return $member;
        } else {
            return $bool;
        }
    }

    /**
     * 关注事件
     */
    private function subscribeMessage()
    {
        $bool = $this->refreshWeChatInfo();
        if ( $this->member->subscribe == 0 ) {
            $this->member->subscribe = 1;
            $this->member->subscribe_time = time();
            $this->member->save();
//            switch ( $this->member->group ) {
//                case 0:    //自主关注
//                    break;
//                case 1:
//                    $this->makeTag(100); // 打上普通党员标签
//                    break;
//                case 2:
//                    $this->makeTag(101); // 打上支部标签
//                    break;
//                case 3:
//                    $this->makeTag(102); // 打上党委标签
//                    break;
//                default:
//                    break;
//            }
        }
        if ( !empty($this->EventKey) ) {
            $this->parseEventKey(8);
        } else {
            $this->subscribeNews();
        }

//        $news = empty($this->image) ? ( empty($this->arg) ? '' : new Text($this->arg) ) : new News($this->image);
        $news = '欢迎关注！';
        return $news;
    }

    /**
     * 扫码事件
     */
    private function SCANMessage()
    {

        $bool = $this->refreshWeChatInfo();
        if ( !empty($this->EventKey) ) {
            $this->parseEventKey(0);
        } else {
            $this->arg = '欢迎' . $this->member->nickname . "！\n【8020】为您服务！";
        }
        $news = empty($this->image) ? ( empty($this->arg) ? '' : new Text($this->arg) ) : new News($this->image);


        return $news;
    }

    /**
     * 刷新微信用户的头像、性别、昵称
     */
    private function refreshWeChatInfo()
    {
        $this->getWeChatInfo();
        $this->member->headimgurl = $this->user['headimgurl'];
        $this->member->nickname   = $this->user['nickname'];
        $this->member->sex        = intval($this->user['sex']);
        $this->member->save();


        return true;
    }

    /**
     * 处理二维码
     *
     * @param $int
     */
    private function parseEventKey( $int )
    {
        $key   = substr($this->EventKey, $int);
        $belong = substr($key , 0 , 1);
        $value = intval(substr($key, 1));
        switch ( $belong ) {
            case 'w': //微信绑定党员
                    $user = User::where('active', 1)->where('openId', $this->openId)->first();
                    if ( is_null($user) ) {
                        $this->image = [
                            new NewsItem([
                                'title'       => '您好，您扫描了绑定微信端的专属码',
                                'description' => '请点击此处进行个人信息匹配',
                                'url'         => $this->dns . '/party/binding',
                                'image'       =>  $this->dns. '/images/carton-doctor.png'
                            ])
                        ];
                    } else {
                        $this->arg = '您好，您的微信已经有绑定关系了,请勿重复扫码。';
                    }

                break;
            case 'q':
                $user = User::where('active', 1)->where('openId', $this->openId)->first();
                if ( is_null($user) ) {
                    $this->arg = '您好，您的身份未绑定。请您联系您所在的支部书记确定您的身份。';
                } else {
                    $part = Participant::where('meeting_id',$value)->where('active',1)->where('uId',$user->id)->first();
                    $meeting = Meeting::find($value);
                    if(is_null($part)){
                            $member = [
                                'meeting_id'   => $value,
                                'uId'          => $user->id,
                                'status'       => 4,
                                'is_pass'      => 0,
                                'timestamp' => time(),
                                'updated_at'    => date('Y-m-d H:i:s',time()),
                                'created_at'   => date('Y-m-d H:i:s',time())
                            ];
                        DB::table('participant')->insert($member);
//                        if($user->political_status == 1 || $user->political_status == 2){
//                            IndividualPointsBase::automatic(85,$user->id,'系统自动执行',1,$value);
//                        }
                        $this->arg = '您好，此次会议您已签到成功，感谢您的参与。';
                    }else{
                        if(time() < strtotime($meeting->valid_time)){
                            $this->arg = '二维码有效时间未开始，请在有效期内扫码。';
                        }else{
                            if($part->status == 1){
                                $this->arg = '您好，此次会议您已经签到了，请不要重复签到。';
                            }elseif($part->status == 0){
                                $part->status = 1;
                                $part->leave_status = 0;
                                $part->timestamp = time();
                                $part->save();
                                if($user->political_status == 1 || $user->political_status == 2){
                                    IndividualPointsBase::automatic(85,$user->id,'系统自动执行',1,$value);
                                }
                                $this->arg = '您好，此次会议您已签到成功，感谢您的参与。';
                            }
                        }

                    }

                }
                break;
            case 'z':
                $user = User::where('active', 1)->where('openId', $this->openId)->first();
                if ( is_null($user) ) {
//                    $this->arg = '您好，请先绑定身份。\n如果您是党员，请在后台系统首页扫描二维码绑定身份。\n如果您是非党员人士且是医院职工，'.'<a href="/noparty/binding">点击这里</a>'.'绑定身份';
                    $this->arg = '您好，您的身份未绑定。请您联系您所在的支部书记确定您的身份。';
                } else {
                    $part = VolunteerUser::where('volunteer_id',$value)->where('active',1)->where('uId',$user->id)->first();
                    $volunteer = Volunteer::find($value);
                    if(!is_null($part)){
                        if(time() < strtotime($volunteer->valid_time)){
                            $this->arg = '二维码有效时间未开始，请在有效期内扫码。';
                        }else{
                            if($part->is_check_in == 1){
                                $this->arg = '您好，此次志愿活动您已经签到了，请不要重复签到。';
                            }elseif($part->is_check_in == 0){
                                $part->is_check_in = 1;
                                $part->leave_status = 0;
                                $part->timestamp = time();
                                $part->save();
                                if($user->political_status == 1 || $user->political_status == 2){
                                    if($part->is_enroll == 1){
                                        $data=['id'=>48,'uId'=>$user->id,'score'=>$volunteer->hours_service,'source_type'=>2,'source_id'=>$value];
                                        $aa =  IndividualPointsBase::manually($data);
                                    }
                                }
                                $this->arg = '您好，此次志愿活动您已签到成功，感谢您的参与。';
                            }
                        }

                    }else{
                        $member = [
                            'volunteer_id'   => $value,
                            'uId'          => $user->id,
                            'is_enroll'       => 0,
                            'is_check_in'      => 2,
                            'is_reply'      => 0,
                            'leave_status'      => 0,
                            'approval_status'      => 0,
                            'push_status'      => 0,
                            'timestamp' => time(),
                            'updated_at'    => date('Y-m-d H:i:s',time()),
                            'created_at'   => date('Y-m-d H:i:s',time())
                        ];
//                        if($user->political_status == 1 || $user->political_status == 2){
//                            $data=['id'=>48,'uId'=>$user->id,'score'=>$volunteer->hours_service,'source_type'=>2,'source_id'=>$value];
//                            $aa =  IndividualPointsBase::manually($data);
//                        }

                        DB::table('v_volunteer_user')->insert($member);
                        $this->arg = '您好，此次志愿活动您已签到成功，感谢您的参与。';
                    }

                }
                break;
            default:
                break;
        }
    }

    /**
     * 点击事件
     */
    private function clickMessage()
    {
        return '';
    }

    /**
     * 取关事件
     */
    private function unSubscribeMessage()
    {
        $this->member->subscribe = 0;
        $this->member->save();

        return '';
    }

    /**
     * 非扫码或扫无意义二维码
     */
    private function subscribeNews()
    {
        $this->arg =  '欢迎' . $this->member->nickname . "！\n【杏林先锋和医党建】为您服务！";
    }

    /**
     * 打标签
     *
     * @param $tag
     * @param $removeTag
     */
    private function makeTag( $tag, $removeTag = null )
    {

            try {
                empty($removeTag) || $this->tag->untagUsers([$this->openId], $removeTag);  //移除标签
                empty($tag) || $this->tag->tagUsers([$this->openId], $tag);                //打上标签
            } catch ( \Exception $e ) {

            }
        
    }
    
}
