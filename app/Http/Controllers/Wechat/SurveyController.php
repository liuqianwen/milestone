<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\Weixin\EventController;
use App\Http\Controllers\Weixin\InterfaceController;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\FollowUp\Information;
use App\Model\FollowUp\Record;
use App\Model\FollowUp\RecordInfo;
use App\Model\FollowUp\Survey;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use EasyWeChat\Factory;
use Illuminate\Support\Facades\Config;


class SurveyController extends Controller
{

    private function init()
    {
        $openId = session::get('wechat.oauth_user.default')->original['openid'];
        return $openId;
    }

    public function index()
    {
        $id = Input::get('id');
        // 查询问卷内容
        $data     = Survey::find($id);
        if($data->information == '' || $data->information == []){
            $data->information = [];
        }else{
            $information = explode('^',$data->information);
            $in = Information::whereIn('_id',$information)->get();
            $infor = [];
            foreach($in as $v){
                $arr = [];
                $arr['id'] = $v->_id;
                $arr['name'] = $v->name;
                $arr['type'] = $v->type;
                $arr['option'] = $v->option;
                $infor[$v->_id] = $arr;
            }
            $aaa = [];
            foreach($information as $v){
                $aaa[] = $infor[$v];
            }
            $data->information = $aaa;

        }
        $js       = InterfaceController::js();
        $argument = [
            'data'         => $data,
            'surveyRecord' => $id,
            'js'           => $js,
            'is_wechat'    => true,
            'survey2_type' => 'blue'
        ];
        return view('wechat.follow_up.survey')->with($argument);
    }

    public function result()
    {
        $data = Input::all();
        return view('wechat.surveyResult')->with($data);
    }

    public function list()
    {
        return view('wechat.follow_up.list');
    }

    public function save()
    {
        $input        = Input::get('data');
        $right = [];
        foreach($input['question'] as $v){
            $right[] = intval($v['right']);
        }
        $result = [1=>'低风险','中风险','高风险'];
        $result = max($right);
        $information  = [];
        extract($input);
        $survey = Survey::find($record_id);
        $openId = $this->init();
        // 保存内容
        $this->surveySave($information, $question, $openId,$survey->_id,max($right));
        return response()->json([ 'status' => 0, 'message' => '添加成功','title'=>$survey->title,'result'=>$result,'id'=>$record_id]);
    }

    private function surveySave( $information, $question, $openId, $survey_id ,$result)
    {
        $data                = [];
        $data['openId'] = $openId;
        $data['survey_id'] = $survey_id;
        $data['information'] = $information;
        $record = new Record();
        $record->type = 1;
        $record->survey_id = $survey_id;
        $record->openId = $openId;
        $record->information = $information;
        $record->result = $result;
        $record->timestamp = time();
        $record->is_active = 1;
        $record->save();
        foreach ( $question as $v ) {
            $recordInfo = new RecordInfo();
            $recordInfo->question_number = (int)$v['number'];
            $recordInfo->type = (int)$v['type'];
            $recordInfo->record_id = $record->_id;
            $recordInfo->survey_id = $survey_id;
            $recordInfo->answer = $v['answer'];
            $recordInfo->right = $v['right'];
            $recordInfo->save();
        }
        return $record;
    }
}
