<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\Weixin\EventController;
use App\Http\Controllers\Weixin\InterfaceController;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Doctor;
use App\Model\Enroll;
use App\Model\EnrollData;
use App\Model\EnrollOrder;
use App\Model\Hospital;
use App\Model\Volunteer;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use EasyWeChat\Factory;
use Illuminate\Support\Facades\Config;


class ActivityController extends Controller
{

    private function data()
    {
        return  [
            ["兰州市口腔医院","1262010043804207X2","285765410@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SN22IY3KNAACIvuQDPbsAASzVwILFe0AAIjW266.pdf"],
            ["陕西省府谷县人民医院","126108224367353152","2339616238@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SN2mIB7BaAACJDWI7swMAASzVwHCzd0AAIkl198.pdf"],
            ["汉中市口腔医院","12610702435956108G","63114572@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SN2KIBfXnAACJAXlxB6MAASzVgHGte0AAIkZ908.pdf"],
            ["宝鸡市口腔医院","12610300435363636T","66381582@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SN16IVBFcAACIv56FTgAAASzVgF3Z3MAAIjX977.pdf"],
            ["楚雄彝族自治州妇幼保健院","125323004318451019","344469119@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SN1eIdpX5AACJU9TPRQ0AASzVQD3FDoAAIlr898.pdf"],
            ["罗平人民医院","12530324431820131Q","2998936001@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SN1KIBitgAACIxQswypMAASzVwAMSi8AAIjd249.pdf"],
            ["官渡区官渡街道社区卫生服务中心","12530111431400436A","654932606@qq.com","1500.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SN0yIJdOsAACJVUQCMhoAASzVgAK5X8AAIlt099.pdf"],
            ["昆明市第一人民医院","1253010043136149XW","xulindentist@126.com","500.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNzyIQxPRAACJDF8ze8IAASzUwLjzUgAAIkk729.pdf"],
            ["贵阳市口腔医院","12520100429251822R","405724410@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNzWIUMsFAACJSSuoMQkAASzUwJm0lUAAIlh167.pdf"],
            ["昆明市妇幼保健院","12530100431361553X","1203932817@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SN0eIVtnqAACJTBnXPK0AASzUwOyfa8AAIlk719.pdf"],
            ["昆明市延安医院","12530100431361529D","yuhongbin6310@163.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SN0GIJMyNAACIw4s23EQAASzUwM4GsQAAIjb413.pdf"],
            ["成都市妇女儿童中心医院","125101005800279116","1634370475@qq.com","1500.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNy6IHix-AACJU6xHS-wAASzUwHejzoAAIlr128.pdf"],
            ["重庆牙科医院","12500103450419626C","15415561@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNyqINyHgAACKCDQS8m8AASzUwF7JxsAAIog632.pdf"],
            ["重庆三峡医药高等专科学校","12500000787471278Q","835061684@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNyOIUsHRAACJXOl7yM4AASzVACpytcAAIl0195.pdf"],
            ["重庆医科大学附属口腔医院","12500000450388591A","","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNx6IWzD2AACJUy8WCPAAASzUgC4QuEAAIlr717.pdf"],
            ["广西壮族自治区民族医院","12451400498870029K","2598451323@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNxmIGvZNAACJShA1k9MAASzUwB36KoAAIli405.pdf"],
            ["柳州市妇幼保健院","12450200498596533T","119699231@qq.com","1500.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNxOIPQB4AACJTQLz_6gAASzUgAUdvYAAIll177.pdf"],
            ["右江民族医学院","12450000499438238J","823417975@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNw6IKY1LAACJRtgqMiwAASzTwPEjbYAAIle357.pdf"],
            ["广西医科大学附属口腔医院","12450000498503800R","279136260@qq.com","500.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNwiIDeeiAACKo8hvBgUAASzTwNgP0IAAIq7301.pdf"],
            ["佛山市口腔医院（佛山市牙病防治指导中心）","124406004560738574","553922383@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNwOIarAEAACJGBeI4qYAASzUQK41pIAAIkw101.pdf"],
            ["深圳市宝安中医院（集团）","124403064557682359","267478303@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNviING2hAACJTQZcskcAASzUQIV76YAAIll395.pdf"],
            ["深圳市大鹏新区葵涌人民医院","124403004558352749","psl7012@163.com","500.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNu2IRrSyAACJ1z3cKR0AASzUQFbtZEAAInv947.pdf"],
            ["北京中医药大学深圳医院(龙岗)","1244030769116912X9","zwx33@126.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNv2IL-LkAACJWUdKhVwAASzTwLO1DgAAIlx246.pdf"],
            ["深圳市口腔医院","12440300MB2C50395L","864160076@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNvGIeLzXAACJTkUQH8gAASzUQGeKOQAAIlm965.pdf"],
            ["深圳市妇幼保健院","12440300455755661J","","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNuaIO3IeAACJR4K3QQUAASzTwFuJrkAAIlf898.pdf"],
            ["南方医科大学深圳医院","12440300319583850J","334673186@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNuCIah99AACJUT5l-AwAASzUADU1ywAAIlp264.pdf"],
            ["南方医科大学南方医院","12440000737590121J","869624720@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNtuIEuaHAACJTjvo5AYAASzUQBTyvAAAIlm014.pdf"],
            ["邵阳市中医医院","12430500445751659H","569228311@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNtSIMNeqAACJzln41a8AASzUAA4uH0AAInm706.pdf"],
            ["湘南学院","124300007533993439","835355896@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNtCIGIKQAACJwj1VESYAASzTgO5uFkAAIna714.pdf"],
            ["湘南学院附属医院","12430000447392477G","754837137@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNsmIOSuVAACIxRKGknAAASzTgNRoWMAAIjd393.pdf"],
            ["湖南省儿童医院","124300004448780682","1348732603@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNsWIBSkFAACIumwGBNAAASzTQNks8wAAIjS212.pdf"],
            ["恩施土家族苗族自治州中心医院","1242280042195737X8","2981006753@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNr6IE3EHAACJFKnZD7wAASzTAMeOXgAAIks080.pdf"],
            ["随州市中心医院","12421300422225226E","780492664@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNrqITtxzAACKFLfwdjwAASzTQLCB0YAAIos003.pdf"],
            ["枝江市口腔医院","12420583420235121R","976907024@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNqaIOtiEAACJRw2r9IgAASzTQGesGIAAIlf968.pdf"],
            ["孝感口腔医院","12420902732695784Y","673323832@qq.com","500.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNrOIUfCFAACIwD48HxkAASzTgIVvJoAAIjY688.pdf"],
            ["襄阳市口腔医院","12420600420424299K","1210936285@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNqyICtYiAACIww_7NWsAASzTgG9MqwAAIjb192.pdf"],
            ["鄂东医疗集团市中心医院（市普爱医院、湖北理工学院附属医院）","1242020042008321XH","908485880@qq.com","500.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNp-IeqSWAACI7ywlmJQAASzTAFjVIoAAIkH445.pdf"],
            ["武汉钢铁（集团）公司第二职工医院","12420000MB1362521D","22070564@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNpmIA1IcAACJHjkOaxsAASzTAESEsIAAIk2616.pdf"],
            ["河南省平舆县妇幼保健院","12412827418965280C","1915932667@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNpKITnXOAACJC5j1WiEAASzTgBFj4cAAIkj329.pdf"],
            ["河南省郸城县妇幼保健院","12411726418726570F","920696264@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNo2IDRwwAACKG7-2o78AASzSQP8xD4AAIoz416.pdf"],
            ["郸城县疾病预防控制中心","12411726418726044C","719532382@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNoeIByLoAACJ2hMOLFoAASzSwPi7IoAAIny493.pdf"],
            ["新乡市妇幼保健院","124107004170862840","943149771@qq.com","1500.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNoCIcJ06AACJTJTXYesAASzSgOzzlAAAIlk488.pdf"],
            ["郑州人民医院","124101004160474512","369729508@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNnyIFGsQAACIuOR9tdQAASzSwNLMvsAAIjQ585.pdf"],
            ["新乡医学院","12410000417087789Y","melynqq@163.com","1500.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNnWIRoseAACJSMQtJZIAASzSQKNpLwAAIlg557.pdf"],
            ["枣庄市妇幼保健院老院","12370400493310276M","yangdelai@126.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNmWIAnpuAACIwBAx2VcAASzSwH0tOIAAIjY446.pdf"],
            ["青岛市口腔医院","12370200427401583G","419809099@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNl-IXFZIAACJUG6Q5cUAASzSwGfhzUAAIlo489.pdf"],
            ["威海市立医院","123710004944300858","1356557867@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNnCIfZYcAACIta7-mPkAASzSwKS478AAIjN413.pdf"],
            ["潍坊市妇幼保健院","12370700493815928Y","sdlilywang@163.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNmqIe-vtAACKG9vsaCQAASzSQHtqbMAAIoz209.pdf"],
            ["舟山市口腔医院","12330900472092118K","zrrking1223@163.com","500.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNlqIUxbDAACJA6yhCYgAASzSQD7HEsAAIkb989.pdf"],
//            ["浙江大学医学院附属口腔医院","12330000470003281H","1679522487@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNlOIPfYKAACIz5M3adIAASzSgEOuKgAAIjn207.pdf"],
            ["浙江大学医学院附属儿童医院","123300004700032571","zhengchen7097@126.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNk-Ie41AAACIyC8kOd8AASzSQBRJEwAAIjg842.pdf"],
            ["张家港市妇幼保健所","12320582467223232U","52209693@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNkiIFe-KAACIw37Ztr0AASzSwArsWcAAIjb201.pdf"],
            ["苏州科技城医院","12320511466993278R","1348669524@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNkSINCDWAACJAxII_AYAASzRwPeGTkAAIkb754.pdf"],
            ["无锡市妇幼保健院","12320200466290644U","grace8229@126.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNj2ISDH7AACKD3V79qQAASzRgO--igAAIon597.pdf"],
            ["上海市黄浦区第二牙病防治所","123101034250298101","niyin_1986@163.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNjeIbqE-AACJCN3rwDsAASzRwMBkxcAAIkg010.pdf"],
            ["海林市口腔医院","12231083744432666H","hlskqyy000@163.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNh6IXyGAAACJ1mRJXvIAASzSAE37j4AAInu290.pdf"],
            ["上海市黄浦区牙病防治所","12310101425026409J","hpyf920@aliyun.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNjSIQZPcAACJR2yju_oAASzRgMcgB4AAIlf796.pdf"],
            ["上海市口腔病防治院","123100004250004951","540948276@qq.com","2000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNimIVd3xAACJQLB3Vt4AASzRgI2at0AAIlY143.pdf"],
            ["黑河市疾病预防控制中心","12231100769238245W","1046738974@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNiWIFk4lAACIyB4Uf1oAASzRgHpG78AAIjg935.pdf"],
            ["林口县牙病防治院","12231025414389025G","lkxybfzy@163.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNhqIMlF4AACIvIeGBNMAASzSAD-FWEAAIjU152.pdf"],
            ["牡丹江市疾病预防控制中心","12231000744442661J","mdjybfzs@163.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNhOIAnCAAACKH5vAueAAASzRwCzro8AAIo3110.pdf"],
            ["牡丹江市口腔医院","122310004143541599","554711885@163.com","2000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNg-ICHgnAACJQwzQ3HAAASzSABVenMAAIlb021.pdf"],
            ["佳木斯大学附属口腔医院","122308004142783878","kongyu19790204@163.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNgiIIfNVAACJDF2sHwkAASzRgBDLSUAAIkk456.pdf"],
            ["佳木斯市口腔病防治院","12230800083227277E","jmskqfz@126.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNgOIfhQxAACIxKW_fnQAASzRQPEoTEAAIjc875.pdf"],
            ["双鸭山市口腔医院","12230500414141663Y","syskq@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNf2ILqdfAACJSA52MnAAASzQwOzT_wAAIlg388.pdf"],
            ["齐齐哈尔医学院","12230200414041152C","xiaolaowu888@163.com","2000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNfiIS6oUAACKD0ZlNcYAASzQwNvwu8AAIon445.pdf"],
            ["哈尔滨市呼兰区口腔病防治所","12230121424322994X","45715424@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNfGIdSoZAACJVZ6BG40AASzRALzTmUAAIlt234.pdf"],
            ["哈尔滨市香坊区动力口腔病防治所","12230106744179453X","915552602@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNe2IPIk4AACIztvJ3jgAASzQwLMlNEAAIjm177.pdf"],
            ["哈尔滨市口腔医院","12230100424004304C","hrbyfb@126.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNeaIJ7xrAACJRVyRYWgAASzQwJzn4wAAIld015.pdf"],
            ["哈尔滨医科大学附属第一医院","12230000414006496Q","120129803@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNeKIXgegAACIyvwFjCIAASzRAImH9IAAIji009.pdf"],
            ["黑龙江护理高等专科学校","1223000041400364XG","61184197@qq.com","1500.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNduIBRs1AACJGlzYyccAASzQwHQsOkAAIky025.pdf"],
            ["延边大学附属医院","12220000412755581Y","45925609@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNdaIHdgXAACIxuxEbCEAASzQwGYGJEAAIje179.pdf"],
            ["吉林省生命科学技术研究院","12220000412753498H","Flora6636@163.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNdCIKc7dAACJDkHVEHcAASzQwFCcxYAAIkm152.pdf"],
            ["锦州市口腔医院","12210700463903214G","354783098@qq.com","2000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNcmIXyxGAACKnlj8-0AAASzRQCGewoAAIq2336.pdf"],
            ["大连市口腔医院","12210200422426079T","ei_ki@hotmail.com","1500.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNcOIfidkAACJR2pBBboAASzRQAokegAAIlf969.pdf"],
            ["锦州医科大学附属第二医院","12210000463001333B","15841663680@163.com","2000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNbyIZO_2AACKIKAax-4AASzQwAM5eoAAIo4878.pdf"],
            ["赤峰学院附属医院","12150400460298186P","bld97@sina.com","1500.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNbeIG500AACJ1iNnvoMAASzQgOGZmsAAInu954.pdf"],
            ["绛县口腔医院","121410314085413968","kqyychq@163.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNbGIe-oxAACKDRwdyR4AASzQANZxcAAAIol624.pdf"],
            ["晋城市妇幼保健院","121405004066011058","772589907@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNaqITU2aAACJAsiyuPgAASzQAMDF8UAAIka734.pdf"],
            ["山西省儿童医院（山西省妇幼保健院）","121400004057045246","316045578@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNZ2Id8BvAACJXOzkK5IAASzQgH3BywAAIl0116.pdf"],
            ["山西白求恩医院（山西医学科学院）","12140000578477964H","chenglin6002@sina.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNaSIJ1_UAACI1sBdAPwAASzQgJiJV8AAIju934.pdf"],
            ["保定市第二医院","121306004018883035","405412374@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNZeIQZEmAACIvhUSnycAASzQAHo8AsAAIjW030.pdf"],
            ["唐山市妇幼保健院","1213020040224848XN","759959160@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNZKIEbdkAACIvw83TwEAASzQAGsYrMAAIjX347.pdf"],
            ["唐山市人民医院","121302004022484634","rmyylkb@163.com","2000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNYyINdwdAACIxXtKDGYAASzQgETo3wAAIjd340.pdf"],
            ["赵县妇幼保健院","12130133402039231E","148072923@qq.com","1500.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNYeIKn5kAACJUCTQciYAASzQAEdRjMAAIlo144.pdf"],
            ["承德医学院附属医院","12130000401942840M","420797955@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNYCIJLEiAACIvkZp3s0AASzQADJg0YAAIjW277.pdf"],
            ["天津市口腔医院","12120000401354440H","chengshuling520@163.com","500.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNXyIbYlhAACJRuUz8H8AASzQQCVPQkAAIle257.pdf"],
            ["天津医学高等专科学校","1212000040120408XF","296736434@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNXWIfhoWAACIz6y75L4AASzQQA2MX0AAIjn071.pdf"],
            ["北京市顺义区妇幼保健院","12110110400940309Q","rzm8866@126.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNXGIdZ7wAACJCLHShpoAASzPwPB7sMAAIkg859.pdf"],
            ["北京市石景山区妇幼保健院","12110107400867180Y","27251686@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNWOIeW8mAACJC-YibasAASzPwMS-m8AAIkj654.pdf"],
            ["中国地质大学武汉","12100000441626770L","2921677299@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNVaICTk-AACI_BoJ6YAAASzPgKEGkwAAIkU721.pdf"],
            ["北京市海淀区妇幼保健院","12110108400880190B","1263349365@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNWqIV8h0AACJQeOUS_wAASzPwNwJyQAAIlZ731.pdf"],
            ["北京市和平里医院","12110101400782670C","wangyan770507@sina.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNVyIctjwAACIuV2lsckAASzPwK0fHAAAIjR812.pdf"],
            ["西安交通大学口腔医院","121000004372039199","1005257599@qq.com","3000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNU-IFTnFAACJ0SoQrlwAASzPwH8g4EAAInp787.pdf"],
            ["吉林大学口腔医院","12100000412755127M","61491622@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNUmIbFBlAACJSMWaJo4AASzPQHa8PsAAIlg079.pdf"],
            ["中国航天科工集团七三一医院","121000004000132736","5975262@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNUKITChhAACJRyjyTbQAASzPwE7OR4AAIlf584.pdf"],
//            ["北京市海淀医院","12110108400880246E","","4500.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNTyIUBEPAACJAnVrvW0AASzPwDhjckAAIka371.pdf"],
//            ["北京市海淀医院","12110108400880246E","","500.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNTeIUtWzAACJOfnY6OUAASzPwCjypgAAIlR896.pdf"],
//            ["浙江大学医学院附属口腔医院","12330000470003281H","zhuhh403@zju.edu.cn","4000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNTGIdebtAACIx7TCu6QAASzPQB1wykAAIjf102.pdf"],
            ["温州医科大学附属口腔医院","12330300670291986B","21236665@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNSyITkEHAACKqfe07KYAASzPwARA9cAAIrB178.pdf"],
            ["武汉第一口腔医院有限责任公司","91420100347255192N","Liu.xf@wuhankq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNSaIfRnlAACIz8B0hEwAASzOwOwqfMAAIjn228.pdf"],
            ["河南蓝洁口腔医院有限公司","91411081MA40J2T91Y","29148688@qq.com","1500.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNSCIOwH3AACJYkdpPdkAASzOgOdQ1IAAIl6574.pdf"],
            ["洛阳友好口腔门诊部有限公司","91410307MA448AFUXE","250824508@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNRmIVFy8AACJXS8mEqsAASzPAMtr7UAAIl1056.pdf"],
            ["东营同泰口腔医院有限公司","91370500MA3C4Y788K","271615997@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNRWIS5YGAACI224Qu9YAASzOgL_vy4AAIjz277.pdf"],
            ["淄博美东医疗服务有限公司张店美东口腔诊所","91370303MA3MLHEX2L","179103276@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNQ6IOXtyAACJLZx6eo0AASzPAKGMO8AAIlF272.pdf"],
            ["青岛瑞旗医疗管理有限公司","91370203MA3EXTJ21U","lilei@arrail-dental.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNPqIVec1AACI2Y2C7IsAASzOwEmiQEAAIjx440.pdf"],
            ["山东启贝医疗投资有限公司（启贝口腔）","91370303MA3DG4U89W","20505335@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNQeIPf-uAACJZ2pcjKgAASzOgI0CP0AAIl_351.pdf"],
            ["青岛咪嘤健康科技有限公司","91370211MA3PPE6R9W","annkim@yeah.net","500.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNQGIDxaGAACI2UdoZ40AASzOwGQ-OEAAIjx280.pdf"],
            ["济南济东口腔医院有限公司","91370102MA3D5NBC2L","253603341@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNPWIEJLKAACJHTaL1yIAASzOgD9OCEAAIk1683.pdf"],
            ["赣州市章贡区逸佳口腔医院有限公司","91360702MA37X2K797","Gzyjkq@126.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNO-IerhPAACI29I9lUsAASzOwBvwnQAAIjz709.pdf"],
            ["青蛙王子（福建）婴童护理用品有限公司","91350603MA2YM6P4X2","cwj@fjajl.cn","3000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNOqIYjsTAACI66YWQcgAASzPAAH7uEAAIkD410.pdf"],
            ["厦门罗雅光科技有限公司","913502003032163357","lrll@roopto.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNOOIc4ZEAACJDX1d2csAASzOQOWt2YAAIkl931.pdf"],
            ["福建晶特尔美可普医疗管理有限公司","91350181MA321Y4E2J","26211976@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNN2IOfkcAACJaAORhHoAASzNwNgIjkAAImA630.pdf"],
            ["东至亚安口腔门诊部","91341721MA2RFG1C5F","775199140@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNNaILdYmAACJWvcRINsAASzOQK1CVkAAIly691.pdf"],
            ["台州市路桥贝雅口腔门诊有限公司","91331004MA2DUJKT2B","120233609@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNNCIEJqTAACI5dCQoRIAASzOQIxfEoAAIj9035.pdf"],
            ["浙江普特医疗器械有限公司","91330523665167251Y","17020138@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNMmINpu9AACIxzqMuFQAASzOAGyoaEAAIjf414.pdf"],
            ["慈溪华阳口腔医院有限公司","91330282MA2AHFX2X1","2524161349@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNMKIdBg1AACI0fmvxW8AASzOAFUZP4AAIjp590.pdf"],
            ["宁波市鄞州儿童口腔医院有限公司","91330212MA2811U82M","786577851@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNL6IKFAYAACJX3e7QqIAASzNwEpJ9MAAIl3971.pdf"],
            ["浙江中医药大学附属口腔医院有限公司","91330185654571XX","609200350@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNLeIOzVpAACJWJ8vNrMAASzNwDXC7cAAIlw165.pdf"],
            ["杭州艾维医疗投资管理有限公司古墩路口腔门诊部","91330106MA28TTLGX5","fangjue@ivydent.cn","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNLOISnN5AACJelxl-LwAASzOABhrrMAAImS545.pdf"],
            ["杭州雅正口腔门诊部有限公司","913301037494825585","244799397@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNKGIUccGAACJVWrUlVoAASzNgNDMH4AAIlt892.pdf"],
            ["杭州青立口腔门诊部有限公司","91330106MA280CQ73K","1114377633@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNKyIQFLrAACI2oUe03EAASzNgP6hqgAAIjy592.pdf"],
            ["杭州口腔医院集团城西口腔医院有限公司","91330106665215997J","343737625@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNKeIe49yAACJW3lGtJAAASzNQO-le8AAIlz587.pdf"],
            ["杭州茗芽口腔门诊部有限公司","91330102MA28UFNE3C","1846117@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNJyIZsofAACKuEWgtiUAASzNQMNtJgAAIrQ967.pdf"],
            ["靖江瑞泰口腔门诊部有限公司","91321282MA1WRW475A","651761815@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNJaIDHKXAACJY3jBn50AASzNALi6-wAAIl7968.pdf"],
            ["扬州蜀冈—瘦西湖风景名胜区佳宁口腔门诊部有限公司","91321011MA1WLK365N","1576856903@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNJGIDNSuAACI9rGWCTsAASzNgJwhVwAAIkO468.pdf"],
            ["连云港中大口腔门诊部","91320707MA1WGR3753","411362336@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNIuIc0SGAACJFjAh0coAASzNQISXlAAAIku553.pdf"],
            ["南通玉蕙口腔医院有限公司","91320600MA1MF5C6XT","837661224@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNISIfmdAAACJYNQKFC8AASzNQGyha0AAIl4660.pdf"],
            ["苏州德熙亿科技有限公司","91320594MA1UU6M81W","656143359@qq.com","500.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNH-IXjQRAACKq_DXb7oAASzNgFauWgAAIrD430.pdf"],
            ["苏州信和隆医疗器械有限公司","91320594MA1MYK869F","903923213@qq.com","2000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNHmINqaFAACJXdNTG80AASzNQEEudUAAIl1852.pdf"],
            ["而至齿科（苏州）有限公司","91320594738299299J","383454131@qq.com","500.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNHSIba2EAACKrx_Um7cAASzNQC0HMwAAIrH218.pdf"],
            ["常熟玉蕙口腔医院有限公司","913205813309912224","452180008@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNGmILlPgAACJThoxkvkAASzNQAVVNMAAIlm374.pdf"],
            ["张家港玉蕙口腔医院有限公司","91320582MA1M94RB0X","yujuanjuan_yuhui@163.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNG2ILmPKAACJZ9VixIYAASzNQBUAsAAAIl_570.pdf"],
            ["苏州口腔医院有限公司","913205083390071055","wangjie@szkqyy.com","2000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNGKIYKs1AACIwpGcFqYAASzMwOSwq4AAIja189.pdf"],
            ["江阴美嘉欣口腔门诊部有限公司","91320281085089023E","615389806@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNFyIE7GbAACJWg8qQ3oAASzMwM1Mc4AAIly565.pdf"],
            ["南京江北口腔医院有限公司","91320191MA1MCUGU3E","812096787@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNFWIaPTiAACJYS3PNgoAASzMgMEnO4AAIl5202.pdf"],
            ["南京博正口腔诊所有限责任公司","91320114MA1W99271C","1439300562@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNE-IdPToAACJI2XTZg0AASzMgKOayEAAIk7227.pdf"],
            ["南京立登尔医疗科技股份有限公司云南北路口腔诊所","91320106580493373C","923325022@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNEiIOmV_AACJJ5pVH_UAASzMQIssQUAAIk_557.pdf"],
            ["南京牙仙子口腔医疗服务有限公司","91320105MA1W8U96X3","81871365@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNEGIN9IhAACI2Boq57AAASzMwGp2rAAAIjw265.pdf"],
            ["煜宜（上海）健康管理有限公司","91310115MA1K3LNW9G","185079587@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SND2Iee7KAACI5yKlWzsAASzMQF8nTkAAIj_546.pdf"],
            ["上海珀珥口腔门诊部有限公司","91310105MA1FW6Y77L","icechilli@yahoo.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNDaIN6SRAACJZY-9AQwAASzMgETTtMAAIl9781.pdf"],
            ["大连金普新区韩颖口腔诊所","91210213MA0UU3815P","5370804@qq.com","500.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNCuIRl4TAACJG6-II8gAASzMQBrjpYAAIkz328.pdf"],
            ["赤峰雅美齿科技术有限公司","91150402692855260L","412469727@qq.com","500.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNBeIK0JDAACIx6UEg7gAASzLwMg_4IAAIjf815.pdf"],
            ["义获嘉伟瓦登特（上海）商贸有限公司","9131000059040723XC","13421319363@163.com","1500.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNDKIfgoDAACJKTYU3pQAASzMQDGb4QAAIlB412.pdf"],
            ["大连众益口腔医院有限公司","91210213311500846M","81986766@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNCSIWX7pAACI0SpWlgoAASzLwPkN08AAIjp169.pdf"],
            ["沈阳和平圣泰口腔门诊部有限公司","91210102MA0U5M9E39","liudan.33@163.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNB6Ieyx1AACJZKJYlPMAASzMAOOQL4AAIl8298.pdf"],
            ["长治市亿大口腔医院有限公司","91140400MA0HEH1A3Y","1274199932@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNBOIcOpxAACJXShxdEUAASzLgL3S4wAAIl1690.pdf"],
            ["太原尔雅口腔门诊部","91140106MA0GUCBP27","289789142@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SNAyICpxFAACJX6AsaZcAASzLgKMzMAAAIl3923.pdf"],
            ["太原玖诺口腔门诊部有限公司","91140105MA0KB30F69","2280711005@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SNAeISvkxAACJYDzIQWMAASzLwIudI8AAIl4861.pdf"],
            ["河北冈大生物科技有限公司","91130702398998703M","li17732757834@163.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SNAGIDJUCAACKqOUyMQQAASzMAHXRlIAAIrA243.pdf"],
            ["秦皇岛纳极口腔门诊有限公司","91130302MA09TA0Q31","530102145@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SM_yIV0mTAACI3Ncup-8AASzLgGMAA0AAIj0497.pdf"],
            ["北京禀赋智享健康科技有限公司","91110304MA01GJ971Q","21792192@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SM-iIK0WuAACJ4-YlD4AAASzLAP4VCcAAIn7709.pdf"],
            ["北京首德口腔门诊部有限公司","91110108MA01EKP16Q","87258674@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SM92ICbYGAACJIAYy4XcAASzLAMXwEkAAIk4157.pdf"],
            ["石家庄和协口腔医院有限公司","91130100308418577L","1124398634@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SM_aIQxVyAACJVYVdXtsAASzLwD8IFMAAIlt247.pdf"],
            ["瑞明（天津）口腔门诊有限公司","91120116MA05UC3QX9","2545639140@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SM--IJE-MAACI42UlYb4AASzLwBvDUsAAIj7978.pdf"],
            ["北京久闻齿科口腔门诊部有限责任公司","91110114MA002XGN8B","18201338939@163.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SM-KIOFtQAACKuUjmey8AASzLANw9eoAAIrR533.pdf"],
            ["北京创享口腔门诊部有限责任公司","91110108MA009T2M8M","1501468621@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SM9eIQIe8AACKLvhgjL0AASzLQJ22EsAAIpG531.pdf"],
            ["北京世纪盖德口腔门诊部有限公司","911101087940934986","913965877@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SM9KIA38uAACJ36ZGH_cAASzKwJGrdAAAIn3128.pdf"],
            ["北京冈大医疗科技有限公司","911101086996483605","277742406@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SM8uIJnZOAACIwLkmgiQAASzLAHqxbMAAIjY198.pdf"],
            ["北京丝桐雨露商贸有限公司","91110108051413470D","2144183525@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SM8eIBhfCAACJC-bRLpQAASzLQGEv7YAAIkj244.pdf"],
            ["北京嘉信泽洋口腔诊所有限责任公司","91110107774740715Q","caresum309@126.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SM8CITGuNAACJVkEepCsAASzKwFMV8oAAIlu494.pdf"],
            ["伢典（北京）医疗技术有限公司","91110106MA00AQYU4T","18510203920@163.com","2000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SM7mIbxdEAACKLp7mag8AASzLQDX_0cAAIpG698.pdf"],
            ["北京维尔丰台口腔医院有限公司","91110106MA0049A31C","qwert815887@163.com","500.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SM7SIAhSfAACI1HXICrsAASzLQCLSfoAAIjs786.pdf"],
            ["北京诺美口腔门诊部","911101060716807017","150040261@qq.com","500.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SM66IH9q2AACKEVaxWHgAASzLAAcMjwAAIop350.pdf"],
            ["北京精德华大口腔诊所","91110105746123584H","liqun@jddental.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SM56ID-wFAACIwjVbuYEAASzKgLkkIAAAIja294.pdf"],
            ["北京正方形口腔门诊部有限公司","91110105MA0192P876","804928044@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SM6mIda0dAACKIVE9TJ4AASzKQPQCRwAAIo5054.pdf"],
            ["北京世恒尔力科贸有限公司","91110105789962986M","654482273@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SM6OIf_ZkAACJUQFt68kAASzKQNE4Q8AAIlp453.pdf"],
            ["北京诚品口腔门诊部有限公司","9111010534428748XD","424295243@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SM5mINFjcAACI0VR2j3UAASzKgKSOdYAAIjp378.pdf"],
            ["北京盖德口腔门诊部有限公司","91110105062788761F","bobo@gaidechina.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SM5OIKwOmAACJELyvM2kAASzKgIcpcIAAIko474.pdf"],
            ["北京瑞城口腔医院有限公司","911101023443962725","15810231665@163.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SM46IdJ5nAACKnTjRb8oAASzKgHgtqoAAIq1054.pdf"],
            ["广州市中西医结合医院工会委员会","81440100669979310X","379027273@qq.com","2000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SM4iIA-xMAACJVWuVLEsAASzKAGnboMAAIlt943.pdf"],
            ["西安轲琳医疗科技有限公司","806010901421009977","909756320@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SM4OIUmrwAACKHenKCxMAASzKAFsKHcAAIo1659.pdf"],
            ["克拉玛依市第二人民医院","650204H41445077","sxm9066@126.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SM3iIIYgIAACJ3C10iwEAASzKQCWMOgAAIn0107.pdf"],
            ["兰州大学口腔医院","620102438044673","529983640@qq.com","2500.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SM22Id2HdAACKGlws1TcAASzJwO5mIcAAIoy495.pdf"],
            ["重庆骑士医院","52500000MJP5663329","1471194754@qq.com","500.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SM2eIT0qGAACJUFtCrUIAASzJQOMYHEAAIlo204.pdf"],
            ["溆浦佳美口腔医院","52431224MJK336466E","674836568@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SM32IPzEJAACJWVlBoYEAASzKQDrXR4AAIlx539.pdf"],
            ["日照口腔医院","52371100MJE7098580","770168537@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SM1uICwFHAACJBYZgdUYAASzJwKfwSUAAIkd791.pdf"],
            ["烟台莱山口腔医院","52370613MJE332915T","4299909@qq.com","1500.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SM1SIJxTTAACJU1L0ib4AASzJgJroPEAAIlr325.pdf"],
            ["洛阳市口腔医院","52410302MJF9508035","451653264@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SM3KIXUAAAACJUc2GUCIAASzKAA-3fAAAIlp756.pdf"],
            ["滨城中雅致雅口腔门诊部","52371602MJF002643C","1119491096@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SM2CIcotjAACJGceumYwAASzJgMBhksAAIkx526.pdf"],
            ["青岛新都口腔医院","52370200MJD8410214","liweizhuan@163.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SM1CIOzm4AACJ4j39f5gAASzJgI1UR0AAIn6067.pdf"],
            ["无锡口腔医院","52320200509221244K","472227144@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SM0mIOleuAACI_94UjBoAASzJQHu5KQAAIkX122.pdf"],
            ["永济口腔医院","52140800MJ1917960R","yjskqyy@163.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SM0WIcdBSAACJ1eSczTwAASzJQHC35gAAInt520.pdf"],
            ["高阳陈氏口腔医院","52130628MJ7731920","cmkouqiang@163.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SMz6IIJ8WAACJDKLlmJUAASzJQFmS_MAAIkk978.pdf"],
            ["保定陈氏口腔门诊部","5213062869466111","yjfkouqiang@163.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SMzqIEnGDAACJ0mAGPaoAASzJgEVcnUAAInq707.pdf"],
            ["秦皇岛京美口腔门诊部","52130300359046320Q","1341610650@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SMzOIXhLfAACJUzH_740AASzJwCbY_YAAIlr743.pdf"],
            ["唐山博创口腔医院","521302005713005848","406625205@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SMy6IP5bKAACJzt00Zn0AASzJgCHT68AAInm851.pdf"],
            ["北京大兴兴业口腔医院","5211011579000105G","meilun_yy@126.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SMyiIZf5EAACIwwSDViAAASzJQBMezsAAIjb717.pdf"],
            ["北京口腔医学会","51110000500313257N","wyd8826.student@sina.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SMyGIQztWAACIsufBxqkAASzJAPY2CYAAIjK786.pdf"],
//            ["湖南中南大学湘雅口腔医院","430105062224819","yifangcsu@163.com","2000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SMxuIIP3EAACJUZDVqEkAASzIwN7CToAAIlp652.pdf"],
//            ["湖南大学湘雅口腔医院","430105062224819","1002026735@qq.com","500.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SMxSIWcpXAACJz7mf7X8AASzIwMpscQAAInn785.pdf"],
            ["来凤县人民医院","422827422135351","1053422849@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SMxCICK77AACJQkLxis8AASzIgMUW_QAAIla563.pdf"],
            ["常州市口腔医院","320400467288845","372158993@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SMwmIelcwAACJyoNrSCAAASzJAKSAqAAAIni383.pdf"],
            ["哈尔滨市香坊区口腔病防治所","12230106424160374E","XFYB777@163.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SMwSICuC7AACJVrcTM4YAASzIgKMG3IAAIlu235.pdf"],
            ["沈阳市和平区牙病防治所","210102410626284","28597087@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/08/wKj6yF3SMv6IJqTXAACJQgqV37MAASzJAHw9noAAIla413.pdf"],
            ["河北省眼科医院","130502403390234","zjhcrqyy@163.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SMvmIdGQ5AACI-0p5LtYAASzIwG8KGsAAIkT124.pdf"],
            ["银川市口腔医院","126401004540410187","13639573362@163.com","2500.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SMvOIR5y4AACJ0i6SaZAAASzIgGiwPUAAInq821.pdf"],
            ["宁夏回族自治区宁东医院","12640000454899639E","bjgyygl@163.com","500.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SMu-IdWfEAACJTHbCB9MAASzIwE-61YAAIlk003.pdf"],
            ["宁夏医科大学","12640000454000013C","376572170@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SMuiIJCFAAACJPqJeUzQAASzIwDhIiYAAIlW407.pdf"],
            ["天水市中西医结合医院","1262050343839119X1","484812494@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/08/wKj6yF3SMuGILoszAACJTZJ92-0AASzIgC77B0AAIll567.pdf"],
            ["吉林百合口腔医院股份有限公司","912201017430194900","63247205@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SMtqIfYkAAACJFd64yXEAASzIwA4kQgAAIkt447.pdf"],
            ["腾冲石龙口腔诊所","92530522MA6KQNQ30H","329836009@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SMtSIbT_PAACJWJhPjD0AASzIQP4xxkAAIlw434.pdf"],
            ["遵义汇川蓝天口腔门诊部","92520303MA6DXW3N4X","308970781@qq.com","1500.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SMs-IFljpAACI3R2EGRoAASzIQO9zWsAAIj1232.pdf"],
            ["胡牙科口腔门诊部","92511403MA657QHF2H","624837527@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMsmIOHSUAACJDtLMlrcAASzIANuC6QAAIkm322.pdf"],
            ["健康口腔推广基地","92510923MA66X26C5K","1819417849@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMsSIam4BAACIykYC9-0AASzHwNIy84AAIji031.pdf"],
            ["潼南区潼华口腔门诊部","92500223MA60H9UKXA","823950580@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMr2IQOdeAACJX6mrFnMAASzIAK-9IgAAIl3018.pdf"],
            ["那坡县荣代口腔诊所","92451026MA5KF7HE8K","2858454043@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMrmID_RpAACJHpK9m_gAASzHwKbf8wAAIk2176.pdf"],
            ["靖西荣代口腔诊所","92451025MA5NHUJQ5K","262620563@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMrKIF46kAACJ5AjA1xQAASzHwJPam4AAIn8712.pdf"],
            ["平果现代牙病防治院","92451023MA5M070U0D","1097285081@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMq6IA5mzAACJRxt18kkAASzHwIZmmQAAIlf398.pdf"],
            ["南宁艾森口腔诊所","92450103MA5L15XK83","582512737@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SMpyINPDwAACJVdr6tsMAASzIQEAEZUAAIlt886.pdf"],
            ["北海市嘉霖口腔门诊部","92450502MA5LW1MW4W","125440039@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMqeITLgzAACJEoyogOAAASzHwHHMigAAIkq852.pdf"],
            ["宾阳牙队长口腔门诊部","92450126MA5L6JAJ98","4314218@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SMqOIAqH4AACJ5LN47Q8AASzIQFKGMIAAIn8628.pdf"],
            ["益阳高新郭医师口腔门诊部","92430900MA4QGGDG20","28283363@qq.com","1500.00","https://inv.jss.com.cn/group4/M01/00/08/wKj6yF3SMpiIfKWqAACJbuI3iH8AASzIQDGepUAAImG349.pdf"],
            ["攸县泓德口腔医院","92430223MA4PKYW29C","214744056@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMpGIMgcdAACIzZ4uuHwAASzIACE6LIAAIjl355.pdf"],
            ["顺昌邱炎英口腔门诊部","92350721MA2YP9MB2K","451937158@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMoyIedhaAACI00L1dncAASzIABLBXUAAIjr318.pdf"],
            ["南安诗山黄哲强口腔诊所","92350583MA2YFD8N86","179039585@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMoaICpF7AACJIedfVGAAASzHQP-ENMAAIk5545.pdf"],
            ["南京市鼓楼区源泉门诊部","92320106MA1QENC40K","1365889551@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMn-IRzwXAACJH7zohUwAASzHAPnPcwAAIk3333.pdf"],
            ["吉林市昌邑区小小精工口腔门诊部","92220202MA16Y00G0Y","124657961@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/07/wKj6yF3SMnmIGQPPAACKNiGqracAASzHgNbDY0AAIpO660.pdf"],
            ["宁晋李磊思口腔诊所","92130528MA09PJHF4T","365163206@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMnKITp8jAACJ58Yt5z8AASzHQMdkyIAAIn_459.pdf"],
            ["新疆凯乐口腔医疗投资管理有限公司","9165010031341700XT","39622691@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMm2IHbrhAACI0we02PsAASzHAMG24wAAIjr256.pdf"],
            ["吴忠西典口腔医院","916403027359733644","jxw1959@163.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMmeID9P9AACIvRMaVUEAASzHAK-I6EAAIjV076.pdf"],
            ["兰州市城关区惠安齿科诊所","91620102784010396K","779437158@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMlyIJU7gAACJ10yawyQAASzHAIynhYAAInv992.pdf"],
            ["银川德昇泰艾齿口腔医院(有限公司)","9164010031771532XU","3062892916@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/07/wKj6yF3SMmKIPF3aAACI58X9ZHUAASzHgJSie0AAIj_305.pdf"],
            ["兰州康美口腔医院有限责任公司","91620100MA74J1TF7D","1427683535@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMleINcLkAACI3zHGmYgAASzHAH-rtEAAIj3433.pdf"],
            ["咸阳小白兔口腔门诊部有限公司","91610404338643353U","317623457@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/07/wKj6yF3SMlGIfIF4AACJVWXZsaQAASzHgFZjqcAAIlt335.pdf"],
            ["西安未央奥拉口腔诊所有限公司","91610112MA6UTLEW1Q","zhyudorian@163.com","500.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMkyICgRjAACJIiAN1n8AASzHQEqh4wAAIk6979.pdf"],
            ["陕西瑞泰尔仓口腔医院有限公司","91610104333781976X","1731704851@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMkWIRc7nAACKJHMugqEAASzHQDOhJkAAIo8497.pdf"],
            ["西安联邦口腔医院管理有限公司","91610102333737780Q","623971884@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMj-ILyoNAACJGrKDqgIAASzHACW2rEAAIky994.pdf"],
            ["昆明蓝橙口腔医院有限责任公司","91530112MA6KCLKG7U","1473491621@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMjiIZe-qAACKNcQ1sNAAASzGgP26asAAIpN843.pdf"],
            ["昆明柏德世纪城口腔医院有限公司","91530100MA6MWCE04G","1193018890@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/07/wKj6yF3SMjGIAsfkAACJJFL3c-gAASzGwNwdvUAAIk8171.pdf"],
            ["云南白药集团健康产品有限公司","91530100216583428N","xiaoxuan103703@163.com","1000.00","https://inv.jss.com.cn/group4/M01/00/07/wKj6yF3SMi2IX1FJAACIz9fY2cYAASzGwMh26IAAIjn236.pdf"],
            ["贵州源隆口腔医疗有限公司","91522731MA6J21UQ2W","2208906689@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMiaIC0BbAACJGL-Q7RkAASzGgLhUwEAAIkw802.pdf"],
            ["资阳贝齿口腔医院有限责任公司","91512000MA676EGR8D","378375228@qq.com","2000.00","https://inv.jss.com.cn/group4/M01/00/07/wKj6yF3SMhuIHfEKAACJZjsitT0AASzGwI7PGQAAIl-358.pdf"],
            ["成都壹舍医疗信息科技有限公司","91510105MA6C9HCW2M","yf865053@163.com","2500.00","https://inv.jss.com.cn/group4/M01/00/07/wKj6yF3SMgeIDZGRAACI4WN7_8kAASzGwEl6KMAAIj5018.pdf"],
            ["兴义市人民医院","915223014297000264","fengqiujing@126.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMiKIQS9qAACKm0vcimUAASzGQLIFDIAAIqz234.pdf"],
            ["眉山市彭山壹牙口腔诊所有限公司","91511403MA66TWQE9K","253552891@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMhSINjZeAACJKEh2zMkAASzGgH6tL8AAIlA803.pdf"],
            ["成都新津兴恒和口腔诊所有限公司","91510132MA639R0898","307387915@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/07/wKj6yF3SMg6IQ8G9AACJ6dUxx3gAASzGwF6DIsAAIoB812.pdf"],
            ["成都皓瑞洁商贸有限公司","91510100MA66YF5B1M","793293167@qq.com","500.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMgGIEIcoAACJ5U7pBfYAASzGQECh8oAAIn9392.pdf"],
            ["四川牙一教育咨询有限公司","91510100MA62MLLX0E","belle.fu@adg-dental.cm","1500.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMfqIfJ2FAACJZdrEFh4AASzGgCJXkQAAIl9352.pdf"],
            ["重庆天鑫时源商贸有限公司","91500108MA5YUMKR9P","1837730046@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/07/wKj6yF3SMfWIV2ugAACJ7cXrGuwAASzGwAOuvkAAIoF132.pdf"],
            ["重庆渝博口腔医院管理有限责任公司","91500103MA5UD8UG8E","980752753@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMe-IRF-9AACI5nMAsdAAASzFwPu9wcAAIj-899.pdf"],
            ["重庆瑞泰口腔医院有限公司","91500000MA5UNUHD5P","317223199@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMeqId1IVAACJZCVFjJsAASzFwO_-1wAAIl8904.pdf"],
            ["重庆登康口腔护理用品股份有限公司","91500000203000772K","269705883@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMeSIfDSOAACJGr-c60IAASzFwN7lSQAAIky095.pdf"],
            ["海南口腔医院有限公司","91460000573061329M","xiaoshihao@0898hnkq.com","1500.00","https://inv.jss.com.cn/group4/M01/00/07/wKj6yF3SMd-IMXfkAACIxRFjpO0AASzGALxONcAAIjd919.pdf"],
            ["都安嘉怡口腔医疗服务有限责任公司","91451228MA5NXR2U9F","315913146@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMdmINKBIAACJJ1SK30EAASzFwLjg1MAAIk_125.pdf"],
            ["靖西荣代口腔门诊部有限责任公司","91451025MA5MWC8F71","262620563@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/07/wKj6yF3SMdKIdQPeAACKtpbBP2MAASzGAIhlXUAAIrO779.pdf"],
            ["柳州华喜口腔医疗管理有限公司谷埠诊所","91450203MA5J0HH8R","1091810245@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMceIHpE0AACI4uPLjHgAASzFgHDEuIAAIj6630.pdf"],
            ["广西德保荣代口腔门诊部","91451024081186977T","1107743757@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMc2IDIvqAACIzN_hjf0AASzFwIq8NAAAIjk138.pdf"],
            ["柳州华喜口腔医疗管理有限公司","91450203MA5NBW1G1C","970115130@qq.com","1500.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMcKIWORwAACJI2N63aQAASzFgGBJXsAAIk7440.pdf"],
            ["东莞口腔医院有限公司","91441900MA4W1LL93A","624150664@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMbuIc8E3AACJV3DhYksAASzFwEmqosAAIlv017.pdf"],
            ["东莞健力口腔医院","914419007429757471","244928675@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMbeIEniyAACJArI4iHUAASzFgDrs0MAAIka959.pdf"],
            ["惠州市惠齿健口腔门诊管理有限公司","91441300584694383H","517487030@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMayIHblQAACJGT-LCFoAASzFgBXRZIAAIkx145.pdf"],
            ["广东牙虎口腔咨询管管理有限公司","91440400MA52AD0G9J","570958737@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMaGIP2DQAACI4thnzKsAASzFAOpb7kAAIj6586.pdf"],
            ["五华世客口腔医院连锁管理有限公司","91441424MA51RCEEOK","1005234233@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMbCIKv9BAACI6tSrg_MAASzFgCLq20AAIkC025.pdf"],
            ["汕头市牙博士口腔门诊有限公司","91440500MA51EL203Y","289099@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/07/wKj6yF3SMaWIND9xAACI4u8G3RUAASzFQOFcK8AAIj6123.pdf"],
            ["珠海市同步口腔医疗管理有限公司同步口腔门诊部","91440400MA4X625D01","2713257096@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/07/wKj6yF3SMZqIDunHAACJ8FNHv-YAASzFQLRv6AAAIoI600.pdf"],
            ["广州市诺晟医疗技术有限公司","91440113558388673M","1652426656@qq.com","2500.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMZSIfqu-AACKK8sGZ_UAASzFALPCuUAAIpD731.pdf"],
            ["广州薇美姿实业有限公司","914401063047779949","xiaojunfang@weimeizi.com","2500.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMY2IUO0MAACJVqFDqVsAASzEwJaHuMAAIlu237.pdf"],
            ["广州欧欧医疗科技有限责任公司","9144010435573541X8","wuyiping@oodental.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMYaIdmjhAACJV3t1Do8AASzEwIET5MAAIlv666.pdf"],
            ["广州市趣看牙健康科技有限公司","91440101MA5CL5NCXM","406808969@qq.com","500.00","https://inv.jss.com.cn/group4/M01/00/07/wKj6yF3SMYKIU8XTAACJI7Cd-bsAASzFQFy8MIAAIk7004.pdf"],
            ["广州卓祥医疗门诊部有限公司","91440101MA59HDX7X3","wangronglailai@163.com","500.00","https://inv.jss.com.cn/group4/M01/00/07/wKj6yF3SMXuIYZkfAACJIzAu9pkAASzFQDzamMAAIk7311.pdf"],
            ["娄底口腔医院有限责任公司","91431300093073750L","daitao8888@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMXaIBQieAACKIoeiuVEAASzEwD2rQ8AAIo6826.pdf"],
            ["湖南青檬健康咨询管理有限公司","91430111MA4PYCHX3U","543181857@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMXCIfoHwAACI5NLiQpUAASzEwCJKhsAAIj8003.pdf"],
            ["湖南瑞泰科尔雅口腔医疗管理有限公司","91430102MA4LJ5M003","zhoupingcs@arrail-dental.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMWOIItPlAACI5aYJebIAASzEAOtrVoAAIj9778.pdf"],
            ["湖南上善一松健康管理有限公司","91430105MA4L1JHU4A","349858558@qq.com","2500.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMWmIDEigAACJK59JFGoAASzEQP-tSQAAIlD367.pdf"],
            ["广水市瑞康口腔门诊部有限公司","91421381MA493HUW81","867645229@qq.com","500.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMV6IX1-zAACI1UupBYAAASzEAN3RJkAAIjt548.pdf"],
            ["当阳德众口腔医院有限责任公司","91420582316577690K","849679630@qq.com","1000.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMVeIXg9BAACJFmGqwyQAASzEQLn4aIAAIku521.pdf"],
            ["黄石美维中山口腔医疗管理有限公司黄石港门诊部","91420200MA492JYW36","360309811@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMU6IY_t1AACJd9PrLD4AASzEAKC0b4AAImP125.pdf"],
            ["武汉清华阳光口腔门诊部有限公司","91420105MA4KX3E26B","727269943@qq.com","1000.00","https://inv.jss.com.cn/group4/M01/00/07/wKj6yF3SMUeIAOcvAACJIkgsTnsAASzEgGOtmYAAIk6040.pdf"],
            ["中美联合马里兰生物科技(武汉)有限公司","91420105MA4KMATP93","524437537@qq.com","2000.00","https://inv.jss.com.cn/group4/M02/00/07/wKj6yF3SMUKIKmbYAACJgxG3hEIAASzEQHP3j0AAImb755.pdf"],
            ["武汉大众口腔医疗股份有限公司","91420100663470628C","418765814@qq.com","1000.00","https://inv.jss.com.cn/group4/M00/00/07/wKj6yF3SMTyIGD74AACI1Fdupi0AASzEAGTQxMAAIjs844.pdf"]
        ];
    }
    public function index()
    {
        $order = EnrollOrder::where('is_active',1)->where('status',1)->get()->toArray();
        $ticket = [];
        foreach($order as $v) {
            $is_jidi = false;
            if($v['pay_ticket']['number']) {
                if ($v['source_id'] == '5d91734a7633793ab6581f3a') $is_jidi = true;
                $ticket[$v['pay_ticket']['number']]['is_jidi'] =  isset($ticket[$v['pay_ticket']['number']]['is_jidi']) &&  $ticket[$v['pay_ticket']['number']]['is_jidi'] == 1 ? true : $is_jidi;
                $ticket[$v['pay_ticket']['number']]['info'][] = [
                    'id' => $v['_id'],
                    'is_jidi' => $v['source_id'] == '5d91734a7633793ab6581f3a' ? 1 : 0,
                    'head' => $v['pay_ticket']['head'],
                    'email' => $v['pay_ticket']['email']
                ];
            }
        }
        $data = $this->data();
        $tmp = [];
        foreach ($data as $v) {
            $tmp[$v[1]] = [
                'name' => $v[0],
                'email' => $v[2],
                'money' => $v[3],
                'remark' => $v[4]
            ];
        }
        $notHas = [];
        $has = [];
        $data1 = array_slice($tmp,0,50);
        $data2 = array_slice($tmp,100,100);
        $data3 = array_slice($tmp,200,100);
//        dd($data1,$data2,$data3);
        $success = [];
        $res = [];
        foreach ($tmp as $k => $v) {
            if (isset($ticket[$k])) {
                $res[$k] = $v;
            }
        }
//        dd($ticket);
//        dd($ticket['1262010043804207X2']);
        foreach ($res as $k => $v) {
//            if (isset($ticket[$k])) {
//                $res[$k] = $v;
                if (count($ticket[$k]['info']) > 1 && $ticket[$k]['is_jidi']) {
                    $is_true = false;
                    foreach ($ticket[$k]['info'] as $k1 => &$v1) {
                        if ($v1['is_jidi'] == 1) {
                            if (!$is_true && $v1['is_jidi']) { //未登记
                                $order = EnrollOrder::find($v1['id']);
                                $ticket1 = $order->pay_ticket;
                                $ticket1['ticket_money'] = $v['money'];
                                $ticket1['remark'] = $v['remark'];
                                $order->pay_ticket = $ticket1;
                                $order->is_ticket = 1;
                                $order->save();
                                $is_true = true;
                                $success[] = [
                                    'id' => $v1['id'],
                                    'number' => $k,
                                    'name'   => $v['name'],
                                    'money'  => $v['money']
                                ];
                            }
                        }
                    }
                }else {
                    $order = EnrollOrder::find($ticket[$k]['info'][0]['id']);
                    $ticket1 = $order->pay_ticket;
                    $ticket1['ticket_money'] = $v['money'];
                    $ticket1['remark'] = $v['remark'];
                    $order->pay_ticket = $ticket1;
                    $order->is_ticket = 1;
                    $order->save();
                    $success[] = [
//                        'id' => $order['id'],
                        'id' => $ticket[$k]['info'][0]['id'],
                        'number' => $k,
                        'name'   => $v['name'],
                        'money'  => $v['money']
                    ];
                }
//            }
        }
//        dd($res);
        dd('success',$success);

//        $enroll = EnrollOrder::where('pay_ticket.head','上海市黄浦区牙病防治所')->where('status',0)->get();
//        foreach($enroll as $v) {
//            $enrollData = EnrollData::where('order_id',$v->_id)->first();
//            $enrollData->is_active = 0;
//            $enrollData->remark = '重复删除';
//            $v->is_active = 0;
//            $v->remark = '重复删除';
//            $v->save();
//            $enrollData->save();
//        }
//        dd($enroll);
//        $enroll->is_active = 0;
//        $enroll->remark = '测试删除';
//        $enroll->save();
//        $enrollData = EnrollData::where('order_id','5dba404376337961f540e7de')->first();
//        dd($enroll,$enrollData);
//        $value = $enrollData->value;
//        $value[6]['value'] = '谢丽琳';
//        $value[7]['value'] = '助理医生';
//        $value[8]['value'] = '13640068452';
//        $value[9]['value'] = '702136494@qq.com';
//        $enrollData->value = $value;
//        $enrollData->is_active = 0;
//        $enrollData->remark = '测试删除';
//        $enrollData->save();
//        dd($enroll,$enrollData);

        $vol = Volunteer::where('is_active', 1)->where('status',1)->orderBy('timestamp', 'desc')->get();
        $vol = $this->getImg($vol);
        //本周
        $start = strtotime(date('Y-m-d', (time() - ((date('w') == 0 ? 7 : date('w')) - 1) * 24 * 3600)));
        $end = strtotime(date('Y-m-d', $start) . '+7day');
        $week = Volunteer::where('is_active', 1)->where('status',1)->where('timestamp', '>', $start)->where('timestamp', '<', $end)->orderBy('timestamp', 'desc')->get();
        $week = $this->getImg($week);
        $data = [
            'top' => $vol,
            'week' => $week
        ];
        //清阅读缓存
        Session::forget('timestamp');
        Session::forget('endtime');
        return view('wechat.activity.index')->with($data);
    }

    private function getImg($data)
    {
        foreach ($data as $v) {
            $img = empty($v['image']) ? '/images/admin/act_default.jpeg' : Storage::disk('s3-public')->url($v['image']);
            $v['image'] = $img;
        }
        return $data;
    }

    /**
     * 公益活动详情
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail()
    {
        $id = Input::get('id');
        $act = Volunteer::find($id);
        $hospital_id = $act['hospital_id'];
        $hospital = Hospital::whereIn('_id', $hospital_id)->where('type', 1)->get();
        $factory = Hospital::whereIn('_id',$hospital_id)->where('type',2)->get();
        foreach ($hospital as $v) {
            $v['logo'] = empty($v['logo']) ? '/images/admin/act_default.jpeg' : Storage::disk('s3-public')->url($v['logo']);
        }
        foreach ($factory as $v) {
            $v['logo'] = empty($v['logo']) ? '/images/admin/act_default.jpeg' : Storage::disk('s3-public')->url($v['logo']);
        }
        $act['image'] = empty($act['image']) ? '/images/admin/act_default.jpeg' : Storage::disk('s3-public')->url($act['image']);
        $tmp = [];
        foreach ($act['hospital_id'] as $v1) {
            foreach ($hospital as $v2) {
                if ($v1 == $v2['_id']) {
                    $tmp[] = $v2;
                }
            }
        }
        $act['hospital'] = $tmp;
        $js = InterfaceController::js();
        return view('wechat.activity.detail')->with(['data' => $act,'factory' => $factory,'js' => $js]);
    }

    /**
     *机构详情
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function hospital()
    {
        $id = Input::get('id');
        $hospital = Hospital::find($id);
        $hospital['logo'] = empty($hospital['logo']) ? '/images/admin/act_default.jpeg' : Storage::disk('s3-public')->url($hospital['logo']);
        $data = [];
        $openId = Session::all()['wechat']['oauth_user']['default']->id;
        if ($hospital['type'] == 1) {
            $doctor = Doctor::where('hospital_id', $hospital->_id)->get();
            foreach ($doctor as $v) {
                $v['headimg'] = empty($v['headimg']) ? '/images/admin/act_default.jpeg' : Storage::disk('s3-public')->url($v['headimg']);
            }
            $data['doctor'] = $doctor;
        } else {
            if (!empty($hospital['goods'])) {
                $tmp =[];
                foreach ($hospital['goods'] as  $k => $v1) {
                    $v1['image'] = empty($v1['image']) ? '/images/admin/act_default.jpeg' : Storage::disk('s3-public')->url($v1['image']);
                    $v1['url']   = $v1['url'].'/'.$openId;
                    $tmp[] = $v1;
                }
                $hospital['goods'] = $tmp;
            }
        }
        //BD-09 转 GCJ-02
        $hospital->pointlat = $hospital->pointlat - 0.0060;
        $hospital->pointlng = $hospital->pointlng - 0.0065;
        $js = InterfaceController::js();
        $data['data'] = $hospital;
        $data['js']   = $js;
        return view('wechat.activity.hospital')->with($data);
    }

    public function getDistance()
    {
        $id = Input::get('id');
        $lat = Input::get('lat');
        $long = Input::get('long');
        $act = Volunteer::find($id);
        $hospital_id = $act['hospital_id'];
        $hospital = Hospital::whereIn('_id', $hospital_id)->whereIn('type',[1,2])->get();
        $origins = $lat . ',' . $long;
        $ouput = $this->distance($hospital,$origins);
        if ($ouput->status == 0) {
            foreach ($ouput->result as $k => $v) {
                $hospital[$k]->distance = number_format($v->distance->value / 1000, 1, '.', '') . 'km';
            }
        }
        return response()->json(['status' => 0, 'data' => $hospital,'res' => $ouput,'origins' => $origins]);
    }

    private function distance($hospital,$origins)
    {
        $destinations = '';
        foreach ($hospital as $v) {
            if (in_array($v['type'],[1,2])) {
                $destinations .=  $v['pointlat'] . ',' . $v['pointlng'] . '|';
                $v['logo'] = empty($v['logo']) ? '/images/admin/act_default.jpeg' : Storage::disk('s3-public')->url($v['logo']);
            }
        }
        $destinations = trim($destinations, '|');
        $ch = curl_init();
        $curl = 'http://api.map.baidu.com/routematrix/v2/driving?output=json&origins=' . $origins . '&destinations=' . $destinations . '&ak=cGTpOWKO0TbIynkaxgOGyUy2H4P4REiy';
        curl_setopt($ch, CURLOPT_URL, $curl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $ouput = curl_exec($ch);
        return json_decode($ouput);
    }

    /**
     * 位置搜索页面
     * @return $this
     */
    public function getSearch()
    {
        $hospital = Hospital::where('is_active',1)->where('type',1)->get();
        foreach($hospital as $v) {
            $v['logo'] = empty($v['logo']) ? '/images/admin/act_default.jpeg' : Storage::disk('s3-public')->url($v['logo']);
        }
        return view('wechat.activity.search')->with(['data' => $hospital]);
    }

    /**
     * 位置搜索内容
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHospital()
    {
        $city = Input::get('city');
        $point = Input::get('point');
        $origins = $point['lat'] . ',' . $point['lng'];
        $hospital = Hospital::where('city',$city)->where('is_active',1)->where('type',1)->get();
        $ouput = $this->distance($hospital,$origins);
        if ($ouput->status == 0) {
            foreach ($ouput->result as $k => $v) {
                $hospital[$k]['distance'] = number_format($v->distance->value / 1000, 1, '.', '') . 'km';
            }
        }
        return response()->json(['status' => 0,'data' => $hospital]);
    }
}
