<?php

namespace App\Http\Controllers\Wechat;

use App\Model\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Ayzy\IM\App\IM;
use Ayzy\IM\Facades\IM as FacadesIM;

class ChatController extends Controller
{
    public function index( Member $guest, IM $im )
    {
        $openId       = session('wechat.oauth_user.default')['id'];
        $info         = $guest->where('_id', $openId)->first();
        $info->openId = $info->_id;
        $kefu         = $im->getKeFu($openId);

        return view('Ayzy::wechat.chat', compact('info', 'kefu'));
    }

    public function assist()
    {
        $openId = FacadesIM::assistUsername();

        $html = FacadesIM::assistHTML('这是标题', 's了是打飞机啊老是看得见法拉盛啥的六块腹肌 三点了副科级');
        $html .= FacadesIM::assistHTML('这是标题', 's了是打飞机啊老是看得见法拉盛啥的六块腹肌 三点了副科级');
        $html .= FacadesIM::assistHTML('这是标题', 's了是打飞机啊老是看得见法拉盛啥的六块腹肌 三点了副科级');
        $html .= FacadesIM::assistHTML('这是标题', 's了是打飞机啊老是看得见法拉盛啥的六块腹肌 三点了副科级');
        $html .= FacadesIM::assistHTML('这是标题', 's了是打飞机啊老是看得见法拉盛啥的六块腹肌 三点了副科级');
        $html .= FacadesIM::assistHTML('这是标题', 's了是打飞机啊老是看得见法拉盛啥的六块腹肌 三点了副科级');
        $html .= FacadesIM::assistHTML('这是标题', 's了是打飞机啊老是看得见法拉盛啥的六块腹肌 三点了副科级');

        return response()->json($html);
    }
}