<?php

namespace App\Http\Controllers\Wechat;

use EasyWeChat\Factory;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Input;

class InterfaceController extends Controller
{
    /**
     * 初始化
     */
    public static function init()
    {
        $options = Config::get('wechat');
        $app = Factory::officialAccount($options);
        return $app;
    }

    /**
     * 发送模版消息
     *
     * @param $userId
     * @param $data
     * @param $url
     * @param $type
     * @return mixed
     */
    public static function notice( $userId, $data, $url, $template )
    {
        $notice       = self::init()->template_message;
        $configNotice = Config::get('notice');
        $templateId   = $configNotice[$template];

        $noticeId = $notice->send([
            'touser'      => $userId,
            'template_id' => $templateId,
            'url'         => $url,
            'data'        => $data,
        ]);

        return $noticeId;
    }

    /**
     * jssdk 数据确认
     *
     * @return \EasyWeChat\Js\Js
     */
    public static function js()
    {
        $js = self::init()->jssdk;
        $returnJs = $js->buildConfig([ 'chooseImage', 'previewImage', 'uploadImage', 'downloadImage', 'getLocation', 'chooseWXPay', 'scanQRCode' ],false, false, true);

        return $returnJs;
    }

    /**
     * 菜单
     */
    public function menu()
    {
        $dns = Config::get('dns');
        $menu = self::init()->menu;
        $buttons = [
            ["type" => "view",
                "name" => "患教",
                "url"  => $dns . "/weixin/education"
            ],
        ];
//        $button1 = [
//        ["name" => "我",
//            "sub_button" => [
//                ["type" => "view", "name" => "我的空间",
//                    "url"  => $dns . "/person/space"
//                ],
//                ["type" => "view",
//                    "name" => "党建引领",
//                    "url"  => $dns . "/party/study"
//                ]
//            ],
//
//        ],
//        ["type" => "view",
//            "name" => "支部",
//            "url"  => $dns . "/party/space"
//        ],
//        ["type" => "view",
//            "name" => "党委",
//            "url"  => $dns . "/committee/space"
//        ],
//    ];
//        $button2 = [
//            ["name" => "我",
//                "sub_button" => [
//                    ["type" => "view", "name" => "我的空间",
//                        "url"  => $dns . "/person/space"
//                    ],
//                    ["type" => "view",
//                        "name" => "党建引领",
//                        "url"  => $dns . "/party/study"
//                    ]
//                ],
//
//            ],
//            ["type" => "view",
//                "name" => "支部",
//                "url"  => $dns . "/party/space"
//            ],
//            ["type" => "view",
//                "name" => "党委",
//                "url"  => $dns . "/committee/space"
//            ],
//        ];
//        $button3 = [
//            ["name" => "我",
//                "sub_button" => [
//                    ["type" => "view", "name" => "我的空间",
//                        "url"  => $dns . "/person/space"
//                    ],
//                    ["type" => "view",
//                        "name" => "党建引领",
//                        "url"  => $dns . "/party/study"
//                    ]
//                ],
//
//            ],
//            ["type" => "view",
//                "name" => "支部",
//                "url"  => $dns . "/party/space"
//            ],
//            ["type" => "view",
//                "name" => "党委",
//                "url"  => $dns . "/committee/space"
//            ],
//        ];
//
//        $mathButton1 = [
//              "tag_id" => "100", // 普通党员
//        ];
//        $mathButton2 = [
//            "tag_id" => "101", // 普通党员
//        ];
//        $mathButton3 = [
//            "tag_id" => "102", // 普通党员
//        ];
        $menu->create($buttons);
//        $menu->create($button1, $mathButton1);
//        $menu->create($button2, $mathButton2);
//        $menu->create($button3, $mathButton3);
        echo 'OK';
    }

    /**
     * 把资源写入服务器
     *
     * @param $mediaid
     * @return string
     */
    public static function temporary( $mediaid,$directory = 'avatar' )
    {
        $app = self::init();
        $content = $app->media->get($mediaid);
        $imgSrc = self::uploadMedia($content,$directory);
        return $imgSrc;
    }

    public static function uploadMedia( $content, $directory )
    {
        $timer = date('Ymd', time());
        $extension = 'jpg';
        $str = str_random(32);
        $newName = $str . '.' . $extension;
//        $content=$base64;
        Storage::disk('local')->put($directory.'/'.$timer.'/'.$newName,$content);
        return $directory.'/'.$timer.'/'.$newName;
    }


    /**
     * 二维码
     */
    public function foreverQrCode( $str )
    {
        // 永久二维码
        $qrcode = self::init()->qrcode;
        $result = $qrcode->forever($str);
        $url = $result['url'];
        return $url;
    }

    public function temporaryQrCode( $int, $time )
    {
        // 临时二维码
        $qrcode = self::init()->qrcode;
        $result = $qrcode->temporary($int, $time);
        $url = $result['url'];
        return $url;
    }

    public function addHospitalCode()
    {
        $data = Input::all();
        if ( isset( $data['hId'] ) ) {
            $hId = $data['hId'];
            $doctorStr = 'docto' . $hId;
            $staffStr = 'staff' . $hId;
            $doctorUrl = $this->foreverQrCode($doctorStr);
            $staffUrl = $this->foreverQrCode($staffStr);

            // 保存到数据库
            return response()->json(['doctor_qrcode' => $doctorUrl, 'staff_qrcode' => $staffUrl]);
        }

        return response()->json([]);
    }

    public function setDoctorCode()
    {
        $data = Input::all();
        if ( isset( $data['hId'] ) && isset( $data['dId'] ) ) {
            $hId = $data['hId'];
            $dId = $data['dId'];
            $doctorUrl = $this->foreverQrCode($dId . $hId);

            return $doctorUrl;
        }

        return '';
    }
}
