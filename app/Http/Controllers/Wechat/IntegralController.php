<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Education;
use App\Model\Integral;
use App\Model\Member;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Session;

class IntegralController extends Controller
{
    private function init()
    {
        $openId = session::get('wechat.oauth_user.default')->original['openid'];
        return $openId;
    }
    public function get()
    {
        $openId = Input::get('openId');
        $integral = Integral::where('openId',$openId)->where('is_active',1)->get();
        $num = 0;
        foreach ($integral as $v) {
            if ($v->math == 0) {
                $num += $v->num;
            } else if ($v->match == 1) {
                $num += $v->num;
            }
        }
        return response()->json(['num' => $num]);
    }
    public function add()
    {

    }

}
