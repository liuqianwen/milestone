<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WebsiteController extends Controller
{

    public function index()
    {
        return view('wechat.website.index');
    }
}
