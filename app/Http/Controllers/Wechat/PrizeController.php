<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Prize;
use Illuminate\Support\Facades\Storage;

class PrizeController extends Controller
{

    public function index()
    {
        $prize = Prize::where('is_active',1)->orderBy('sort','asc')->limit(8)->get();
        $prize_arr = [];
        foreach ($prize as $v){
            $arr['id'] = $v->_id;
            $arr['prize'] = $v->name;
            $arr['img'] = Storage::disk('s3-public')->url($v->image);
            $arr['v'] = $v->odds;
            $prize_arr[] = $arr;
         }
//        $prize_arr = array(
//            '0' => array('id'=>1,'prize'=>'20元话费','v'=>5),
//            '1' => array('id'=>2,'prize'=>'300M流量','v'=>30),
//            '2' => array('id'=>3,'prize'=>'500M流量','v'=>20),
//            '3' => array('id'=>4,'prize'=>'50元话费','v'=>2),
//            '4' => array('id'=>5,'prize'=>'谢谢参与','v'=>50),
//            '5' => array('id'=>6,'prize'=>'100元话费','v'=>1),
//            '6' => array('id'=>7,'prize'=>'1G流量','v'=>19),
//            '7' => array('id'=>8,'prize'=>'谢谢参与','v'=>50),
//        );
        return view('wechat.prize.index')->with(['data'=>$prize_arr]);
    }

    public function getResult()
    {
        $prize = Prize::where('is_active',1)->orderBy('sort','asc')->limit(8)->get();
        $prize_arr = [];
        foreach ($prize as $k=>$v){
            $arr['id'] = $k+1;
            $arr['prize_id'] = $v->_id;
            $arr['prize'] = $v->name;
            $arr['type'] = $v->type;
            $arr['img'] = Storage::disk('s3-public')->url($v->image);
            $arr['v'] = $v->odds;
            $prize_arr[] = $arr;
        }
        //如果中奖数据是放在数据库里，这里就需要进行判断中奖数量
//在中1、2、3等奖的，如果达到最大数量的则unset相应的奖项，避免重复中大奖
//code here eg:unset($prize_arr['0'])
        $arr = [];
        foreach ($prize_arr as $key => $val) {
            $arr[$val['id']] = $val['v'];
        }

        $rid = $this->get_rand($arr); //根据概率获取奖项id
//
//        $res['yes'] = $prize_arr[$rid-1]['prize']; //中奖项
//        //将中奖项从数组中剔除，剩下未中奖项，如果是数据库验证，这里可以省掉
//        unset($prize_arr[$rid-1]);
//        shuffle($prize_arr); //打乱数组顺序
//        for($i=0;$i<count($prize_arr);$i++){
//            $pr[] = $prize_arr[$i]['prize'];
//        }
//        $res['no'] = $pr;
        return response()->json(['status'=>0,'result'=>$rid-1,'type'=>$prize_arr[$rid-1]['type'],'name'=>$prize_arr[$rid-1]['prize'],'img'=>$prize_arr[$rid-1]['img']]);
    }


    function get_rand($proArr) {
        $result = '';
        //概率数组的总概率精度
        $proSum = array_sum($proArr);

        //概率数组循环
        foreach ($proArr as $key => $proCur) {
            $randNum = mt_rand(1, $proSum);
            if ($randNum <= $proCur) {
                $result = $key;
                break;
            } else {
                $proSum -= $proCur;
            }
        }
        unset ($proArr);

        return $result;
    }
}
