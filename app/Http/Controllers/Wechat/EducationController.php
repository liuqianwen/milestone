<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\Weixin\InterfaceController;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Education;
use App\Model\Integral;
use App\Model\Label;
use App\Model\Like;
use EasyWeChat\Factory;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;


class EducationController extends Controller
{

    public function index()
    {
        $res = $this->rmTag();
        $article = $res['article'];
        $video = $res['video'];
        $label = $res['label1'];
        $text = '热门';
        $param = Input::all();
        if (isset($param['id'])) {
            //标签搜索
            $searchData = $this->getData($param['type'], $param['id']);
            $article = $searchData['article'];
            $video = $searchData['video'];
            if ($param['id'] == 'all') {
                $text = '全部';
            } else {
                $text = $this->getLable()[$param['id']];
            }

        }
        if (isset($param['search'])) {
            //输入框内容搜索
            $searchData = $this->getData($param['type'], null, $param['search']);
            $article = $searchData['article'];
            $video = $searchData['video'];
            $text = $param['search'];
        }
        $top = Education::where('is_active', 1)->where('type', 1)->orderBy('top','desc')->orderBy('created_at','desc')->get();
        foreach ($top as $v) {
            $v['image'] = $v->image == '' ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : Storage::disk('s3-public')->url($v->image);
        }
//        //获取阅读时间
//        $time = $this->getTime()['timestamp'];
//        $endtime = $this->getTime()['endtime'];
//        $is_end = $this->getTime()['is_end'];
//        //查找当天阅读时间积分记录
//        $user = $this->getOpenId();
//        $integral = Integral::where('openId', $user['openId'])
//            ->where('is_active', 1)
//            ->orderBy('timestamp', 'desc')
//            ->first();
//        if ($integral) {
//            $is_first = 0;
//        } else {
//            $is_first = 1;
//        }
//        $data = ['article' => $article, 'video' => $video, 'label1' => $label, 'text' => $text, 'top' => $top, 'time' => $time, 'endtime' => $endtime, 'is_end' => $is_end, 'is_first' => $is_first];
        $data = ['article' => $article, 'video' => $video, 'label1' => $label, 'text' => $text, 'top' => $top];
        return view('wechat.education.index')->with($data);
    }

    /**
     * 更多选择页
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function all()
    {
        $type = Input::get('type');
        $data = [];
        $label = $this->rmTag()['label1'];
        $tag = $this->getLable();
        foreach ($tag as $k => $v) {
            $data[] = [
                'id' => $k,
                'name' => $v
            ];
        }
        //重置阅读时间
//        $this->pageTime();
        return view('wechat.education.all')->with(['data' => $data, 'label' => $label, 'type' => $type]);
    }

    /**
     * 视频列表
     * @return mixed
     */
    public function videoList()
    {
        $res = $this->rmTag();
        $article = $res['article'];
        $video = $res['video'];
        $label = $res['label1'];
        $text = '热门';
        $param = Input::all();
        if (isset($param['id'])) {
            //标签搜索
            $searchData = $this->getData($param['type'], $param['id']);
            $article = $searchData['article'];
            $video = $searchData['video'];
            if ($param['id'] == 'all') {
                $text = '全部';
            } else {
                $text = $this->getLable()[$param['id']];
            }

        }
        if (isset($param['search'])) {
            //输入框内容搜索
            $searchData = $this->getData($param['type'], null, $param['search']);
            $article = $searchData['article'];
            $video = $searchData['video'];
            $text = $param['search'];
        }
        //重置阅读时间
//        $this->pageTime();
        $data = ['article' => $article, 'video' => $video, 'label1' => $label, 'text' => $text, 'all' => $video];
        return view('wechat.education.videoList')->with($data);
    }

    /**
     * 视频搜索ajax
     * @return \Illuminate\Http\JsonResponse
     */
    public function videoSearch()
    {
        $data = Input::all();
        $query = Education::where('is_active', 1)->where('status', 1)->where('type', 2)->orderBy('read', 'desc');
        $res = $this->rmTag();
        $label2 = $res['label1'];
        $video = [];
        $allVideo = $query->get();
        $text = '';
        if (isset($data['search'])) //内容搜索
        {
            $text = $data['search'];
            $label = Label::where('name', 'like', '%' . $data['search'] . '%')->where('is_active', 1)->get();
            $label_id = [];
            foreach ($label as $v) {
                $label_id[] = $v['_id'];
            }
            $all = $query->get();
            $tmp1 = [];
            foreach ($all as $v) {
                foreach ($v['type_id'] as $v1) {
                    if (in_array($v1, $label_id)) {
                        $tmp1[$v['_id']] = $v;
                    }
                }
            }
            $query = $query->orWhere('name', 'like', '%' . $data['search'] . '%')->orWhere('description', 'like', '%' . $data['search'] . '%');
            $video = $query->get();
            foreach ($video as $v) {
                if (!isset($tmp1[$v['_id']])) {
                    $tmp1[$v['_id']] = $v;
                }
            }
            $video = $tmp1;
        }
        if (isset($data['id'])) //标签搜索
        {
            if ($data['id'] !== 'all') {
                $all = $query->get();
                $tmp = [];
                foreach ($all as $v) {
                    if (in_array(intval($data['id']), $v->type_id)) {
                        $tmp[] = $v;
                    }
                }
                $video = $tmp;
                $text = Label::find(intval($data['id']))->name;
            } else {
                $video = $query->get();
                $text = '全部';
            }
        }
        foreach ($video as $v) {
            $v->image = $v->image == '' ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : Storage::disk('s3-public')->url($v->image);
            $type_id = $v->type_id;
            $type_tmp = [];
            foreach ($type_id as $k => $v1) {
                $type_tmp[$k] = $this->getLable()[$v1];
            }
            $v->type_id = $type_tmp;
        }
        foreach ($allVideo as $v) {
            $v->image = $v->image == '' ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : Storage::disk('s3-public')->url($v->image);
            $type_id = $v->type_id;
            $type_tmp = [];
            foreach ($type_id as $k => $v1) {
                $type_tmp[$k] = $this->getLable()[$v1];
            }
            $v->type_id = $type_tmp;
        }
        //重置阅读时间
//        $this->pageTime();
        $data = ['video' => $video, 'label1' => $label2, 'text' => $text, 'all' => $allVideo];
        return view('wechat.education.videoList')->with($data);
    }

    public function videoMore()
    {
        return view('wechat/education/more');
    }

    /**
     * 视频详情页
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function video()
    {
        header("Access-Control-Allow-Origin: *");
        $id = Input::get('id');
        $video = Education::find($id);
        $video->read = $video->read + 1;
        $video->save();
        $type = Label::all();
        $type_name = [];
        foreach ($type as $v) {
            $type_name[$v['id']] = $v['name'];
        }
        $tmp = [];
        foreach ($video['type_id'] as $k => $v) {
            $tmp[$k] = $type_name[$v];
        }
        $video['type_id'] = $tmp;
        $video->image = $video->image == '' ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : Storage::disk('s3-public')->url($video->image);
        //推荐
        $user = $this->getOpenId();
        $like = Like::where('is_active', 1)
            ->where('openId', $user['openId'])
            ->where('source_id', $id)
            ->where('type', 2)
            ->first();
        if (!is_null($like)) {
            $tmp = $like->like_status;
            $collect = $like->collect_status;
            $like = $tmp;
        } else {
            $like = 0;
            $collect = 0;
        }
        //重置阅读时间
//        $this->pageTime();
        return view('wechat.education.video')->with(['data' => $video, 'user' => $user, 'like' => $like, 'collect' => $collect]);
    }

    /**
     * 文章详情页
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function article()
    {
        $id = Input::get('id');
        $article = Education::find($id);
        $article->read = $article->read + 1;
        $article->save();
        $user = $this->getOpenId();
        $likeRow = Like::where('openId', $user['openId'])->where('type', 1)->where('source_id', $id)->where('is_active', 1)->first();
        if ($likeRow) {
            $like = $likeRow->like_status;
            $collect = $likeRow->collect_status;
        } else {
            $like = 0;
            $collect = 0;
        }
        $js = InterfaceController::js();
        //重置阅读时间
//        $this->pageTime();
        //阅读积分
        $data['type'] = 7;
        $data['math'] = 0;
        $data['openId'] = $this->getOpenId()['openId'];
        $data['num']    = 1;
        $data['description'] = '阅读文章加分';
        $res = Integral::add($data);
        return view('wechat.education.article')->with(['data' => $article, 'user' => $user, 'like' => $like, 'collect' => $collect, 'js' => $js]);
    }

    /**
     * 赞、收藏
     * @return \Illuminate\Http\JsonResponse
     */
    public function like()
    {
        $data = Input::get('data');
        $data['type'] = intval($data['type']);
        $openId = $data['openId'];
        $source_id = $data['source_id'];
        $type = $data['type'];
        $like = Like::where('openId', $openId)->where('type', $type)->where('source_id', $source_id)->first();
        if ($like) {
            if ($data['status_type'] == 1) {
                $data['like_status'] = $data['status'] == 0 ? 1 : 0;
                $res = $data['like_status'];
                $like->like_status = $data['like_status'];
            } else {
                $data['collect_status'] = $data['status'] == 0 ? 1 : 0;
                $res = $data['collect_status'];
                $like->collect_status = $data['collect_status'];
            }
            $like->save();
        } else {
            if ($data['status_type'] == 1) {
                $data['like_status'] = $data['status'] == 0 ? 1 : 0;
                $data['collect_status'] = 0;
                $res = $data['like_status'];
            } else {
                $data['collect_status'] = $data['status'] == 0 ? 1 : 0;
                $data['like_status'] = 0;
                $res = $data['collect_status'];
            }
            $row = Like::add($data);
        }
        return response()->json(['status' => 0, 'data' => $res]);
    }

    /**
     * 获取所有标签
     * @return array
     */
    private function getLable()
    {
        $type = Label::all();
        $type_name = [];
        foreach ($type as $v) {
            $type_name[$v['id']] = $v['name'];
        }
        return $type_name;
    }

    /**
     * 获取默认页内容和热门标签
     * @return array
     */
    private function rmTag()
    {
        $type_name = $this->getLable();
        $article = Education::where('is_active', 1)->where('type', 1)->where('status', 1)->orderBy('top','desc')->orderBy('created_at','desc')->offset(0)->limit(10)->get();
        $video = Education::where('is_active', 1)->where('type', 2)->where('status', 1)->orderBy('top','desc')->orderBy('created_at','desc')->offset(0)->limit(10)->get();
        $label1 = [];
        foreach ($article as $v) {
            $tmp1 = [];
            $v->image = $v->image == '' ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : Storage::disk('s3-public')->url($v->image);
            foreach ($v['type_id'] as $v2) {
                $label1[] = $v2;
            }
            foreach ($v['type_id'] as $k => $v1) {
                $tmp1[$k] = $type_name[$v1];
            }
            $v['type_id'] = $tmp1;
        }
        foreach ($video as $v) {
            $tmp1 = [];
            $v->image = $v->image == '' ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : Storage::disk('s3-public')->url($v->image);
            foreach ($v['type_id'] as $v2) {
                $label1[] = $v2;
            }
            foreach ($v['type_id'] as $k => $v1) {
                $tmp1[$k] = $type_name[$v1];
            }
            $v['type_id'] = $tmp1;
        }
        $label1 = array_unique($label1);
        $tmp1 = [];
        foreach ($label1 as $v) {
            $tmp1[] = [
                'id' => $v,
                'name' => $type_name[$v]
            ];
        }
        $data = [
            'article' => $article,
            'video' => $video,
            'label1' => $tmp1,
        ];
        return $data;
    }

    private function getData($type, $id = null, $searchText = null)
    {
        $typeName = $this->getLable();
        $query = Education::where('is_active', 1)->where('status', 1)->orderBy('top','desc')->orderBy('read', 'desc')->orderBy('created_at','desc');
        $res = [];
        //热门标签搜索
        if ($id) {
            $edu = $query->get();
            if ($id !== 'all') {
                foreach ($edu as $v) {
                    if (in_array($id, $v['type_id'])) {
                        $res[] = $v;
                    }
                }
            }
            if ($id !== 'all') {
                $search = $typeName[$id];
            }
        }
        //内容搜索
        if ($searchText) {
            $all = $query->get();
            foreach ($all as $v) {
                $bool = false;
                foreach ($v['type_id'] as $k => $v1) {
                    //标签查找
                    if (strpos($typeName[$v1], $searchText) !== false) {
                        $bool = true;
                    }
                }
                if ($bool) $res[] = $v;
            }
            $search = $searchText;
        }

        if ($id == 'all') {//搜索全部
            $edu = $query->get();
        } else {
            $query = $query->orwhere('name', 'like', '%' . $search . '%')
                ->orWhere('description', 'like', '%' . $search . '%');
            $edu = $query->get();
        }
        //合并内容
        foreach ($edu as $v) {
            $res[] = $v;
        }
        $test = [];
        $id = [];
        foreach ($res as $v) {
            $tmp = [];
            if (!in_array($v['_id'], $id)) {
                $v->image = $v->image == '' ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : Storage::disk('s3-public')->url($v->image);
                foreach ($v['type_id'] as $k => $v1) {
                    $tmp[$k] = $typeName[$v1];
                }
                $v['type_id'] = $tmp;
                $test[] = $v;
                $id[] = $v['_id'];
            }
        }
        $res = $test;
        unset($test);
        $article = [];
        $video = [];
        foreach ($res as $v) {
            if ($v['type'] == 1) {
                $article[] = $v;
            } else if ($v['type'] == 2) {
                $video[] = $v;
            }
        }
        $data = [
            'article' => $article,
            'video' => $video
        ];
        return $data;
    }

    private function getTime()
    {
        $time = time();
        if (Session::has('is_end')) {
            $endtime = $time;
            $is_end = 1;
        } else {
            Session::put('timestamp', $time);
            $is_end = 0;
            if (Session::has('timestamp') && Session::has('endtime')) {
                $endtime = Session::get('endtime');
            } else {
                $endtime = $time + 60 * 10;
                Session::put('endtime', $endtime);
            }
        }
        return ['timestamp' => $time, 'endtime' => $endtime, 'is_end' => $is_end];
    }

    /**
     * 重置时间
     * @return \Illuminate\Http\JsonResponse
     */
    public function setTime()
    {
        $is_end = Input::get('is_end');
        if ($is_end == 1) {
            Session::forget('timestamp');
            Session::forget('endtime');
            Session::put('is_end', 1);
        } else {
            $time = time();
            Session::put('timestamp', $time);
        }
        return response()->json(['status' => 0]);
    }

    /**
     * 积分领取
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveIntegral()
    {
        $user = $this->getOpenId();
        $integral = DB::table('integral')
            ->where('openId', $user['openId'])
            ->where('is_active', 1)
            ->orderBy('timestamp', 'desc')
            ->first();
        $data = [];
        $data['num'] = 5;
        if ($integral) {
            if (date('Y-m-e', $integral['timestamp']) == date('Y-m-d', time())) {
                $data['num'] = 10;
            }

        }
        $data['openId'] = $user['openId'];
        $data['type'] = 7;
        $data['description'] = '持续阅读时长奖励';
        $data['math'] = 0;
        $res = Integral::add($data);
        if ($res) {
            $time = time();
            $endtime = $time + 60 * 30;
            Session::put('timestamp', $time);
            Session::put('endtime', $endtime);
            Session::forget('is_end');
            return response()->json(['status' => 0]);
        }
    }

    /**
     * 跳页重置时间
     * @return bool
     */
    private function pageTime()
    {
        if (!(Session::has('is_end') && Session::get('is_end') == 1)) {

            $time = time();
            if (Session::has('timestamp') && Session::has('endtime')) {
                Session::put('timestamp', $time);
            } else {
                $endtime = $time + 60 * 10;
                Session::put('timestamp', $time);
                Session::put('endtime', $endtime);
                Session::forget('is_end');
            }
        }
        return true;
    }

    private function getOpenId()
    {
        $session = Session::get('wechat')['oauth_user']['default'];
        $str = 'http://thirdwx.qlogo.cn/mmopen/';
        $user = [
            'openId' => $session['id'],
            'name' => $session['nickname'],
            'avatar' => str_replace($str, '', $session['avatar'])
        ];
        return $user;
    }
}
