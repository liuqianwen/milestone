<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\Pay\Payment;
use App\Model\Enroll;
use App\Model\EnrollData;
use App\Model\EnrollOrder;
use function Composer\Autoload\includeFile;
use Faker\Provider\Person;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class SignController extends Controller
{
    public function index()
    {
        $data = Enroll::where('is_active',1)->where('status',1)->orderBy('updated_at','desc')->get();
        foreach ($data as $v) {
            $signData = EnrollData::where('is_active',1)->where('status',1)->where('member',$this->init())->where('source_id',$v->_id)->get();
            $un_pay = EnrollData::where('is_active',1)->where('status',0)->where('pay_type',2)->where('member',$this->init())->where('source_id',$v->_id)->count();
            if (count($signData) > 0) {
                $v->signed = 1;
            }else {
                $v->signed = 0;
            }
            if ($un_pay > 0) {
                $v->un_pay = 1;
            }else {
                $v->un_pay = 0;
            }
            $v->image = $v->image == '' ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : Storage::disk('s3-public')->url($v->image);
        }
        $js = \App\Http\Controllers\Weixin\InterfaceController::js();
        return view('wechat.sign.index')->with(['data' => $data,'js' => $js]);
    }
    public function detail($id)
    {
        $detail = Enroll::find($id);
        if ($detail) {
            $detail->image = $detail->image = $detail->image == '' ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : Storage::disk('s3-public')->url($detail->image);
            return view('wechat.sign.detail')->with(['data' => $detail]);
        }else {
            return '无权限';
        }
    }
    public function form($id)
    {
//        if($id == '5d91714f7633790a207d3b74' || $id == '5d91734a7633793ab6581f3a'){
//            exit('无权限进入');
//        }
//        exit;
        $detail = Enroll::find($id);
        if ($detail) {
            $form = $detail->form;
            $js = \App\Http\Controllers\Weixin\InterfaceController::js();
            $data = [
                'form' => $form,
                'id' => $id,
                'is_pay' => $detail->is_pay,
                'pay_type' => $detail->pay_type,
                'price' => $detail->pay_price,
                'js' => $js
            ];
            return view('wechat.sign.form')->with($data);
        }else {
            return '无权限';
        }
    }
    public function list($id)
    {
        $tmp = explode(',',$id);
        $price = 0;
        $enroll = Enroll::find($tmp[0]);
        $query = EnrollData::where('source_id',$tmp[0])->where('is_active',1)->where('member',$this->init());
        if ($tmp[1] == 1) {
            $title = '已报名';
            $query = $query->where('status',1);
        }else if($tmp[1] == 2){
            $title = '待确认';
            $query = $query->where('status',0)->where('pay_type',2);
        }
        $data = $query->get();
        if ($enroll->is_pay) {
            $price = $enroll->pay_price;
        }
        $num = count($data);
        $length = [];
        $lengthTotal = 0;
        foreach ($enroll->form as $v) {
           $length[$v['number']] =  ($v['length'] === null) ? 8 : $v['length'];
           $lengthTotal += ($v['length'] === null) ? 8 : $v['length'];
        }
        $res = [
            'id'   => $tmp[0],
            'data' => $data,
            'num'  => $num,
            'price' => $price,
            'totalPrice'  => $num * $price,
            'is_ticket'   => $enroll->is_pay,
            'length'      => $length,
            'lengthTotal' => $lengthTotal,
            'title'       => $title
        ];
        return view('wechat.sign.signedList')->with($res);
    }
    public function add()
    {
//        exit;
        $data = Input::get('data');
        $id   = Input::get('id');
        $price = Input::get('price');
        $num   = Input::get('num');
        $ticket = Input::get('ticket');//发票信息
        $pay_type = Input::get('pay_type');
        $member = $this->init();
        $enroll = Enroll::find($id);
        $res   = '';
        /*保存订单*/
        $order = [
            'num' => $num,
            'price' => $price,
            'total_price' => $num * $price,
            'source_id'   => $id,
            'member'      => $member,
            'wx_order'    => '',
            'pay_ticket'  => $ticket,
            'pay_type'    => $enroll->is_pay ? intval($pay_type) : 0
        ];
        if (!$enroll->is_pay) $order['status'] = 1;
        $order_id = EnrollOrder::add($order);
        /*保存数据*/
        foreach ($data as $v) {
            $v['source_id'] = $id;
            $v['order_id']  = $order_id['_id'];
            $v['order_number'] = $order_id['order_number'];
            $v['member']    = $member;
            $v['pay_type']  = intval($pay_type);
            if (!$enroll->is_pay) $v['status'] = 1;
            $res = EnrollData::add($v);
        }
        if ($enroll->is_pay && $pay_type == 1) {
            $dns = Config::get('dns');
//        if ($enroll)
            $attributes = [
                'body' => '报名费',
                'detail' => $enroll->name,
                'out_trade_no' => $order_id['_id'],
                'trade_type' => 'JSAPI',
                'openid' => $this->init(),
                'total_fee' => floatval($num * $price * 100),
//            'total_fee'  => 1,
                'notify_url' => $dns . '/weixin/notify', // 支付结果通知网址，如果不设置则会使用配置里的默认地址
            ];
            $json = Payment::pay($attributes);
            return response()->json(['status' => 0, 'pay' => $json]);
        }else {
            return response()->json(['status' => 0]);
        }
    }

    public function ticket()
    {
        $id = Input::get('id');
        Log::info($id);
        Log::info($this->init());
        $orders = EnrollOrder::where('member',$this->init())->where('source_id',$id)->where('status',1)->where('is_active',1)->get();
        if(is_null($orders)){
            $orders = EnrollOrder::where('member',$this->init())->where('source_id',$id)->where('pay_type',2)->where('is_active',1)->orderBy('created_at','desc')->get();
        }
        Log::info($orders);
        $data = [];
        $id = isset($orders[0]) ? $orders[0]->_id : '';
        $time = isset($orders[0]) ? ($orders[0]->pay_time ?  date('Y-m-d H:i',strtotime($orders[0]->pay_time)) : '') :'';
        $is_time = false;
        foreach ($orders as $v) {
            $ticket = $v->pay_ticket;
            if ($ticket['head'] || $ticket['number'] || $ticket['name'] || $ticket['money'] || $ticket['phone'] || $ticket['email']) {
                if (!$id) $id = $v->_id;
                if (!$is_time) {
                    $time = date('Y-m-d H:i',$v->pay_time ? $v->pay_time : strtotime($v->updated_at));
                    $is_time = true;
                }
                $data[] = [
//                    'time' => date('Y-m-d H:i',$v->pay_time),
                    'ticket' => $ticket,
                ];
            }
        }
        return view('wechat.sign.signedTicket')->with(['data' => $data,'id' => $id,'time' => $time]);
    }

    public function ticketSave()
    {
        $id = Input::get('id');
        $data = Input::get('ticket');
        $order = EnrollOrder::find($id);
        $status = 1;
        if ($data['number'] && $data['head'] && $data['money']) {
            $order->pay_ticket = $data;
            $res = $order->save();
            $status  = 0;
        }
        return response()->json(['status' => $status]);
    }

    private function init()
    {
        $openId = Session::get('wechat.oauth_user.default')->original['openid'];
        return $openId;
    }
}
