<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\Pay\Payment;
use App\Model\Enroll;
use App\Model\EnrollData;
use App\Model\EnrollOrder;
use function Composer\Autoload\includeFile;
use Faker\Provider\Person;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Weixin\InterfaceController;

class EnrollSignController extends Controller
{
    public function index()
    {
        $js = InterfaceController::js();
        return view('wechat.enrollSign.index')->with(['js' => $js]);
    }

    public function result()
    {
        $js = InterfaceController::js();
        return view('wechat.enrollSign.result')->with(['js' => $js]);
    }
    private function init()
    {
        $openId = Session::get('wechat.oauth_user.default')->original['openid'];
        return $openId;
    }
}
