<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\Weixin\CodeController;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Education;
use App\Model\Integral;
use App\Model\Member;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Session;

class PersonController extends Controller
{
    private function init()
    {
        $openId = session::get('wechat.oauth_user.default')->original['openid'];
        return $openId;
    }

    /**
     * @return $this
     * 个人中心页面
     */
    public function index()
    {
        $member = Member::find($this->init());
        $top = Education::where('is_active',1)->where('type',1)->orderBy('read','desc')->limit(2)->get();
        foreach ($top as $v) {
            $v['image'] = $v->image == '' ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : Storage::disk('s3-public')->url($v->image);
        }
        //积分
        $integral = Integral::where('is_active',1)->where('openId',$this->init())->get();
        $num = 0;
        foreach($integral as $v){
            if($v->math == 0){
                $num += $v->num;
            }else{
                $num -= $v->num;
            }
        }

        return view('wechat.person.index')->with(['member'=>$member,'data'=>$top,'num'=>$num]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * 跳转商城前的加载页面
     */
    public function load()
    {
        $openId = $this->init();
        return view('wechat.person.load')->with(['openId'=>$openId]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * 个人信息页面
     */
    public function information()
    {
        $member = Member::find($this->init());
        return view('wechat.person.information')->with(['member'=>$member]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * 个人资料保存
     */
    public function save()
    {
        $data = Input::get('data');
        $member = Member::find($this->init());
        if($member->name == ''){
            $arr['type'] = 3;
            $arr['openId'] = $this->init();
            $arr['num'] = 20;
            $arr['math'] = 0;
            $arr['description'] = '完善个人资料奖励';
            Integral::add($arr);
        }
        $member->name = $data['name'];
        $member->sex = $data['sex'];
        $member->birthday = $data['birthday'];
        $member->save();
        return response()->json(['status'=>0]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * 捐款页面
     */
    public function donation()
    {
        //积分
        $integral = Integral::where('is_active',1)->where('openId',$this->init())->get();
        $num = 0;
        foreach($integral as $v){
            if($v->math == 0){
                $num += $v->num;
            }else{
                $num -= $v->num;
            }
        }
        return view('wechat.person.donation')->with(['num'=>$num]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * 每日签到页面
     */
    public function sign()
    {
        $status = 0;
        $start = mktime(0,0,0,date('m'),1,date('Y'));
        $end = mktime(23,59,59,date('m'),date('t'),date('Y')) + 86400;
        $inter = Integral::where('is_active',1)->where('openId',$this->init())->where('type',1)->whereBetween('timestamp',[$start,$end])->get();
        $day = [];
        foreach($inter as $v){
            $day[] = date('d',$v->timestamp) - 1;
            if(date('d',$v->timestamp) == date('d')){
                $status = 1;
            }
        }
        //连续签到
        $lianxun = Integral::where('is_active',1)->where('openId',$this->init())->where('type',1)->whereBetween('timestamp',[$start,$end])->orderBy('timestamp','desc')->first();
        $data['lianxu'] = isset($lianxun->day) ? $lianxun->day : 0;
        //本月签到
        $data['benyue'] = count($inter);
        //总共签到数
        $all_integral = Integral::where('is_active',1)->where('openId',$this->init())->where('type',1)->orderBy('timestamp','desc')->get();
        $data['zong'] = count($all_integral);
        //签到累计奖励
        $num = 0;
        foreach($all_integral as $v){
            $num += $v->num;
        }
        $data['num'] = $num;

        return view('wechat.person.sign')->with(['day'=>\GuzzleHttp\json_encode($day),'data'=>$data,'record'=>$all_integral,'status'=>$status]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * 签到保存数据
     */
    public function signAdd()
    {
        $inter = Integral::where('is_active',1)->where('openId',$this->init())->where('type',1)->orderBy('timestamp','desc')->first();
        $tmp['lianxu'] = 0;
        $tmp['benyue'] = 1;
        $tmp['zong'] = 1;
        $tmp['leiji'] = 10;
        if(is_null($inter)){
            $arr['day'] = 1;
            $arr['num'] = 10;
        }else{
            //判断今天是否签到
            if(date('Ymd',$inter->timestamp) == date('Ymd',time())) return response()->json(['status'=>1]);
            //判断是否同一自然月
            if(date('Ym',$inter->timestamp) == date('Ym',time())){
               //判断是否是连续签到
               if(date('Ymd',$inter->timestamp) == date('Ymd',strtotime('-1 day'))){
                   $arr['day'] = $inter->day + 1;
                   $arr['num'] = $inter->num + 3;
                   $tmp['lianxu'] = 1;
                   $tmp['leiji'] = $inter->num + 3;
                   switch($inter->day + 1){
                       case 7:
                           $extra['num'] = 50;
                           $tmp['leiji'] += 50;
                           break;
                       case 15:
                           $extra['num'] = 100;
                           $tmp['leiji'] += 100;
                           break;
                       case 30:
                           $extra['num'] = 150;
                           $tmp['leiji'] += 150;
                           break;
                   }
                   if(isset($extra)){
                       $extra['type'] = 2;
                       $extra['openId'] = $this->init();
                       $extra['math'] = 0;
                       $extra['description'] = '连续签到满'.($inter->day + 1).'天额外奖励';
                       Integral::add($extra);
                   }
               }else{
                   $arr['day'] = 1;
                   $arr['num'] = 10;
                 }
            }else{
                $arr['day'] = 1;
                $arr['num'] = 10;
            }
        }
        $arr['type'] = 1;
        $arr['openId'] = $this->init();
        $arr['math'] = 0;
        $arr['description'] = '每日签到奖励';
        Integral::add($arr);
        $arr['date'] = date('Y-m-d H:i:s',time());
        $data['everyday'] = $arr;
        if(isset($extra)){
            $extra['date'] = date('Y-m-d H:i:s',time());
            $data['extra'] = $extra;
        }
        return response()->json(['status'=>0,'data'=>$data,'tmp'=>$tmp]);
    }


    public function record()
    {
        $data = Integral::where('is_active',1)->where('openId',$this->init())->orderBy('timestamp','desc')->get();
        $num = 0;
        foreach($data as $v){
            if($v->math == 0){
                $num += $v->num;
            }else{
                $num -= $v->num;
            }
        }
        return view('wechat.person.record')->with(['data'=>$data,'num'=>$num]);
    }



}
