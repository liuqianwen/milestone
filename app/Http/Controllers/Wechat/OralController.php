<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Education;
use App\Model\Label;
use App\Model\Like;
use EasyWeChat\Factory;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;


class OralController extends Controller
{
    public function index()
    {
        return view('wechat.follow_up.survey');
    }
}