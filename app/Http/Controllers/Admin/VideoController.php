<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\S3Base;
use App\Model\Education;
use App\Model\Label;
use App\Model\Video;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    public function index()
    {
        $label = Label::where('status',1)->where('is_active',1)->get();
        return view('admin.video')->with(['label'=>$label]);
    }
    public function data()
    {
        $limit = intval(Input::get('limit'));
        $offset = intval(Input::get('offset'));
        $search = Input::get('search');
        $where = ['type'=>2];
        $data = Education::getData($limit,$offset,$search,$where);
        $rows = [];
        $status          = ['是','否'];
        foreach  ($data['rows'] as $val) {
            $arr         = [];
            $arr['id']   = $val->_id;
            $arr['title'] = $val->name;
            $image = $val->image == '' ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : Storage::disk('s3-public')->url($val->image);
            $arr['image'] = '<img src="'.$image.'" alt="" style="width:80px" />';
            $arr['img'] = $val->image;
            $arr['imgUrl'] = $image;
            $arr['description'] = $val->description;
            $arr['url'] = $val->video_url;
            $arr['type_id'] = $val->type_id;
            $arr['status'] = $status[$val->status];
            $arr['top'] = $val->top == 1 ? '置顶' : '正常';
            $arr['tops'] = $val->top;
            $arr['created_at'] = date('Y/m/d', strtotime($val->created_at));
            $rows[] = $arr;
        }
        return response()->json(array('total' => $data['total'], 'rows' => $rows));

    }
    public function add(Request $request)
    {
        $data = json_decode($request->data, true);
        Education::add($data);
        return response()->json(['say' => 'ok']);
    }
    public function edit(Request $request)
    {
        $data = json_decode($request->data, true);
        $video = Education::find($data['id']);
        if (isset($data['status'])){
            if ($data['status'] == 1) {
                $video->status = 0;
            }else {
                $video->status = 1;
            }
        }else{
            $video->name = $data['title'];
            $video->video_url = $data['url'];
            $video->type_id = $data['label'];
            $video->description = $data['introduce'];
            $video->image = $data['image'];
        }
        $video->save();
        return response()->json(['say' => 'ok']);
    }

    public function top(Request $request)
    {
        $data = json_decode($request->data, true);
        $video = Education::find($data['id']);
        if($video->top == 1){
            $video->top = 0;
        }else{
            $video->top = 1;
        }
        $video->save();
        return response()->json(['say' => 'ok']);
    }

}
