<?php

namespace App\Http\Controllers\Admin;

use App\Model\Education;
use App\Model\Hospital;
use App\Model\Label;
use App\Model\Volunteer;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class VolunteerController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $hospital = Hospital::where('status',1)->where('is_active',1)->get();
        return view('admin.volunteer')->with(['hospital'=>$hospital]);
    }


    /**
     * 数据
     */
    public function data()
    {
        $limit = intval(Input::get('limit'));
        $offset = intval(Input::get('offset'));
        $search = Input::get('search');
        $where = [];
        $data = Volunteer::getData($limit,$offset,$search,$where);
        $rows = [];
        $statusArr = ['停用', '启用'];
        foreach ($data['rows'] as $val) {
            $arr = array();
            $arr['id'] = $val->_id;
            $arr['name'] = $val->name;
            $arr['description'] = $val->description;
            $arr['content'] = $val->content;
            $arr['type_id'] = $val->hospital_id;
            $arr['image'] = $val->image;
            $arr['imgUrl'] = $val->image == '' ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537254584770&di=0400b2dea6923ae9928287d5b935dec3&imgtype=0&src=http%3A%2F%2Fbpic.wotucdn.com%2F15%2F93%2F69%2F15936965-806042199ced6a02ee158d0cfc2d0124-1.jpg' : Storage::disk('s3-public')->url($val->image);
            $arr['content'] = $val->content;
            $arr['status'] = $statusArr[$val->status];
            $arr['createTime'] = date('Y/m/d', strtotime($val->created_at));
            $rows[] = $arr;
        }
        return response()->json(array('total' => $data['total'], 'rows' => $rows));
    }


    /**
     * 添加和修改
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save( Request $request )
    {
        $input = json_decode($request->data, true);
        if ( empty($input['id']) ) {
            $cms = new Volunteer();
            $cms->timestamp = time();
            $cms->status = 1;
            $cms->is_active = 1;
        } else {
            $cms = Volunteer::find($input['id']);
            if(is_null($cms)){
                $cms = new Volunteer();
                $cms->timestamp = time();
                $cms->status = 1;
                $cms->is_active = 1;
            }
        }
        $cms->hospital_id = array_unique($input['oldLabel']);
        $cms->name        = $input['title'];
        $cms->image       = $input['image'];
        $cms->description = $input['description'];
        $cms->content     = str_replace(PHP_EOL, '', $input['content']);
        $cms->save();
        return response()->json(['status' => 0, 'message' => '添加成功', 'data' => ['id' => $cms->_id]]);
    }


    public function edit(Request $request)
    {
        $data = json_decode($request->data, true);
        $volunteer= Volunteer::find($data['id']);
        if ($data['status'] == 1) {
            $volunteer->status = 0;
        }else {
            $volunteer->status = 1;
        }
        $volunteer->save();
        return response()->json(['say' => 'ok']);
    }


}
