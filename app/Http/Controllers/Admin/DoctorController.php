<?php

namespace App\Http\Controllers\Admin;
use App\Model\Doctor;
use App\Model\Hospital;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

/**
 * 医生管理
 * Class DoctorController
 * @package App\Http\Controllers\Admin
 */
class DoctorController extends Controller
{
    /**
     * 医生管理空页面
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $id = Input::get('id');
        if(!isset($id)){
            $id = '';
        }
        return view('admin.doctor')->with(['id'=>$id]);
    }


    /**
     * 数据
     */
    public function data()
    {
        $hospital = Hospital::where('is_active',1)->get();
        $hName = [];
        foreach($hospital as $v){
            $hName[$v->_id] = $v;
        }
        $limit = intval(Input::get('limit'));
        $offset = intval(Input::get('offset'));
        $search = Input::get('search');
        $id = Input::get('id');
        if($id){
            $where = ['hospital_id'=>$id];
        }else{
            $where = [];
        }
        $data = Doctor::getData($limit,$offset,$search,$where);
        $rows = [];
        $statusArr = ['停用', '正常'];
        foreach ($data['rows'] as $val) {
            $arr = array();
            $arr['id'] = $val->_id;
            $arr['hospital'] = $hName[$val->hospital_id]->name;
            $arr['name'] = $val->name;
            $arr['department'] = $val->department;
            $arr['position'] = $val->position;
            $arr['image'] = $val->headimg;
            $arr['imgUrl'] = $val->headimg == '' ? 'https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/sfy/noHeadImg.jpg' : Storage::disk('s3-public')->url($val->headimg);
            $arr['tel'] = $val->tel;
            $arr['good'] = $val->good;
            $arr['introduce'] = $val->introduce;
            $arr['status'] = $statusArr[$val->status];
            $arr['hospital_status'] = $statusArr[$hName[$val->hospital_id]->status];
            $arr['createTime'] = date('Y/m/d', strtotime($val->created_at));
            $rows[] = $arr;
        }
        return response()->json(array('total' => $data['total'], 'rows' => $rows));
    }


    public function add(Request $request)
    {
        $data = json_decode($request->data, true);
        $id = Doctor::add($data);
        if($id){
            return response()->json(['status'=>0]);
        }else{
            return response()->json(['status'=>1]);
        }

    }

    /**
     * 医生管理编辑
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit()
    {
        $doctorId = Input::get('id');
        $doctor   = Doctor::find($doctorId);
        if ( is_null($doctor) ) return response()->json([ 'say' => 'no', 'status' => 1, 'message' => '医生不存在！' ]);
        $data = Input::get('data');
        Doctor::edit($doctorId, $data);
        return response()->json([ 'say' => 'no', 'status' => 0, 'message' => '更新成功！' ]);
    }

}
