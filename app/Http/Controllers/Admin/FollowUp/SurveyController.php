<?php

namespace App\Http\Controllers\Admin\FollowUp;

use App\Base\SurveyBase;
use App\Model\FollowUp\Survey;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;


class SurveyController extends Controller
{
    public function index()
    {

        return view('admin.follow_up.survey');
    }

    public function autoComplete()
    {
        $q    = Input::get('q');
        $type = Type::where('name', 'like', '%' . $q . '%')->where('is_active', 1)->where('type', 1)->where('institution_id', Auth::user()->institution_id)->get();
        $data = [];
        foreach ( $type as $v ) $data[] = $v->name;

        return response()->json($data);
    }

    public function data()
    {
        $data   = Input::all();
        $result = SurveyBase::data($data);

        return $result;
    }

    public function templateList()
    {
        $result = SurveyBase::templateList();

        return $result;
    }

    public function template()
    {
        $id     = Input::get('id');
        $result = SurveyBase::template($id);

        return $result;
    }

    public function editGet()
    {
        $id     = Input::get('id');
        $result = SurveyBase::editGet($id);

        return $result;

    }

    public function stop()
    {
        $id     = Input::get('id');
        $result = SurveyBase::stop($id);

        return $result;
    }

    public function save()
    {
        $data = Input::get('data');
        if ( isset($data['id']) ) {
            $result = SurveyBase::edit($data);
        } else {
            $result = SurveyBase::add($data);
        }

        return $result;
    }

    public function preview()
    {
        $data = Input::get('data');
        $result = SurveyBase::preview($data);
        return $result;
    }

    public function classPreview(){
        $data = Input::get('data');
        $result = SurveyBase::classPreview($data);
        return $result;
    }

    public function previewTwo()
    {
        $id = Input::get('id');
        $result = SurveyBase::previewTwo($id);
        return $result;
    }
    public function classpreviewTwo()
    {
        $id = Input::get('id');
        $result = SurveyBase::classpreviewTwo($id);
        return $result;
    }

}
