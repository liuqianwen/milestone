<?php

namespace App\Http\Controllers\Admin\FollowUp;

use App\Base\InformationBase;
use App\Model\FollowUp\Information;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;


class InformationController extends Controller
{
    public function index()
    {
        return view('admin.follow_up.information');
    }

    public function data()
    {
        $data = Input::all();
        $result = InformationBase::data($data);
        return $result;
    }

    public function search()
    {
        $result = InformationBase::search();
        return $result;
    }

    public function add()
    {
        $data = Input::get('postdata');
        $data['type'] = (int)$data['type'];
        if($data['type'] != 1) $data['option'] = [];
        $result = InformationBase::add($data);
        return response()->json(array('say'=>'ok'));
    }

    public function edit()
    {
        $id = Input::get('id');
        $status = intval(Input::get('status'));
        $information = Information::find($id);
        $information->status = $status;
        $information->save();
        return response()->json(array('say'=>'ok'));
    }
}
