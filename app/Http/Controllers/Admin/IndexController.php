<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * 返回首页内容和初始化一些页面元素
     *
     * @return $this
     */
    public function index()
    {

        return view('admin.index');
    }
}
