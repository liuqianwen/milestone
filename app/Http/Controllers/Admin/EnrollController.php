<?php

namespace App\Http\Controllers\Admin;

use App\Model\Education;
use App\Model\Enroll;
use App\Model\EnrollData;
use App\Model\EnrollOrder;
use App\Model\Label;
use http\Env\Response;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rules\In;

class EnrollController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $label = Label::where('status',1)->where('is_active',1)->get();
        return view('admin.enroll')->with(['label'=>$label]);
    }


    /**
     * 数据
     */
    public function data()
    {
        $limit = intval(Input::get('limit'));
        $offset = intval(Input::get('offset'));
        $search = Input::get('search');
        $where = [];
        $data = Enroll::getData($limit,$offset,$search,$where);
        $rows = [];
        $statusArr = ['待发布', '启用','停用'];
        $enroll_id = [];
        $enroll_user_count = [];
        $enroll_order_count = [];
        $enroll_order_count1 = [];
        foreach ($data['rows'] as $val){
            $enroll_id[] = $val->_id;
            $enroll_user_count[$val->_id] = 0;
            $enroll_order_count[$val->_id] = 0;
            $enroll_order_count1[$val->_id] = 0;
        }
        $enroll_data = EnrollData::whereIn('source_id',$enroll_id)->where('is_active',1)->where('status',1)->get();
        foreach ($enroll_data as $val){
            $enroll_user_count[$val->source_id] += 1;
        }
        $enroll_order = EnrollOrder::whereIn('source_id',$enroll_id)->where('is_active',1)->where('status',1)->get();
        foreach ($enroll_order as $val){
            $enroll_order_count[$val->source_id] += 1;
        }
        $enroll_order1 = EnrollOrder::whereIn('source_id',$enroll_id)->where('is_active',1)->where('pay_type',2)->where('status',0)->get();
        foreach ($enroll_order1 as $val){
            $enroll_order_count1[$val->source_id] += 1;
        }
        foreach ($data['rows'] as $val) {
            $arr = array();
            $arr['id'] = $val->_id;
            $arr['name'] = $val->name;
            $arr['description'] = $val->description;
            $arr['content'] = $val->content;
            $arr['type_id'] = $val->type_id;
            $arr['image'] = $val->image;
            $arr['imgUrl'] = $val->image == '' ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : Storage::disk('s3-public')->url($val->image);
            $arr['content'] = $val->content;
            $arr['statusVal'] = $val->status;
            $arr['status'] = $statusArr[$val->status];
            $arr['createTime'] = date('Y/m/d H:i:s', strtotime($val->created_at));
            $arr['order'] = $enroll_order_count[$val->_id] ?? 0;
            $arr['user'] = $enroll_user_count[$val->_id] ?? 0;
            $arr['confirm_order'] = $enroll_order_count1[$val->_id] ?? 0;
            if($val->is_step == 1 ){
                $arr['caozuo'] = '<div class="visible-md visible-lg visible-sm visible-xs edit-enroll" style="">
<a href="#" class="btn btn-xs btn-blue tooltips TableDetail" data-placement="top" data-toggle="tooptip" title="编辑"><i class="fa fa-edit fa-lg"></i></a>
</div>';
            }else{
                $arr['caozuo'] = '<div class="visible-md visible-lg visible-sm visible-xs info-enroll" style="">
<a href="#" class="btn btn-xs btn-blue tooltips TableDetail" data-toggle="tooptip" title="详情"><i class="fa fa-info-circle fa-lg"></i></a>
</div>';
            }
            $arr['form'] = $val->form;
            $arr['is_step'] = $val->is_step;
            $arr['price'] = $val->is_pay == 0 ? 0 : $val->pay_price;
            $rows[] = $arr;
        }
        return response()->json(array('total' => $data['total'], 'rows' => $rows));
    }


    /**
     * 添加和修改
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save( Request $request )
    {
        $data = Input::get('data');
        $enroll = Enroll::find($data['id']);
        if(!is_null($enroll)){
            $enroll->form = $data['form'];
            $enroll->is_pay = intval($data['is_pay']);
            if( $enroll->is_pay == 0){
                $enroll->pay_prcie = '';
                $enroll->pay_type = 0;
            }else{
                $enroll->pay_price = floatval($data['pay_price']);
                $enroll->pay_type = intval($data['pay_type']);
            }
            $enroll->status = 1;
            $enroll->is_step = 2;
            $enroll->save();
            return response()->json(['status' => 0]);
        }else{
            return response()->json(['status' => 1]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * 保存活动内容
     */
    public function step1Save( Request $request )
    {
        $input = json_decode($request->data, true);
        if ( empty($input['id']) ) {
            $cms = new Enroll();
            $cms->timestamp = time();
            $cms->status = 0;
            $cms->is_step = 1;
            $cms->is_active = 1;
        } else {
            $cms = Enroll::find($input['id']);
            if(is_null($cms)){
                $cms = new Enroll();
                $cms->timestamp = time();
                $cms->status = 0;
                $cms->is_active = 1;
                $cms->is_step = 1;
            }
        }
        $cms->name        = $input['title'];
        $cms->image       = $input['image'];
        $cms->description = $input['description'];
        $cms->content     = str_replace(PHP_EOL, '', $input['content']);
        $cms->save();
        return response()->json(['status' => 0, 'message' => '添加成功', 'id' => $cms->_id]);
    }


    public function edit(Request $request)
    {
        $data = json_decode($request->data, true);
        $enroll = Enroll::find($data['id']);
        $enroll->status = (int)$data['status'];
        $enroll->save();
        return response()->json(['status' => 0]);
    }

    /**
     *
     *导出报名信息
     */
    public function export()
    {
        $id = Input::get('id');
        $enroll = Enroll::find($id);
        $enroll_data = EnrollData::where('source_id',$id)->where('is_active',1)->where('status',1)->orderBy('timestamp','desc')->get();
//        $enroll_data = EnrollData::where('source_id',$id)->orderBy('timestamp','desc')->get();
        $arr = [];
        foreach ($enroll->form as $v){
            $arr[] = $v['name'];
        }
        if($enroll->is_pay == 1){
            $arr[] = '报名费支付方式';
            $arr[] = '系统订单号';
        }
        $arr[] = '报名时间';
        $cellData[] = $arr;
        $order_id = [];
        foreach ($enroll_data as $v){
            $order_id[] = $v->order_id;
        }
        $enroll_order = EnrollOrder::whereIn('_id',$order_id)->get();
        $order_data = [];
        foreach ($enroll_order as $v){
            $order_data[$v->_id] = $v->order_number;
        }
        foreach ($enroll_data as $v){
            $tmp = [];
            foreach ($v->value as $val){
                $tmp[] = $val['value'];
            }
            if($enroll->is_pay == 1){
                $tmp[] = $v->pay_type == 1 ? '微信支付' : ($v->pay_type == 2 ? '对公打款' : '');
                $tmp[] = '"'.$order_data[$v->order_id].'"';
            }
            $tmp[] = date('Y-m-d H:i:s',$v->timestamp);
            $cellData[] = $tmp;
        }
        $excel = App::make('excel');
        $excel->create($enroll->name.'报名表',function($excel) use ($cellData){
            $excel->sheet('score', function($sheet) use ($cellData){
                $sheet->rows($cellData);
            });
        })->export('xls');
    }


    /**
     * 导出订单
     */
    public function exportOrder()
    {
        $id = Input::get('id');
        $enroll = Enroll::find($id);
        $enroll_order = EnrollOrder::where('source_id',$id)->where('is_active',1)->where('status',1)->where('pay_type','<>','0')->orderBy('timestamp','desc')->get();
        $cellData = [
            ['系统订单号','支付方式','微信支付订单号','单价','数量','总支付金额','抬头','税号','缴费人员姓名','开票金额','手机','邮箱','创建时间']
        ];


        foreach ($enroll_order as $v){
            $arr = [
                '"'.$v->order_number.'"',
                $v->pay_type == 1 ? '微信支付' : ($v->pay_type == 2 ? '对公打款' : ''),
                $v->wx_order,
                $v->price,
                $v->num,
                $v->total_price,
                $v->pay_ticket['head'] ?? '',
                '"'.$v->pay_ticket['number'].'"' ?? '',
                $v->pay_ticket['name'] ?? '',
                $v->pay_ticket['money'] ?? '',
                $v->pay_ticket['phone'] ?? '',
                $v->pay_ticket['email'] ?? '',
                $v->created_at
            ];
            $cellData[] = $arr;
        }
        $excel = App::make('excel');
        $excel->create($enroll->name.'订单表',function($excel) use ($cellData){
            $excel->sheet('score', function($sheet) use ($cellData){
                $sheet->rows($cellData);
            });
        })->export('xls');
    }


    public function orderData()
    {
        $id = Input::get('id');
        $limit = intval(Input::get('limit'));
        $offset = intval(Input::get('offset'));
        $search = Input::get('search');
        $where = ['source_id'=>$id];
        $data = EnrollOrder::getData($limit,$offset,$search,$where);
        $rows = [];
        $statusArr = ['<span style="color: red;">待审核</span>', '支付成功'];
        $oId = [];
        foreach ($data['rows'] as $val){
            $oId[] = $val->_id;
        }
        $enroll_data = EnrollData::whereIn('order_id',$oId)->get();
        $base_name = [];
        foreach ($enroll_data as $v){
            if($v->source_id == '5d91714f7633790a207d3b74'){
                $base_name[$v->order_id] = $v->value[1]['value'];
            }elseif ($v->source_id == '5d91734a7633793ab6581f3a'){
                $base_name[$v->order_id] = $v->value[0]['value'];
            }
        }
        foreach ($data['rows'] as $val) {
            $arr = array();
            $arr['id'] = $val->_id;
            $arr['base'] = $base_name[$val->_id] ?? '';
            $arr['order_number'] = $val->order_number;
            $arr['pay_type'] = $val->pay_type == 1 ? '微信支付' : ($val->pay_type == 2 ? '对公打款' : '');
            $arr['wx_order'] = $val->wx_order;
            $arr['num'] = $val->num;
            $arr['price'] = $val->price;
            $arr['total_price'] = $val->total_price;
            $arr['ticket_number'] = $val->pay_ticket['number'] ?? '';
            $arr['ticket_link'] = $val->pay_ticket['name'] ?? '';
            $arr['ticket_tel']  = $val->pay_ticket['phone'] ?? '';
            $arr['ticket_email'] = $val->pay_ticket['email'] ?? '';
            $arr['statusVal'] = $val->status;
            $arr['status'] = $statusArr[$val->status];
            $arr['createTime'] = date('Y/m/d H:i:s', strtotime($val->created_at));
            if($val->status == 0 ){
                $arr['caozuo'] = '<div class="visible-md visible-lg visible-sm visible-xs info-order"  style="">
<a href="#" class="btn btn-xs btn-blue tooltips TableDetail" data-placement="top" data-toggle="tooptip" title="编辑"><i class="fa fa-edit fa-lg"></i></a>
</div>';
            }else{
                $arr['caozuo'] = '<div class="visible-md visible-lg visible-sm visible-xs info-order " style="" data-id="'.$val->_id.'">
<a href="#" class="btn btn-xs btn-blue tooltips TableDetail" data-toggle="tooptip" title="详情"><i class="fa fa-info-circle fa-lg"></i></a>
</div><br><div class="visible-md visible-lg visible-sm visible-xs ticket-confirm" style="" data-id="'.$val->_id.'">
<a href="#" class="btn btn-xs btn-blue tooltips" title="确认已开发票"><i class="fa fa-ticket fa-lg"></i></a></div>';
            }
            $rows[] = $arr;
        }
        return response()->json(array('total' => $data['total'], 'rows' => $rows));
    }


    public function orderInfo()
    {
        $id = Input::get('id');
        $order = EnrollOrder::find($id);
        $user = EnrollData::where('order_id',$id)->where('is_active',1)->get();
        $enroll = Enroll::find($order->source_id);
        $arr = [];
        foreach ($enroll->form as $v){
            $arr[] = $v['name'];
        }
        return response()->json(['status'=>1,'order'=>$order,'user'=>$user,'arr'=>$arr]);
    }


    public function orderEdit()
    {
        $id = Input::get('id');
        $order = EnrollOrder::find($id);
        $order->status = 1;
        $order->save();
        $user = EnrollData::where('order_id',$id)->where('is_active',1)->update(['status'=>1]);
        return response()->json(['status'=>1]);
    }


    public function orderTable()
    {
        $id= Input::get('id');
        $where = ['source_id'=>$id];
        $query = EnrollOrder::where('source_id',$id)->orderBy('status','asc')->orderBy('created_at','desc')->where('is_active',1);
        $query = $query->where(function ($query) {
            $query = $query->where(function ($query) {
//                $query->where('pay_type', 1)
                   $query->where('status', 1);
            })->orWhere('pay_type',2);
        });
        $data = $query->get();
        $rows = [];
        $statusArr = ['<span style="color: red;">待审核</span>', '支付成功','<span style="color: #16d250;">已开票</span>'];
        $oId = [];
        foreach ($data as $val){
            $oId[] = $val->_id;
        }
        $enroll_data = EnrollData::whereIn('order_id',$oId)->get();
        $base_name = [];
        foreach ($enroll_data as $v){
            if($v->source_id == '5d91714f7633790a207d3b74'){
                $base_name[$v->order_id] = $v->value[1]['value'];
            }elseif ($v->source_id == '5d91734a7633793ab6581f3a'){
                $base_name[$v->order_id] = $v->value[0]['value'];
            }
        }
        foreach ($data as $val) {
            $ticket_remark = $val->pay_ticket['remark'] ?? '';
            $remark = $val->remark ?? '';
            $remark = $remark . '<br>'. $ticket_remark;
            $arr = array();
            $arr['id'] = $val->_id;
            $arr['base'] = $base_name[$val->_id] ?? '';
            $arr['order_number'] = $val->order_number;
            $arr['pay_type'] = $val->pay_type == 1 ? '微信支付' : ($val->pay_type == 2 ? '对公打款' : '');
            $arr['wx_order'] = $val->wx_order;
            $arr['num'] = $val->num;
            $arr['price'] = $val->price;
            $arr['total_price'] = $val->total_price;
            $arr['ticket_number'] = $val->pay_ticket['number'] ?? '';
            $arr['ticket_link'] = $val->pay_ticket['name'] ?? '';
            $arr['ticket_tel']  = $val->pay_ticket['phone'] ?? '';
            $arr['ticket_email'] = $val->pay_ticket['email'] ?? '';
            $arr['ticket_money'] = $val->pay_ticket['money'] ?? '';
            $arr['ticket_confirm_money'] = $val->pay_ticket['ticket_money'] ?? '';
            $arr['remark'] = $remark;
            $arr['statusVal'] = $val->status;
            $arr['status'] = isset($val->pay_ticket['status']) && ($val->pay_ticket['status'] == 1) ? $statusArr[2] : $statusArr[$val->status];
            $arr['createTime'] = date('Y/m/d H:i:s', strtotime($val->created_at));
            if($val->status == 0 ){
                $arr['caozuo'] = '<div class="visible-md visible-lg visible-sm visible-xs info-order" style="" data-id="'.$val->_id.'">
<a href="#" class="btn btn-xs btn-blue tooltips TableDetail" data-placement="top" data-toggle="tooptip" title="编辑"><i class="fa fa-edit fa-lg"></i></a>
</div>';
            }else{
                $arr['caozuo'] = '<div class="visible-md visible-lg visible-sm visible-xs info-order" style="" data-id="'.$val->_id.'">
<a href="#" class="btn btn-xs btn-blue tooltips TableDetail" data-toggle="tooptip" title="详情"><i class="fa fa-info-circle fa-lg"></i></a>

</div><br><div class="visible-md visible-lg visible-sm visible-xs ticket-confirm" style="" data-id="'.$val->_id.'">
<a href="#" class="btn btn-xs btn-blue tooltips" title="确认已开发票"><i class="fa fa-ticket fa-lg"></i></a></div>';
            }
            $rows[] = $arr;
        }
        return response()->json(array('total' => count($data), 'rows' => $rows,'status'=>0));
    }

    public function orderTicket()
    {
        $data = Input::get('data');
        $enrollOrder = EnrollOrder::find($data['orderId']);
        $ticket = $enrollOrder->pay_ticket;
        $ticket['ticket_money'] = $data['money'];
        $ticket['status'] = 1;
        $ticket['remark'] = $data['remark'];
        unset($ticket['ticket_money']);
        unset($ticket['status']);
        unset($ticket['remark']);
        $enrollOrder->pay_ticket = $ticket;
        $res = $enrollOrder->save();
        if ($res) return response()->json(['status' => 0]);
    }

}
