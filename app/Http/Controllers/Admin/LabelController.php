<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Label;
use App\Http\Requests;
use Auth;
use Illuminate\Support\Facades\Input;

class LabelController extends Controller
{
    public function index()
    {
        $data = ['usernames' => Auth::user()->name];
        return view('admin.label')->with($data);
    }

    /**
     * 数据
     */
    public function data()
    {
        $limit = intval(Input::get('limit'));
        $offset = intval(Input::get('offset'));
        $search = Input::get('search');
        $data = Label::getData($limit,$offset,$search);

        $rows = [];
        $statusArr = ['暂停', '正常'];
        foreach ($data['rows'] as $val) {
            $arr = array();
            $arr['bh'] = $val->_id;
            $arr['name'] = $val->name;
            $arr['status'] = $statusArr[$val->status];
            $arr['ctime'] = date('Y/m/d', strtotime($val->created_at));
            $rows[] = $arr;
        }
        return response()->json(array('total' => $data['total'], 'rows' => $rows));
    }

    /**
     * 添加
     */
    public function add()
    {
        $getIllnessLabel = Input::get('postdata');
        if(isset($getIllnessLabel['name']) && isset($getIllnessLabel['status'])){
            $label = Label::where('name',$getIllnessLabel['name'])->where('hId',0)->get();
            if(count($label) > 0){
                return response()->json(['status' => 1, 'message' => '添加失败,已经存在相同的标签']);
            }
            $bool = Label::addData($getIllnessLabel);
            if($bool){
                $say = 'ok';
            }else{
                $say = 'no';
            }
        }else{
            $say = 'no';
        }
        return response()->json(array('say' => $say));
    }

    /*
     * 修改数据
     * */
    public function edit()
    {
        $getIllnessLabel = Input::get('postdata');
        Label::editData($getIllnessLabel);
        return response()->json(array('say' => 'ok'));
    }
}
