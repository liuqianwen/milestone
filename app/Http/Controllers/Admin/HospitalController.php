<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Model\Hospital;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class HospitalController extends Controller
{
    public function index()
    {
        $bId = 0;
        $data = Input::all();
        if(isset($data['bId'])){
            $bId = intval($data['bId']);
        }
        return view('admin.hospital')->with(['usernames' => Auth::user()->name,'bId'=>$bId]);
    }
    public function data(){
        $limit = intval(Input::get('limit'));
        $offset = intval(Input::get('offset'));
        $search = Input::get('search');
        $where = ['type'=>1];
        $data = Hospital::getData($limit,$offset,$search,$where);
        $rows = [];
        $status          = ['停用','启用'];
        foreach  ($data['rows'] as $val) {
            $arr         = [];
            $arr['id']   = $val->_id;
            $arr['name'] = $val->name;
            $arr['address'] = $val->address;
            $arr['status'] = $status[$val->status];
            $arr['imgUrl'] = $val->logo == '' ? 'http://www.youbian.com/Images/201313/13653239592670.jpg' : Storage::disk('s3-public')->url($val->logo);
            $arr['img'] = $val->logo;
            $arr['badge'] = $val->badge;
            $arr['introduce'] = $val->introduce;
            $rows[] = $arr;
        }
        return response()->json(array('total' => $data['total'], 'rows' => $rows));
    }
    public function add(Request $request){
        $data = json_decode($request->data, true);
        Hospital::add($data);
        return response()->json(['status' => 0, 'message' => '添加成功!']);
    }
    public function edit()
    {
        $data = Input::get('data');
        $id = Input::get('hId');
        Hospital::edit($id, $data);

        return response()->json(['status' => 0, 'message' => '修改成功']);
    }

}
