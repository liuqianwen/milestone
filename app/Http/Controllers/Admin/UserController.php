<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    /**
     * 返回管理员的视图
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.user');
    }

    /**
     * 返回管理员分页表格数据
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $offset = Input::get('offset');
        $limit  = Input::get('limit');
        $search = Input::get('search');
        $query  = User::where('is_active', 1);
        if (!empty($search)) {
            $GLOBALS['search'] = '%' . $search . '%';
            $query             = $query->where(function ($query) {
                $query->where('name', $GLOBALS['search'])->orwhere('email', $GLOBALS['search']);
            });
        }
        $total = $query->count();
        if ($total == $offset) $offset = $offset - $limit;
        $query    = $query->skip(intval($offset));
        $query    = $query->limit(intval($limit));
        $result   = $query->get();
        $rows     = [];
        $isActive = ['删除', '正常'];
        foreach ($result as $v) {
            $rows[] = [
                '_id'        => $v->_id,
                'name'       => $v->name,
                'email'      => $v->email,
                'is_active'  => $isActive[$v->is_active],
                'createTime' => date('Y/m/d', strtotime($v->created_at)),
            ];
        }

        return response()->json(['rows' => $rows, 'total' => $total]);
    }

    /**
     * 添加管理员
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function add()
    {
        $input = Input::get('data');
        extract($input);
        if (empty($email)) return response()->json(['status' => 1, 'message' => '参数不正确']);
        if (empty($name)) return response()->json(['status' => 1, 'message' => '参数不正确']);
        $user = User::where('email', $email)->where('is_active', 1)->first();
        if (!is_null($user)) return response()->json(['status' => 2, 'message' => '用户名重复']);
        unset($user);
        $user            = new User();
        $user->email     = $email;
        $user->name      = $name;
        $user->password  = bcrypt(123456);
        $user->role      = 1;
        $user->is_active = 1;
        $user->save();

        return response()->json(['status' => 0, 'message' => '添加成功', 'data' => $user->_id]);
    }

    /**
     * 修改管理员名字、密码
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit()
    {
        $input = Input::get('data');
        $id    = (int)Input::get('id');
        $user  = User::find($id);
        if (is_null($user)) return response()->json(['status' => 2, 'message' => '用户未找到']);
        extract($input);
        $data = [];
        isset($name) && $data['name'] = $name;
        isset($password) && $data['password'] = bcrypt($password);
        foreach ($data as $k => $v) $user->$k = $v;
        $user->save();

        return response()->json(['status' => 0, 'message' => '修改成功']);
    }
}
