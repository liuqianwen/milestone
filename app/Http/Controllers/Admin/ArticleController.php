<?php

namespace App\Http\Controllers\Admin;

use App\Model\Education;
use App\Model\EnrollData;
use App\Model\EnrollOrder;
use App\Model\Label;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class ArticleController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $label = Label::where('status',1)->where('is_active',1)->get();
        return view('admin.article')->with(['label'=>$label]);
    }


    /**
     * 数据
     */
    public function data()
    {
        $limit = intval(Input::get('limit'));
        $offset = intval(Input::get('offset'));
        $search = Input::get('search');
        $where = ['type'=>1];
        $data = Education::getData($limit,$offset,$search,$where);
        $rows = [];
        $statusArr = ['停用', '启用'];
        $topArr = ['正常', '置顶'];
        foreach ($data['rows'] as $val) {
            $arr = array();
            $arr['id'] = $val->_id;
            $arr['name'] = $val->name;
            $arr['description'] = $val->description;
            $arr['content'] = $val->content;
            $arr['type_id'] = $val->type_id;
            $arr['image'] = $val->image;
            $arr['imgUrl'] = $val->image == '' ? 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1537406633&di=3ec6779f2a8933ea0fcc1305e1108ff0&imgtype=jpg&er=1&src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F04%2F26%2F21%2F6958397be528af3.jpg' : Storage::disk('s3-public')->url($val->image);
            $arr['content'] = $val->content;
            $arr['status'] = $statusArr[$val->status];
            $arr['top'] = $topArr[$val->top] ?? '正常';
            $arr['tops'] = $val->top;
            $arr['createTime'] = date('Y/m/d', strtotime($val->created_at));
            $rows[] = $arr;
        }
        return response()->json(array('total' => $data['total'], 'rows' => $rows));
    }


    /**
     *
     *
     * 添加和修改
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save( Request $request )
    {
        $input = json_decode($request->data, true);
        if ( empty($input['id']) ) {
            $cms = new Education();
            $cms->timestamp = time();
            $cms->read = 0;
            $cms->top = 0;
            $cms->status = 1;
            $cms->is_active = 1;
        } else {
            $cms = Education::find($input['id']);
            if(is_null($cms)){
                $cms = new Education();
                $cms->timestamp = time();
                $cms->read = 0;
                $cms->top = 0;
                $cms->status = 1;
                $cms->is_active = 1;
            }
        }
//        if(!empty($input['newLabel'])){
//            $new = explode(',',$input['newLabel']);
//            foreach($new as $v){
//                $label = Label::where('name',$v)->first();
//                if(is_null($label)){
//                    $id = Label::addData(['name'=>$v,'status'=>1]);
//                    $input['oldLabel'][] = $id;
//                }else{
//                    $input['oldLabel'][] = $label->_id;
//                }
//            }
//        }
        $cms->type_id = array_unique($input['oldLabel']);
        $cms->type = 1;
        $cms->name        = $input['title'];
        $cms->image       = $input['image'];
        $cms->description = $input['description'];
        $cms->content     = str_replace(PHP_EOL, '', $input['content']);
        $cms->save();
        return response()->json(['status' => 0, 'message' => '添加成功', 'data' => ['id' => $cms->id]]);
    }


    public function edit(Request $request)
    {
        $data = json_decode($request->data, true);
        $video = Education::find($data['id']);
        if ($data['status'] == 1) {
            $video->status = 0;
        }else {
            $video->status = 1;
        }
        $video->save();
        return response()->json(['say' => 'ok']);
    }


    public function top(Request $request)
    {
        $data = json_decode($request->data, true);
        $article = Education::find($data['id']);
        if ($article->top == 1) {
            $article->top = 0;
        }else {
            $article->top = 1;
        }

        $article->save();
        return response()->json(['say' => 'ok']);
    }


}
