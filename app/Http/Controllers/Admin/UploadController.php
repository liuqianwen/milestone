<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    public function index( Request $request )
    {
        $path = 'milestone/attachment';
        if(isset($request->path)) $path = $request->path;
        $url = $this->image($request->img, $path);
        $route = Storage::disk('s3-public')->url($url);
        return response()->json(['status' => 0, 'message' => '成功', 'data' => ['route' => $route,'url'=>$url]]);
    }


    public function uploads( Request $request )
    {
        $file  = $request->file();
        $path  = '';
        $url = '';
        foreach ( $file as $k => $v ) {
            $url = Storage::disk('s3-public')->put('/milestone/article',$v);
            $path = Storage::disk('s3-public')->url($url);
        }
        $data['file_path'] = $path;
        $data['file_uri'] = $url;
        $data['msg']       = "上传成功!";
        $data['success']   = true;
        return response()->json($data);
    }

    /**
     * 上传图片
     *
     * @param $base64_image_content base64文件
     * @param $path                 string  存储路径
     * @return bool | 服务器存储url
     */
    static public function image( $base64_image_content, $path )
    {
        if ( preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image_content, $result) ) {
            $type    = $result[2];
            $newName = date('Ymd') . '/' . str_random(32) . '.' . $type;
            $base64  = base64_decode(str_replace($result[1], '', $base64_image_content));
            if ( Storage::disk('s3-public')->put($path . '/' . $newName, $base64) ) {
                return $path . '/' . $newName;
            }
        }

        return false;
    }




}
