<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Model\Hospital;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class VenderController extends Controller
{
    public function index()
    {
        return view('admin.vender');
    }
    public function data(){
        $limit = intval(Input::get('limit'));
        $offset = intval(Input::get('offset'));
        $search = Input::get('search');
        $where = ['type'=>2];
        $data = Hospital::getData($limit,$offset,$search,$where);
        $rows = [];
        $status          = ['停用','启用'];
        foreach  ($data['rows'] as $val) {
            $arr         = [];
            $arr['id']   = $val->_id;
            $arr['name'] = $val->name;
            $arr['address'] = $val->address;
            $arr['status'] = $status[$val->status];
            $arr['imgUrl'] = $val->logo == '' ? 'http://www.youbian.com/Images/201313/13653239592670.jpg' : Storage::disk('s3-public')->url($val->logo);
            $arr['img'] = $val->logo;
            $arr['genre'] = $val->genre;
            $arr['content'] = $val->content;
            $goods = $val->goods;
            if($goods){
                foreach($goods  as $k=>$v){
                    $goods[$k]['imgUrl'] = Storage::disk('s3-public')->url($v['image']);
                }
            }
            $arr['goods'] = $goods;
            $arr['introduce'] = $val->introduce;
            $rows[] = $arr;
        }
        return response()->json(array('total' => $data['total'], 'rows' => $rows));
    }
    public function add(Request $request){
        $data = json_decode($request->data, true);
        $vender = new Hospital();
        $vender->name = $data['name'];
        $vender->address = $data['address'];
        $vender->introduce = $data['introduce'];
        $vender->logo = $data['logo'];
        $vender->pointlat = $data['pointlat'];
        $vender->pointlng = $data['pointlng'];
        $vender->city = $data['city'];
        $vender->type = 2;
        $vender->genre = intval($data['genre']);
        $vender->status = 1;                                  // 医院状态
        $vender->is_active = 1;
        $vender->timestamp = time();
        if(intval($data['genre']) == 1){
            $vender->content = str_replace(PHP_EOL, '', $data['content']);
        }elseif(intval($data['genre']) == 2){
            $vender->goods = $data['goods'];
        }
        $vender->save();
        return response()->json(['status' => 0, 'message' => '添加成功!']);
    }
    public function edit()
    {
        $data = Input::get('data');
        $id = Input::get('hId');
        Hospital::edit($id, $data);

        return response()->json(['status' => 0, 'message' => '修改成功']);
    }

}
