<?php

namespace App\Http\Controllers\Admin;

use App\Model\Prize;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Label;
use App\Http\Requests;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class PrizeController extends Controller
{
    public function index()
    {
        $data = ['usernames' => Auth::user()->name];
        return view('admin.prize')->with($data);
    }

    /**
     * 数据
     */
    public function data()
    {
        $limit = intval(Input::get('limit'));
        $offset = intval(Input::get('offset'));
        $search = Input::get('search');
        $data = Prize::getData($limit,$offset,$search);
        $rows = [];
        $type = ['谢谢参与','积分','礼包'];
        foreach ($data['rows'] as $val) {
            $arr = array();
            $arr['id'] = $val->_id;
            $arr['name'] = $val->name;
            $arr['type'] = $type[$val->type];
            $arr['typeVal'] = $val->type;
            $arr['sort'] = $val['sort'];
            $arr['odds'] = $val['odds'];
            $arr['image'] = $val->image == '' ? '' : Storage::disk('s3-public')->url($val->image);
            if($val->type == 1){
                $arr['integral'] = $val->integral;
            }
            $rows[] = $arr;
        }
        return response()->json(array('total' => $data['total'], 'rows' => $rows));
    }

    /**
     * 添加
     */
    public function add()
    {
        $data = Input::get('postdata');
        $prize = new Prize();
        $prize->name = $data['name'];
        $prize->type = (int)$data['type'];
        if($prize->type == 1){
            $prize->integral = (int)$data['integral'];
        }
        $prize->sort = (int)$data['sort'];
        $prize->odds = (int)$data['odds'];
        $prize->image = $data['image'];
        $prize->is_active = 1;
        $prize->save();
        return response()->json(array('status' => 0));
    }

    /*
     * 修改数据
     * */
    public function edit()
    {
        $data = Input::get('postdata');
        $prize = Prize::find($data['id']);
        $prize->name = $data['name'];
        $prize->type = (int)$data['type'];
        if($prize->type == 1){
            $prize->integral = (int)$data['integral'];
        }
        $prize->sort = (int)$data['sort'];
        $prize->odds = (int)$data['odds'];
        if($data['image'] != ''){
            $prize->image = $data['image'];
        }
        $prize->save();
        return response()->json(array('say' => 'ok'));
    }
}
