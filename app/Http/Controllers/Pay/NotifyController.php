<?php

namespace App\Http\Controllers\Pay;

use App\Http\Controllers\Weixin\InterfaceController;
use App\Model\Enroll;
use App\Model\EnrollData;
use App\Model\EnrollOrder;
use EasyWeChat\Payment\Kernel\BaseClient;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Config;
use Illuminate\Support\Facades\Log;

class NotifyController extends Controller
{
    public function index()
    {
        $app = Payment::payment();
//        BaseClient::setDefaultOptions([
//            'curl' => [
//                CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4,
//            ],
//            'verify'=>false               //不开启CURLOPT_SSL_VERIFYPEER, 这里后来线上ssl报错加的，原因忘了
//        ]);
        $response = $app->handlePaidNotify(function ($notify, $fail) {
            $order_id = $notify['out_trade_no'];//商户订单号
            $trade_no = $notify['transaction_id'];//微信支付订单号
            $order = EnrollOrder::find($order_id);
            if (!is_null($order)) {
                if ($order->status == 1) {
                    return true;
                } else {
                    if ($notify['return_code'] === 'SUCCESS' && array_get($notify, 'result_code') === 'SUCCESS') {
                        //加入自己支付完成后该做的事
                        $order->status = 1;
                        $order->wx_order = $trade_no;
                        $order->pay_time = time();
                        $res_order = $order->save();
                        if ($res_order) {
                            $enrollData = EnrollData::where('order_id',$order->_id)->where('order_number',$order->order_number)->where('status',0)->where('is_active',1)->get();
                            foreach ($enrollData as $v) {
                                $v->status = 1;
                                $res = $v->save();
                            }
//                            $dns = Config::get('dns');
//                            $openId = $order->member_patient_id;
//                            $enroll = Enroll::where('_id',intval($order->source_id))->find();
//                            $goodsInfo = $enroll->title;
//                            $num = count($enrollData);
//                            $data = [
//                                'first' => '您好，您的报名费已支付成功！',
//                                'keyword1' => number_format(floatval($order['price'])+floatval($order['freight']),2,'.',',').'元',
//                                'keyword2' => $goodsInfo,
//                                'keyword3' => '微信支付',
//                                'keyword4' => $order_number,
//                                'keyword5' => date('Y年m月d日 H:i',$order->pay_time),
//                                'remark'   => '感谢您的购买，点击查看订单详情'
//                            ];
//                            $url = $dns.'/wechat/member/order/single/'.$order['_id'];
//                            try {
//                                InterfaceController::notice($openId, $data, $url, 'pay_success');
//                            } catch (\Exception $e) {

//                            }
                            return true;
                        } else {
                            return 'fail';
                        }
                    } else {

                        return 'order not paid';
                    }
                }
            } else {
                return true;
            }
        });

        return $response;
    }

}