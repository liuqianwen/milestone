<?php

namespace App\Http\Controllers\Pay;

//use EasyWeChat\Foundation\Application;
use EasyWeChat\Payment\Application;
use EasyWeChat\Factory;
use Config;

class Payment
{
    public static function payment()
    {
        $options = Config::get('wechat.payment.default');
        $app = Factory::payment($options);
        return $app;
    }

    /**
     * 返回 json
     * @param $attributes
     * @return mixed
     */
    public static function pay($attributes)
    {
        $options = Config::get('wechat.payment.default');
        $payment = Factory::payment($options);
        $result = $payment->order->unify($attributes);
        $prepayId = $result['prepay_id'];
        $json = $payment->jssdk->sdkConfig($prepayId);
        return $json;
    }

    /**
     * 返回二维码
     * @param $attributes
     * @return mixed
     */
    public function pay_print($attributes)
    {
        $payment = self::payment();

        $order = new Order($attributes);
        $result = $payment->prepare($order);
        return $result->code_url;
    }
}