<?php

namespace App\Http\Controllers;

use App\Model\Enroll;
use App\Model\EnrollData;
use App\Model\EnrollOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $id = Input::get('id');
//        $jd = '5d91734a7633793ab6581f3a'; //基地
        $ds = '5d91714f7633790a207d3b74'; //大使
        $enroll = Enroll::find($ds);
        $enroll_data = EnrollData::where('source_id',$ds)->get();
        $arr = [];
        foreach ($enroll->form as $v){
            $arr[] = $v['name'];
        }
        if($enroll->is_pay == 1){
            $arr[] = '报名费支付方式';
            $arr[] = '应付金额';
            $arr[] = '实付金额';
            $arr[] = '系统订单号';
        }
        $arr[] = '报名时间';
        $arr[] = '状态';
        $arr[] = '备注';
        $cellData[] = $arr;
        $order_id = [];
        foreach ($enroll_data as $v){
            $order_id[] = $v->order_id;
        }
        $enroll_order = EnrollOrder::whereIn('_id',$order_id)->get();
        $order_data = [];
        foreach ($enroll_order as $v){
            $order_data[$v->_id] = [
              'order_number' => $v->order_number,
                'money'      => $v->pay_ticket['money'] ?? '',
                'pay_money'  => $v->pay_ticket['ticket_money'] ?? ''
            ];
        }
        foreach ($enroll_data as $v){
            $tmp = [];
            foreach ($v->value as $val){
                $tmp[] = $val['value'];
            }
            if($enroll->is_pay == 1){
                $tmp[] = $v->pay_type == 1 ? '微信支付' : ($v->pay_type == 2 ? '对公打款' : '');
                $tmp[] = $order_data[$v->order_id]['money'];
                $tmp[] = $order_data[$v->order_id]['pay_money'];
                $tmp[] = '"'.$order_data[$v->order_id]['order_number'].'"';
            }
            $tmp[] = date('Y-m-d H:i:s',$v->timestamp);
            $tmp[] = ($v->is_active == 0  || $v->status == 0) ? '删除' : '正常';
            $tmp[] = $v->remark;
            $cellData[] = $tmp;
        }
        $excel = App::make('excel');
        $excel->create($enroll->name.'报名表',function($excel) use ($cellData){
            $excel->sheet('score', function($sheet) use ($cellData){
                $sheet->rows($cellData);
            });
        })->export('xls');
        exit;
        $id = Input::get('id');
        $enroll = Enroll::find($ds);
        $enroll_order = EnrollOrder::where('source_id',$ds)->get();
        dd($enroll_order);
        $cellData = [
            ['系统订单号','支付方式','微信支付订单号','是否开票','单价','数量','总支付金额','抬头','税号','缴费人员姓名','开票金额','实际开票金额','手机','邮箱','创建时间','状态','备注']
        ];
        foreach ($enroll_order as $v){
            $arr = [
                '"'.$v->order_number.'"',
                $v->pay_type == 1 ? '微信支付' : ($v->pay_type == 2 ? '对公打款' : ''),
                $v->wx_order,
                isset($v->is_ticket) && ($v->is_ticket == 1 ) ? '已开票' : ($v->pay_ticket['number'] ? '未开票' :''),
                $v->price,
                $v->num,
                $v->total_price,
                $v->pay_ticket['head'] ?? '',
                '"'.$v->pay_ticket['number'].'"' ?? '',
                $v->pay_ticket['name'] ?? '',
                $v->pay_ticket['money'] ?? '',
                $v->pay_ticket['ticket_money'] ?? '',
                $v->pay_ticket['phone'] ?? '',
                $v->pay_ticket['email'] ?? '',
                $v->created_at
            ];
            $cellData[] = $arr;
        }
        $excel = App::make('excel');
        $excel->create($enroll->name.'订单表',function($excel) use ($cellData){
            $excel->sheet('score', function($sheet) use ($cellData){
                $sheet->rows($cellData);
            });
        })->export('xls');
        exit;
        $excel = App::make('excel');
        $filePath = '01.xls';
        $excel->load($filePath, function ($reader) {
           $data = $reader->get();
           foreach ($data as $v){
               dd($v);
           }
    });
        return view('home');
        $users = DB::collection('users')->get();

        dd($users);
        $environment = App::environment();
        echo $environment;
        $env = env('DB_CONNECTION');
        echo $env;
        exit;

    }
}
