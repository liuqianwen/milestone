<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;
use EasyWeChat\Factory;
use App\Model\Member;

class WechatMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Session::get('wechat.oauth_user.default');
        $openId = $user['id'];
        $member = Member::find($openId);
        if(is_null($member)) {
//            $options = Config::get('wechat');
//            $app = Factory::officialAccount($options['official_account']['default']);
//            $userService = $app->user;
//            $user = $userService->get($openId);
            $data['openId']        = $openId;
            $data['nickname']   = $user['nickname'];
            $data['sex']        = $user['sex'];
            $data['headimgurl'] = $user['headimgurl'];
            $bool = Member::add($data);
        } else {
            $bool = true;
        }
        if ($bool) {
            return $next($request);
        } else {
            exit;
        }
    }
}
