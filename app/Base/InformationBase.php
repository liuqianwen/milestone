<?php

namespace App\Base;

use App\Model\FollowUp\Information;
use Illuminate\Support\Facades\Auth;

class InformationBase
{
    /**
     * 添加问卷时选择常量
     * @return mixed
     */
    public static function search()
    {
        $information = Information::where('is_active',1)->where('status',1)->get();
        $data = [];
        foreach ($information as $val) {
            $data[] = [
                'id' => strval($val->_id),
                'show' => $val->name,
                'text' => $val->name
            ];
        }
        return response()->json($data);
    }

    /**
     * 常量表格数据
     * @param $data
     * @return mixed
     */
    public static function data($data)
    {
        $search = isset($data['search']) ? $data['search'] : '';
        $offset = isset($data['offset']) ? intval($data['offset']) : 0;
        $limit = isset($data['limit']) ? intval($data['limit']) : 0;
        $where['is_active'] = 1;
        $like = [
            'search' => $search,
            'searchField' => ['name']
        ];
        $orderBy = ['timestamp', 'asc'];

        $result = Information::whereLike($where, $orderBy, $offset, $limit, $like);

        $total = $result['total'];
        $rows = [];
        $typeArray = ['填空', '下拉', '日期'];
        $statusArray = ['停用', '启用'];
        foreach ($result['row'] as $val) {
            $rows[] = [
                'id' => $val->_id,
                'name' => $val->name,
                'type' => $typeArray[$val->type],
                'types' => $val->type,
                'option' => $val->option,
                'status' => $val->status,
                'statusI' => $statusArray[$val->status]
            ];
        }
        return response()->json(['rows' => $rows, 'total' => $total]);
    }

    /**
     * 添加常量
     * @param $data
     * @return bool|int
     */
    public static function add($data)
    {
        if(!isset($data['name']) || $data['name'] == '') return false;
        if(!isset($data['type']) || $data['type'] === '') return false;
        if($data['type'] == 1){
            if(!isset($data['option']) || !is_array($data['option']) || empty($data['option'])) return false;
        }
        $information = Information::add($data);
        return $information;
    }


    public static function edit($id,$data)
    {
       $result = Information::edit($id,$data);
        return $result;
    }


}