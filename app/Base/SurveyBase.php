<?php

namespace App\Base;

use App\Model\FollowUp\Preview;
use App\Model\FollowUp\Strategy;
use App\Model\FollowUp\Survey;
use App\Model\Malady;
use App\Model\Type;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Ayzy\Survey\Survey as AyzySurvey;
use Ayzy\Survey\Preview as AyzyPreview;

class SurveyBase
{
    /**
     * 问卷表格数据
     *
     * @param $data
     * @return mixed
     */
    public static function data( $data )
    {
        $limit = isset($data['limit']) ? intval($data['limit']) : 0;
        $offset = isset($data['offset']) ? intval($data['offset']) : 0;
        $search = isset($data['search']) ? $data['search'] : '';

        $where = ['is_active'=>1];
        $like =  [
            'search' => $search,
            'searchField' => ['title', 'description']
        ];
        $orderBy = ['timestamp', 'asc'];
        $result = Survey::whereLike($where, $orderBy, $offset, $limit, $like);
        $total = $result['total'];
        $rows = [];
        $statusArray = ['停止', '正常'];
        $type = [1=> '口腔自测','党课考试','民主考评'];
        foreach ($result['row'] as $val) {
            $rows[] = [
                'dataId' => strval($val->_id),
                'title' => $val->title,
                'type' => $type[$val->type],
                'status' => $statusArray[$val->status],
                'status_value' => $val->status,
            ];
        }

        return response()->json(['rows' => $rows, 'total' => $total]);
    }

    /**
     * 模版列表
     * @return array
     */
    public static function templateList()
    {
        $data = [];

        $survey = Survey::where('is_active',1)->get();
        $data[1] = [];
        $data[2] = [];
        $data[3] = [];
        foreach ($survey as $val) $data[$val->type][] = ['id' => $val->_id, 'name' => $val->title];

        return response()->json($data);
    }

    /**
     * 问卷列表
     *
     * @return array
     */
    public static function surveyList()
    {
        $data = [];

        $survey = Survey::where('is_active',1)->get();
        $data[1] = [];
        $data[2] = [];
        $data[3] = [];
        foreach ($survey as $val) $data[$val->type][] = ['id' => $val->_id, 'name' => $val->title];

        return response()->json($data);
    }

    /**
     * 全部问卷列表
     *
     * @return array
     */
    public static function allSurvey()
    {
        $survey = Survey::whereIn('type',[1,3])->get();
        $data = ['请选择'];

        foreach ($survey as $val) $data[strval($val->_id)] = $val->title;

        return $data;
    }

    /**
     * 模版数据
     *
     * @param $id
     * @return mixed
     */
    public static function template( $id )
    {
        $returnData = [];
        $survey = Survey::find($id);
        if ($survey) {
            $returnData['title'] = $survey->title;
            $returnData['description'] = $survey->description;
            $returnData['information'] = $survey->information;
            $returnData['remark'] = $survey->remark;
            $question = $survey->question;
            foreach ($question as $key => $val) {
                $question[$key] = (array)$val;
                $question[$key]['jump'] = implode(',', $question[$key]['jump']);
            }
            $returnData['question'] = $question;
        }
        return response()->json(['data' => $returnData]);
    }

    public static function editGet( $id )
    {
        $returnData = [];
        $survey = Survey::find($id);
        if ($survey) {
            $returnData['id'] = $id;
            $returnData['title'] = $survey->title;
            $returnData['type'] = $survey->type;
            $returnData['description'] = $survey->description;
            $returnData['information'] = $survey->information;
            $question = $survey->question;
            foreach ($question as $key => $val) {
                $question[$key] = (array)$val;
                $question[$key]['jump'] = implode(',', $question[$key]['jump']);
            }
            $returnData['question'] = $question;
        }
        $template = true;
        return response()->json(['data' => $returnData, 'template' => $template]);
    }

    /**
     * 添加问卷
     *
     * @param $data
     * @return mixed
     */
    public static function add( $data )
    {
        $result = Survey::add($data);
        if ($result) {
            $response = ['say' => 'ok', 'status' => 0, 'message' => '添加成功！'];
        } else {
            $response = ['say' => 'no', 'status' => 1, 'message' => '数据不正确！'];
        }

        return response()->json($response);
    }

    /**
     * 修改问卷
     *
     * @param $data
     * @return mixed
     */
    public static function edit( $data )
    {
        $id = $data['id'];
        unset($data['id']);
        unset($data['type']);
        $local_survey = Survey::find($id);
        if ($local_survey->end != 0) return response()->json(['say' => 'no', 'status' => 1, 'message' => '修改失败！']);
        $survey = Survey::find($id);
//        if ($local_survey->start != 0) {
//            $question = $survey->question;
//            foreach ($question as $key => $val) $question[$key] = (array)$val;
//            $local_json_question = json_encode($question);
//            $upload_json_question = json_encode($data['question']);
//            if ($local_json_question !== $upload_json_question) return response()->json(['say' => 'no', 'status' => 1, 'message' => '修改失败！']);
//        }
        $result = Survey::edit($id, $data);
        if ($result) {
            $response = ['say' => 'ok', 'status' => 0, 'message' => '修改成功！'];
        } else {
            $response = ['say' => 'no', 'status' => 1, 'message' => '修改失败！'];
        }
        return response()->json($response);
    }

    public static function stop( $id )
    {
        $survey = Survey::find($id);
        if ($survey->end == 0) {
            $survey->status = 0;
            $survey->end = time();
            $survey->save();
        }
        return response()->json(['say' => 'ok', 'status' => 0, 'message' => '停用成功！']);
    }

    /**
     * 预览问卷
     * @param $data
     * @return mixed
     */
    public static function preview($data)
    {
        $response = ['say' => 'no', 'message' => '数据不正确！'];

        $result = Preview::add($data);
        if ($result) {
            $url = Config::get('dns') . '/push/preview/survey/' . $result;
            $src = 'data:image/png;base64,' . base64_encode(QrCode::format('png')->encoding('UTF-8')->size(250)->generate($url));
            $response = ['say' => 'ok', 'status' => 0, 'src' => $src];
        }
        return response()->json($response);
    }

    public static function previewTwo($id)
    {
        $url = Config::get('dns') .'/push/preview/page/' . $id;
        $src = 'data:image/png;base64,' . base64_encode(QrCode::format('png')->encoding('UTF-8')->size(250)->generate($url));
        $response = ['say' => 'ok', 'status' => 0, 'src' => $src, 'url' => $url];
        return response()->json($response);
    }

    /**
     * 预览党课考试
     * @param $data
     * @return mixed
     */
    public static function classPreview($data)
    {
        $response = ['say' => 'no', 'message' => '数据不正确！'];

        $result = Preview::add($data);
        if ($result) {
            $url = Config::get('dns') . '/push/preview/class/' . $result;
            $src = 'data:image/png;base64,' . base64_encode(QrCode::format('png')->encoding('UTF-8')->size(250)->generate($url));
            $response = ['say' => 'ok', 'status' => 0, 'src' => $src];
        }
        return response()->json($response);
    }

    public static function classpreviewTwo($id)
    {
        $url = Config::get('dns') .'/push/preview/class/two/' . $id;
        $src = 'data:image/png;base64,' . base64_encode(QrCode::format('png')->encoding('UTF-8')->size(250)->generate($url));
        $response = ['say' => 'ok', 'status' => 0, 'src' => $src, 'url' => $url];
        return response()->json($response);
    }
}
