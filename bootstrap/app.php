<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

$app = new Illuminate\Foundation\Application(
        realpath(__DIR__ . '/../')
);

/*
|--------------------------------------------------------------------------
| Bind Important Interfaces
|--------------------------------------------------------------------------
|
| Next, we need to bind some important interfaces into the container so
| we will be able to resolve them when needed. The kernels serve the
| incoming requests to this application from both the web and CLI.
|
*/

$app->singleton(
        Illuminate\Contracts\Http\Kernel::class,
        App\Http\Kernel::class
);

$app->singleton(
        Illuminate\Contracts\Console\Kernel::class,
        App\Console\Kernel::class
);

$app->singleton(
        Illuminate\Contracts\Debug\ExceptionHandler::class,
        App\Exceptions\Handler::class
);

/**
 * 根据开发环境不同读取不同文件。
 */
$app->detectEnvironment(function() {
    if ( file_exists(__DIR__ . '/../.env') ) {
        $dotenv = new Dotenv\Dotenv(__DIR__ . '/../', '.env');
        $dotenv->overload(); //this is important
        if ( getenv('APP_ENV') && file_exists(__DIR__ . '/../.' . getenv('APP_ENV') . '.env') ) {
            $dotenv = new Dotenv\Dotenv(__DIR__ . '/../', '.' . getenv('APP_ENV') . '.env');
            $dotenv->overload(); //this is important
        }
    }
});


/**
 * 控制laravel 生成日志文件
 */
$app->configureMonologUsing(function( Monolog\Logger $monolog ) {
    $processUser = posix_getpwuid(posix_geteuid());
    $processName = $processUser['name'];
    $filename    = storage_path('logs/laravel-' . php_sapi_name() . '-' . $processName . '.log');
    $handler     = new Monolog\Handler\RotatingFileHandler($filename);
    $monolog->pushHandler($handler);
});


/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;
